package id.co.oob.db.qris.merchant.onboarding.service;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import id.co.oob.db.qris.merchant.onboarding.dao.EmailLogTableDao;
import id.co.oob.db.qris.merchant.onboarding.repository.log.EmailLogTable;
import id.co.oob.lib.common.merchant.onboarding.BaseCommon;

@Component
public class BaseSvc extends BaseCommon{

	@Value("${oob_dns}")
	protected  String OOB_DNS;
	
	@Autowired
	private EmailLogTableDao emailLogTableDao;
	
	public void kirimEmail(String useremail, String tujuan, String statusSend, String logSubject) {
		System.out.println("log subject email untuk : "+ useremail +" : " + logSubject + " di tanggal : " + new Date());
		
		if(logSubject!=null) {
			 logSubject = logSubject.substring(0, 200);
		 }
		final String ls = logSubject;
//		Thread thread = new Thread(){
//			    public void run(){
//			    	EmailLogTable emailLogTable = new EmailLogTable();
//			    	emailLogTable.setLogCc("None");
//			    	emailLogTable.setLogId(generateIdEmailFromExisting());
//			    	emailLogTable.setLogResend("none");
//			    	emailLogTable.setLogStatus(statusSend);
//			    	emailLogTable.setLogSubject(ls);
//			    	emailLogTable.setLogTglWaktu(new Date());
//			    	emailLogTable.setLogTujuan(tujuan);
//			    	emailLogTable.setLogUser(useremail);
//			    	emailLogTableDao.save(emailLogTable);
//			    }
//			  };
//
//	    thread.start();
		

    	EmailLogTable emailLogTable = new EmailLogTable();
    	emailLogTable.setLogCc("None");
    	emailLogTable.setLogId(generateIdEmailFromExisting());
    	emailLogTable.setLogResend("none");
    	emailLogTable.setLogStatus(statusSend);
    	emailLogTable.setLogSubject(ls);
    	emailLogTable.setLogTglWaktu(new Date());
    	emailLogTable.setLogTujuan(tujuan);
    	emailLogTable.setLogUser(useremail);
    	emailLogTableDao.save(emailLogTable);
		
	}
	
	public Long generateIdEmailFromExisting() {
		Long id = 1L;
		while(emailLogTableDao.existsById(id)) {
			id++;
		}
		return id;
	};
	
}
