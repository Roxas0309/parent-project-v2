package id.co.oob.db.qris.merchant.onboarding.repository.log;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "Web_Service_Log")
public class WebServiceLog {

	@Id
	@Column(name = "log_id")
	private Long logId;

	@Column(name = "log_tgl_waktu", nullable = true)
	@Temporal(TemporalType.TIMESTAMP)
	private Date logTglWaktu;
	
	@Lob
	@Column(name = "request_url", nullable = true)
	private String request_url;
	
	@Lob
	@Column(name = "request_body", nullable = true)
	private String requestBody;
	
	@Lob
	@Column(name = "response_body", nullable = true)
	private String responseBody;

	
	
	public String getRequest_url() {
		return request_url;
	}

	public void setRequest_url(String request_url) {
		this.request_url = request_url;
	}

	public Long getLogId() {
		return logId;
	}

	public void setLogId(Long logId) {
		this.logId = logId;
	}

	public Date getLogTglWaktu() {
		return logTglWaktu;
	}

	public void setLogTglWaktu(Date logTglWaktu) {
		this.logTglWaktu = logTglWaktu;
	}

	public String getRequestBody() {
		return requestBody;
	}

	public void setRequestBody(String requestBody) {
		this.requestBody = requestBody;
	}

	public String getResponseBody() {
		return responseBody;
	}

	public void setResponseBody(String responseBody) {
		this.responseBody = responseBody;
	}
	
	
	
}
