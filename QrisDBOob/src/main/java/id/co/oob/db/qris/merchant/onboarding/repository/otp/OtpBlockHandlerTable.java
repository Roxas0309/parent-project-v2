package id.co.oob.db.qris.merchant.onboarding.repository.otp;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="Otp_Block_Handler_Table")
public class OtpBlockHandlerTable {

	@Id
	@Column(name="id")
	//@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id; 
	
	@Column(name="phone_Number", nullable = false,updatable = false)
	private String phoneNumber;
	
	@Column(name="reason", nullable = false,updatable = false)
	private String reason;  //WRONG_OTP_INPUT
	
	@Column(name="create_date", nullable = false,updatable = false)
	@Temporal(TemporalType.TIMESTAMP)
    private Date createDate;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	
	
}
