package id.co.oob.db.qris.merchant.onboarding.repository.alamat;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name="provinsi_table")
public class ProvinsiTable {

	@Id
	@Column(name="id")
	private Long id; 
	
	@Column(name="nama")
	private String nama;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNama() {
		return nama;
	}

	public void setNama(String nama) {
		this.nama = nama;
	}
	
	
}
