package id.co.oob.db.qris.merchant.onboarding.dao.otp;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import id.co.oob.db.qris.merchant.onboarding.repository.otp.OtpHandlerTable;

@Repository
public interface OtpHandlerTableDao extends JpaRepository<OtpHandlerTable, Long>{
	public final static String ON_VALIDATION = "ON_VALIDATION";
	public final static String CLEARED = "CLEARED";
	public final static String WRONG_AND_INVALID = "WRONG_AND_INVALID"; 
	public final static String ERROR_ON_SENDING = "ERROR_ON_SENDING";
	public final static String EXPIRED = "EXPIRED";
	
	@Query("select a from OtpHandlerTable a where a.userPurposed = ?1 "
			+ " and"
			+ " a.otpOnMenu = ?2 "
			+ " and "
			+ " a.secretKey = ?3"
			+ " and "
			+ " a.statusOtp = '"+ON_VALIDATION+"'")
	public OtpHandlerTable getInfoOtpOnValidationByUser(String userPurposed, String otpOnMenu, String secretKey);
	
	
	@Query("select a from OtpHandlerTable a where a.userPurposed = ?1 "
			+ " and"
			+ " a.otpOnMenu = ?2 "
			+ " and "
			+ " a.secretKey = ?3"
			+ " and "
			+ " a.statusOtp = '"+ON_VALIDATION+"'")
	public List<OtpHandlerTable> getInfoOtpsOnValidationByUser(String userPurposed, String otpOnMenu, String secretKey);
	
	@Query("delete from OtpHandlerTable where userPurposed = ?1  "
			+ " and "
			+ " otpOnMenu = ?2 "
			+ " and "
			+ " secretKey = ?3"
			+ " and "
			+ " statusOtp = '"+CLEARED+"'")
	@Modifying
	public void deleteAllClearedDataByUserProposedAndSecretKey(
			String userPurposed, 
			String otpOnMenu, 
			String secretKey);
	
	
	@Query("select a from OtpHandlerTable a where a.userPurposed = ?1 "
			+ " and"
			+ " a.otpOnMenu = ?2 "
			+ " and "
			+ " a.secretKey = ?3"
			+ " and "
			+ " a.statusOtp = '"+CLEARED+"'")
	public OtpHandlerTable getInfoOtpOnWhenWillSuccessRegistByUser
			(String userPurposed, String otpOnMenu, String secretKey);
	
	
}
