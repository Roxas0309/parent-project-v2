package id.co.oob.db.qris.merchant.onboarding.service.entityManager;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import id.co.oob.db.qris.merchant.onboarding.service.BaseSvc;

@Repository
@Transactional
@Service
public class QrisDatabaseQuery extends BaseSvc{
	
	@Value("${application.profile-name}")
	public String applicationProfileName;
	

	@PersistenceContext
	private EntityManager entityManager;
	
	public Object resultQuery(String query) {
		return entityManager.createNativeQuery(query).getResultList();
	}

	public Object resultQueryDelete(String query) {
		return entityManager.createNativeQuery(query).executeUpdate();
	}
	
	@SuppressWarnings("unchecked")
	public List<String> getAllDataMidThatUpgrade(){
		List<String> mids = new ArrayList<String>();
		
    String rplc = "QRIS_PROD";
		
	if(!applicationProfileName.equalsIgnoreCase("prod")) {
		rplc = "QRIS";
	}
	
	System.out.println("getAllDataMidThatUpgrade using table : " + rplc);
	
		String query = " SELECT a.MID FROM "+rplc+".DM_TBMERLIMIT a \n " + 
				" WHERE a.STS_EMAIL = 0\n " + 
				" AND \n " + 
				" a.MID IN (SELECT b.mid_number FROM  "+rplc+".USER_OOB_MID_DTL_TABLE b)\n " +
				" AND \n " + 
				" a.MID IN (SELECT uomdt.mid_number FROM "+rplc+".USER_OOB_MID_DTL_TABLE uomdt) "
				+ " AND "
				+ " a.PAYMENT_LIMIT_DAY != 500000 ";
		mids= entityManager.createNativeQuery(query).getResultList();
		
		for (String mid : mids) {
			String queryUpdate = "UPDATE "+rplc+".DM_TBMERLIMIT SET STS_EMAIL = 1\n" + 
					"WHERE \n" + 
					"MID = '"+mid+"' \n";
			entityManager.createNativeQuery(queryUpdate).executeUpdate();
		}
		
		return mids;
	}
	
}
