package id.co.oob.db.qris.merchant.onboarding.dao.user;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import id.co.oob.db.qris.merchant.onboarding.repository.user.UserOobMidDtlTable;

@Repository
public interface UserOobMidDtlTableDao extends JpaRepository<UserOobMidDtlTable, Long>{

	@Query("select a from UserOobMidDtlTable a where a.idOob = ?1")
	public UserOobMidDtlTable getUserOobMidDtlTableByIdOob(Long idOob);
	
	@Query("select a from UserOobMidDtlTable a where a.midNumber = ?1")
	public List<UserOobMidDtlTable> getAllUserWithSameMid(String mid);
	
}
