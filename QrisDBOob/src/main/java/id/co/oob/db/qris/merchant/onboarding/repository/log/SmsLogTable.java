package id.co.oob.db.qris.merchant.onboarding.repository.log;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "sms_log_table")
public class SmsLogTable {
	@Id
	@Column(name = "log_id")
	private Long logId;

	@Column(name = "log_tgl_waktu", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date logTglWaktu;

	@Column(name = "log_tujuan", nullable = false)
	private String logTujuan;

	@Column(name = "log_subject", nullable = false)
	private String logSubject;
	
	@Column(name = "log_status", nullable = false)
	private String logStatus;

	@Column(name = "log_user", nullable = false)
	private String logUser;

	public Long getLogId() {
		return logId;
	}

	public void setLogId(Long logId) {
		this.logId = logId;
	}

	public Date getLogTglWaktu() {
		return logTglWaktu;
	}

	public void setLogTglWaktu(Date logTglWaktu) {
		this.logTglWaktu = logTglWaktu;
	}

	public String getLogTujuan() {
		return logTujuan;
	}

	public void setLogTujuan(String logTujuan) {
		this.logTujuan = logTujuan;
	}

	public String getLogStatus() {
		return logStatus;
	}

	public void setLogStatus(String logStatus) {
		this.logStatus = logStatus;
	}

	public String getLogSubject() {
		return logSubject;
	}

	public void setLogSubject(String logSubject) {
		this.logSubject = logSubject;
	}


	public String getLogUser() {
		return logUser;
	}

	public void setLogUser(String logUser) {
		this.logUser = logUser;
	}



}