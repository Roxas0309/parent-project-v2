package id.co.oob.db.qris.merchant.onboarding.dao.alamat;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import id.co.oob.db.qris.merchant.onboarding.repository.alamat.KabupatenTable;


@Repository
public interface KabupatenTableDao extends JpaRepository<KabupatenTable, Long>{

	@Query("select a from KabupatenTable a order by nama asc")
	public List<KabupatenTable> findAllKabupatenTable();
	
	@Query("select a from KabupatenTable a where a.id_provinsi = ?1 order by nama asc")
	public List<KabupatenTable> findAllKabupatenTableByIdProvinsi(Long idProvinsi);
	
}
