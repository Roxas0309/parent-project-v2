package id.co.oob.db.qris.merchant.onboarding.repository.user;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "User_Auth_Token_Table")
public class UserAuthTokenTable {

	@Id
	@Column(name = "id_token")
	private String idToken;
	
	@Column(name = "id_oob")
	private Long idOob;
	
	@Column(name="login_date", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date loginDate;
	
	@Column(name="session_pengenal", nullable = false)
	private String sessionPengenal;
	
	@Column(name="refresh_date", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date refreshDate;

	
	
	public String getSessionPengenal() {
		return sessionPengenal;
	}

	public void setSessionPengenal(String sessionPengenal) {
		this.sessionPengenal = sessionPengenal;
	}

	public String getIdToken() {
		return idToken;
	}

	public void setIdToken(String idToken) {
		this.idToken = idToken;
	}

	public Long getIdOob() {
		return idOob;
	}

	public void setIdOob(Long idOob) {
		this.idOob = idOob;
	}

	public Date getLoginDate() {
		return loginDate;
	}

	public void setLoginDate(Date loginDate) {
		this.loginDate = loginDate;
	}

	public Date getRefreshDate() {
		return refreshDate;
	}

	public void setRefreshDate(Date refreshDate) {
		this.refreshDate = refreshDate;
	}

	
	
	
}
