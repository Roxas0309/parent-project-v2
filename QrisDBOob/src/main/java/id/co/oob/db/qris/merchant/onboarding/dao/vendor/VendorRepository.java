package id.co.oob.db.qris.merchant.onboarding.dao.vendor;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import id.co.oob.db.qris.merchant.onboarding.repository.apivendor.VendorModel;

@Repository
public interface VendorRepository extends JpaRepository<VendorModel, Long>{

}
