package id.co.oob.db.qris.merchant.onboarding.repository.alamat;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name="kecamatan_table")
public class KecamatanTable {
	
	@Id
	@Column(name="id")
	private Long id; 
	
	@Column(name="id_kota")
	private Long id_kota; 
	
	@Column(name="nama")
	private String nama;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getId_kota() {
		return id_kota;
	}

	public void setId_kota(Long id_kota) {
		this.id_kota = id_kota;
	}

	public String getNama() {
		return nama;
	}

	public void setNama(String nama) {
		this.nama = nama;
	}
	
	

	
}
