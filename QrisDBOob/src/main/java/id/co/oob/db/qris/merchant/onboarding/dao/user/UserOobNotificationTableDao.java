package id.co.oob.db.qris.merchant.onboarding.dao.user;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import id.co.oob.db.qris.merchant.onboarding.repository.user.UserOobNotificationTable;

@Repository
public interface UserOobNotificationTableDao extends JpaRepository<UserOobNotificationTable, String>{

	@Query("select a from UserOobNotificationTable a where a.tokenPage = ?1 ")
	public UserOobNotificationTable selectByToken(String token);
	
}
