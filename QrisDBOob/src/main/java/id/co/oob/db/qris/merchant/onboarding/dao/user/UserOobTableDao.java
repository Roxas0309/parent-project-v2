package id.co.oob.db.qris.merchant.onboarding.dao.user;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import id.co.oob.db.qris.merchant.onboarding.repository.user.UserAuthTokenTable;
import id.co.oob.db.qris.merchant.onboarding.repository.user.UserOobMidDtlTable;
import id.co.oob.db.qris.merchant.onboarding.repository.user.UserOobTable;
import id.co.oob.db.qris.merchant.onboarding.repository.user.UserOobTableAuthDtl;


@Repository
public interface UserOobTableDao extends JpaRepository<UserOobTable, Long>{

//	@Query("select a from UserOobTable a where a.idMaas = ?1")
//	public UserOobTable getUserOobTableByMaasId(String maasId);
//	
	
	@Query("select a from UserOobTable a where a.idStatusProses = 2")
	public List<UserOobTable> getAllUSerOobOnStatusCekPten();
	
	@Query("select a from UserOobTable a where a.nomorRekening = ?1 and a.idStatusProses != -1 ")
	public List<UserOobTable> getDataByNomorRekening(String noRekening);
	
	@Query("select a from UserOobTable a where a.id = ?1")
	public UserOobTable getDetailUserByItsIdOob(Long id);
	
	@Query("select a from UserOobTable a where a.id = ?1 and a.idStatusProses = 3")
	public UserOobTable getDetailUserByItsIdOobActive(Long id);
	
	@Query("select a from UserOobTable a where a.id = ?1 and a.idStatusProses = 2")
	public UserOobTable getDetailUserByItsIdOobWhoseId2(Long id);
	
	@Query("select a from UserOobTable a where a.id = ?1 and a.noHandphone = ?2")
	public UserOobTable getDetailUserOobByNoidAndPhoneNumber(Long id, String noHandphone);
	
	@Query("select a from UserOobTable a where upper(a.emailPemilikUsaha) = upper(?1) and a.idStatusProses = 3 ")
	public List<UserOobTable> getDetailUserOobsByEmail(String email);
	
	@Query("select a from UserOobTable a where a.noHandphone = ?1 and a.idStatusProses = 3 ")
	public List<UserOobTable> getDetailUserOobByPhoneNumber(String noHandphone);
	
	@Query("select a,b from UserOobTable a, UserOobTableAuthDtl b where a.id = b.idUserOob "
			+ " and a.noHandphone = ?1 and b.password = ?2")
	public List<Object[]> getUserOobDetailForLogin(String noHp, String password);
	
	@Query("select a,b from UserOobTable a, UserOobTableAuthDtl b where a.id = b.idUserOob "
			+ " and a.noHandphone = ?1 and b.password = ?2 and a.id = ?3")
	public List<Object[]> getUserOobDetailForLoginWithIdOob(String noHp, String password, Long idOob);
	
	@Query("select b from UserOobTable a, UserOobTableAuthDtl b where a.id = b.idUserOob "
			+ " and a.noHandphone = ?1 ")
	public List<UserOobTableAuthDtl> getUserOobDetailForLoginWithoutDtlPassword
									 (String noHp);
	
	@Query("select a from UserOobTable a where a.idStatusProses = 2 and a.nomorKtp = ?1 ")
	public List<UserOobTable> getUserOobTableDaoByKtp(String ktp);
	
	
	@Query("select a,b,c from UserOobTable a, UserOobTableAuthDtl b, UserOobMidDtlTable c "
			+ " where "
			+ " a.id = b.idUserOob "
			+ " and "
			+ " a.id = c.idOob "
			+ " and "
			+ " a.id = ?1")
	public List<Object[]> getSuperDetailUserById(Long id);
	
	@Query("select a from UserOobTable a where a.idStatusProses = 1")
	public List<UserOobTable> getAllOnProgressSendStatusProses();
	
	@Query("select a,b,c,d from UserOobTable a, UserOobTableAuthDtl b, "
			+ "	UserOobMidDtlTable c, UserAuthTokenTable d"
			+ " where "
			+ " a.id = b.idUserOob "
			+ " and "
			+ " a.id = c.idOob "
			+ " and "
			+ " a.id = d.idOob "
			+ " and "
			+ " d.idToken= ?1")
	public List<Object[]> getSuperDetailUserByToken(String token);
	
	@Query("select a,b,c from UserOobTable a, UserOobTableAuthDtl b, "
			+ "	UserOobMidDtlTable c "
			+ " where "
			+ " a.id = b.idUserOob "
			+ " and "
			+ " a.id = c.idOob "
			+ " and "
			+ " a.id= ?1")
	public List<Object[]> getSuperDetailUserByTokenById(Long token);
}
