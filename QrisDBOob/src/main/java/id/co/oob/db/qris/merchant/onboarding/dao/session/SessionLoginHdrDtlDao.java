package id.co.oob.db.qris.merchant.onboarding.dao.session;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import id.co.oob.db.qris.merchant.onboarding.repository.user.SessionLoginHdr;
import id.co.oob.db.qris.merchant.onboarding.repository.user.SessionLoginHdrDtl;

@Repository
public interface SessionLoginHdrDtlDao extends JpaRepository<SessionLoginHdrDtl, Long>{
	
	@Query("select a from SessionLoginHdrDtl a where a.sessionToken = ?1 and a.urlBlocked = ?2")
    public List<SessionLoginHdrDtl> SessionLoginHdrDtl(String sessionId, String url);
	
	@Query("select a from SessionLoginHdrDtl a where a.urlBlocked = ?1")
    public List<SessionLoginHdrDtl> SessionLoginHdrDtlWithUrlBlocked(String url);
}
