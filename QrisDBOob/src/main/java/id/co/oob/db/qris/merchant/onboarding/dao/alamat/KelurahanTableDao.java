package id.co.oob.db.qris.merchant.onboarding.dao.alamat;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import id.co.oob.db.qris.merchant.onboarding.repository.alamat.KecamatanTable;
import id.co.oob.db.qris.merchant.onboarding.repository.alamat.KelurahanTable;

@Repository
public interface KelurahanTableDao extends JpaRepository<KelurahanTable, Long>{
	
	
	@Query("select a from KelurahanTable a order by a.nama asc")
	public List<KelurahanTable> findAllKeluarahanTable();
	
	@Query("select a from KelurahanTable a where a.id = ?1")
	public KelurahanTable findKelurahanTableById(Long id);
	
	@Query("select a from KelurahanTable a where a.id_kecamatan = ?1 order by a.nama asc")
	public List<KelurahanTable> findAllKeluarahanTableByIdKecamatan(Long idKecamatan);
	
	@Query("select a,b,c,d from KelurahanTable a, KecamatanTable b, KabupatenTable c, ProvinsiTable d "
			+ " where a.id = ?1"
			+ " and "
			+ " a.id_kecamatan = b.id " 
			+ " and "
			+ " b.id_kota =  c.id "
			+ " and "
			+ " c.id_provinsi = d.id ")
	public List<Object[]> getFullAddressAlamatFromKelurahan(Long id);

}
