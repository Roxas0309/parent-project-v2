package id.co.oob.db.qris.merchant.onboarding.repository.user;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


//Table Ini hanya digunakan jika ada perubahan data pada table USER_OOB_TABLE
@Entity
@Table(name = "User_Oob_History_Table")
public class UserOobHistoryTable {
	
	@Id
	@Column(name="id")
    private Long id;
	
	@Column(name="id_oob")
    private Long idOob;
	
	@Column(name="from_column")
	private String fromColumn;
	
	@Column(name="value_before")
	private String valueBefore;
	
	@Column(name="value_After")
	private String valueAfter;
	
	@Column(name="create_date", nullable = false,updatable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date createDate; 
	
	@Column(name="create_by", nullable = false,updatable = false)
	private String createBy;

	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}



	public Long getIdOob() {
		return idOob;
	}

	public void setIdOob(Long idOob) {
		this.idOob = idOob;
	}

	

	public String getFromColumn() {
		return fromColumn;
	}

	public void setFromColumn(String fromColumn) {
		this.fromColumn = fromColumn;
	}

	public String getValueBefore() {
		return valueBefore;
	}

	public void setValueBefore(String valueBefore) {
		this.valueBefore = valueBefore;
	}

	public String getValueAfter() {
		return valueAfter;
	}

	public void setValueAfter(String valueAfter) {
		this.valueAfter = valueAfter;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getCreateBy() {
		return createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	} 
	
	
	

}
