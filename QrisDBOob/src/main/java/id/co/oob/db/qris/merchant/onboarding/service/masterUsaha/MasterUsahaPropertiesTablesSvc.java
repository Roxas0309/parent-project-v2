package id.co.oob.db.qris.merchant.onboarding.service.masterUsaha;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.co.oob.db.qris.merchant.onboarding.dao.masterUsaha.MasterUsahaPropertiesTablesDao;
import id.co.oob.db.qris.merchant.onboarding.repository.masterUsaha.MasterUsahaPropertiesTable;
import id.co.oob.db.qris.merchant.onboarding.service.BaseSvc;
import id.co.oob.lib.common.merchant.onboarding.dto.masterUsaha.MasterUsahaPropertiesTableDto;
import id.co.oob.lib.common.merchant.onboarding.throwable.NotFoundDataException;

@Service
@Transactional
public class MasterUsahaPropertiesTablesSvc extends BaseSvc{

	@Autowired
	private MasterUsahaPropertiesTablesDao masterUsahaPropertiesTablesDao;
	
	public void checkValidationMaster(String idName, String tipeData) {
		if(tipeData.equals(MasterUsahaPropertiesTablesDao.JENIS_USAHA)) {
			if(masterUsahaPropertiesTablesDao.checkIdJenisUsaha(idName)==null) {
				throw new NotFoundDataException("Id Jenis Usaha Tidak Ditemukan");
			}
		}
		
		if(tipeData.equals(MasterUsahaPropertiesTablesDao.LOKASI_NON_PERMANEN_USAHA)) {
			if(masterUsahaPropertiesTablesDao.checkIdLokasiNonPermanenUsaha(idName)==null) {
				throw new NotFoundDataException("Id Lokasi Usaha Non Permanen Tidak Ditemukan");
			}
		}
		
		if(tipeData.equals(MasterUsahaPropertiesTablesDao.LOKASI_PERMANEN_USAHA)) {
			if(masterUsahaPropertiesTablesDao.checkIdLokasiPermanenUsaha(idName)==null) {
				throw new NotFoundDataException("Id Lokasi Usaha Permanen Tidak Ditemukan");
			}
		}
		
		if(tipeData.equals(MasterUsahaPropertiesTablesDao.OMSET_USAHA)) {
			if(masterUsahaPropertiesTablesDao.checkIdOmsetUsaha(idName)==null) {
				throw new NotFoundDataException("Id Omset Usaha Tidak Ditemukan");
			}
		}
	}
	
	public List<MasterUsahaPropertiesTableDto> getAllMasterUsahaTablesByTipeData(String tipe){
		List<MasterUsahaPropertiesTable> masterUsahaPropertiesTables = masterUsahaPropertiesTablesDao.getAllMasterUsahaPropertiesBaseTipeData(tipe);
		return mapperFacade.mapAsList(masterUsahaPropertiesTables, MasterUsahaPropertiesTableDto.class);
	}
	
	public List<MasterUsahaPropertiesTableDto> save(List<MasterUsahaPropertiesTableDto> masterUsahaPropertiesTableDtos){
		List<MasterUsahaPropertiesTable> masterUsahaPropertiesTables = new ArrayList<MasterUsahaPropertiesTable>();
		for (MasterUsahaPropertiesTableDto masterUsahaPropertiesTable : masterUsahaPropertiesTableDtos) {
			MasterUsahaPropertiesTable usahaPropertiesTable = mapperFacade.map(masterUsahaPropertiesTable, MasterUsahaPropertiesTable.class);
			usahaPropertiesTable.setIdName(generateIdFromExisting());
			masterUsahaPropertiesTablesDao.save(usahaPropertiesTable);
			masterUsahaPropertiesTables.add(usahaPropertiesTable);
		}
		return mapperFacade.mapAsList(masterUsahaPropertiesTables, MasterUsahaPropertiesTableDto.class);
	}
	
	public String generateIdFromExisting() {
		String saltString = getSaltString(4);
		while(masterUsahaPropertiesTablesDao.existsById(saltString)) {
			saltString = getSaltString(4);
		}
		return saltString;
	}
	
}
