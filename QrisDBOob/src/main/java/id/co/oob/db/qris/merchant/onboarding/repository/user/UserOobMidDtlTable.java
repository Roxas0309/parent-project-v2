package id.co.oob.db.qris.merchant.onboarding.repository.user;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "User_Oob_Mid_Dtl_Table")
public class UserOobMidDtlTable {

	@Id
	@Column(name="id_Oob")
    //@GeneratedValue(strategy = GenerationType.AUTO)
    private Long idOob;
	
	@Column(name="mid_number")
	private String midNumber;
	
	@Column(name="nmid_number")
	private String nmidNumber;
	
	@Column(name="akun_oob_mid")
	private String akunOobMid; //Only For standard And Upgrade
	
	@Column(name="application_number")
	private String applicationNumber;
	
	@Column(name="path_qr_number")
	private String pathQrNumber;
	
	@Column(name = "id_status_proses")
	private Integer idStatusProses;
	
	@Column(name="create_date", nullable = false,updatable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date createDate;

	@Column(name="update_date", nullable = true)
	@Temporal(TemporalType.TIMESTAMP)
	private Date updateDate;
	

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public Integer getIdStatusProses() {
		return idStatusProses;
	}

	public void setIdStatusProses(Integer idStatusProses) {
		this.idStatusProses = idStatusProses;
	}

	public String getAkunOobMid() {
		return akunOobMid;
	}

	public void setAkunOobMid(String akunOobMid) {
		this.akunOobMid = akunOobMid;
	}

	public Long getIdOob() {
		return idOob;
	}

	public void setIdOob(Long idOob) {
		this.idOob = idOob;
	}

	public String getMidNumber() {
		return midNumber;
	}

	public void setMidNumber(String midNumber) {
		this.midNumber = midNumber;
	}

	public String getNmidNumber() {
		return nmidNumber;
	}

	public void setNmidNumber(String nmidNumber) {
		this.nmidNumber = nmidNumber;
	}

	public String getApplicationNumber() {
		return applicationNumber;
	}

	public void setApplicationNumber(String applicationNumber) {
		this.applicationNumber = applicationNumber;
	}

	public String getPathQrNumber() {
		return pathQrNumber;
	}

	public void setPathQrNumber(String pathQrNumber) {
		this.pathQrNumber = pathQrNumber;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	
	
	
}
