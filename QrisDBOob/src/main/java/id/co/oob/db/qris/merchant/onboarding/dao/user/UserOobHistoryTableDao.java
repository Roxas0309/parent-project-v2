package id.co.oob.db.qris.merchant.onboarding.dao.user;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import id.co.oob.db.qris.merchant.onboarding.repository.user.UserOobHistoryTable;

@Repository
public interface UserOobHistoryTableDao extends JpaRepository<UserOobHistoryTable, Long>{
	
	public static String ID_STATUS_PROSES = "ID_STATUS_PROSES";
	
	@Query("select a from UserOobHistoryTable a where a.idOob = ?1 and a.valueAfter = ?2 and a.fromColumn = '"+ID_STATUS_PROSES+"'")
	public List<UserOobHistoryTable> getAllHistoryUserOobIdStatusProses(Long idOob, String idStatusAfter);
	
}
