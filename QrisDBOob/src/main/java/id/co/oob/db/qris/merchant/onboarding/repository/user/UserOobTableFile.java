package id.co.oob.db.qris.merchant.onboarding.repository.user;

import org.springframework.data.jpa.repository.Query;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "user_oob_table")
public class UserOobTableFile {

    @Id
    @Column(name = "id")
    // @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "email_pemilik_usaha", nullable = false)
    private String emailPemilikUsaha;

    @Column(name = "path_ktp")
    private String pathKtp;

    @Column(name = "no_Handphone", nullable = false)
    private String noHandphone;

    @Column(name = "path_wajah_ktp")
    private String pathWajahKtp;

    @Column(name = "path_npwp")
    private String pathNpwp;

    @Column(name = "kode_Pos_Alamat_Pemilik_Usaha")
	private String kodePosAlamatPemilikUsaha;
    
    @Column(name = "no_npwp")
    private String noNpwp;

    @Column(name = "nama_usaha")
    private String namaUsaha;

    @Column(name = "nama_pemilik_usaha", nullable = false)
    private String namaPemilikUsaha;

    @Column(name = "nomor_telp")
    private String nomorTelp;

    @Column(name = "jenis_usaha")
    private String jenisUsaha;

    @Column(name = "omzet")
    private String omzet;

    @Column(name = "path_foto_tempat_usaha")
    private String pathFotoTempatUsaha;

    @Column(name = "path_foto_barang_atau_jasa")
    private String pathFotoBarangAtauJasa;

    @Column(name = "path_foto_pemilik_tempat_usaha")
    private String pathFotoPemilikTempatUsaha;

    @Column(name = "kategori_lokasi_usaha")
    private String kategoriLokasiUsaha;

    @Column(name = "jenis_lokasi_usaha")
    private String jenisLokasiUsaha;

    @Column(name = "alamat_lokasi_usaha_saat_ini")
    private String alamatLokasiUsahaSaatIni;

    @Column(name = "nomor_ktp", nullable = false)
    private String nomorKtp;

    @Column(name = "dob", nullable = false)
    private String dob;

    @Column(name = "nama_ibu_kandung", nullable = false)
    private String namaIbuKandung;

    @Column(name = "nomorRekening")
    private String nomorRekening;

    @Column(name = "alamat_pemilik_usaha")
    private String alamatPemilikUsaha;

    

    public String getKodePosAlamatPemilikUsaha() {
		return kodePosAlamatPemilikUsaha;
	}

	public void setKodePosAlamatPemilikUsaha(String kodePosAlamatPemilikUsaha) {
		this.kodePosAlamatPemilikUsaha = kodePosAlamatPemilikUsaha;
	}

	public String getNomorRekening() {
        return nomorRekening;
    }

    public void setNomorRekening(String nomorRekening) {
        this.nomorRekening = nomorRekening;
    }

    public String getNamaIbuKandung() {
        return namaIbuKandung;
    }

    public void setNamaIbuKandung(String namaIbuKandung) {
        this.namaIbuKandung = namaIbuKandung;
    }

    public String getNoHandphone() {
        return noHandphone;
    }

    public String getAlamatPemilikUsaha() {
        return alamatPemilikUsaha;
    }

    public void setAlamatPemilikUsaha(String alamatPemilikUsaha) {
        this.alamatPemilikUsaha = alamatPemilikUsaha;
    }

    public void setNoHandphone(String noHandphone) {
        this.noHandphone = noHandphone;
    }

    public Long getId() {
        return id;
    }

    public String getNamaPemilikUsaha() {
        return namaPemilikUsaha;
    }

    public void setNamaPemilikUsaha(String namaPemilikUsaha) {
        this.namaPemilikUsaha = namaPemilikUsaha;
    }
    public void setId(Long id) {
        this.id = id;
    }

    public String getEmailPemilikUsaha() {
        return emailPemilikUsaha;
    }

    public void setEmailPemilikUsaha(String emailPemilikUsaha) {
        this.emailPemilikUsaha = emailPemilikUsaha;
    }

    public String getPathKtp() {
        return pathKtp;
    }


    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public void setPathKtp(String pathKtp) {
        this.pathKtp = pathKtp;
    }

    public String getPathWajahKtp() {
        return pathWajahKtp;
    }

    public void setPathWajahKtp(String pathWajahKtp) {
        this.pathWajahKtp = pathWajahKtp;
    }

    public String getPathNpwp() {
        return pathNpwp;
    }

    public void setPathNpwp(String pathNpwp) {
        this.pathNpwp = pathNpwp;
    }

    public String getNoNpwp() {
        return noNpwp;
    }

    public void setNoNpwp(String noNpwp) {
        this.noNpwp = noNpwp;
    }

    public String getNamaUsaha() {
        return namaUsaha;
    }

    public void setNamaUsaha(String namaUsaha) {
        this.namaUsaha = namaUsaha;
    }

    public String getNomorTelp() {
        return nomorTelp;
    }

    public void setNomorTelp(String nomorTelp) {
        this.nomorTelp = nomorTelp;
    }

    public String getJenisUsaha() {
        return jenisUsaha;
    }

    public void setJenisUsaha(String jenisUsaha) {
        this.jenisUsaha = jenisUsaha;
    }

    public String getOmzet() {
        return omzet;
    }

    public void setOmzet(String omzet) {
        this.omzet = omzet;
    }

    public String getPathFotoTempatUsaha() {
        return pathFotoTempatUsaha;
    }

    public void setPathFotoTempatUsaha(String pathFotoTempatUsaha) {
        this.pathFotoTempatUsaha = pathFotoTempatUsaha;
    }

    public String getPathFotoBarangAtauJasa() {
        return pathFotoBarangAtauJasa;
    }

    public void setPathFotoBarangAtauJasa(String pathFotoBarangAtauJasa) {
        this.pathFotoBarangAtauJasa = pathFotoBarangAtauJasa;
    }

    public String getPathFotoPemilikTempatUsaha() {
        return pathFotoPemilikTempatUsaha;
    }

    public void setPathFotoPemilikTempatUsaha(String pathFotoPemilikTempatUsaha) {
        this.pathFotoPemilikTempatUsaha = pathFotoPemilikTempatUsaha;
    }

    public String getKategoriLokasiUsaha() {
        return kategoriLokasiUsaha;
    }

    public void setKategoriLokasiUsaha(String kategoriLokasiUsaha) {
        this.kategoriLokasiUsaha = kategoriLokasiUsaha;
    }

    public String getJenisLokasiUsaha() {
        return jenisLokasiUsaha;
    }

    public void setJenisLokasiUsaha(String jenisLokasiUsaha) {
        this.jenisLokasiUsaha = jenisLokasiUsaha;
    }

    public String getAlamatLokasiUsahaSaatIni() {
        return alamatLokasiUsahaSaatIni;
    }

    public void setAlamatLokasiUsahaSaatIni(String alamatLokasiUsahaSaatIni) {
        this.alamatLokasiUsahaSaatIni = alamatLokasiUsahaSaatIni;
    }

    public String getNomorKtp() {
        return nomorKtp;
    }

    public void setNomorKtp(String nomorKtp) {
        this.nomorKtp = nomorKtp;
    }

}
