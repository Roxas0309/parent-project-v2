package id.co.oob.db.qris.merchant.onboarding.service.user;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import id.co.oob.db.qris.merchant.onboarding.dao.user.*;
import id.co.oob.db.qris.merchant.onboarding.repository.alamat.KelurahanTable;
import id.co.oob.db.qris.merchant.onboarding.repository.masterUsaha.MasterUsahaPropertiesTable;
import id.co.oob.db.qris.merchant.onboarding.repository.user.*;

import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;

import id.co.oob.db.qris.merchant.onboarding.dao.alamat.KelurahanTableDao;
import id.co.oob.db.qris.merchant.onboarding.dao.masterUsaha.MasterUsahaPropertiesTablesDao;
import id.co.oob.db.qris.merchant.onboarding.service.BaseSvc;
import id.co.oob.db.qris.merchant.onboarding.service.alamat.AlamatCreatorSvc;
import id.co.oob.lib.common.merchant.onboarding.convert.CharacterRemoval;
import id.co.oob.lib.common.merchant.onboarding.convert.ConvertPhoneNumber;
import id.co.oob.lib.common.merchant.onboarding.dto.user.LinkChangePassDto;
import id.co.oob.lib.common.merchant.onboarding.dto.user.UserAuthTokenTableDto;
import id.co.oob.lib.common.merchant.onboarding.dto.user.UserOobMidDtlTableDto;
import id.co.oob.lib.common.merchant.onboarding.dto.user.UserOobTableAfterPathDto;
import id.co.oob.lib.common.merchant.onboarding.dto.user.UserOobTableAuthDtlDto;
import id.co.oob.lib.common.merchant.onboarding.dto.user.UserOobTableSuccessDto;
import id.co.oob.lib.common.merchant.onboarding.dto.user.UserOobTableUploadFileDto;
import id.co.oob.lib.common.merchant.onboarding.email.EmailBody;
import id.co.oob.lib.common.merchant.onboarding.encryptor.EncryptDataFiller;
import id.co.oob.lib.common.merchant.onboarding.encryptor.MaskingNumber;
import id.co.oob.lib.common.merchant.onboarding.file.LogStatusEmailDto;
import id.co.oob.lib.common.merchant.onboarding.throwable.AnauthorizedException;
import id.co.oob.lib.common.merchant.onboarding.throwable.InternalServerErrorException;
import id.co.oob.lib.common.merchant.onboarding.throwable.NotAcceptanceDataException;
import id.co.oob.lib.common.merchant.onboarding.throwable.NotFoundDataException;
import id.co.oob.lib.common.merchant.onboarding.validation.DateValidation;

@Service
@Transactional
public class UserOobLogicSvc extends BaseSvc {

	protected static final String TICK_CODE_INF = "Informasi-";

	@Autowired
	private KelurahanTableDao kelurahanTableDao;

	@Autowired
	private UserOobHistoryTableDao userOobHistoryTableDao;

	@Autowired
	private AlamatCreatorSvc alamatCreatorSvc;

	@Autowired
	private UserOobMidDtlTableDao userOobMidDtlTableDao;

	@Autowired
	private MasterUsahaPropertiesTablesDao masterUsahaPropertiesTablesDao;

	@Autowired
	private UserOobTableDao userOobTableDao;

	@Autowired
	private UserNotifDtlTableDao userOobNotificationDtlTableDao;

	@Autowired
	private UserOobTableAuthDtlDao userOobTableAuthDtlDao;

	public List<UserOobMidDtlTable> upgradeDataByMid(String mid) {
		List<UserOobMidDtlTable> userOobMidDtlTables = userOobMidDtlTableDao.getAllUserWithSameMid(mid);
		System.out.println("gson data userOobMidDtlTables " + new Gson().toJson(userOobMidDtlTables));
		List<UserOobMidDtlTable> userOobMidDtlTablesSaved = new ArrayList<UserOobMidDtlTable>();
		for (UserOobMidDtlTable userOobMidDtlTable : userOobMidDtlTables) {
			System.out.println("update data " + new Gson().toJson(userOobMidDtlTable));
			userOobMidDtlTable.setAkunOobMid("59UQR-85D7L-ULFZJ-6RGV5");
			userOobMidDtlTable.setUpdateDate(new Date());

			userOobMidDtlTableDao.save(userOobMidDtlTable);
			userOobMidDtlTablesSaved.add(userOobMidDtlTable);

			UserNotifDtlTable userNotifDtlTable = new UserNotifDtlTable();
			userNotifDtlTable.setBody("Nikmati pencairan lebih dari Rp 500.000 per hari dengan tipe Akun Bisnis!");
			userNotifDtlTable.setCreateDate(new Date());
			userNotifDtlTable.setHeader("Limit Pencairan Lebih Besar");
			userNotifDtlTable.setIdOob(userOobMidDtlTable.getIdOob());
			userNotifDtlTable.setNotification("Informasi");
			userNotifDtlTable.setUniqueTick(userOobMidDtlTable.getIdOob() + TICK_CODE_INF + "UPGRADE");
			createNotification(userNotifDtlTable);
		}
		return userOobMidDtlTablesSaved;
	}

	public Boolean itsDataExistByRekening(String noRekening) {
		List<UserOobTable> rekenings = userOobTableDao.getDataByNomorRekening(noRekening);
		System.out.println(
				"banyaknya data yang telah terdaftar untuk no rekening : " + noRekening + " : " + rekenings.size());
		if (rekenings == null || rekenings.size() == 0) {
			return false;
		} else {

			// untuk cek rekening nasabah yang sudah registrasi atau sedang regist.
			for (UserOobTable oobCekIsReadyRek : rekenings) {
				if (oobCekIsReadyRek.getId() == 1 || oobCekIsReadyRek.getId() == 2) {
					return true;
				}
			}

			// untuk cek rekening nasabah yang berhasil verifikasi.
			for (UserOobTable oob : rekenings) {
				UserOobTableAuthDtl oobTableAuthDtl = userOobTableAuthDtlDao.getUserOobTableAuthDtl(oob.getId());
				if (oobTableAuthDtl != null) {
					Boolean isFirstLogin = oobTableAuthDtl.getIsFirstLogin();
					if (!isFirstLogin) {
						Date lastLogin = oobTableAuthDtl.getLastLogin();
						if (DateValidation.getDifferentDays(lastLogin) >= 90) {
							oobTableAuthDtl.setIsLocked(1);
							userOobTableAuthDtlDao.save(oobTableAuthDtl);
							return false;
						}
					}
				}
			}

			return true;
		}
	}

	public List<UserNotifDtlTable> getNotficationByItsId(Long idOob) {
		List<UserNotifDtlTable> userOobNotificationDtlTables = userOobNotificationDtlTableDao
				.getNotificationById(idOob);
		return userOobNotificationDtlTables;
	}

	public UserNotifDtlTable createNotification(UserNotifDtlTable oobNotificationDtlTable) {
		List<UserNotifDtlTable> oobNotificationDtlTables = userOobNotificationDtlTableDao
				.getNotificationByUniqueTickData(oobNotificationDtlTable.getUniqueTick());

		if (oobNotificationDtlTables != null && oobNotificationDtlTables.size() > 0) {
			return oobNotificationDtlTable;
		} else {

			oobNotificationDtlTable.setIdNotification(generateIdNotifFromExisting());
			oobNotificationDtlTable.setIsRead(false);
			oobNotificationDtlTable.setIsSent(false);
			// oobNotificationDtlTable.setCreateDate(new Date());
			userOobNotificationDtlTableDao.save(oobNotificationDtlTable);
		}
		return oobNotificationDtlTable;
	}

	public Boolean updateToFirstRegistWhenForgotPassword(String email) {
		List<UserOobTable> userOobTables = userOobTableDao.getDetailUserOobsByEmail(email);
		if (userOobTables == null || userOobTables.size() == 0) {
			return false;
		}
		for (UserOobTable userOobTable : userOobTables) {
			String randomPassword = randomPassword();
			UserOobTableAuthDtl userOobTableAuthDtl = userOobTableAuthDtlDao
					.getUserOobTableAuthDtl(userOobTable.getId());

			if (userOobTableAuthDtl != null) {
				// sekarang tidak non aktifkan lagi usernya.
//			userOobTableAuthDtl.setIsFirstLogin(true);
//			userOobTableAuthDtl.setLastUpdateDate(new Date());
//			userOobTableAuthDtl.setLastUpdateBy(userOobTable.getId()+"");
//			userOobTableAuthDtl.setCreateDate(new Date());
//			userOobTableAuthDtl.setPassword(randomPassword);
//			userOobTableAuthDtlDao.save(userOobTableAuthDtl);

				LinkChangePassDto linkChangePassDto = new LinkChangePassDto();
				linkChangePassDto.setId(userOobTableAuthDtl.getIdUserOob());
				linkChangePassDto.setDate(new SimpleDateFormat("ddMMyyyyHHmmss").format(new Date()));
				String secretKey = getSaltString(4).replace("-", "").toUpperCase();
				String encResult = EncryptDataFiller.encrypt(new Gson().toJson(linkChangePassDto), secretKey);

				System.out.println("secret key : " + secretKey + " enc resul : " + encResult);
				String urlUbahPassword = OOB_DNS + "/forgetPassword?_tracQ=" + encResult + "&_sk=" + secretKey;
				LogStatusEmailDto logStatusEmailDto = EmailBody.bodyPerubahanPasswordMitraQris(
						MaskingNumber.maskingPhoneNumber(userOobTable.getNoHandphone()),
						userOobTable.getNamaPemilikUsaha(), email, randomPassword, urlUbahPassword);
				kirimEmail(logStatusEmailDto.getLogTujuan(), logStatusEmailDto.getLogUser(),
						logStatusEmailDto.getLogStatus(), logStatusEmailDto.getLogSubject());
			}

		}
		return true;

	}

	public void updateToFirstRegistWhenForgotPassword(String noHp, String password) {
		List<Object[]> objs = userOobTableDao.getUserOobDetailForLogin(noHp, password);
		for (Object[] obj : objs) {
			String randomPassword = randomPassword();
			UserOobTable userOobTable = (UserOobTable) obj[0];
			UserOobTableAuthDtl userOobTableAuthDtl = (UserOobTableAuthDtl) obj[1];
			userOobTableAuthDtl.setIsFirstLogin(true);
			userOobTableAuthDtl.setLastUpdateDate(new Date());
			userOobTableAuthDtl.setLastUpdateBy(userOobTable.getId() + "");
			userOobTableAuthDtl.setCreateDate(new Date());
			userOobTableAuthDtl.setWrongPasswordCount(userOobTableAuthDtl.getWrongPasswordCount() + 1);
			userOobTableAuthDtl.setPassword(randomPassword);

			System.err.println("login yang akan masuk " + new Gson().toJson(userOobTableAuthDtl));
			userOobTableAuthDtlDao.save(userOobTableAuthDtl);

			LogStatusEmailDto logStatusEmailDto = EmailBody.bodyForSuccessPendaftaranSementara(
					MaskingNumber.maskingPhoneNumber(userOobTable.getNoHandphone()), userOobTable.getNamaPemilikUsaha(),
					randomPassword, userOobTable.getEmailPemilikUsaha(), OOB_DNS + "/login",
					OOB_DNS + "/changePassword");
			kirimEmail(logStatusEmailDto.getLogTujuan(), logStatusEmailDto.getLogUser(),
					logStatusEmailDto.getLogStatus(), logStatusEmailDto.getLogSubject());
		}
	}

	public List<UserOobHistoryTable> getAllHistoryUSerOobIdStatus(Long idOob, Integer idStatusProses) {
		List<UserOobHistoryTable> userOobHistoryTables = userOobHistoryTableDao
				.getAllHistoryUserOobIdStatusProses(idOob, Integer.toString(idStatusProses));
		if (userOobHistoryTables == null || userOobHistoryTables.size() == 0) {
			return null;
		}
		return userOobHistoryTables;
	}

	public UserOobTable updateNasabahTo3(Long idNumber, String secretId) {
		UserOobTable userOobTable = userOobTableDao.getOne(idNumber);
		if (userOobTable.getIdStatusProses() == 3) {
			throw new NotAcceptanceDataException("Id Status Sudah 3");
		}
		UserOobHistoryTable userOobHistoryTable = new UserOobHistoryTable();
		userOobHistoryTable.setCreateBy(secretId);
		userOobHistoryTable.setCreateDate(new Date());
		userOobHistoryTable.setFromColumn(UserOobHistoryTableDao.ID_STATUS_PROSES);
		userOobHistoryTable.setId(generateIdHistory());
		userOobHistoryTable.setIdOob(idNumber);
		userOobHistoryTable.setValueAfter(Integer.toString(3));
		userOobHistoryTable.setValueBefore(Integer.toString(userOobTable.getIdStatusProses()));
		userOobTable.setIdStatusProses(3);
		userOobHistoryTableDao.save(userOobHistoryTable);
		userOobTableDao.save(userOobTable);

		return userOobTable;
	}

	public UserOobTable updateNasabahTo3New(Long idNumber, String secretId) {
		UserOobTable userOobTable = userOobTableDao.getDetailUserByItsIdOob(idNumber);

		UserOobHistoryTable userOobHistoryTable = new UserOobHistoryTable();
		userOobHistoryTable.setCreateBy(secretId);
		userOobHistoryTable.setCreateDate(new Date());
		userOobHistoryTable.setFromColumn(UserOobHistoryTableDao.ID_STATUS_PROSES);
		userOobHistoryTable.setId(generateIdHistory());
		userOobHistoryTable.setIdOob(idNumber);
		userOobHistoryTable.setValueAfter(Integer.toString(3));
		userOobHistoryTable.setValueBefore(Integer.toString(userOobTable.getIdStatusProses()));
		userOobTable.setIdStatusProses(3);
		userOobHistoryTableDao.save(userOobHistoryTable);
		userOobTableDao.save(userOobTable);

		return userOobTable;
	}

	public UserOobTable updateNasabahToMinus1New(Long idNumber, String secretId) {
		UserOobTable userOobTable = userOobTableDao.getDetailUserByItsIdOob(idNumber);

		UserOobHistoryTable userOobHistoryTable = new UserOobHistoryTable();
		userOobHistoryTable.setCreateBy(secretId);
		userOobHistoryTable.setCreateDate(new Date());
		userOobHistoryTable.setFromColumn(UserOobHistoryTableDao.ID_STATUS_PROSES);
		userOobHistoryTable.setId(generateIdHistory());
		userOobHistoryTable.setIdOob(idNumber);
		userOobHistoryTable.setValueAfter(Integer.toString(-1));
		userOobHistoryTable.setValueBefore(Integer.toString(userOobTable.getIdStatusProses()));
		userOobTable.setIdStatusProses(-1);
		userOobHistoryTableDao.save(userOobHistoryTable);
		userOobTableDao.save(userOobTable);

		return userOobTable;
	}

	public UserOobTable updateNasabahTo2(Long idNumber, String secretId) {

		UserOobTable userOobTable = userOobTableDao.getDetailUserByItsIdOob(idNumber);
		if (userOobTable.getIdStatusProses() == 2) {
			throw new NotAcceptanceDataException("Id Status Sudah 2");
		}

		if (userOobTable.getIdStatusProses() > 2) {
			throw new NotAcceptanceDataException("Id Status Tidak dapat Diturunkan");
		}

		UserOobHistoryTable userOobHistoryTable = new UserOobHistoryTable();
		userOobHistoryTable.setCreateBy(secretId);
		userOobHistoryTable.setCreateDate(new Date());
		userOobHistoryTable.setFromColumn(UserOobHistoryTableDao.ID_STATUS_PROSES);
		userOobHistoryTable.setId(generateIdHistory());
		userOobHistoryTable.setIdOob(idNumber);
		userOobHistoryTable.setValueAfter(Integer.toString(2));
		userOobHistoryTable.setValueBefore(Integer.toString(userOobTable.getIdStatusProses()));
		userOobTable.setIdStatusProses(2);
		userOobHistoryTableDao.save(userOobHistoryTable);
		userOobTableDao.save(userOobTable);
		return userOobTable;
	}

	public AllUserOobDtlRepo getDetailAllUserByItsId(String token) {
		List<Object[]> objs = userOobTableDao.getSuperDetailUserByToken(token);
		AllUserOobDtlRepo allUserOobDtlRepo = new AllUserOobDtlRepo();

		if (objs == null || objs.size() == 0) {
			throw new AnauthorizedException("Token Yang Diberikan Tidak Valid");
		}

		UserAuthTokenTable userAuthTokenTable = (UserAuthTokenTable) objs.get(0)[3];
		UserOobMidDtlTable userOobMidDtlTable = (UserOobMidDtlTable) objs.get(0)[2];
		UserOobTable userOobTable = (UserOobTable) objs.get(0)[0];
		UserOobTableAuthDtl userOobTableAuthDtl = (UserOobTableAuthDtl) objs.get(0)[1];
		;

		UserAuthTokenTableDto userAuthTokenTableDto = mapperFacade.map(userAuthTokenTable, UserAuthTokenTableDto.class);
		UserOobMidDtlTableDto userOobMidDtlTableDto = mapperFacade.map(userOobMidDtlTable, UserOobMidDtlTableDto.class);
		userOobMidDtlTableDto.setAkunOobMidDesc(
				masterUsahaPropertiesTablesDao.checkIdAkunOobQr(userOobMidDtlTableDto.getAkunOobMid()).getNama());
		UserOobTableSuccessDto userOobTableSuccessDto = mapperFacade.map(userOobTable, UserOobTableSuccessDto.class);
		if (userOobTableSuccessDto.getKategoriLokasiUsaha().equals("0")) {
			userOobTableSuccessDto.setKategoriLokasiUsahaDesc("Permanen");
			userOobTableSuccessDto.setJenisLokasiUsahaDesc(masterUsahaPropertiesTablesDao
					.checkIdLokasiPermanenUsaha(userOobTableSuccessDto.getJenisLokasiUsaha()).getNama());

		} else if (userOobTableSuccessDto.getKategoriLokasiUsaha().equals("1")) {
			userOobTableSuccessDto.setKategoriLokasiUsahaDesc("Non Permanen");
			userOobTableSuccessDto.setJenisLokasiUsahaDesc(masterUsahaPropertiesTablesDao
					.checkIdLokasiNonPermanenUsaha(userOobTableSuccessDto.getJenisLokasiUsaha()).getNama());
		}
		userOobTableSuccessDto.setJenisUsahaDesc(
				masterUsahaPropertiesTablesDao.checkIdJenisUsaha(userOobTableSuccessDto.getJenisUsaha()).getNama());
		userOobTableSuccessDto.setOmzetDesc(
				masterUsahaPropertiesTablesDao.checkIdOmsetUsaha(userOobTableSuccessDto.getOmzet()).getNama());

		UserOobTableAuthDtlDto userOobTableAuthDtlDto = mapperFacade.map(userOobTableAuthDtl,
				UserOobTableAuthDtlDto.class);

		allUserOobDtlRepo.setUserAuthTokenTableDto(userAuthTokenTableDto);
		allUserOobDtlRepo.setUserOobMidDtlTableDto(userOobMidDtlTableDto);
		allUserOobDtlRepo.setUserOobTableAuthDtlDto(userOobTableAuthDtlDto);
		allUserOobDtlRepo.setUserOobTableSuccessDto(userOobTableSuccessDto);

		return allUserOobDtlRepo;
	}

	public AllUserOobDtlRepo getDetailAllUserByItsId(Long id) {
		List<Object[]> objs = userOobTableDao.getSuperDetailUserByTokenById(id);
		AllUserOobDtlRepo allUserOobDtlRepo = new AllUserOobDtlRepo();

		if (objs == null || objs.size() == 0) {
			throw new AnauthorizedException("Token Yang Diberikan Tidak Valid");
		}

		UserOobMidDtlTable userOobMidDtlTable = (UserOobMidDtlTable) objs.get(0)[2];
		UserOobTable userOobTable = (UserOobTable) objs.get(0)[0];
		UserOobTableAuthDtl userOobTableAuthDtl = (UserOobTableAuthDtl) objs.get(0)[1];
		;

		UserOobMidDtlTableDto userOobMidDtlTableDto = mapperFacade.map(userOobMidDtlTable, UserOobMidDtlTableDto.class);
		userOobMidDtlTableDto.setAkunOobMidDesc(
				masterUsahaPropertiesTablesDao.checkIdAkunOobQr(userOobMidDtlTableDto.getAkunOobMid()).getNama());
		UserOobTableSuccessDto userOobTableSuccessDto = mapperFacade.map(userOobTable, UserOobTableSuccessDto.class);
		if (userOobTableSuccessDto.getKategoriLokasiUsaha().equals("0")) {
			userOobTableSuccessDto.setKategoriLokasiUsahaDesc("Permanen");
			userOobTableSuccessDto.setJenisLokasiUsahaDesc(masterUsahaPropertiesTablesDao
					.checkIdLokasiPermanenUsaha(userOobTableSuccessDto.getJenisLokasiUsaha()).getNama());

		} else if (userOobTableSuccessDto.getKategoriLokasiUsaha().equals("1")) {
			userOobTableSuccessDto.setKategoriLokasiUsahaDesc("Non Permanen");
			userOobTableSuccessDto.setJenisLokasiUsahaDesc(masterUsahaPropertiesTablesDao
					.checkIdLokasiNonPermanenUsaha(userOobTableSuccessDto.getJenisLokasiUsaha()).getNama());
		}
		userOobTableSuccessDto.setJenisUsahaDesc(
				masterUsahaPropertiesTablesDao.checkIdJenisUsaha(userOobTableSuccessDto.getJenisUsaha()).getNama());
		userOobTableSuccessDto.setOmzetDesc(
				masterUsahaPropertiesTablesDao.checkIdOmsetUsaha(userOobTableSuccessDto.getOmzet()).getNama());

		UserOobTableAuthDtlDto userOobTableAuthDtlDto = mapperFacade.map(userOobTableAuthDtl,
				UserOobTableAuthDtlDto.class);

		allUserOobDtlRepo.setUserOobMidDtlTableDto(userOobMidDtlTableDto);
		allUserOobDtlRepo.setUserOobTableAuthDtlDto(userOobTableAuthDtlDto);
		allUserOobDtlRepo.setUserOobTableSuccessDto(userOobTableSuccessDto);

		return allUserOobDtlRepo;
	}

	public UserOobTableAuthDtl createPasswordUser(Long oobId) {
		UserOobTableAuthDtl userOobTableAuthDtl = new UserOobTableAuthDtl();
		userOobTableAuthDtl.setCreateDate(new Date());
		userOobTableAuthDtl.setIdUserOob(oobId);
		userOobTableAuthDtl.setIsFirstLogin(true);
		userOobTableAuthDtl.setIsLocked(0);
		userOobTableAuthDtl.setPassword(randomPassword());
		userOobTableAuthDtl.setWrongPasswordCount(0);
		userOobTableAuthDtlDao.save(userOobTableAuthDtl);
		return userOobTableAuthDtl;
	}

	public UserOobMidDtlTable createQrNumberUserWithCommon(Long oobId, String nmid, String mid) {

		UserOobMidDtlTable userOobMidDtlTable = userOobMidDtlTableDao.getOne(oobId);
		userOobMidDtlTable.setIdStatusProses(3);
		userOobMidDtlTable.setMidNumber(mid);
		userOobMidDtlTable.setNmidNumber(nmid);
		userOobMidDtlTable.setUpdateDate(new Date());
		userOobMidDtlTable.setPathQrNumber("oob-nasabah-qr-code/" + nmid + "/" + nmid + "_A01.png");
		userOobMidDtlTableDao.save(userOobMidDtlTable);

		return userOobMidDtlTable;
	}

	public UserOobMidDtlTable createQrNumberUserWithCommon(Long oobId, String nmid, String mid, String pathQr) {

		UserOobMidDtlTable userOobMidDtlTable = userOobMidDtlTableDao.getUserOobMidDtlTableByIdOob(oobId);
		userOobMidDtlTable.setIdStatusProses(3);
		userOobMidDtlTable.setMidNumber(mid);
		userOobMidDtlTable.setNmidNumber(nmid);
		userOobMidDtlTable.setUpdateDate(new Date());
		userOobMidDtlTable.setPathQrNumber(pathQr);
		userOobMidDtlTableDao.save(userOobMidDtlTable);

		return userOobMidDtlTable;
	}

	public UserOobMidDtlTable createQrNumberUser(Long oobId) {

		UserOobMidDtlTable userOobMidDtlTable = userOobMidDtlTableDao.getOne(oobId);
		userOobMidDtlTable.setIdStatusProses(3);
//		userOobMidDtlTable.setApplicationNumber("xxxx-xxxx-xxxx");
//		userOobMidDtlTable.setCreateDate(new Date());
//		userOobMidDtlTable.setIdOob(oobId);
//		userOobMidDtlTable.setAkunOobMid("H1H0U-8WGRC-GHGYO-JYRAH");
//		userOobMidDtlTable.setMidNumber("xxxx-xxxx-xxxx");
//		userOobMidDtlTable.setNmidNumber("ID1020049086918");
		userOobMidDtlTable.setPathQrNumber("oob-nasabah-qr-code/ID1020049086918/ID1020049086918_A01.png");
		userOobMidDtlTableDao.save(userOobMidDtlTable);

		return userOobMidDtlTable;
	}

	public UserOobTable checkStatusPendaftaran(String noPendaftaran, String noHandphone) {
		String[] getAllFormatNoPhone = ConvertPhoneNumber.getAllNumberInAllFormat(noHandphone,
				"Format No Handphone " + noHandphone + " Yang Diberikan Tidak Valid.");
		Long idPendaftaran = CharacterRemoval.removeStaticZero(noPendaftaran,
				"Nomor Pendaftaran " + noPendaftaran + " tidak dapat ditemukan");
		for (String phone : getAllFormatNoPhone) {
			UserOobTable userOobTable = userOobTableDao.getDetailUserOobByNoidAndPhoneNumber(idPendaftaran, phone);
			if (userOobTable != null) {
				return userOobTable;
			}
		}

		throw new NotFoundDataException(
				"Nomor Pendaftaran " + noPendaftaran + " dengan no telp " + noHandphone + " tidak dapat ditemukan");
	}

	public UserOobTable saveRegistrasi(UserOobTableAfterPathDto userOobTableAfterPathDto) {
		UserOobTable userOobTable = new UserOobTable();
		userOobTable = mapperFacade.map(userOobTableAfterPathDto, UserOobTable.class);
		
		String alamatRumah = userOobTable.getAlamatPemilikUsaha();
		if (alamatRumah != null) {
			if (alamatRumah.length() > 255) {
				alamatRumah = alamatRumah.substring(0, 255);
			}
			alamatRumah = alamatRumah.replace("|", "");
		}
		
		String alamatLokasiUsahaSaatIni = userOobTable.getAlamatLokasiUsahaSaatIni();
		if(!Strings.isBlank(alamatLokasiUsahaSaatIni)) {
			if (alamatLokasiUsahaSaatIni.length() > 255) {
				alamatLokasiUsahaSaatIni = alamatLokasiUsahaSaatIni.substring(0, 255);
			}
			alamatLokasiUsahaSaatIni = alamatLokasiUsahaSaatIni.replace("|", "");
		}

		
		String namaUsaha = userOobTable.getNamaUsaha();
		if(!Strings.isBlank(namaUsaha)) {
			if (namaUsaha.length() > 255) {
				namaUsaha = namaUsaha.substring(0, 255);
			}
			namaUsaha = namaUsaha.replace("|", "");
		}
		
		userOobTable.setNamaUsaha(namaUsaha);
		userOobTable.setAlamatLokasiUsahaSaatIni(alamatLokasiUsahaSaatIni);
		userOobTable.setAlamatPemilikUsaha(alamatRumah);
		userOobTable.setId(generateIdFromExisting());
		userOobTable.setIdStatusProses(1);
		userOobTable.setCreateDate(new Date());
		userOobTableDao.save(userOobTable);

		UserOobMidDtlTable userOobMidDtlTable = new UserOobMidDtlTable();
		userOobMidDtlTable.setApplicationNumber(applicationNumber(userOobTable.getId()));
		userOobMidDtlTable.setCreateDate(new Date());
		userOobMidDtlTable.setIdOob(userOobTable.getId());
		userOobMidDtlTable.setAkunOobMid("H1H0U-8WGRC-GHGYO-JYRAH");
		userOobMidDtlTable.setMidNumber(null);
		userOobMidDtlTable.setNmidNumber(null);
		userOobMidDtlTable.setPathQrNumber(null);
		userOobMidDtlTable.setIdStatusProses(1);
		userOobMidDtlTableDao.save(userOobMidDtlTable);

		return userOobTable;
	}

	public String applicationNumber(Long number) {
		String str = "";

		String numberInStr = Long.toString(number);
		int countNumber = 8;

		int projectileNumb = countNumber - numberInStr.length();
		while (projectileNumb != 0) {
			str += "0";
			projectileNumb--;
		}
		str += numberInStr;
		return str;
	}

	public Long generateIdQr() {
		Long i = userOobMidDtlTableDao.count() + 1;
		while (userOobMidDtlTableDao.existsById(i)) {
			i++;
		}
		return i;
	}

	public Long generateIdHistory() {
		Long i = userOobHistoryTableDao.count() + 1;
		while (userOobHistoryTableDao.existsById(i)) {
			i++;
		}
		return i;
	}

	public Long generateIdFromExisting() {
		Long id = userOobTableDao.count() + 1;
		while (userOobTableDao.existsById(id)) {
			id++;
		}
		return id;
	}

	public Long generateIdNotifFromExisting() {
		Long id = userOobNotificationDtlTableDao.count() + 1;
		while (userOobNotificationDtlTableDao.existsById(id)) {
			id++;
		}
		return id;
	}

	public List<UserOobTableFile> getFileExcel() {
		List<UserOobTableFile> userOobTableFileList = new ArrayList<>();
		List<UserOobTable> listUserData = userOobTableDao.findAll(Sort.by("id").ascending());
//		List<MasterUsahaPropertiesTable> masterUsahaProperties = masterUsahaPropertiesTablesDao.findById();
//		return userOobTableDao.findAll(Sort.by("id").ascending());

		for (UserOobTable data : listUserData) {
			MasterUsahaPropertiesTable masterUsaha = masterUsahaPropertiesTablesDao.findById(data.getJenisUsaha())
					.get();
			MasterUsahaPropertiesTable omzetUsaha = masterUsahaPropertiesTablesDao.findById(data.getOmzet()).get();
			MasterUsahaPropertiesTable lokasiUsaha = masterUsahaPropertiesTablesDao.findById(data.getJenisLokasiUsaha())
					.get();
			UserOobTableFile item = new UserOobTableFile();

			item.setId(data.getId());
//			item.setJenisUsaha(masterUsaha.getNama());
			item.setJenisUsaha(data.getJenisUsaha());
			item.setEmailPemilikUsaha(data.getEmailPemilikUsaha());
			item.setPathKtp(data.getPathKtp());
			item.setNoHandphone(data.getNoHandphone());
			item.setPathWajahKtp(data.getPathWajahKtp());
			item.setPathNpwp(data.getPathNpwp());
			item.setNoNpwp(data.getNoNpwp());
			item.setNamaUsaha(data.getNamaUsaha());
			item.setNamaPemilikUsaha(data.getNamaPemilikUsaha());
			item.setNomorTelp(data.getNomorTelp());
//			item.setOmzet(omzetUsaha.getNama());
			item.setOmzet(data.getOmzet());
			item.setPathFotoTempatUsaha(data.getPathFotoTempatUsaha());
//			item.setKategoriLokasiUsaha(lokasiUsaha.getTipe());
			item.setKategoriLokasiUsaha(data.getKategoriLokasiUsaha());
//			item.setJenisLokasiUsaha(lokasiUsaha.getNama());
			item.setJenisLokasiUsaha(data.getJenisLokasiUsaha());
			item.setAlamatLokasiUsahaSaatIni(data.getAlamatLokasiUsahaSaatIni());
			item.setNomorKtp(data.getNomorKtp());
			item.setDob(data.getDob());
			item.setNamaIbuKandung(data.getNamaIbuKandung());
			item.setNomorRekening(data.getNomorRekening());
			item.setAlamatPemilikUsaha(data.getAlamatPemilikUsaha());
			item.setNoNpwp(data.getNoNpwp());
			item.setPathFotoBarangAtauJasa(data.getPathFotoBarangAtauJasa());
			item.setPathFotoPemilikTempatUsaha(data.getPathFotoPemilikTempatUsaha());
			userOobTableFileList.add(item);
		}
		return userOobTableFileList;
	}

	public List<UserOobTableUploadFileDto> getDataForSendToFtp() {
		List<UserOobTableUploadFileDto> userOobTableFileList = new ArrayList<>();
		List<UserOobTable> listUserData = userOobTableDao.getAllOnProgressSendStatusProses();
//		if(listUserData==null||listUserData.size()==0) {
//			throw new NotFoundDataException("Tidak ada data yang akan dikirim");
//		}

//		List<MasterUsahaPropertiesTable> masterUsahaProperties = masterUsahaPropertiesTablesDao.findById();
//		return userOobTableDao.findAll(Sort.by("id").ascending());

		for (UserOobTable data : listUserData) {
//			MasterUsahaPropertiesTable masterUsaha = masterUsahaPropertiesTablesDao.findById(data.getJenisUsaha()).get();
//			MasterUsahaPropertiesTable omzetUsaha = masterUsahaPropertiesTablesDao.findById(data.getOmzet()).get();
//			MasterUsahaPropertiesTable lokasiUsaha = masterUsahaPropertiesTablesDao.findById(data.getJenisLokasiUsaha()).get();
//			
			UserOobTableUploadFileDto item = new UserOobTableUploadFileDto();

			item.setId(data.getId());
			item.setJenisUsaha(data.getJenisUsaha());
			item.setEmailPemilikUsaha(data.getEmailPemilikUsaha());

			String[] getPathKtpSplitter = data.getPathKtp().split("/");
			String getPathKtp = getPathKtpSplitter[getPathKtpSplitter.length - 2] + ";"
					+ getPathKtpSplitter[getPathKtpSplitter.length - 1];
			// System.err.println("getPathKtp " + getPathKtp);
			item.setPathKtp(getPathKtp);
			item.setCompletePathKtp(data.getPathKtp()); // photo 1

			item.setKodePosAlamatPemilikUsaha(data.getKodePosAlamatPemilikUsaha());
			item.setNoHandphone(data.getNoHandphone());

			String[] getPathWajahKtpSplitter = data.getPathWajahKtp().split("/");
			String getPathWajahKtp = getPathWajahKtpSplitter[getPathWajahKtpSplitter.length - 2] + ";"
					+ getPathWajahKtpSplitter[getPathWajahKtpSplitter.length - 1];
			item.setPathWajahKtp(getPathWajahKtp);
			item.setCompletePathWajahKtp(data.getPathWajahKtp()); // photo 2

			if (data.getPathNpwp() != null) {
				String[] getPathNpwpSplitter = data.getPathNpwp().split("/");
				String getPathNpwp = getPathNpwpSplitter[getPathNpwpSplitter.length - 2] + ";"
						+ getPathNpwpSplitter[getPathNpwpSplitter.length - 1];
				item.setPathNpwp(getPathNpwp);
				item.setCompletePathNpwp(data.getPathNpwp()); // photo 3
			}

			item.setNoNpwp(data.getNoNpwp());
			item.setNamaUsaha(CharacterRemoval.removeAllCharForNewUsaha(data.getNamaUsaha()));
			item.setNamaPemilikUsaha(data.getNamaPemilikUsaha());
			item.setNomorTelp(data.getNomorTelp());
//			item.setOmzet(omzetUsaha.getNama());
			item.setOmzet(data.getOmzet());

			String[] getPathFotoTempatUsaha = data.getPathFotoTempatUsaha().split("/");
			String getPathFotoTempatUsahaSpliter = getPathFotoTempatUsaha[getPathFotoTempatUsaha.length - 2] + ";"
					+ getPathFotoTempatUsaha[getPathFotoTempatUsaha.length - 1];
			item.setPathFotoTempatUsaha(getPathFotoTempatUsahaSpliter);
			item.setCompletePathFotoTempatUsaha(data.getPathFotoPemilikTempatUsaha()); // photo 4
//			item.setKategoriLokasiUsaha(lokasiUsaha.getTipe());

			item.setKategoriLokasiUsaha(data.getKategoriLokasiUsaha());
//			item.setJenisLokasiUsaha(lokasiUsaha.getNama());
			item.setJenisLokasiUsaha(data.getJenisLokasiUsaha());
			if (Strings.isNotEmpty(data.getAlamatLokasiUsahaSaatIni())) {
				item.setAlamatLokasiUsahaSaatIni(
						CharacterRemoval.removeAllCharForNewAddress(data.getAlamatLokasiUsahaSaatIni()));
				KelurahanTable kelurahanTable = kelurahanTableDao.findKelurahanTableById(data.getIdKelurahan());
				if (kelurahanTable != null) {
					item.setKodePosAlamatMerchant(kelurahanTable.getKodePos());
				}
			}

			item.setNomorKtp(data.getNomorKtp());
			item.setDob(data.getDob());
			item.setNamaIbuKandung(data.getNamaIbuKandung());
			item.setNomorRekening(data.getNomorRekening());
			item.setAlamatPemilikUsaha(CharacterRemoval.removeAllCharForNewAddress(data.getAlamatPemilikUsaha()));
			item.setNoNpwp(data.getNoNpwp());

			String[] getPathFotoBarangAtauJasa = data.getPathFotoBarangAtauJasa().split("/");
			String getPathFotoBarangAtauJasaSplitter = getPathFotoBarangAtauJasa[getPathFotoBarangAtauJasa.length - 2]
					+ ";" + getPathFotoBarangAtauJasa[getPathFotoBarangAtauJasa.length - 1];
			item.setPathFotoBarangAtauJasa(getPathFotoBarangAtauJasaSplitter);
			item.setCompletePathFotoBarangAtauJasa(data.getPathFotoBarangAtauJasa()); // photo 5

			String[] getPathFotoPemilikTempatUsaha = data.getPathFotoPemilikTempatUsaha().split("/");
			String getPathFotoPemilikTempatUsahaSplitter = getPathFotoPemilikTempatUsaha[getPathFotoPemilikTempatUsaha.length
					- 2] + ";" + getPathFotoPemilikTempatUsaha[getPathFotoPemilikTempatUsaha.length - 1];
			item.setPathFotoPemilikTempatUsaha(getPathFotoPemilikTempatUsahaSplitter);
			item.setCompletePathFotoPemilikTempatUsaha(data.getPathFotoPemilikTempatUsaha());
			userOobTableFileList.add(item);
		}
		return userOobTableFileList;
	}

}
