package id.co.oob.db.qris.merchant.onboarding.service.alamat;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;

import id.co.oob.db.qris.merchant.onboarding.dao.alamat.KabupatenTableDao;
import id.co.oob.db.qris.merchant.onboarding.dao.alamat.KecamatanTableDao;
import id.co.oob.db.qris.merchant.onboarding.dao.alamat.KelurahanTableDao;
import id.co.oob.db.qris.merchant.onboarding.dao.alamat.ProvinsiTableDao;
import id.co.oob.db.qris.merchant.onboarding.dao.alamat.TblKodeposDao;
import id.co.oob.db.qris.merchant.onboarding.repository.alamat.KabupatenTable;
import id.co.oob.db.qris.merchant.onboarding.repository.alamat.KecamatanTable;
import id.co.oob.db.qris.merchant.onboarding.repository.alamat.KelurahanTable;
import id.co.oob.db.qris.merchant.onboarding.repository.alamat.ProvinsiTable;
import id.co.oob.db.qris.merchant.onboarding.repository.alamat.TblKodepos;
import id.co.oob.db.qris.merchant.onboarding.service.BaseSvc;
import id.co.oob.lib.common.merchant.onboarding.dto.alamat.KabupatenTableDto;
import id.co.oob.lib.common.merchant.onboarding.dto.alamat.KecamatanTableDto;
import id.co.oob.lib.common.merchant.onboarding.dto.alamat.KelurahanTableDto;
import id.co.oob.lib.common.merchant.onboarding.dto.alamat.ProvinsiTableDto;
import id.co.oob.lib.common.merchant.onboarding.throwable.NotFoundDataException;

@Service("alamatCreatorSvc")
public class AlamatCreatorSvc extends BaseSvc{

	@Autowired
	private TblKodeposDao tblKodeposDao;
	
	@Autowired
	private KabupatenTableDao kabupatenTableDao;
	
	@Autowired
	private KecamatanTableDao kecamatanTableDao;
	
	@Autowired
	private KelurahanTableDao kelurahanTableDao;
	
	@Autowired
	private ProvinsiTableDao provinsiTableDao;
	
	public List<ProvinsiTable> updateAllListUpdateProvinsi(){
		List<ProvinsiTable> provinsiTables = new ArrayList<ProvinsiTable>();
		
		List<String> allProvinsi = tblKodeposDao.getAllProvinsi();
		System.out.println("all prov " + new Gson().toJson(allProvinsi));
		Long i = 1L;
		for (String str : allProvinsi) {
			ProvinsiTable provinsiTable = new ProvinsiTable();
			provinsiTable.setId(i);
			provinsiTable.setNama(str);
			provinsiTableDao.save(provinsiTable);
			i++;
		}
		
		return provinsiTables;
	}
	
	public void validationIdProvinsi(Long id) {
		if(!provinsiTableDao.existsById(id)) {
			throw new NotFoundDataException("Id Provinsi "+id+" tidak ditemukan.");
		}
	}
	
	public void validationIdKabupaten(Long id) {
		if(!kabupatenTableDao.existsById(id)) {
			throw new NotFoundDataException("Id Kabupaten "+id+" tidak ditemukan.");
		}
	}
	
	public void validationIdKelurahan(Long id) {
		if(!kelurahanTableDao.existsById(id)) {
			throw new NotFoundDataException("Id Kelurahan "+id+" tidak ditemukan.");
		}
	}
	
	public void validationIdKecamatan(Long id) {
		if(!kecamatanTableDao.existsById(id)) {
			throw new NotFoundDataException("Id Kabupaten "+id+" tidak ditemukan.");
		}
	}
	
	public Map<String, Object> getFullAddressAlamatFromKelurahan(Long id){
		List<Object[]> objects = getDetailAlamatByKelurahan(id);
		KelurahanTable kelurahanTable = new KelurahanTable();
		KecamatanTable kecamatanTable = new KecamatanTable();
		KabupatenTable kabupatenTable = new KabupatenTable();
		ProvinsiTable provinsiTable = new ProvinsiTable();
		for (Object[] obj : objects) {
			kelurahanTable = (KelurahanTable) obj[0];
			kecamatanTable = (KecamatanTable) obj[1];
			kabupatenTable = (KabupatenTable) obj[2];
			provinsiTable = (ProvinsiTable) obj[3];
		}
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("kelurahan", kelurahanTable);
		map.put("kecamatan", kecamatanTable);
		map.put("kabupaten", kabupatenTable);
		map.put("provinsiTable", provinsiTable);
		map.put("onString", provinsiTable.getNama()+", " + kabupatenTable.getNama()+", " 
		+ kecamatanTable.getNama()+", "+ kelurahanTable.getNama()+", ");
		return map;
	}
	
	public List<Object[]> getDetailAlamatByKelurahan(Long id){
		List<Object[]> objects = kelurahanTableDao.getFullAddressAlamatFromKelurahan(id);
		return objects;
	}
	
	public List<ProvinsiTableDto> saveAllProvinsiTable(List<ProvinsiTableDto> provinsiTableDtos){
		List<ProvinsiTable> provinsiTables = mapperFacade.mapAsList
				(provinsiTableDtos, ProvinsiTable.class);
		provinsiTableDao.saveAll(provinsiTables);
		return mapperFacade.mapAsList(provinsiTables, ProvinsiTableDto.class);
	}
	
	public List<ProvinsiTableDto> findAllProvinsiInTable(){
		List<ProvinsiTable> provinsiTables = provinsiTableDao.findAllProvinsi();
		return mapperFacade.mapAsList(provinsiTables, ProvinsiTableDto.class);
	}
	
	public List<KabupatenTableDto> generateAllKabupatenTable(){
		List<ProvinsiTableDto> provinsiTableDtos = findAllProvinsiInTable();
		List<KabupatenTable> kabupatenTables = new ArrayList<KabupatenTable>();
		Long i = 1L;
			for (ProvinsiTableDto provinsi : provinsiTableDtos) {
				List<String> kabupaten = tblKodeposDao.getAllListKabupaten(provinsi.getNama());
				for (String kabupatenTable : kabupaten) {
					KabupatenTable table = new KabupatenTable();
					table.setId(i);
					table.setId_provinsi(provinsi.getId());
					table.setNama(kabupatenTable);
					kabupatenTables.add(table);
					i++;
				}
			}
		
		kabupatenTableDao.saveAll(kabupatenTables);
		return mapperFacade.mapAsList(kabupatenTables, KabupatenTableDto.class);
			
	}
	
	public List<KabupatenTableDto> findAllKabupatenTable(){
		List<KabupatenTable> kabupatenTables = kabupatenTableDao.findAllKabupatenTable();
		//
		return mapperFacade.mapAsList(kabupatenTables, KabupatenTableDto.class);
	}
	
	public List<KabupatenTableDto> findAllKabupatenTableByIdProvins(Long idProvinsi){
		List<KabupatenTable> kabupatenTables = kabupatenTableDao.findAllKabupatenTableByIdProvinsi(idProvinsi);
		return mapperFacade.mapAsList(kabupatenTables, KabupatenTableDto.class);
	}
	
	public List<KecamatanTableDto> generateAllKecamatanTable(){
		List<KabupatenTableDto> kabupatenTableDtos = findAllKabupatenTable();
		List<KecamatanTable> kecamatanTables = new ArrayList<KecamatanTable>();
		Long i = 1L;
		for (KabupatenTableDto kab : kabupatenTableDtos) {
			List<String> kecamatan = tblKodeposDao.getAllListKecamatan(kab.getNama());
			for (String kabupatenTable : kecamatan) {
				KecamatanTable table = new KecamatanTable();
				table.setId(i);
				table.setId_kota(kab.getId());
				table.setNama(kabupatenTable);
				kecamatanTables.add(table);
				i++;
			}
		}
		kecamatanTableDao.saveAll(kecamatanTables);
		return mapperFacade.mapAsList(kecamatanTables, KecamatanTableDto.class);
	}
	
	public List<KecamatanTableDto> findAllKecamatanTable(){
		List<KecamatanTable> kecamatanTable = kecamatanTableDao.findAllKecamatanTable();
		return mapperFacade.mapAsList(kecamatanTable, KecamatanTableDto.class);
	}
	
	public List<KecamatanTableDto> findAllKecamatanTableByIdKabupatem(Long idKabupaten){
		List<KecamatanTable> kecamatanTable = kecamatanTableDao.findAllKecamatanTableByIdKota(idKabupaten);
		return mapperFacade.mapAsList(kecamatanTable, KecamatanTableDto.class);
	}
	
	public List<KelurahanTableDto> generateAllKelurahanTable(){
		List<KecamatanTable> kecamatanTableDtos = kecamatanTableDao.findAllKecamatanTableFilter();
		List<KelurahanTable> kelurahanTables = new ArrayList<KelurahanTable>();
		Long i = 89596L;
		for (KecamatanTable kec : kecamatanTableDtos) {
			List<TblKodepos> kecamatan = tblKodeposDao.getAllListKelurahan(kec.getNama());
			for (TblKodepos kel : kecamatan) {
				System.out.println("sedang  menyimpan data : " + kel.getKelurahan());
				KelurahanTable table = new KelurahanTable();
				table.setId(i);
				table.setId_kecamatan(kec.getId());
				table.setNama(kel.getKelurahan());
				table.setKodePos(kel.getKodepos());
				kelurahanTableDao.save(table);
				i++;
			}
		}
		//kelurahanTableDao.saveAll(kelurahanTables);
		return mapperFacade.mapAsList(kelurahanTables, KelurahanTableDto.class);
	}	
	
	public List<KelurahanTableDto> findAllKelurahanTable(){
		List<KelurahanTable> kelurahanTables = kelurahanTableDao.findAllKeluarahanTable();
		return mapperFacade.mapAsList(kelurahanTables, KelurahanTableDto.class);
	}
	
	public List<KelurahanTableDto> findAllKelurahanTableByIdKecamatan(Long idKecamatan){
		List<KelurahanTable> kecamatanTable = kelurahanTableDao.findAllKeluarahanTableByIdKecamatan(idKecamatan);
		return mapperFacade.mapAsList(kecamatanTable, KelurahanTableDto.class);
	}
	
}
