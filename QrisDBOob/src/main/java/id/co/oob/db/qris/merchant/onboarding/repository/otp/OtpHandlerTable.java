package id.co.oob.db.qris.merchant.onboarding.repository.otp;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="Otp_Handler_Table")
public class OtpHandlerTable {

	@Id
	@Column(name="id")
	//@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id; 
	
	@Column(name="user_purposed", nullable = false)
	private String userPurposed;
	
	@Column(name="otp_value", nullable = false)
	private String otpValue;
	
	@Column(name="secret_key", nullable = false)
	private String secretKey;
	
	@Column(name="wrong_counter", nullable = false)
	private Integer wrongCounter;
	
	@Column(name="otp_on_menu", nullable = false)
	private String otpOnMenu;
	
	@Column(name="status_otp", nullable = false)
	private String statusOtp;  /* Put Only 
								* ON_VALIDATION, 
	                            * CLEARED, 
	                            * WRONG_AND_INVALID, 
	                            * ERROR_ON_SENDING
	                            * AND EXPIRED
	                            */
	
	@Column(name="create_date", nullable = false,updatable = false)
	@Temporal(TemporalType.TIMESTAMP)
    private Date createDate;

	
	
	public String getOtpOnMenu() {
		return otpOnMenu;
	}

	public void setOtpOnMenu(String otpOnMenu) {
		this.otpOnMenu = otpOnMenu;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUserPurposed() {
		return userPurposed;
	}

	public void setUserPurposed(String userPurposed) {
		this.userPurposed = userPurposed;
	}

	public String getOtpValue() {
		return otpValue;
	}

	public void setOtpValue(String otpValue) {
		this.otpValue = otpValue;
	}

	public String getSecretKey() {
		return secretKey;
	}

	public void setSecretKey(String secretKey) {
		this.secretKey = secretKey;
	}

	public Integer getWrongCounter() {
		return wrongCounter;
	}

	public void setWrongCounter(Integer wrongCounter) {
		this.wrongCounter = wrongCounter;
	}

	public String getStatusOtp() {
		return statusOtp;
	}

	public void setStatusOtp(String statusOtp) {
		this.statusOtp = statusOtp;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	 
	 
}
