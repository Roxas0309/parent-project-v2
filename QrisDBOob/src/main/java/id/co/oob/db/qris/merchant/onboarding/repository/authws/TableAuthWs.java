package id.co.oob.db.qris.merchant.onboarding.repository.authws;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="auth_ws_table")
public class TableAuthWs {

	@Id
	@Column(name="id")
	//@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id; 
	
	@Column(name="secret_key", unique = true)
	private String secretKey; 
	
	@Column(name="secret_id", unique = true)
	private String secretId;
	
	@Column(name="description")
	private String description;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSecretKey() {
		return secretKey;
	}

	public void setSecretKey(String secretKey) {
		this.secretKey = secretKey;
	}

	public String getSecretId() {
		return secretId;
	}

	public void setSecretId(String secretId) {
		this.secretId = secretId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	
	
	
}
