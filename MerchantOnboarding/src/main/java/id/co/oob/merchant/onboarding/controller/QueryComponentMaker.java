package id.co.oob.merchant.onboarding.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import id.co.oob.db.qris.merchant.onboarding.service.entityManager.QrisDatabaseQuery;

@RestController
@RequestMapping("/queryComponentMaker")
public class QueryComponentMaker extends BaseCtl{

	@Autowired
	private QrisDatabaseQuery qrisDatabaseQuery;
	
	@PostMapping("/post-native-query-select")
	public Object postNativeQuery(@RequestBody String query) {
		
		return qrisDatabaseQuery.resultQuery(query);
	}
	
	@PostMapping("/post-native-query-delete")
	public Object postNativeQueryDelete(@RequestBody String query) {
		
		return qrisDatabaseQuery.resultQueryDelete(query);
	}
	
}
