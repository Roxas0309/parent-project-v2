package id.co.oob.merchant.onboarding.controller.firebase;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

public class UserNotifDtlTableDto {
	private Long idNotification;

	private Long idOob;

	private String header;

	private String notification; // untuk saat ini Transaksi dan Informasi

	private String uniqueTick;

	private String body;

	private Boolean isRead;

	private Boolean isSent;

	private Date createDate;

	private String dateInStr;

	public Long getIdNotification() {
		return idNotification;
	}

	public void setIdNotification(Long idNotification) {
		this.idNotification = idNotification;
	}

	public Long getIdOob() {
		return idOob;
	}

	public void setIdOob(Long idOob) {
		this.idOob = idOob;
	}

	public String getHeader() {
		return header;
	}

	public void setHeader(String header) {
		this.header = header;
	}

	public String getNotification() {
		return notification;
	}

	public void setNotification(String notification) {
		this.notification = notification;
	}

	public String getUniqueTick() {
		return uniqueTick;
	}

	public void setUniqueTick(String uniqueTick) {
		this.uniqueTick = uniqueTick;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public Boolean getIsRead() {
		return isRead;
	}

	public void setIsRead(Boolean isRead) {
		this.isRead = isRead;
	}

	public Boolean getIsSent() {
		return isSent;
	}

	public void setIsSent(Boolean isSent) {
		this.isSent = isSent;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getDateInStr() {
		return dateInStr;
	}

	public void setDateInStr(String dateInStr) {
		this.dateInStr = dateInStr;
	}
	
	

}
