package id.co.oob.merchant.onboarding.config.swagger;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.RestController;

import id.co.oob.merchant.onboarding.controller.BaseCtl;
import springfox.documentation.service.Parameter;
import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig extends BaseCtl{
	    @Bean
	    public Docket api() { 
	        return new Docket(DocumentationType.SWAGGER_2) 
	          .forCodeGeneration(true)
	    	//  .globalOperationParameters(globalParameterList()) 
	          .select()             
	          .apis(RequestHandlerSelectors.any())              
	          .paths(PathSelectors.any())                          
	          .build();                                           
	    }
	    
	    private List<Parameter> globalParameterList() {
	    	List<Parameter> parameters = new ArrayList<Parameter>();
	    	parameters.addAll(Collections.singletonList(new ParameterBuilder()
	                .name(SECRET_ID) 
	                .modelRef(new ModelRef("string")) 
	                .required(true) 
	                .parameterType("header") 
	                .build()));
	    	parameters.addAll(Collections.singletonList(new ParameterBuilder()
	                .name(SECRET_KEY) 
	                .modelRef(new ModelRef("string")) 
	                .required(true) 
	                .parameterType("header") 
	                .build()));
	        return parameters;
	      }
	    
}
