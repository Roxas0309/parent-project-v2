//package id.co.oob.merchant.onboarding.service;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//
//import id.co.oob.db.qris.merchant.onboarding.dao.TableTestDao;
//import id.co.oob.db.qris.merchant.onboarding.repository.test.TestTable;
//
//@Service
//public class TestServiceSvc {
//
//	@Autowired
//	private TableTestDao tableTestDao;
//	
//	public TestTable testTable() {
//		TestTable testTable = new TestTable();
//		testTable.setTestColumn("Hello world");
//		tableTestDao.save(testTable);
//		return testTable;
//	}
//	
//}
