package id.co.oob.merchant.onboarding.controller.firebase;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthException;
import com.google.firebase.messaging.FirebaseMessagingException;
import com.google.gson.Gson;

import id.co.oob.db.qris.merchant.onboarding.dao.user.UserNotifDtlTableDao;
import id.co.oob.db.qris.merchant.onboarding.dao.user.UserOobNotificationTableDao;
import id.co.oob.db.qris.merchant.onboarding.repository.user.AllUserOobDtlRepo;
import id.co.oob.db.qris.merchant.onboarding.repository.user.UserNotifDtlTable;
import id.co.oob.db.qris.merchant.onboarding.repository.user.UserOobNotificationTable;
import id.co.oob.db.qris.merchant.onboarding.service.notif.UserNotifDtlTableSvc;
import id.co.oob.lib.common.merchant.onboarding.convert.DateConverter;
import id.co.oob.lib.common.merchant.onboarding.dto.success.SuccessValidation;
import id.co.oob.lib.common.merchant.onboarding.dto.user.UserOobNotificationTableDto;
import id.co.oob.lib.common.merchant.onboarding.firebase.PushNotificationRequest;
import id.co.oob.merchant.onboarding.config.firebase.FCMService;
import id.co.oob.merchant.onboarding.controller.BaseCtl;
import id.co.oob.merchant.onboarding.service.FireBaseSvc;

@RequestMapping("/send-notification")
@RestController
public class FirebaseCtl extends BaseCtl {

	@Autowired
	private FireBaseSvc fireBaseSvc;
	
	@Autowired
	private UserNotifDtlTableSvc userNotifDtlTableSvc;

	@Autowired
	private UserNotifDtlTableDao userNotifDtlTableDao;
	
	@Autowired
	private UserOobNotificationTableDao userOobNotificationTableDao;

	private String token = "eLs0WItVXAERsHw2:APA91bFxeQ0-BPVWb6IX905s8ZacvVR6x1DYlp3-ikfitZwGMONyYT7mBMDBLRB07kbdWIzXCm";

	@Autowired
	FCMService fcmService;

	@GetMapping("/get-all-my-notification")
	public List<UserNotifDtlTableDto> getAllMyNotifications
			(@RequestParam("type") String type, @RequestParam("idOob") Long idOob){
		Date endDate = new Date();
		Calendar c = Calendar.getInstance();
	        c.setTime(endDate);
	        c.add(Calendar.DATE, -6);
		Date startDate = c.getTime();
		List<UserNotifDtlTable> userNotifDtlTables = userNotifDtlTableSvc.getllListTableNotifById
				(idOob, startDate, endDate,type);
		List<UserNotifDtlTableDto> userNotifDtlTableDtos = new ArrayList<UserNotifDtlTableDto>();
		for (UserNotifDtlTable userNotifDtlTable : userNotifDtlTables) {
			UserNotifDtlTableDto userNotifDtlTableDto = mapperFacade.map(userNotifDtlTable, UserNotifDtlTableDto.class);
			
			String datePro = DateConverter.convertDateToString(endDate, "ddMMyyyy");
			String datePro2 = DateConverter.convertDateToString(userNotifDtlTable.getCreateDate(), "ddMMyyyy");
			
			if(datePro.equals(datePro2)) {
			userNotifDtlTableDto.setDateInStr(DateConverter.convertDateToString(userNotifDtlTable.getCreateDate(), "HH:mm"));
			}
			else {
				userNotifDtlTableDto.setDateInStr(DateConverter.convertDateToString(userNotifDtlTable.getCreateDate(), "dd MMM yy"));
					
			}
			
			userNotifDtlTableDtos.add(userNotifDtlTableDto);
		}
		
		
		return userNotifDtlTableDtos;
	}
	
	@Scheduled(cron = "0 0 23 * * ?")
	@GetMapping("/update-all-notif-to-read-after-1-week")
	public ResponseEntity<Object> getAllMyNotificationsAuth(){
		Date newDate = new Date();
		Calendar c = Calendar.getInstance();
        c.setTime(newDate);
        c.add(Calendar.DATE, -6);
		List<UserNotifDtlTable> userNotifDtlTablesTransaksis = userNotifDtlTableSvc.getNotificationAndFilterBetweenStartDateEndDate
				(c.getTime(),"Transaksi");
		List<UserNotifDtlTable> userNotifDtlTablesInformasis = userNotifDtlTableSvc.getNotificationAndFilterBetweenStartDateEndDate
				(c.getTime(),"Informasi");
		
		
		List<UserNotifDtlTable> userNotifDtlTable = new ArrayList<UserNotifDtlTable>();
		for (UserNotifDtlTable userNotifDtlTabled : userNotifDtlTablesTransaksis) {
			userNotifDtlTabled.setIsRead(true);
			userNotifDtlTableDao.save(userNotifDtlTabled);
			userNotifDtlTable.add(userNotifDtlTabled);
		}
		
	    for (UserNotifDtlTable userNotifDtlTabled : userNotifDtlTablesInformasis) {
	    	userNotifDtlTabled.setIsRead(true);
			userNotifDtlTableDao.save(userNotifDtlTabled);
			userNotifDtlTable.add(userNotifDtlTabled);
		}
		
		
		return new ResponseEntity<Object>(
				new SuccessValidation("Data Sukses Retrieve",userNotifDtlTable),
				HttpStatus.OK);
	} 
	
	@GetMapping("/get-all-my-notification/auth")
	public ResponseEntity<Object> getAllMyNotificationsAuth
			(@RequestParam("type") String type,
					@RequestHeader(value = SECRET_ID, required = true) String secretId,
					@RequestHeader(value = SECRET_KEY, required = true) String secretKey,
					@RequestHeader(value = SECRET_TOKEN, required = true) String secretToken){
		AuthorizedResponseFirst(secretKey, secretId);
		AllUserOobDtlRepo allUserOobDtlRepo = getAllUserInfoByToken(secretToken, secretKey);
		String mid = allUserOobDtlRepo.getUserOobMidDtlTableDto().getMidNumber();
		Date endDate = new Date();
		Calendar c = Calendar.getInstance();
	        c.setTime(endDate);
	        c.add(Calendar.DATE, -6);
		Date startDate = c.getTime();
		List<UserNotifDtlTable> userNotifDtlTables = userNotifDtlTableSvc.getllListTableNotifById
				(allUserOobDtlRepo.getUserOobTableAuthDtlDto().getIdUserOob(), startDate, endDate,type);
		List<UserNotifDtlTableDto> userNotifDtlTableDtos = new ArrayList<UserNotifDtlTableDto>();
		for (UserNotifDtlTable userNotifDtlTable : userNotifDtlTables) {
			UserNotifDtlTableDto userNotifDtlTableDto = mapperFacade.map(userNotifDtlTable, UserNotifDtlTableDto.class);
			
			String datePro = DateConverter.convertDateToString(endDate, "ddMMyyyy");
			String datePro2 = DateConverter.convertDateToString(userNotifDtlTable.getCreateDate(), "ddMMyyyy");
			
			if(datePro.equals(datePro2)) {
			userNotifDtlTableDto.setDateInStr(DateConverter.convertDateToString(userNotifDtlTable.getCreateDate(), "HH:mm"));
			}
			else {
				userNotifDtlTableDto.setDateInStr(DateConverter.convertDateToString(userNotifDtlTable.getCreateDate(), "dd MMM yy"));
					
			}
			if(!userNotifDtlTable.getIsRead()) {
			userNotifDtlTableSvc.upgradeToRead(userNotifDtlTable);
			}
			userNotifDtlTableDtos.add(userNotifDtlTableDto);
		}
		return new ResponseEntity<Object>(
				new SuccessValidation("Data Sukses Retrieve",userNotifDtlTableDtos),
				HttpStatus.OK);
	}
	
	@PostMapping("/post-my-notif-token")
	public UserOobNotificationTable getPostMyNotifToken(
			@RequestBody UserOobNotificationTableDto userOobNotificationTableDto) {
		UserOobNotificationTable userOobNotificationTableSave = userOobNotificationTableDao
				.selectByToken(userOobNotificationTableDto.getTokenPage());
		UserOobNotificationTable userOobNotificationTable = new UserOobNotificationTable();
		if(userOobNotificationTableSave==null) {
		userOobNotificationTable = mapperFacade.map(userOobNotificationTableDto,
				UserOobNotificationTable.class);
		userOobNotificationTable.setUpdateDate(null);
		userOobNotificationTable.setCreateDate(new Date());
		userOobNotificationTableDao.save(userOobNotificationTable);
		}
		return userOobNotificationTable;
	}
	
	@PostMapping("/post-my-notif-token-with-idoob")
	public UserOobNotificationTable detailToken(@RequestHeader(value = SECRET_ID, required = true) String secretId,
			@RequestHeader(value = SECRET_KEY, required = true) String secretKey,
			@RequestBody UserOobNotificationTableDto userOobNotificationTableDto) {
		AuthorizedResponseFirst(secretKey, secretId);
		System.err.println("userOobNotificationTableDto token : " + userOobNotificationTableDto.getTokenPage());
		UserOobNotificationTable userOobNotificationTable = new UserOobNotificationTable();
		if (userOobNotificationTableDto.getTokenPage() != null) {
			UserOobNotificationTable userOobNotificationTableSave = userOobNotificationTableDao
					.selectByToken(userOobNotificationTableDto.getTokenPage());
			if (userOobNotificationTableSave == null) {
				userOobNotificationTableSave = new UserOobNotificationTable();
				userOobNotificationTableSave.setCreateDate(new Date());
				userOobNotificationTableSave.setIdOob(userOobNotificationTableDto.getIdOob());
				userOobNotificationTableSave.setTokenPage(userOobNotificationTableDto.getTokenPage());
				userOobNotificationTableDao.save(userOobNotificationTableSave);
			} else {
				userOobNotificationTableSave.setUpdateDate(new Date());
				userOobNotificationTableSave.setIdOob(userOobNotificationTableDto.getIdOob());
				userOobNotificationTableDao.save(userOobNotificationTableSave);
			}
			userOobNotificationTable = mapperFacade.map(userOobNotificationTableSave, UserOobNotificationTable.class);
		}
		return userOobNotificationTable;
	}


	@PostMapping("/post-my-notif-token-with-auth")
	public UserOobNotificationTable detailToken(@RequestHeader(value = SECRET_ID, required = true) String secretId,
			@RequestHeader(value = SECRET_KEY, required = true) String secretKey,
			@RequestHeader(value = SECRET_TOKEN, required = true) String secretToken,
			@RequestBody UserOobNotificationTableDto userOobNotificationTableDto) {
		AuthorizedResponseFirst(secretKey, secretId);
		System.err.println("secret_tokennya adalah : " + secretToken);
		System.err.println("userOobNotificationTableDto token : " + userOobNotificationTableDto.getTokenPage());
		AllUserOobDtlRepo allUserOobDtlRepo = getAllUserInfoByToken(secretToken, secretKey);
		UserOobNotificationTable userOobNotificationTable = new UserOobNotificationTable();
		if (userOobNotificationTableDto.getTokenPage() != null) {
			UserOobNotificationTable userOobNotificationTableSave = userOobNotificationTableDao
					.selectByToken(userOobNotificationTableDto.getTokenPage());
			if (userOobNotificationTableSave == null) {
				userOobNotificationTableSave = new UserOobNotificationTable();
				userOobNotificationTableSave.setCreateDate(new Date());
				userOobNotificationTableSave.setIdOob(allUserOobDtlRepo.getUserAuthTokenTableDto().getIdOob());
				userOobNotificationTableSave.setTokenPage(userOobNotificationTableDto.getTokenPage());
				userOobNotificationTableDao.save(userOobNotificationTableSave);
			} else {
				userOobNotificationTableSave.setUpdateDate(new Date());
				System.err.println("allUserOobDtlRepo token : " + new Gson().toJson(allUserOobDtlRepo));
				userOobNotificationTableSave.setIdOob(allUserOobDtlRepo.getUserAuthTokenTableDto().getIdOob());
				userOobNotificationTableDao.save(userOobNotificationTableSave);
			}
			userOobNotificationTable = mapperFacade.map(userOobNotificationTableSave, UserOobNotificationTable.class);
		}
		return userOobNotificationTable;
	}

	@GetMapping("/get-all-notif-token")
	public List<UserOobNotificationTable> getAllNotifToken() {
		return userOobNotificationTableDao.findAll();
	}

	@PostMapping("/send-me")
	public void sendPolicy(@RequestBody String token)
			throws InterruptedException, ExecutionException, FirebaseAuthException {
		PushNotificationRequest pushNotificationRequest = new PushNotificationRequest();
		pushNotificationRequest.setMessage("Selamat Datang Di Uji Coba Push Notifikasi "+OOB_DNS);
		pushNotificationRequest.setTitle("Uji Coba Push Notifikasi");
		pushNotificationRequest.setToken(token);
		Map<String, String> appData = new HashMap<>();
		appData.put("name", "PushNotification");
		fcmService.sendMessage(appData, pushNotificationRequest);

	}
}
