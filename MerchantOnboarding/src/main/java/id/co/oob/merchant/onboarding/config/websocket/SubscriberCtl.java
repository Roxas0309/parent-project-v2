package id.co.oob.merchant.onboarding.config.websocket;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;

import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;
import org.springframework.web.util.HtmlUtils;

import com.google.gson.Gson;

import id.co.oob.db.qris.merchant.onboarding.repository.user.AllUserOobDtlRepo;
import id.co.oob.db.qris.merchant.onboarding.service.notif.UserNotifDtlTableSvc;
import id.co.oob.lib.common.merchant.onboarding.ws.Greeting;
import id.co.oob.lib.common.merchant.onboarding.ws.HelloMessage;
import id.co.oob.lib.common.merchant.onboarding.ws.NoticeNotifDto;
import id.co.oob.merchant.onboarding.controller.BaseCtl;

@Controller
public class SubscriberCtl extends BaseCtl{

	@Autowired
	private UserNotifDtlTableSvc userNotifDtlTableSvc;
	
	    @MessageMapping("/hello")
	    @SendTo("/topic/greetings")
	    public String greeting(@Payload HelloMessage message) throws Exception {
	    	System.out.println("your greeting is " + new Gson().toJson(message));
	    	
	        return "hai";
	    }
	
	    @MessageMapping("/notification-push")
	    @SendTo("/topic/notice-notif")
	    public Map<String, Object> noticeNotif(@Payload Map<String, Object> notif) throws Exception {
	    	AuthorizedResponseFirst((String)notif.get("secret-key"), (String)notif.get("secret-id"));
			AllUserOobDtlRepo allUserOobDtlRepo = getAllUserInfoByToken((String)notif.get("secret-token"), (String)notif.get("secret-key"));
			Boolean isThereIsDataNotif = userNotifDtlTableSvc.isThereIsDataAccessNotif(allUserOobDtlRepo.getUserOobTableAuthDtlDto().getIdUserOob()); 
		    Map<String, Object> map = new HashMap<String, Object>();
			map.put("yourSecret", (String)notif.get("secret-token"));
			map.put("result", isThereIsDataNotif);
			return map;
	    }
	    
}
