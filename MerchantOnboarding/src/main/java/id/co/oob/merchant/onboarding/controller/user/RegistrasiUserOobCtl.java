	package id.co.oob.merchant.onboarding.controller.user;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import id.co.oob.db.qris.merchant.onboarding.repository.user.UserOobTableFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Query;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.base.Strings;
import com.google.gson.Gson;

import id.co.oob.db.qris.merchant.onboarding.dao.WebServiceLogDao;
import id.co.oob.db.qris.merchant.onboarding.dao.masterUsaha.MasterUsahaPropertiesTablesDao;
import id.co.oob.db.qris.merchant.onboarding.repository.log.WebServiceLog;
import id.co.oob.db.qris.merchant.onboarding.repository.masterUsaha.MasterUsahaPropertiesTable;
import id.co.oob.db.qris.merchant.onboarding.repository.user.UserOobHistoryTable;
import id.co.oob.db.qris.merchant.onboarding.repository.user.UserOobMidDtlTable;
import id.co.oob.db.qris.merchant.onboarding.repository.user.UserOobTable;
import id.co.oob.db.qris.merchant.onboarding.repository.user.UserOobTableAuthDtl;
import id.co.oob.db.qris.merchant.onboarding.service.alamat.AlamatCreatorSvc;
import id.co.oob.db.qris.merchant.onboarding.service.entityManager.QrisDatabaseQuery;
import id.co.oob.db.qris.merchant.onboarding.service.masterUsaha.MasterUsahaPropertiesTablesSvc;
import id.co.oob.db.qris.merchant.onboarding.service.otp.OtpHandlerTableSvc;
import id.co.oob.db.qris.merchant.onboarding.service.user.UserOobLogicSvc;
import id.co.oob.lib.common.merchant.onboarding.convert.CharacterRemoval;
import id.co.oob.lib.common.merchant.onboarding.convert.DateConverter;
import id.co.oob.lib.common.merchant.onboarding.dto.authentication.TblAuthWsDto;
import id.co.oob.lib.common.merchant.onboarding.dto.mandiri.CustomerAccountInquiryByCifDto;
import id.co.oob.lib.common.merchant.onboarding.dto.party.SearchPersonResponse;
import id.co.oob.lib.common.merchant.onboarding.dto.success.SuccessValidation;
import id.co.oob.lib.common.merchant.onboarding.dto.user.CheckStatusPendaftaranDto;
import id.co.oob.lib.common.merchant.onboarding.dto.user.StatusPendaftaranBodyRequest;
import id.co.oob.lib.common.merchant.onboarding.dto.user.StatusPendaftaranBodyResponse;
import id.co.oob.lib.common.merchant.onboarding.dto.user.UserOobTableAfterPathDto;
import id.co.oob.lib.common.merchant.onboarding.dto.user.UserOobTableDto;
import id.co.oob.lib.common.merchant.onboarding.dto.user.UserOobValidasiInformasiRekening;
import id.co.oob.lib.common.merchant.onboarding.dto.user.UserOobValidasiResponse;
import id.co.oob.lib.common.merchant.onboarding.email.EmailBody;
import id.co.oob.lib.common.merchant.onboarding.encryptor.MaskingNumber;
import id.co.oob.lib.common.merchant.onboarding.file.Base64FileDto;
import id.co.oob.lib.common.merchant.onboarding.file.LogStatusEmailDto;
import id.co.oob.lib.common.merchant.onboarding.throwable.ConflictDataException;
import id.co.oob.lib.common.merchant.onboarding.throwable.InternalServerErrorException;
import id.co.oob.lib.common.merchant.onboarding.throwable.NotAcceptanceDataException;
import id.co.oob.lib.common.merchant.onboarding.throwable.NotFoundDataException;
import id.co.oob.lib.common.merchant.onboarding.throwable.SizeDataOverCapacityException;
import id.co.oob.lib.common.merchant.onboarding.validation.DateValidation;
import id.co.oob.merchant.onboarding.controller.BaseCtl;
import org.supercsv.io.CsvBeanWriter;
import org.supercsv.io.ICsvBeanWriter;
import org.supercsv.prefs.CsvPreference;

import javax.servlet.http.HttpServletResponse;

@RestController
@RequestMapping("/UserOob")
public class RegistrasiUserOobCtl extends BaseCtl {
	private static final String REGISTRASI_MENU = "registrasi-menu";

	@Autowired
	private OtpHandlerTableSvc otpHandlerTableSvc;

	@Autowired
	private UserOobLogicSvc userOobLoginSvc;

	@Autowired
	private AlamatCreatorSvc alamatCreatorSvc;

	@Autowired
	private MasterUsahaPropertiesTablesSvc masterUsahaPropertiesTablesSvc;

	@Autowired
	private QrisDatabaseQuery qrisDatabaseQuery;

	// ini untuk scheduler already upgrade
	@Scheduled(fixedRate = 60 * 5 * 1000L)
	@GetMapping("/getAllDataThatAlreadyUpgrade")
	public List<UserOobMidDtlTable> getAllDatas() {
		System.out.println("scheduler getAllDataThatAlreadyUpgrade update start on " + SDF.format(new Date()));
		List<UserOobMidDtlTable> userOobMidDtlTables = new ArrayList<UserOobMidDtlTable>();
		List<String> mids = qrisDatabaseQuery.getAllDataMidThatUpgrade();
		for (String mid : mids) {
			System.out.println("update mid to upgrade bisnis: " + mid);
			List<UserOobMidDtlTable> midTables = userOobLogicSvc.upgradeDataByMid(mid);
			userOobMidDtlTables.addAll(midTables);
		}
		System.out.println("scheduler getAllDataThatAlreadyUpgrade update end on " + SDF.format(new Date()));
		return userOobMidDtlTables;
	}

	@GetMapping("/updateNasabahToUpgrade")
	public ResponseEntity<Object> updateNasabahToUpgrade(@RequestParam("mid") String mid) {
		return new ResponseEntity<Object>(
				new SuccessValidation("Status Nasabah Berhasil Diupgrade", userOobLogicSvc.upgradeDataByMid(mid)),
				HttpStatus.OK);
	}

	@PostMapping("/checkStatusPendaftaran")
	public ResponseEntity<Object> checkStatusPendaftaran(
			@RequestHeader(value = SECRET_ID, required = true) String secretId,
			@RequestHeader(value = SECRET_KEY, required = true) String secretKey,
			@RequestBody StatusPendaftaranBodyRequest statusPendaftaranBodyRequest) {

		UserOobTable userOobTable = userOobLoginSvc.checkStatusPendaftaran(
				statusPendaftaranBodyRequest.getqNoPendaftaran(), statusPendaftaranBodyRequest.getqNoHp());
		Map<Integer, CheckStatusPendaftaranDto> mapStatus = new HashMap<>();
		CheckStatusPendaftaranDto checkStatusPendaftaranDto = new CheckStatusPendaftaranDto();
		checkStatusPendaftaranDto.setHeaderStatus("Pendaftaran Diterima");

		checkStatusPendaftaranDto.setBodyStatus(null);
		Date date1 = userOobTable.getCreateDate();
		checkStatusPendaftaranDto.setStatusInDate(DateConverter.convertDateToString(date1, "dd MMM yyy"));
		checkStatusPendaftaranDto.setStatusInTime(DateConverter.convertDateToString(date1, "HH:mm:ss") + " WIB");
		checkStatusPendaftaranDto.setStatusDate(date1);

		if (userOobTable.getIdStatusProses() == 1) {
			checkStatusPendaftaranDto.setPercentage("20%");
			checkStatusPendaftaranDto.setIsOnOpen(new Boolean(true));
		}
		mapStatus.put(1, checkStatusPendaftaranDto);

		checkStatusPendaftaranDto = new CheckStatusPendaftaranDto();
		checkStatusPendaftaranDto.setHeaderStatus("Pendaftaran Diproses");

		if (userOobTable.getIdStatusProses() == 2) {
			checkStatusPendaftaranDto.setBodyStatus("Kami sedang memproses aplikasi Anda dalam waktu 7 hari"
					+ " kerja sejak aplikasi diterima. Jika membutuhkan bantuan, hubungi Hi Yokke 14021.");
			checkStatusPendaftaranDto.setIsOnOpen(new Boolean(true));
			checkStatusPendaftaranDto.setPercentage("60%");
		}

		List<UserOobHistoryTable> tables = userOobLoginSvc.getAllHistoryUSerOobIdStatus(userOobTable.getId(), 2);

		if (tables != null) {
			Date date = tables.get(0).getCreateDate();
			checkStatusPendaftaranDto.setStatusDate(date);
			checkStatusPendaftaranDto.setStatusInDate(DateConverter.convertDateToString(date, "dd MMM yyy"));
			checkStatusPendaftaranDto.setStatusInTime(DateConverter.convertDateToString(date, "HH:mm:ss") + " WIB");
		}
		mapStatus.put(2, checkStatusPendaftaranDto);

		checkStatusPendaftaranDto = new CheckStatusPendaftaranDto();
		checkStatusPendaftaranDto.setStatusDate(new Date());
		if(userOobTable.getIdStatusProses()==3) {
		checkStatusPendaftaranDto.setHeaderStatus("Pendaftaran Berhasil");
		checkStatusPendaftaranDto.setPercentage("100%");
		checkStatusPendaftaranDto
				.setBodyStatus("Akses akun MORIS Anda dengan password yang diberikan melalui E-mail dan SMS.");
		checkStatusPendaftaranDto.setIsOnOpen(new Boolean(true));
		}
		else if(userOobTable.getIdStatusProses()==-1) {
			checkStatusPendaftaranDto.setHeaderStatus("Pendaftaran Ditolak");
			checkStatusPendaftaranDto.setPercentage("100%");
			checkStatusPendaftaranDto
					.setBodyStatus("Pendaftaran Akun MORIS Anda sudah ditolak.");
			checkStatusPendaftaranDto.setIsOnOpen(new Boolean(true));
			checkStatusPendaftaranDto.setIsOnTolak(new Boolean(true));
		}else {
			checkStatusPendaftaranDto.setHeaderStatus("Pendaftaran Berhasil");
		}
		

		List<UserOobHistoryTable> tables2 = userOobLoginSvc.getAllHistoryUSerOobIdStatus(userOobTable.getId(), 3);

		if (tables2 != null) {
			Date date2 = tables2.get(0).getCreateDate();
			checkStatusPendaftaranDto.setStatusDate(date2);
			checkStatusPendaftaranDto.setStatusInDate(DateConverter.convertDateToString(date2, "dd MMM yyy"));
			checkStatusPendaftaranDto.setStatusInTime(DateConverter.convertDateToString(date2, "HH:mm:ss") + " WIB");

		}
		   mapStatus.put(3, checkStatusPendaftaranDto);
	
		
		
		StatusPendaftaranBodyResponse bodyResponse = new StatusPendaftaranBodyResponse();
		bodyResponse.setMapStatus(mapStatus);
		bodyResponse.setInfoNasabah(mapperFacade.map(userOobTable, UserOobTableDto.class));

		return new ResponseEntity<Object>(new SuccessValidation("Status Pendaftaran Berhasil Didapat", bodyResponse),
				HttpStatus.OK);
	}

	@PutMapping("/acceptNasabahRequestPassword/{idNumber}")
	public ResponseEntity<Object> acceptNasabahRequestPassword(@PathVariable("idNumber") Long idNumber,
			@RequestHeader(value = SECRET_ID, required = true) String secretId,
			@RequestHeader(value = SECRET_KEY, required = true) String secretKey) {
		Map<String, Object> map = new HashMap<String, Object>();
		UserOobTable userOobTable = userOobLoginSvc.updateNasabahTo3(idNumber, secretId);
		UserOobTableAuthDtl userOobTableAuthDtl = userOobLoginSvc.createPasswordUser(idNumber);
		EmailBody.bodyForSuccessPendaftaran(userOobTable.getNamaPemilikUsaha(), userOobTable.getNoHandphone(),
				userOobTableAuthDtl.getPassword(), userOobTable.getEmailPemilikUsaha(), OOB_DNS + "/login",
				OOB_DNS + "/makePassword");
//		map.put("userOob", userOobTable);
//		map.put("authDtl", userOobTableAuthDtl);
		map.put("qrDtl", userOobLoginSvc.createQrNumberUser(idNumber));
		return new ResponseEntity<Object>(new SuccessValidation("Nasabah Berhasil Diupdate", map), HttpStatus.OK);
	}

//	@PutMapping("/updateTo3/{idNumber}")
//	public ResponseEntity<Object> updateTo3(
//			@PathVariable("idNumber") Long idNumber,
//			@RequestHeader(value = SECRET_ID, required = true) String secretId,
//			@RequestHeader(value = SECRET_KEY, required = true) String secretKey
//			){
//		Map<String, Object> map = new HashMap<String, Object>();
//		UserOobTable userOobTable=  userOobLoginSvc.updateNasabahTo3(idNumber, secretId);
//		map.put("userOobIsStatus", userOobTable.getIdStatusProses());
//		map.put("valid", true);
//		return new ResponseEntity<Object>(new SuccessValidation("Nasabah Berhasil Diupdate",map), HttpStatus.OK);
//	}

	@PutMapping("/updateTo2/{idNumber}")
	public ResponseEntity<Object> updateTo2(@PathVariable("idNumber") Long idNumber,
			@RequestHeader(value = SECRET_ID, required = true) String secretId,
			@RequestHeader(value = SECRET_KEY, required = true) String secretKey) {
		Map<String, Object> map = new HashMap<String, Object>();
		UserOobTable userOobTable = userOobLoginSvc.updateNasabahTo2(idNumber, secretId);
		map.put("userOobIsStatus", userOobTable.getIdStatusProses());
		map.put("valid", true);
		return new ResponseEntity<Object>(new SuccessValidation("Nasabah Berhasil Diupdate", map), HttpStatus.OK);
	}

	@PutMapping("/validasiInformasiRekening")
	public ResponseEntity<Object> validasiInformasiRekening(
			@RequestHeader(value = SECRET_ID, required = true) String secretId,
			@RequestHeader(value = SECRET_KEY, required = true) String secretKey,
			@RequestBody UserOobValidasiInformasiRekening validasiInformasiRekening) {
		AuthorizedResponseFirst(secretKey, secretId);
		
		saveYourLogsHere(validasiInformasiRekening, "/validasiInformasiRekening");

		System.err.println("your UserOobValidasiInformasiRekening " + new Gson().toJson(validasiInformasiRekening));

		validasiInformasiRekening(validasiInformasiRekening);

		String ciffNumber = callCifNumberFromMandiri(validasiInformasiRekening.getNamaPemilikUsaha(),
				validasiInformasiRekening.getNamaIbuKandung(), validasiInformasiRekening.getNomorKtp(),
				validasiInformasiRekening.getDob()); // this where u call dedup party;

		CustomerAccountInquiryByCifDto personResponse = getCustomerAccountInquiryByCifDto(ciffNumber,
				validasiInformasiRekening.getNomorRekening());
		String encrp = encryptTheCif(ciffNumber, secretKey, PURPOSE_TO_REGIST);
		String address = "";

		if (!Strings.isNullOrEmpty(personResponse.getCustomerAddress1())) {
			address += personResponse.getCustomerAddress1().trim() + ", ";
		}
		if (!Strings.isNullOrEmpty(personResponse.getCustomerAddress2())) {
			address += personResponse.getCustomerAddress2().trim() + ", ";
		}
		if (!Strings.isNullOrEmpty(personResponse.getCustomerAddress3())) {
			address += personResponse.getCustomerAddress3().trim() + ", ";
		}
		if (!Strings.isNullOrEmpty(personResponse.getCustomerAddress4())) {
			address += personResponse.getCustomerAddress4().trim();
			if (!Strings.isNullOrEmpty(personResponse.getCustomerAddress5())) {
				address += ", " + personResponse.getCustomerAddress5().trim();
			}
		}
//		
//		if(address!=null&&address.length()>150) {
//			throw new NotAcceptanceDataException(
//					"Panjang Karakter Alamat Tidak Boleh Lebih Dari 150 Karakter");
//		}
//	
//		

		String namaPengusaha = personResponse.getName();

//		if(namaPengusaha!=null&&namaPengusaha.length()>25) {
//			throw new NotAcceptanceDataException(
//					"Panjang Karakter Nama Pemilik Usaha Tidak Boleh Lebih Dari 25 Karakter");
//		}
		
		
		String phoneNumber = getPhoneNumberFromCustomerAccountInquiry(personResponse);
		return new ResponseEntity<Object>(new SuccessValidation("Verifikasi Data Berhasil",
				new UserOobValidasiResponse(encrp, address, MaskingNumber.maskingPhoneNumber(phoneNumber),
						namaPengusaha, personResponse.getNoRekeningYangValid())),
				HttpStatus.OK);
	}

	@PutMapping("/validasiInformasiRekening/nonLog")
	public ResponseEntity<Object> validasiInformasiRekeningNonLog(
			@RequestHeader(value = SECRET_ID, required = true) String secretId,
			@RequestHeader(value = SECRET_KEY, required = true) String secretKey,
			@RequestBody UserOobValidasiInformasiRekening validasiInformasiRekening) {
		AuthorizedResponseFirst(secretKey, secretId);

		// saveYourLogsHere(validasiInformasiRekening, "/validasiInformasiRekening");

		System.err.println("your UserOobValidasiInformasiRekening " + new Gson().toJson(validasiInformasiRekening));

		validasiInformasiRekening(validasiInformasiRekening);

		String ciffNumber = callCifNumberFromMandiri(validasiInformasiRekening.getNamaPemilikUsaha(),
				validasiInformasiRekening.getNamaIbuKandung(), validasiInformasiRekening.getNomorKtp(),
				validasiInformasiRekening.getDob()); // this where u call dedup party;

		CustomerAccountInquiryByCifDto personResponse = getCustomerAccountInquiryByCifDto(ciffNumber,
				validasiInformasiRekening.getNomorRekening());
		String encrp = encryptTheCif(ciffNumber, secretKey, PURPOSE_TO_REGIST);
		String address = "";

		if (!Strings.isNullOrEmpty(personResponse.getCustomerAddress1())) {
			address += personResponse.getCustomerAddress1().trim() + ", ";
		}
		if (!Strings.isNullOrEmpty(personResponse.getCustomerAddress2())) {
			address += personResponse.getCustomerAddress2().trim() + ", ";
		}
		if (!Strings.isNullOrEmpty(personResponse.getCustomerAddress3())) {
			address += personResponse.getCustomerAddress3().trim() + ", ";
		}
		if (!Strings.isNullOrEmpty(personResponse.getCustomerAddress4())) {
			address += personResponse.getCustomerAddress4().trim();
			if (!Strings.isNullOrEmpty(personResponse.getCustomerAddress5())) {
				address += ", " + personResponse.getCustomerAddress5().trim();
			}
		}
		String namaPengusaha = personResponse.getName();
		String phoneNumber = getPhoneNumberFromCustomerAccountInquiry(personResponse);
		return new ResponseEntity<Object>(new SuccessValidation("Verifikasi Data Berhasil",
				new UserOobValidasiResponse(encrp, address, MaskingNumber.maskingPhoneNumber(phoneNumber),
						namaPengusaha, personResponse.getNoRekeningYangValid())),
				HttpStatus.OK);
	}

	public void validasiInformasiRekening(UserOobValidasiInformasiRekening userOobValidasiInformasiRekening) {
		System.out.println("validasi informasi rekening pada jam " + new Date());

		int getAge = DateValidation.getAge(userOobValidasiInformasiRekening.getDob(), USER_OOB_DATE_FORMAT);

		if (getAge < 17) {
			throw new NotAcceptanceDataException("Hanya umur 17 tahun ke atas yang dapat melakukan registrasi");
		}

		if (userOobLogicSvc.itsDataExistByRekening(userOobValidasiInformasiRekening.getNomorRekening())) {
			throw new ConflictDataException("Data Telah Terdaftar");

		}

		if (Strings.isNullOrEmpty(userOobValidasiInformasiRekening.getNamaIbuKandung())) {
			throw new NotAcceptanceDataException("Mohon Masukkan Nama Ibu Kandung");
		}

		if (Strings.isNullOrEmpty(userOobValidasiInformasiRekening.getNamaIbuKandung().trim())) {
			throw new NotAcceptanceDataException("Mohon Masukkan Nama Ibu Kandung");
		}

		if (userOobValidasiInformasiRekening.getNamaIbuKandung().length() > 50) {
			throw new SizeDataOverCapacityException(
					"Panjang Karakter Nama I2bu Kandung Tidak Boleh Lebih Dari 50 Karakter");
		}

		if (Strings.isNullOrEmpty(userOobValidasiInformasiRekening.getNamaPemilikUsaha())) {
			throw new NotAcceptanceDataException("Mohon Masukkan Nama Pemilik Usaha");
		}

		if (Strings.isNullOrEmpty(userOobValidasiInformasiRekening.getNamaPemilikUsaha().trim())) {
			throw new NotAcceptanceDataException("Mohon Masukkan Nama Pemilik Usaha");
		}

		if (userOobValidasiInformasiRekening.getNamaPemilikUsaha().length() > 50) {
			throw new SizeDataOverCapacityException(
					"Panjang Karakter Nama Pemilik Usaha Tidak Boleh Lebih Dari 50 Karakter");
		}

		if (Strings.isNullOrEmpty(userOobValidasiInformasiRekening.getNomorRekening())) {
			throw new NotAcceptanceDataException("Mohon Masukkan Nomor Rekening Anda");
		}

		if (Strings.isNullOrEmpty(userOobValidasiInformasiRekening.getNomorRekening().trim())) {
			throw new NotAcceptanceDataException("Mohon Masukkan Nomor Rekening Anda");
		}

		if (userOobValidasiInformasiRekening.getNomorRekening().length() != 13) {
			throw new NotAcceptanceDataException("Panjang Karakter Nomor Rekening harus 13 Digit");
		}
		if (Strings.isNullOrEmpty(userOobValidasiInformasiRekening.getNomorKtp())) {
			throw new NotAcceptanceDataException("Mohon Masukkan Nomor KTP Anda");
		}
		if (Strings.isNullOrEmpty(userOobValidasiInformasiRekening.getNomorKtp().trim())) {
			throw new NotAcceptanceDataException("Mohon Masukkan Nomor KTP Anda");
		}

		if (userOobValidasiInformasiRekening.getNomorKtp().length() != 16) {
			throw new NotAcceptanceDataException("Panjang Karakter Nomor KTP harus 16 Digit");
		}
	}

	@PostMapping("/save")
	public ResponseEntity<Object> save(@RequestHeader(value = SECRET_ID, required = true) String secretId,
			@RequestHeader(value = SECRET_KEY, required = true) String secretKey,
			@RequestBody UserOobTableDto userOobTableDto) {

		AuthorizedResponseFirst(secretKey, secretId);

		saveYourLogsHere(userOobTableDto, "/save");

		if (!Strings.isNullOrEmpty(userOobTableDto.getEmailPemilikUsaha())) {
			System.out.println("validasi email : " + userOobTableDto.getEmailPemilikUsaha() + " : "
					+ userOobTableDto.getEmailPemilikUsaha().length());
			if (userOobTableDto.getEmailPemilikUsaha().length() > 50) {
				throw new NotAcceptanceDataException("Panjang Karakter Email Anda maksimal 50 Karakter");
			}
		}
		else {
			throw new NotAcceptanceDataException("Email Tidak Boleh Kosong. ");
		}
		
		
//		if (!Strings.isNullOrEmpty(userOobTableDto.getNamaUsaha())) {
//			System.out.println("validasi nama usaha : " + userOobTableDto.getNamaUsaha() + " : "
//					+ userOobTableDto.getNamaUsaha().length());
//			if (userOobTableDto.getNamaUsaha().length() > 100) {
//				String namaUsahaCrop = userOobTableDto.getNamaUsaha();
//				namaUsahaCrop = namaUsahaCrop.substring(0,100);
//				userOobTableDto.setNamaUsaha(namaUsahaCrop);
////				throw new NotAcceptanceDataException("Panjang Karakter Nama Usaha Anda maksimal 100 Karakter");
//			}
//		}
//		else {
//			throw new NotAcceptanceDataException("Nama Usaha Tidak Boleh Kosong. ");
//		}

		
//		if (!Strings.isNullOrEmpty(userOobTableDto.getAlamatPemilikUsaha())) {
//			System.out.println("validasi alamat pemilik usaha : " + userOobTableDto.getAlamatPemilikUsaha() + " : "
//					+ userOobTableDto.getAlamatPemilikUsaha().length());
//			if (userOobTableDto.getAlamatPemilikUsaha().length() > 255) {
//				String alamatPemilikUsahaCrop = userOobTableDto.getAlamatPemilikUsaha();
//				alamatPemilikUsahaCrop = alamatPemilikUsahaCrop.substring(0, 255);
//				userOobTableDto.setAlamatPemilikUsaha(alamatPemilikUsahaCrop);
//			}
//		}
//		else {
//			throw new NotAcceptanceDataException("Alamat Pemilik Usaha Tidak Boleh Kosong. ");
//		}
//		
		
//		if (!Strings.isNullOrEmpty(userOobTableDto.getAlamatLokasiUsahaSaatIni())) {
//			System.out.println("validasi alamat lokasi usaha saat ini : " + userOobTableDto.getAlamatLokasiUsahaSaatIni() + " : "
//					+ userOobTableDto.getAlamatLokasiUsahaSaatIni().length());
//			if (userOobTableDto.getNamaUsaha().length() > 255) {
//				String alamatLokasiUsahaSaatIniCrop = userOobTableDto.getAlamatLokasiUsahaSaatIni();
//				alamatLokasiUsahaSaatIniCrop = alamatLokasiUsahaSaatIniCrop.substring(0, 255);
//				userOobTableDto.setAlamatLokasiUsahaSaatIni(alamatLokasiUsahaSaatIniCrop);
//			}
//		}
		
		validasiInformasiRekening(mapperFacade.map(userOobTableDto, UserOobValidasiInformasiRekening.class));
		String cifNumber = callCifNumberFromMandiri(userOobTableDto.getNamaPemilikUsahaValidasi(),
				userOobTableDto.getNamaIbuKandung(), userOobTableDto.getNomorKtp(), userOobTableDto.getDob());

		UserOobTableAfterPathDto userOobTableAfterPathDto = new UserOobTableAfterPathDto();
		userOobTableAfterPathDto = mapperFacade.map(userOobTableDto, UserOobTableAfterPathDto.class);

		validationTipsKategoriUsaha(userOobTableAfterPathDto.getKategoriLokasiUsaha(),
				userOobTableAfterPathDto.getJenisLokasiUsaha(), userOobTableAfterPathDto.getIdKelurahan());
		masterUsahaPropertiesTablesSvc.checkValidationMaster(userOobTableAfterPathDto.getOmzet(),
				MasterUsahaPropertiesTablesDao.OMSET_USAHA);
		masterUsahaPropertiesTablesSvc.checkValidationMaster(userOobTableAfterPathDto.getJenisUsaha(),
				MasterUsahaPropertiesTablesDao.JENIS_USAHA);

		userOobTableAfterPathDto
				.setPathFotoBarangAtauJasa(getFileNewJpeg(new Base64FileDto(userOobTableDto.getBase64FotoBarangAtauJasa()),
						REGISTRASI_MENU + "/" + cifNumber + "/", "FOTO_BARANG_JASA"));
		userOobTableAfterPathDto.setPathFotoPemilikTempatUsaha(
				getFileNewJpeg(new Base64FileDto(userOobTableDto.getBase64FotoPemilikTempatUsaha()),
						REGISTRASI_MENU + "/" + cifNumber + "/", "FOTO_PEMILIK_TEMPAT_USAHA"));
		userOobTableAfterPathDto
				.setPathFotoTempatUsaha(getFileNewJpeg(new Base64FileDto(userOobTableDto.getBase64FotoTempatUsaha()),
						REGISTRASI_MENU + "/" + cifNumber + "/", "FOTO_TEMPAT_USAHA"));
		userOobTableAfterPathDto.setPathKtp(getFileNewJpeg(new Base64FileDto(userOobTableDto.getBase64Ktp()),
				REGISTRASI_MENU + "/" + cifNumber + "/", "FOTO_KTP"));
		userOobTableAfterPathDto.setPathNpwp(getFileNewJpeg(new Base64FileDto(userOobTableDto.getBase64Npwp()),
				REGISTRASI_MENU + "/" + cifNumber + "/", "FOTO_NPWP"));
		userOobTableAfterPathDto.setPathWajahKtp(getFileNewJpeg(new Base64FileDto(userOobTableDto.getBase64WajahKtp()),
				REGISTRASI_MENU + "/" + cifNumber + "/", "FOTO_WAJAH_KTP"));

		CustomerAccountInquiryByCifDto personResponse = getCustomerAccountInquiryByCifDto(cifNumber,
				userOobTableDto.getNomorRekening());
		String address = "";
		
		if (!Strings.isNullOrEmpty(personResponse.getCustomerAddress1())) {
			address += personResponse.getCustomerAddress1().trim() + ", ";
		}
		if (!Strings.isNullOrEmpty(personResponse.getCustomerAddress2())) {
			address += personResponse.getCustomerAddress2().trim() + ", ";
		}
		if (!Strings.isNullOrEmpty(personResponse.getCustomerAddress3())) {
			address += personResponse.getCustomerAddress3().trim() + ", ";
		}
		if (!Strings.isNullOrEmpty(personResponse.getCustomerAddress4())) {
			address += personResponse.getCustomerAddress4().trim();
			if (!Strings.isNullOrEmpty(personResponse.getCustomerAddress5())) {
				address += ", " + personResponse.getCustomerAddress5().trim();
			}
		}

		
		if (userOobTableAfterPathDto.getKategoriLokasiUsaha().equals(1)) {
			userOobTableAfterPathDto.setAlamatLokasiUsahaSaatIni(null);
			userOobTableAfterPathDto.setIdKelurahan(null);
		}

		userOobTableAfterPathDto.setKodePosAlamatPemilikUsaha(personResponse.getCustomerAddress5());
		userOobTableAfterPathDto.setNoHandphone(getPhoneNumberFromCustomerAccountInquiry(personResponse));
		if (!Strings.isNullOrEmpty(userOobTableAfterPathDto.getNoNpwp())) {
			userOobTableAfterPathDto.setNoNpwp(CharacterRemoval.removeAllChar(userOobTableAfterPathDto.getNoNpwp()));
		} else {
			userOobTableAfterPathDto.setNoNpwp(null);
			userOobTableAfterPathDto.setPathNpwp(null);
		}
		userOobTableAfterPathDto.setCifNumber(cifNumber);
		userOobTableAfterPathDto.setAlamatPemilikUsaha(address);
		userOobTableAfterPathDto.setIsUserAgree(new Boolean(true));
		
		UserOobTable oobTable = userOobLoginSvc.saveRegistrasi(userOobTableAfterPathDto);

		LogStatusEmailDto logStatusEmailDto = EmailBody.bodyForPendaftaranUsahaMandiri(oobTable.getNamaPemilikUsaha(),
				MaskingNumber.maskingNomorPendaftaran(String.valueOf(oobTable.getId()), 8),
				oobTable.getEmailPemilikUsaha(), OOB_DNS + "/cekStatusPendaftaran");

		kirimEmail(logStatusEmailDto.getLogTujuan(), logStatusEmailDto.getLogUser(), logStatusEmailDto.getLogStatus(),
				logStatusEmailDto.getLogSubject());

		String smsWord = "Nomor Pendaftaran MORIS Anda adalah "
				+ MaskingNumber.maskingNomorPendaftaran(String.valueOf(oobTable.getId()), 8) + ". "
				+ "			Silahkan cek status pendaftaran di " + OOB_DNS + "/cekStatusPendaftaran ";
		kirimSms(userOobTableAfterPathDto.getNoHandphone(), smsWord);
		otpHandlerTableSvc.isYourOtpAlreadyValidToSave(cifNumber, secretKey, MENU_REGISTRASI);
		return new ResponseEntity<Object>(oobTable, HttpStatus.OK);
	}

	private void validationTipsKategoriUsaha(String kategoriUsaha, String jenisLokasiUsaha, Long idKelurahan) {
		if (Strings.isNullOrEmpty(kategoriUsaha)) {
			throw new NotFoundDataException("Kategori Usaha Tidak Boleh Kosong");
		}

		if (Strings.isNullOrEmpty(jenisLokasiUsaha)) {
			throw new NotFoundDataException("Jenis Lokasi Usaha Tidak Boleh Kosong");
		}

		if (!(kategoriUsaha.equalsIgnoreCase("0") || kategoriUsaha.equalsIgnoreCase("1"))) {
			throw new NotAcceptanceDataException("ID Kategori Usaha Saat Ini Hanya Menerima 1 Atau 0");
		}

		if (kategoriUsaha.equalsIgnoreCase("0")) {
			masterUsahaPropertiesTablesSvc.checkValidationMaster(jenisLokasiUsaha,
					MasterUsahaPropertiesTablesDao.LOKASI_PERMANEN_USAHA);
			alamatCreatorSvc.validationIdKelurahan(idKelurahan);
		}

		if (kategoriUsaha.equalsIgnoreCase("1")) {
			masterUsahaPropertiesTablesSvc.checkValidationMaster(jenisLokasiUsaha,
					MasterUsahaPropertiesTablesDao.LOKASI_NON_PERMANEN_USAHA);
		}
	}

	@GetMapping("/users/export")
	public void exportToCsv(HttpServletResponse response) throws IOException {
		response.setContentType("text/csv");
		DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");
		String currentDateTime = dateFormatter.format(new Date());

		String headerKey = "Content-Disposition";
		String headerValue = "attachment; filename=users_" + currentDateTime + ".csv";
		response.setHeader(headerKey, headerValue);

		List<UserOobTableFile> listFile = userOobLoginSvc.getFileExcel();

		CsvPreference PIPE_DELIMITED = new CsvPreference.Builder('"', '|', "\n").build();
		ICsvBeanWriter csvWriter = new CsvBeanWriter(response.getWriter(), PIPE_DELIMITED);

		String[] csvHeader = { "ID", "Nama Pemilik Usaha", "tanggal lahir", "Nomor Ktp", "Nama Gadis Ibu Kandung",
				"Nomor Rekening", "Nomor Handphone", "Alamat KTP", "Email", "Url Foto KTP", "Url Foto KTP Wajah",
				"Url Foto NPWP", "Nomor NPWP", "Nama Usaha", "Nomor Telepon", "Jenis Usaha", "Omzet per Bulan",
				"Url Tempat Usaha", "Url Foto Barang atau Jasa", "Url Foto Pemilik di Tempat Usaha", "Lokasi Usaha",
				"Alamat Lokasi Usaha", "Kategori Usaha" };

		String[] nameMapping = { "id", "namaPemilikUsaha", "dob", "nomorKtp", "namaIbuKandung", "nomorRekening",
				"noHandphone", "alamatPemilikUsaha", "emailPemilikUsaha", "pathKtp", "pathWajahKtp", "pathNpwp",
				"noNpwp", "namaUsaha", "nomorTelp", "jenisUsaha", "omzet", "pathFotoTempatUsaha",
				"pathFotoBarangAtauJasa", "pathFotoPemilikTempatUsaha", "jenisLokasiUsaha", "alamatLokasiUsahaSaatIni",
				"kategoriLokasiUsaha" };

		csvWriter.writeHeader(csvHeader);
		for (UserOobTableFile user : listFile) {
			csvWriter.write(user, nameMapping);
		}
		csvWriter.close();

	}

}
