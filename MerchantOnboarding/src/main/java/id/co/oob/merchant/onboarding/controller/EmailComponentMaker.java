package id.co.oob.merchant.onboarding.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import id.co.oob.db.qris.merchant.onboarding.dao.session.SessionLoginHdrDao;
import id.co.oob.db.qris.merchant.onboarding.dao.session.SessionLoginHdrDtlDao;
import id.co.oob.db.qris.merchant.onboarding.repository.user.SessionLoginHdrDtl;
import id.co.oob.db.qris.merchant.onboarding.service.BaseSvc;
import id.co.oob.db.qris.merchant.onboarding.service.session.SessionServiceMaster;
import id.co.oob.db.qris.merchant.onboarding.service.user.UserOobLogicSvc;
import id.co.oob.lib.common.merchant.onboarding.dto.email.EmailMakerDto;
import id.co.oob.lib.common.merchant.onboarding.email.EmailBody;
import id.co.oob.lib.common.merchant.onboarding.email.EmailContent;
import id.co.oob.lib.common.merchant.onboarding.throwable.AnauthorizedException;
import id.co.oob.lib.common.merchant.onboarding.throwable.ForbiddenAuthorizedException;
import id.co.oob.lib.common.merchant.onboarding.throwable.NotFoundDataException;

@RestController
@RequestMapping("/email-maker")
public class EmailComponentMaker extends BaseCtl {

	@Autowired
	private UserOobLogicSvc userOobLogicSvc;
	@Autowired
	private SessionLoginHdrDao sessionLoginHdrDao;
	@Autowired
	private SessionLoginHdrDtlDao sessionLoginHdrDtlDao;
	@Autowired
	private SessionServiceMaster sessionServiceMaster;

	@PostMapping("/send-mail")
	public Boolean sendMail(@RequestBody EmailMakerDto emailMakerDto) {

		return EmailContent.sendMailTest(emailMakerDto.getTo(), emailMakerDto.getSubject(), emailMakerDto.getBody(),
				null, null);
	}

	@PostMapping("/send-forgot-password")
	public String sendForgotPassword(@RequestHeader(value = SECRET_ID, required = true) String secretId,
			@RequestHeader(value = SECRET_KEY, required = true) String secretKey,
			@RequestBody Map<String, Object> emailMakerDto) {


		saveYourLogsHere(emailMakerDto, "/email-maker/send-forgot-password");
		
		AuthorizedResponseFirst(secretKey, secretId);
		if (emailMakerDto.get("email-accept") == null) {
			throw new NotFoundDataException("Key Body Tidak Ditemukan");
		}

		String sessionLocal = (String) emailMakerDto.get("sessionLocal");
		if (sessionLoginHdrDao.getSessionLoginHdr(sessionLocal) == null) {
			throw new ForbiddenAuthorizedException("Session Tidak Valid");
		}

		SessionLoginHdrDtl sess = sessionServiceMaster.getSessionLoginDtl(sessionLocal,
				"/email-maker/send-forgot-password");

		if (sess != null) {
			throw new AnauthorizedException(
					"Mohon maaf, Anda sudah melakukan percobaan mengganti password sebanyak 7 kali. Anda dapat melakukan pergantian password kembali pada jam "
							+ new SimpleDateFormat("HH:mm").format(sess.getBlockedOpenDate()));
		}

		sessionServiceMaster.countBlockNeeded(sessionLocal, "/email-maker/send-forgot-password", 7, 1);

		String emailAccept = (String) emailMakerDto.get("email-accept");
		Boolean canEmail = userOobLogicSvc.updateToFirstRegistWhenForgotPassword(emailAccept);

		
		
//		if (!canEmail) {
//			throw new NotFoundDataException("Mohon masukkan email yang telah terdaftar.");
//		}
		
		return "Jika alamat e-mail dikenal oleh sistem kami, link untuk rubah password akan dikirimkan segera.";
	}

	@Scheduled(fixedRate = 1 * 60 * 1000L)
	public void removeSessionForgotPasswordEach1Minutes() {
		List<SessionLoginHdrDtl> sessionLoginHdrDtls = sessionLoginHdrDtlDao
				.SessionLoginHdrDtlWithUrlBlocked("/email-maker/send-forgot-password");
		for (SessionLoginHdrDtl sessionLoginHdrDtl : sessionLoginHdrDtls) {
			Date date = sessionLoginHdrDtl.getBlockedOpenDate();
			if (date != null) {
				if (new Date().after(date)) {
					sessionLoginHdrDtlDao.deleteById(sessionLoginHdrDtl.getIdDtl());
				}
			}
			else {
				sessionLoginHdrDtlDao.deleteById(sessionLoginHdrDtl.getIdDtl());
			}
		}

	}
}
