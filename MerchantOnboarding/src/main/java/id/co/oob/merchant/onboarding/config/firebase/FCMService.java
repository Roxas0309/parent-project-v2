package id.co.oob.merchant.onboarding.config.firebase;

import java.time.Duration;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import org.springframework.stereotype.Service;

import com.google.firebase.messaging.AndroidConfig;
import com.google.firebase.messaging.AndroidNotification;
import com.google.firebase.messaging.ApnsConfig;
import com.google.firebase.messaging.Aps;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.Message;
import com.google.firebase.messaging.Notification;
import com.google.firebase.messaging.WebpushConfig;
import com.google.firebase.messaging.WebpushNotification;

import id.co.oob.lib.common.merchant.onboarding.firebase.PushNotificationRequest;

@Service
public class FCMService {
	public void sendMessage(Map<String, String> data, PushNotificationRequest request)
			throws InterruptedException, ExecutionException {
		Message message = getPreconfiguredMessageWithData(data, request);
		String response = sendAndGetResponse(message);
	}

	private String sendAndGetResponse(Message message) throws InterruptedException, ExecutionException {
		return FirebaseMessaging.getInstance().sendAsync(message).get();
	}

	private AndroidConfig getAndroidConfig() {
		return AndroidConfig.builder()
			.setTtl(Duration.ofMinutes(60).toMillis())
			.setPriority(AndroidConfig.Priority.HIGH)
			.setNotification(AndroidNotification.builder()
					.setTitle("Hai").setBody("Itu Dia")
					.setSound("starwars3.wav").setColor("#FFFF00").build())
			.build();
	}

	private ApnsConfig getApnsConfig() {
		return ApnsConfig.builder().setAps(Aps.builder().setSound("default").build()).build();
	}

	private Message getPreconfiguredMessageWithData(Map<String, String> data, PushNotificationRequest request) {
		return getPreconfiguredMessageBuilder(request).putAllData(data).setToken(request.getToken()).build();
	}

	private Message.Builder getPreconfiguredMessageBuilder(PushNotificationRequest request) {
		AndroidConfig androidConfig = getAndroidConfig();
		ApnsConfig apnsConfig = getApnsConfig();
		return Message.builder()
			.setApnsConfig(apnsConfig)
			.setAndroidConfig(androidConfig)
			.setWebpushConfig(WebpushConfig.builder().
					setNotification(new WebpushNotification
					(request.getTitle(), request.getMessage()
							,"https://finarya-dev-storage.oss-ap-southeast-5.aliyuncs.com/oob-nasabah-qr-code/ID1020049086918/ID1020049086918_A01.png?Expires=1610114020&OSSAccessKeyId=LTAI4Fcz8McUthgnGPUmHQ8c&Signature=%2FG7dSCektDsPjD3csyN9KWpPTVI%3D")
					).build());
		   //  .setNotification(new Notification(request.getTitle(), request.getMessage()))
			
	}
}
