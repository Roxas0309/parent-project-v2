package id.co.oob.merchant.onboarding.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONObject;
import org.json.JSONTokener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import id.co.oob.db.qris.merchant.onboarding.service.entityManager.QrisDatabaseQuery;
import id.co.oob.lib.common.merchant.onboarding.ws.ConnectionPortIpDto;
import id.co.oob.merchant.onboarding.MerchantOnboardingApplication;
import id.co.oob.merchant.onboarding.service.ConnectionCheckingPort;
import id.co.oob.merchant.onboarding.service.RestartService;

@RestController
@RequestMapping("/configuration-component-maker")
public class ConfigurationComponentMaker extends BaseCtl {

	@Autowired
	private QrisDatabaseQuery qrisDatabaseQuery;
	
	@Autowired
	private RestartService restartService;
	
	@PostMapping("/run-command")
	public String runCommandFromCmd(@RequestBody String commandValue) throws IOException, InterruptedException {
		  String s = "";
	        Process p;
	        String myLine = "";
	        try {
	            p = Runtime.getRuntime().exec(commandValue);
	            BufferedReader br = new BufferedReader(
	                new InputStreamReader(p.getInputStream()));
	            while ((s = br.readLine()) != null) {
	            	myLine += s + "\n";
	            }
	            p.waitFor();
	            System.out.println ("exit: " + p.exitValue());
	            p.destroy();
	        } catch (Exception e) {}
	        return myLine;
	}
	
	  @PostMapping("/restart")
	    public String restartApp() {
		 restartService.restartApp();
		 return "restart done..";
	  } 
	 
	 @PostMapping("/check-connection")
	 public String checkConnection(@RequestBody ConnectionPortIpDto connectionPortIpDto) {
		 Boolean isConnect = ConnectionCheckingPort.isConnectedBetweenPortAndIp(connectionPortIpDto.getIp(), connectionPortIpDto.getPort());
		 if(isConnect) {
			 return "Connected to IP : " + connectionPortIpDto.getIp() + ", PORT : " + connectionPortIpDto.getPort();
		 }
		 else{
			 return "[obbadmin@OOB-PROD1 ~]$ telnet " + connectionPortIpDto.getIp() + " " + connectionPortIpDto.getPort() + " \n "
					 +"Trying " + connectionPortIpDto.getIp()+"... \n"
					 +"telnet: connect to address " + connectionPortIpDto.getIp() + ": " + "connection timed out";
		 }
	 }
	 
	 
	
	 @GetMapping("/checkPoolSize")
	 public String checkPoolSize() {
		 return "True";
	 }

	@GetMapping("/get-camera-component")
	public Map<String, Object> getCameraComponentFirst() {
		InputStreamResource resource = new InputStreamResource(
				getInputStreamAliOss("configuration-meta/configurasi-camera.json"));
		InputStream is = null;
		try {
			is = resource.getInputStream();
		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Map<String, Object> map = new HashMap<String, Object>();

		if (is == null) {
			map.put("video", true);
			return map;
		}

		BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(is));
		JSONTokener tokener = new JSONTokener(bufferedReader);
		JSONObject json = new JSONObject(tokener);
		String valJson = json.toString();
		map = mapperJsonToHashMap(valJson);
		return map;

	}

}
