package id.co.oob.merchant.onboarding.lib.encrypt;
import java.nio.charset.StandardCharsets;
import java.security.*;
import java.util.*;

import javax.crypto.Cipher;
public class XSignatureSHA256withRSA {


    private static final byte[] toBeSigned = new byte[] {
        0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x10
    };

    private static List<byte[]> generatedSignatures = new ArrayList<>();

    public static void main(String... argv) throws Exception {
        //First generate a public/private key pair
        KeyPairGenerator generator = KeyPairGenerator.getInstance("RSA");
        generator.initialize(2048, new SecureRandom());
        KeyPair pair = generator.generateKeyPair();

        //The private key can be used to sign (not encrypt!) a message. The public key holder can then verify the message.

        String message = "fceda4da-ae95-4d14-8cb6-eef392139a1b|2020-05-12T16:12:54.893T+0700";

        //Let's sign our message
        Signature privateSignature = Signature.getInstance("SHA256withRSA");
        privateSignature.initSign(pair.getPrivate());
        privateSignature.update(message.getBytes(StandardCharsets.UTF_8));

        byte[] signature = privateSignature.sign();

        //Let's check the signature
        Signature publicSignature = Signature.getInstance("SHA256withRSA");
        publicSignature.initVerify(pair.getPublic());
        publicSignature.update(message.getBytes(StandardCharsets.UTF_8));
        boolean isCorrect = publicSignature.verify(signature);

        System.out.println("Signature correct: " + isCorrect);

        //The public key can be used to encrypt a message, the private key can be used to decrypt it.
        //Encrypt the message
        Cipher encryptCipher = Cipher.getInstance("RSA");
        encryptCipher.init(Cipher.ENCRYPT_MODE, pair.getPublic());

        byte[] cipherText = encryptCipher.doFinal(message.getBytes());

        //Now decrypt it
        Cipher decriptCipher = Cipher.getInstance("RSA");
        decriptCipher.init(Cipher.DECRYPT_MODE, pair.getPrivate());

      //  String encrpyMessage = new String(encryptCipher.doFinal(cipherText), StandardCharsets.UTF_8);
        String decipheredMessage = new String(decriptCipher.doFinal(cipherText), StandardCharsets.UTF_8);

        System.out.println("hasilnya : " +Base64.getEncoder().encodeToString(cipherText));
    }
}
