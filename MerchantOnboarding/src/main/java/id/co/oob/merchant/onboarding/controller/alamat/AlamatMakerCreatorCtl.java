package id.co.oob.merchant.onboarding.controller.alamat;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import id.co.oob.db.qris.merchant.onboarding.service.alamat.AlamatCreatorSvc;
import id.co.oob.lib.common.merchant.onboarding.dto.alamat.ProvinsiTableDto;
import id.co.oob.merchant.onboarding.controller.BaseCtl;

@RestController
@RequestMapping("/Alamat")
public class AlamatMakerCreatorCtl extends BaseCtl {

	@Autowired
	private AlamatCreatorSvc alamatCreatorSvc;

	@PostMapping("/Save/Provinsi")
	public ResponseEntity<Object> saveNewProvinsi(
			@RequestHeader(value = SECRET_ID, required = true) String secretId,
			@RequestHeader(value = SECRET_KEY, required = true) String secretKey,
			@RequestBody List<ProvinsiTableDto> provinsiTableDtos) {
		AuthorizedResponseFirst(secretKey, secretId);
		//List<ProvinsiTableDto> provinsesSaved = alamatCreatorSvc.saveAllProvinsiTable(provinsiTableDtos);
		alamatCreatorSvc.updateAllListUpdateProvinsi();
		return new ResponseEntity<Object>(null, HttpStatus.OK);
	}
	
	@GetMapping("/FindFullAlamatFromKelurahan/")
	public ResponseEntity<Object> FindFullAlamatFromKelurahan(
			@RequestParam(name = "id_kelurahan", required = true) Long idKelurahan,
			@RequestHeader(value = SECRET_ID, required = true) String secretId,
			@RequestHeader(value = SECRET_KEY, required = true) String secretKey) {
		AuthorizedResponseFirst(secretKey, secretId);
		Map<String, Object> map = alamatCreatorSvc.getFullAddressAlamatFromKelurahan(idKelurahan);
		return new ResponseEntity<Object>(map, HttpStatus.OK);
	}


	@GetMapping("/FindAll/Provinsi")
	public ResponseEntity<Object> findAllProvinsi(
			@RequestHeader(value = SECRET_ID, required = true) String secretId,
			@RequestHeader(value = SECRET_KEY, required = true) String secretKey) {
		AuthorizedResponseFirst(secretKey, secretId);
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("provinsi", alamatCreatorSvc.findAllProvinsiInTable());
		return new ResponseEntity<Object>(map, HttpStatus.OK);
	}

	@PutMapping("/Generate/KabupatenOrKota")
	public ResponseEntity<Object> generateNewKabupaten(@RequestHeader(value = SECRET_ID, required = true) String secretId,
			@RequestHeader(value = SECRET_KEY, required = true) String secretKey) {
		AuthorizedResponseFirst(secretKey, secretId);
		return new ResponseEntity<Object>(alamatCreatorSvc.generateAllKabupatenTable(), HttpStatus.OK);
	}

	@GetMapping("/FindAll/KabupatenOrKota")
	public ResponseEntity<Object> findAllKabupaten(
			@RequestHeader(value = SECRET_ID, required = true) String secretId,
			@RequestHeader(value = SECRET_KEY, required = true) String secretKey) {
		AuthorizedResponseFirst(secretKey, secretId);
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("kota_kabupaten", alamatCreatorSvc.findAllKabupatenTable());
		return new ResponseEntity<Object>(map, HttpStatus.OK);
	}

	@GetMapping("/FindAll/KabupatenOrKota/ByIdProvinsi")
	public ResponseEntity<Object> findAllKabupatenByIdProvinsi(
			@RequestHeader(value = SECRET_ID, required = true) String secretId,
			@RequestHeader(value = SECRET_KEY, required = true) String secretKey,
			@RequestParam(name = "id_provinsi", required = true) Long idProvinsi) {
		AuthorizedResponseFirst(secretKey, secretId);
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("kota_kabupaten", alamatCreatorSvc.findAllKabupatenTableByIdProvins(idProvinsi));
		return new ResponseEntity<Object>(map, HttpStatus.OK);
	}

	@PutMapping("/Generate/Kecamatan")
	public ResponseEntity<Object> generateNewKecamatan(
			@RequestHeader(value = SECRET_ID, required = true) String secretId,
			@RequestHeader(value = SECRET_KEY, required = true) String secretKey) {
		AuthorizedResponseFirst(secretKey, secretId);
		return new ResponseEntity<Object>(alamatCreatorSvc.generateAllKecamatanTable(), HttpStatus.OK);
	}

	@GetMapping("/FindAll/Kecamatan")
	public ResponseEntity<Object> findAllKecamatan(
			@RequestHeader(value = SECRET_ID, required = true) String secretId,
			@RequestHeader(value = SECRET_KEY, required = true) String secretKey) {
		AuthorizedResponseFirst(secretKey, secretId);
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("kecamatan", alamatCreatorSvc.findAllKecamatanTable());
		return new ResponseEntity<Object>(map, HttpStatus.OK);
	}

	@GetMapping("/FindAll/Kecamatan/ByIdKabupatenOrKota")
	public ResponseEntity<Object> findAllKecamatanTableByIdKabupatem(
			@RequestHeader(value = SECRET_ID, required = true) String secretId,
			@RequestHeader(value = SECRET_KEY, required = true) String secretKey,
			@RequestParam(name = "id_kabupaten", required = true) Long idKabupaten) {
		AuthorizedResponseFirst(secretKey, secretId);
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("kecamatan", alamatCreatorSvc.findAllKecamatanTableByIdKabupatem(idKabupaten));
		return new ResponseEntity<Object>(map, HttpStatus.OK);
	}

	@PutMapping("/Generate/Kelurahan")
	public ResponseEntity<Object> generateNewKelurahan(@RequestHeader(value = SECRET_ID, required = true) String secretId,
			@RequestHeader(value = SECRET_KEY, required = true) String secretKey) {
		AuthorizedResponseFirst(secretKey, secretId);
		return new ResponseEntity<Object>(alamatCreatorSvc.generateAllKelurahanTable(), HttpStatus.OK);
	}

	@GetMapping("/FindAll/Kelurahan")
	public ResponseEntity<Object> findAllKelurahan(@RequestHeader(value = SECRET_ID, required = true) String secretId,
			@RequestHeader(value = SECRET_KEY, required = true) String secretKey) {
		AuthorizedResponseFirst(secretKey, secretId);
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("kelurahan", alamatCreatorSvc.findAllKelurahanTable());
		return new ResponseEntity<Object>(map, HttpStatus.OK);
	}

	@GetMapping("/FindAll/Kelurahan/ByIdKecamatan")
	public ResponseEntity<Object> findAllKelurahanByIdKecamatan(
			@RequestHeader(value = SECRET_ID, required = true) String secretId,
			@RequestHeader(value = SECRET_KEY, required = true) String secretKey,
			@RequestParam(name = "id_kecamatan", required = true) Long idKecamatan) {
		AuthorizedResponseFirst(secretKey, secretId);
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("kelurahan", alamatCreatorSvc.findAllKelurahanTableByIdKecamatan(idKecamatan));
		return new ResponseEntity<Object>(map, HttpStatus.OK);
	}
}
