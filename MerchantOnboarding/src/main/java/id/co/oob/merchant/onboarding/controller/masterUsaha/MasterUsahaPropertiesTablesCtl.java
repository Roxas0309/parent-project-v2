package id.co.oob.merchant.onboarding.controller.masterUsaha;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import id.co.oob.db.qris.merchant.onboarding.dao.masterUsaha.MasterUsahaPropertiesTablesDao;
import id.co.oob.db.qris.merchant.onboarding.service.masterUsaha.MasterUsahaPropertiesTablesSvc;
import id.co.oob.lib.common.merchant.onboarding.dto.masterUsaha.MasterUsahaPropertiesTableDto;
import id.co.oob.merchant.onboarding.controller.BaseCtl;

@RestController
@RequestMapping("/usahaProperties")
public class MasterUsahaPropertiesTablesCtl extends BaseCtl{

	@Autowired
	private MasterUsahaPropertiesTablesSvc masterUsahaPropertiesTablesSvc;
	
	@PostMapping("/saveMany")
	public List<MasterUsahaPropertiesTableDto> saveMasterUsaha(
			@RequestHeader(value = SECRET_ID, required = true) String secretId,
			@RequestHeader(value = SECRET_KEY, required = true) String secretKey,
			@RequestBody List<MasterUsahaPropertiesTableDto> masterUsahaPropertiesTableDtos
			){
		List<MasterUsahaPropertiesTableDto> propertiesTableDtos = masterUsahaPropertiesTablesSvc.save(masterUsahaPropertiesTableDtos);
		return propertiesTableDtos;
	}
	
	
	
	@GetMapping("/jenisUsaha")
	public List<MasterUsahaPropertiesTableDto> getJenisUsaha(
			@RequestHeader(value = SECRET_ID, required = true) String secretId,
			@RequestHeader(value = SECRET_KEY, required = true) String secretKey
			){
		List<MasterUsahaPropertiesTableDto> propertiesTableDtos = masterUsahaPropertiesTablesSvc.
				getAllMasterUsahaTablesByTipeData(MasterUsahaPropertiesTablesDao.JENIS_USAHA); 
		return propertiesTableDtos;
	}
	
	@GetMapping("/omsetUsaha")
	public List<MasterUsahaPropertiesTableDto> getOmsetUsaha(
			@RequestHeader(value = SECRET_ID, required = true) String secretId,
			@RequestHeader(value = SECRET_KEY, required = true) String secretKey
			){
		List<MasterUsahaPropertiesTableDto> propertiesTableDtos = masterUsahaPropertiesTablesSvc.
				getAllMasterUsahaTablesByTipeData(MasterUsahaPropertiesTablesDao.OMSET_USAHA); 
		return propertiesTableDtos;
	}
	
	@GetMapping("/lokasiPermanenUsaha")
	public List<MasterUsahaPropertiesTableDto> getLokasiPermanenUsaha(
			@RequestHeader(value = SECRET_ID, required = true) String secretId,
			@RequestHeader(value = SECRET_KEY, required = true) String secretKey
			){
		List<MasterUsahaPropertiesTableDto> propertiesTableDtos = masterUsahaPropertiesTablesSvc.
				getAllMasterUsahaTablesByTipeData(MasterUsahaPropertiesTablesDao.LOKASI_PERMANEN_USAHA); 
		return propertiesTableDtos;
	}
	
	@GetMapping("/lokasiNonPermanenUsaha")
	public List<MasterUsahaPropertiesTableDto> getLokasiNonPermanenUsaha(
			@RequestHeader(value = SECRET_ID, required = true) String secretId,
			@RequestHeader(value = SECRET_KEY, required = true) String secretKey
			){
		List<MasterUsahaPropertiesTableDto> propertiesTableDtos = masterUsahaPropertiesTablesSvc.
				getAllMasterUsahaTablesByTipeData(MasterUsahaPropertiesTablesDao.LOKASI_NON_PERMANEN_USAHA); 
		return propertiesTableDtos;
	}
	
	@GetMapping("/akunOobQr")
	public List<MasterUsahaPropertiesTableDto> getAkunOobQr(
			@RequestHeader(value = SECRET_ID, required = true) String secretId,
			@RequestHeader(value = SECRET_KEY, required = true) String secretKey
			){
		List<MasterUsahaPropertiesTableDto> propertiesTableDtos = masterUsahaPropertiesTablesSvc.
				getAllMasterUsahaTablesByTipeData(MasterUsahaPropertiesTablesDao.AKUN_OOB_QR); 
		return propertiesTableDtos;
	}
	
}
