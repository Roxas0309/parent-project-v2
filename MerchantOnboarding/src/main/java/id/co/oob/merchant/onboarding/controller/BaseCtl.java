package id.co.oob.merchant.onboarding.controller;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.util.UriUtils;

import com.aliyun.oss.ClientConfiguration;
import com.aliyun.oss.OSS;
import com.google.common.base.Strings;
import com.google.gson.Gson;

import id.co.oob.db.qris.merchant.onboarding.dao.EmailLogTableDao;
import id.co.oob.db.qris.merchant.onboarding.dao.SmsLogTableDao;
import id.co.oob.db.qris.merchant.onboarding.dao.WebServiceLogDao;
import id.co.oob.db.qris.merchant.onboarding.repository.log.EmailLogTable;
import id.co.oob.db.qris.merchant.onboarding.repository.log.SmsLogTable;
import id.co.oob.db.qris.merchant.onboarding.repository.log.WebServiceLog;
import id.co.oob.db.qris.merchant.onboarding.repository.user.AllUserOobDtlRepo;
import id.co.oob.db.qris.merchant.onboarding.service.authentication.AllTheAuthenticationCircuitSvc;
import id.co.oob.db.qris.merchant.onboarding.service.user.UserOobLogicSvc;
import id.co.oob.lib.common.merchant.onboarding.BaseCommon;
import id.co.oob.lib.common.merchant.onboarding.convert.CharacterRemoval;
import id.co.oob.lib.common.merchant.onboarding.convert.DateConverter;
import id.co.oob.lib.common.merchant.onboarding.dto.SmsBlastDto;
import id.co.oob.lib.common.merchant.onboarding.dto.mandiri.AccountInfoDetailDto;
import id.co.oob.lib.common.merchant.onboarding.dto.mandiri.CustomerAccountInquiryByCifDto;
import id.co.oob.lib.common.merchant.onboarding.dto.mandiri.CustomerCifInquiryByNikDto;
import id.co.oob.lib.common.merchant.onboarding.dto.mandiri.MaasResponseDto;
import id.co.oob.lib.common.merchant.onboarding.dto.mandiri.PhoneInfoDetailDto;
import id.co.oob.lib.common.merchant.onboarding.dto.otp.OtpNotificationRequest;
import id.co.oob.lib.common.merchant.onboarding.dto.otp.OtpNotificationResponse;
import id.co.oob.lib.common.merchant.onboarding.dto.token.AuthenticationSpiritMaker;
import id.co.oob.lib.common.merchant.onboarding.dto.token.MandiriTokenResponse;
import id.co.oob.lib.common.merchant.onboarding.dto.token.UserAuthTokenTableDto;
import id.co.oob.lib.common.merchant.onboarding.dto.user.CifDecryptor;
import id.co.oob.lib.common.merchant.onboarding.encryptor.Base64EncodeCustom;
import id.co.oob.lib.common.merchant.onboarding.encryptor.EncryptDataFiller;
import id.co.oob.lib.common.merchant.onboarding.encryptor.SHA256withRSAEncrypted;
import id.co.oob.lib.common.merchant.onboarding.file.Base64FileDto;
import id.co.oob.lib.common.merchant.onboarding.throwable.AnauthorizedException;
import id.co.oob.lib.common.merchant.onboarding.throwable.InternalServerErrorException;
import id.co.oob.lib.common.merchant.onboarding.throwable.NotAcceptanceDataException;
import id.co.oob.lib.common.merchant.onboarding.throwable.NotFoundDataException;
import id.co.oob.merchant.onboarding.lib.encrypt.XSignatureHmacSha512Vers2;

@Component
public class BaseCtl extends BaseCommon {

	//13158186562 
	
	@Value("${database.telnet.please}")
	public String databaseTelnetPlease;
	
	@Value("${application.profile-name}")
	public String applicationProfileName;
	
	@Value("${sftp.path.file}")
	public String sftpPathFile;
	
	@Value("${oob_dns}")
	protected  String OOB_DNS;

	@Value("${oss.folder.endpoint}")
	protected  String OSS_FOLDER_ENDPOINT;

	@Value("${sms.engine.username}")
	protected String SMS_ENGINE_USERNAME;
	
	@Value("${sms.engine.password}")
	protected String SMS_ENGINE_PASSWORD;
	
	@Value("${sms.engine.sender}")
	protected String SMS_ENGINE_SENDER;
	
	@Value("${spring.cloud.alicloud.oss.endpoint}")
	protected String springCloudAlicloudOssEndpoint;
	
	@Value("${spring.cloud.alicloud.access-key}")
	protected String springCloudAlicloudAccessKey;
	
	@Value("${spring.cloud.alicloud.secret-key}")
	protected String springCloudAlicloudSecretKey;
	
	@Autowired
	protected OSS ossClient;

	@Autowired
	private AllTheAuthenticationCircuitSvc allTheAuthenticationCircuitSvc;

	@Autowired
	private SmsLogTableDao smsLogTableDao;
	
	@Autowired
	private EmailLogTableDao emailLogTableDao;
	
	@Autowired
	protected UserOobLogicSvc userOobLogicSvc;

	protected static final SimpleDateFormat SDF = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
	
	protected static final String PURPOSE_TO_REGIST = "PURPOSE_TO_REGIST";
	protected static final String PURPOSE_TO_OTP = "PURPOSE_TO_OTP";

	protected static final Integer ERROR_CODE = 0;
	protected static final String SECRET_ID = "secret-id";
	protected static final String SESSION_ID = "session-id";
	protected static final String SECRET_TOKEN = "secret-token";
	protected static final String SESSION_ITEM = "session-item";
	protected static final String SECRET_KEY = "secret-key";
	protected static final String URL_CODE = "http://10.243.213.179:5555";
	@Value("${oss_BUCKET_NAME}")
	protected String BUCKET_NAME;
	protected static final String USER_NAME_OTP = "apiuser";
	protected static final String PASSWORD_OTP = "apipassword";
	protected static final String URL_HEADER_OTP = "https://smsblast.id/api/sendsingle.json";
	
	protected static final String TICK_CODE_TRX = "Transaksi-";
	protected static final String TICK_CODE_PRC = "Pencairan-";
	protected static final String TICK_CODE_INF = "Informasi-";
	
	//production
	@Value("${maas_url}")
	protected String MAAS_URL;

	//development
	//protected static final String MAAS_URL = "http://149.129.239.139:28081/MaasQueryTrxCtl/queryMe";
	
	@Value("${clientSecret}")
	public String clientSecret;
	
	@Value("${clientId}")
	public String clientId;
	
//	public String keyPassword = "opensoa123";
//	public String keyAlias = "OpenSOADev";
//	public String urlJks = "oob-mandiri-jks/opensoadev.trust.jks";
	
	@Value("${urlJks}")
	public String urlJks;
	
	@Value("${keyPassword}")
	public String keyPassword;
	
	@Value("${keyAlias}")
	public String keyAlias;
	
	@Value("${sms.engine.username}")
	public String smsEngineUserName;
	
	@Value("${sms.engine.password}")
	public String smsEnginePassword = "mtiP6521";
	
	@Value("${sms.engine.url}")
	public String smsEngineUrl;
	
	@Value("${sms.engine.sender}")
	public String smsEngineSender;
	
	public String keyStoreUrl = "/home/iforce/Desktop/opensoadev.trust.jks";
	public String deployMentFrom = "LOCAL";

	@Value("${mandiri_url}")
	protected String MANDIRI_URL;
	
	@Autowired
	private WebServiceLogDao webServiceLogDao;
	
	public void saveYourLogsHere(Object requestBody, String url) {
	
		Thread thread = new Thread(){
			    public void run(){
			    	Long id = webServiceLogDao.count();
			    	System.out.println("count log " + id);
			    	while (webServiceLogDao.existsById(id)) {
						id++;
					}
			    	System.out.println("id log web service log : " + id);
			    	WebServiceLog webServiceLog = new WebServiceLog();
			    	webServiceLog.setLogId(id);
			    	webServiceLog.setLogTglWaktu(new Date());
			    	webServiceLog.setRequest_url(url);
			    	webServiceLog.setRequestBody(new Gson().toJson(requestBody));
			    	webServiceLogDao.save(webServiceLog);
			    }
			  };

	    thread.start();
	}
	
	public void saveYourLogsHere(Object requestBody, String url, Object responseBody) {
		
		Thread thread = new Thread(){
			    public void run(){
			    	Long id = webServiceLogDao.count();
			    	WebServiceLog webServiceLog = new WebServiceLog();
			    	webServiceLog.setLogId(id+1);
			    	webServiceLog.setLogTglWaktu(new Date());
			    	webServiceLog.setRequest_url(url);
			    	webServiceLog.setResponseBody(new Gson().toJson(responseBody));
			    	webServiceLog.setRequestBody(new Gson().toJson(requestBody));
			    	webServiceLogDao.save(webServiceLog);
			    }
			  };

	    thread.start();
	}
	

	public String encodeUrl(String word) {
		return UriUtils.encode(word, StandardCharsets.UTF_8);  
	}
	
	public String responseKirimSms(String userPhoneNumber, String message) {
    	
    	MultiValueMap<String, String> map= new LinkedMultiValueMap<String, String>();
    	map.add("sender", smsEngineSender);
    	map.add("msisdn", userPhoneNumber);
    	map.add("message", message);
    	
    	
    	Map<String, String> headerMap = new HashMap<String, String>();
    	headerMap.put("Authorization", "Basic " + 
    	                    Base64EncodeCustom.resultEncodeStringSmsBlast(smsEngineUserName,smsEnginePassword));
		
		ResponseEntity<String> response = wsBodySmsBlast(smsEngineUrl, map, HttpMethod.POST, headerMap);
    	
		return response.getBody();
	}
	
	public void kirimSms(String userPhoneNumber, String message) {
		
		 Thread thread = new Thread(){
			    public void run(){
			    	
//					untuk smsgw.sprintasia.net
			    	
//			    	String uName = "t35S4ndez4";
//			    	String pWord = "b5e0fKswe";
//			    	String url = "https://smsgw.sprintasia.net/api/msg.php?u="+uName+"&p="+pWord+""
//			    			+ "&d="+userPhoneNumber+"&m="+message;
//					
//					ResponseEntity<String> response = wsBody(url, null, HttpMethod.GET, null);
				
			    	String responseSms = responseKirimSms(userPhoneNumber, message);
			    	
					SmsLogTable smsLogTable = new SmsLogTable();
					smsLogTable.setLogId(generateIdFromExisting());
					smsLogTable.setLogStatus(responseSms);
					smsLogTable.setLogSubject(message);
					smsLogTable.setLogTglWaktu(new Date());
					smsLogTable.setLogTujuan(userPhoneNumber);
					smsLogTable.setLogUser(userPhoneNumber);
					smsLogTableDao.save(smsLogTable);
			    }
			  };

	    thread.start();
		
	}
	

	public void kirimEmail(String useremail, String tujuan, String statusSend, String logSubject) {
		System.out.println("log subject email untuk : "+ useremail +" : " + logSubject + " di tanggal : " + new Date());
		
		if(logSubject!=null) {
			 logSubject = logSubject.substring(0, 200);
		 }
		final String ls = logSubject;
		Thread thread = new Thread(){
			    public void run(){
			    	EmailLogTable emailLogTable = new EmailLogTable();
			    	emailLogTable.setLogCc("None");
			    	emailLogTable.setLogId(generateIdEmailFromExisting());
			    	emailLogTable.setLogResend("none");
			    	emailLogTable.setLogStatus(statusSend);
			    	emailLogTable.setLogSubject(ls);
			    	emailLogTable.setLogTglWaktu(new Date());
			    	emailLogTable.setLogTujuan(tujuan);
			    	emailLogTable.setLogUser(useremail);
			    	emailLogTableDao.save(emailLogTable);
			    }
			  };

	    thread.start();
		
	}
	
	public Long generateIdFromExisting() {
		Long id = smsLogTableDao.count()+1;
		while(smsLogTableDao.existsById(id)) {
			id++;
		}
		return id;
	};
	
	public Long generateIdEmailFromExisting() {
		Long id = emailLogTableDao.count()+1;
		while(emailLogTableDao.existsById(id)) {
			id++;
		}
		return id;
	};
	
	public OtpNotificationResponse getSmsOtp(OtpNotificationRequest request) {
		String urlHeader = "https://smsblast.id/api/sendsingle.json";
		String urlComplete = urlHeader+
				"?username="+USER_NAME_OTP+"&password="+PASSWORD_OTP+
				"&sender=OOB&msisdn="+request.getNoPhone()+"&message="+request.getMessage();
		ResponseEntity<String> obj = 
				wsBody(urlComplete, null, HttpMethod.GET, new HashMap<String, String>());
		OtpNotificationResponse response = new OtpNotificationResponse();
		try {
			 response = mapperJsonToSingleDto(obj.getBody(), OtpNotificationResponse.class);
		} catch (Exception e) {
			e.printStackTrace();
			throw new InternalServerErrorException("tidak dapat memetakan OtpNotificationResponse ke dari response object ");
		}
		
		if(response.getCode()==0) {
			throw new InternalServerErrorException(response.getMessage());
			
		}
		return response;
	}
	
	public List<Object[]>  getMaasQuerySvc(String query){
		ResponseEntity<String> response = wsBody(MAAS_URL, query, HttpMethod.POST, 
				null);
		//System.err.println("maas response data " + new Gson().toJson(response));
		List<Object[]> listArrays = new ArrayList<Object[]>();
		try {
			listArrays = mapperJsonToListDto(response.getBody(), Object[].class);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new InternalServerErrorException("error while mapping MaasResponseDto in BaseCtl");
		}
		return listArrays;
	}
	
	public ResponseEntity<String>  callMandiriCtl(
			Object body, String path){
		String objBody = new Gson().toJson(body);
		objBody = objBody.replace("\\u003c", "<");
		objBody = objBody.replace("\\u0027", "'");
		objBody = objBody.replace("\\u0026", "&");
		objBody = objBody.replace("\\u003d", "=");
		System.out.println("objBody " + objBody);
		String url = MANDIRI_URL+"/openapi/auth/token";
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("grant_type", "client_credentials");
		String timeStamp = DateConverter.convertDateToString(new Date(),
				"yyyy-MM-dd'T'HH:mm:ss.SSS'T'+0700");
		System.out.println("mandiri_url " + MANDIRI_URL);
		System.out.println("clientSecret " + clientSecret);
		System.out.println("clientId " + clientId);
		System.out.println("urlJks " + urlJks);
		System.out.println("keyPassword " + keyPassword );
		System.out.println("keyAlias " + keyAlias );
		String message = clientId+"|"+timeStamp;
		System.out.println("message " + message);


		Map<String, Object> mappo = null;
		try {
			
			mappo = SHA256withRSAEncrypted.mySignatureWithInputStream
					(message, keyPassword, keyAlias,getInputStreamAliOss
							(urlJks));
			
		} catch (UnrecoverableKeyException | InvalidKeyException | KeyStoreException | NoSuchAlgorithmException
				| CertificateException | SignatureException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new NotAcceptanceDataException(e.getMessage());
		}

		Map<String, String> headerMap = new HashMap<String, String>();
		headerMap.put("X-Mandiri-Key", clientId);
		headerMap.put("X-SIGNATURE", (String) mappo.get(SHA256withRSAEncrypted.SIGNATURE));
		headerMap.put("X-TIMESTAMP", timeStamp);
		ResponseEntity<String> response = 
				wsBody(url, map, HttpMethod.POST, headerMap);
		MandiriTokenResponse mandiriTokenResponse = new MandiriTokenResponse();
		try {
			mandiriTokenResponse = mapperJsonToSingleDto(response.getBody(), MandiriTokenResponse.class);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new InternalServerErrorException("Token Mandiri tidak dapat digenerate");
		}

		System.err.println("req payload " + objBody);

		String value = "POST:"+path+":"+mandiriTokenResponse.getAccessToken()+
				":"+objBody+":"+timeStamp;
		System.out.println("value : " + value);
		AuthenticationSpiritMaker authenticationSpiritMaker = new AuthenticationSpiritMaker();
		authenticationSpiritMaker.setAuthorization(mandiriTokenResponse.getTokenType()+" " + mandiriTokenResponse.getAccessToken());
		authenticationSpiritMaker.setxTimestamp(timeStamp);
		authenticationSpiritMaker.setxSignature(XSignatureHmacSha512Vers2.buildHmacSignature
				(value, clientSecret));
		Map<String, String> mapApi = new HashMap<String, String>();
		mapApi.put("Authorization", authenticationSpiritMaker.getAuthorization());
		mapApi.put("X-SIGNATURE", authenticationSpiritMaker.getxSignature());
		mapApi.put("X-TIMESTAMP", authenticationSpiritMaker.getxTimestamp());
		ResponseEntity<String> responseApi = wsBody
				(MANDIRI_URL+path, body, HttpMethod.POST, mapApi);
		return responseApi;
	}

	protected String getPhoneNumberFromCustomerAccountInquiry(CustomerAccountInquiryByCifDto customerAccountInquiryByCifDto ) {
		System.err.println("cek data handphone dengan inquiry customerAccountInquiryByCifDto : " + new Gson().toJson(customerAccountInquiryByCifDto.getPhoneInfoDetails()));
	
		
		List<PhoneInfoDetailDto> phoneInfoDetailDtosOnlineBanking = 
				customerAccountInquiryByCifDto.getPhoneInfoDetails()
				.stream()
				.sorted(Comparator.comparing(PhoneInfoDetailDto::getPhoneNoUpdateDate).reversed())
				.filter(phoneDtl-> phoneDtl.getPhoneNoType().equalsIgnoreCase("Online Banking Phone"))
				.collect(Collectors.toList());
		System.out.println("phone info detail Online Banking " + new Gson().toJson(phoneInfoDetailDtosOnlineBanking));
		for (PhoneInfoDetailDto phoneInfoDetailDto : phoneInfoDetailDtosOnlineBanking) {
			return phoneInfoDetailDto.getPhoneNumber().replaceFirst("62", "0").replaceAll("\\+", ""); 
		}
			
		List<PhoneInfoDetailDto> phoneInfoDetailDtosDebitOnlinePhone = 
				customerAccountInquiryByCifDto.getPhoneInfoDetails()
				.stream()
				.filter(phoneDtl-> phoneDtl.getPhoneNoType().equalsIgnoreCase("Debit Online Phone"))
				.collect(Collectors.toList());
		System.out.println("phone info detail Online Banking " + new Gson().toJson(phoneInfoDetailDtosDebitOnlinePhone));
		for (PhoneInfoDetailDto phoneInfoDetailDto : phoneInfoDetailDtosDebitOnlinePhone) {
			return phoneInfoDetailDto.getPhoneNumber().replaceFirst("62", "0").replaceAll("\\+", ""); 
		}
		
		
		List<PhoneInfoDetailDto> phoneInfoDetailDtosHandphoneICS = 
				customerAccountInquiryByCifDto.getPhoneInfoDetails()
				.stream()
				.filter(phoneDtl-> phoneDtl.getPhoneNoType().equalsIgnoreCase("Handphone") && phoneDtl.getPhoneSourceSystem().equalsIgnoreCase("Bank Mandiri - ICS"))
				.collect(Collectors.toList());
		System.out.println("phone info detail Online Banking " + new Gson().toJson(phoneInfoDetailDtosHandphoneICS));
		for (PhoneInfoDetailDto phoneInfoDetailDto : phoneInfoDetailDtosHandphoneICS) {
			return phoneInfoDetailDto.getPhoneNumber().replaceFirst("62", "0").replaceAll("\\+", ""); 
		}
		
		
		List<PhoneInfoDetailDto> phoneInfoDetailDtosHandphoneEmas = 
				customerAccountInquiryByCifDto.getPhoneInfoDetails()
				.stream()
				.sorted(Comparator.comparing(PhoneInfoDetailDto::getPhoneSourceSystem))
				.filter(phoneDtl-> phoneDtl.getPhoneNoType().equalsIgnoreCase("Handphone") && phoneDtl.getPhoneSourceSystem().equalsIgnoreCase("Bank Mandiri - eMas"))
				.collect(Collectors.toList());
		System.out.println("phone info detail Online Banking " + new Gson().toJson(phoneInfoDetailDtosHandphoneEmas));
		for (PhoneInfoDetailDto phoneInfoDetailDto : phoneInfoDetailDtosHandphoneEmas) {
			return phoneInfoDetailDto.getPhoneNumber().replaceFirst("62", "0").replaceAll("\\+", ""); 
		}
		
		
//		for (int i = customerAccountInquiryByCifDto.getPhoneInfoDetails().size()-1; i>= 0; i--) {
//			PhoneInfoDetailDto phoneDtl = customerAccountInquiryByCifDto.getPhoneInfoDetails().get(i);
//			System.out.println("phone type yang akan divalidasiin : " + new Gson().toJson(phoneDtl) );
//			
//			if(phoneDtl.getPhoneNoType().equalsIgnoreCase("Handphone") && phoneDtl.getPhoneSourceSystem().equalsIgnoreCase("Bank Mandiri - ICS")) {
//				return phoneDtl.getPhoneNumber().replaceFirst("62", "0").replaceAll("\\+", "");
//			//	 getPhoneDetailIcs.add(phoneDtl);
//			}
//			else if(phoneDtl.getPhoneNoType().equalsIgnoreCase("Handphone")  && phoneDtl.getPhoneSourceSystem().equalsIgnoreCase("Bank Mandiri - eMas")) {
//				return phoneDtl.getPhoneNumber().replaceFirst("62", "0").replaceAll("\\+", "");
//			}
//			else if(phoneDtl.getPhoneNoType().equalsIgnoreCase("Online Banking Phone") && phoneDtl.getPhoneSourceSystem().equalsIgnoreCase("Bank Mandiri - ICS")) {
//				return phoneDtl.getPhoneNumber().replaceFirst("62", "0").replaceAll("\\+", "");
//			}
//			else if(phoneDtl.getPhoneNoType().equalsIgnoreCase("Debit Online Phone") && phoneDtl.getPhoneSourceSystem().equalsIgnoreCase("Bank Mandiri - ICS")) {
//				return phoneDtl.getPhoneNumber().replaceFirst("62", "0").replaceAll("\\+", "");
//			}
//			else if(phoneDtl.getPhoneNoType().equalsIgnoreCase("Online Banking Phone")  && phoneDtl.getPhoneSourceSystem().equalsIgnoreCase("Bank Mandiri - eMas")) {
//				return phoneDtl.getPhoneNumber().replaceFirst("62", "0").replaceAll("\\+", "");
//			}
//			else if(phoneDtl.getPhoneNoType().equalsIgnoreCase("Debit Online Phone")  && phoneDtl.getPhoneSourceSystem().equalsIgnoreCase("Bank Mandiri - eMas")) {
//				return phoneDtl.getPhoneNumber().replaceFirst("62", "0").replaceAll("\\+", "");
//			}
//		}
//		
////		if(getPhoneDetailIcs!=null&& getPhoneDetailIcs.size()>0) {
////			return getPhoneDetailIcs.get(0).getPhoneNumber().replaceFirst("62", "0").replaceAll("\\+", "");
////		}
////		
////		else if(getPhoneDetaiEmas!=null&& getPhoneDetaiEmas.size()>0)
////		{
////			return getPhoneDetaiEmas.get(0).getPhoneNumber().replaceFirst("62", "0").replaceAll("\\+", "");
////		}
		

		throw new NotFoundDataException("Data Handphone Tidak Dapat Ditemukan");
	}

	protected CustomerAccountInquiryByCifDto getCustomerAccountInquiryByCifDto(String cifNumber) {
		CustomerAccountInquiryByCifDto customerAccountInquiryByCifDto = new CustomerAccountInquiryByCifDto();
		Map<String, Object> mapBody = new HashMap<String, Object>();
		mapBody.put("cifNumber", cifNumber);

		int trialError = 5;
		String errorResponse = "5000000";
		while(errorResponse.equalsIgnoreCase("5000000") && trialError!=0) {
			ResponseEntity<String> mandiriSvcResponse = callMandiriCtl(mapBody, "/openapi/customers/v1.0/CustomerAccountInquiryByCIF");

			System.out.println("CustomerAccountInquiryByCifDto response " + new Gson().toJson(mandiriSvcResponse));

			try {
				customerAccountInquiryByCifDto = mapperJsonToSingleDto(mandiriSvcResponse.getBody(), CustomerAccountInquiryByCifDto.class);
			} catch (Exception e) {
				e.printStackTrace();
				throw new NotAcceptanceDataException("Tidak dapat memetakan mandiriSvcResponse ke customerAccountInquiryByCifDto");
			}
			
			errorResponse = customerAccountInquiryByCifDto.getResponseCode();
			trialError--;
			if(!errorResponse.equalsIgnoreCase("5000000")) {
				trialError = 0;
			}
		}

		if(!customerAccountInquiryByCifDto.getResponseCode().equals("2000000")) {
			throw new NotFoundDataException("tidak dapat menemukan Info Akun dengan data cif yang diberikan");
		}

		return customerAccountInquiryByCifDto;
	}

	protected CustomerAccountInquiryByCifDto getCustomerAccountInquiryByCifDto(String cifNumber, String noRekening) {
		
		if(!noRekening.matches("^[0-9]+$")) {
			throw new NotAcceptanceDataException("Invalid No Rekening Format.");
		}
		
		if(noRekening!= null && noRekening.length()!=13) {
			System.out.println("akun rekening tidak 13 karakter");
			String countZero = "";
			for (int i = 1; i <= 13-noRekening.length(); i++) {
				countZero += "0";
			}
			noRekening = countZero.concat(noRekening);
		}
		
		CustomerAccountInquiryByCifDto customerAccountInquiryByCifDto = new CustomerAccountInquiryByCifDto();
		System.out.println("cifNumber : " + cifNumber + " noRekening : " + noRekening);
		customerAccountInquiryByCifDto = getCustomerAccountInquiryByCifDto(cifNumber);
		boolean isRekeningValid = false;
		for (AccountInfoDetailDto akunInfo : customerAccountInquiryByCifDto.getAccountInfoDetails()) {
			String akunRekeningValid = akunInfo.getAccountNo();
			if(akunRekeningValid!= null && akunRekeningValid.length()!=13) {
				System.out.println("akun rekening tidak 13 karakter");
				String countZero = "";
				for (int i = 1; i <= 13-akunRekeningValid.length(); i++) {
					countZero += "0";
				}
				akunRekeningValid = countZero.concat(akunRekeningValid);
			}
			System.out.println("sedang cek no rekening SOA : " + akunRekeningValid + " dengan status "+ akunInfo.getAccountStatus() +" dengan memvalidasikan : " + noRekening);
			if(akunRekeningValid.equals(noRekening) 
					&& akunInfo.getAccountStatus().equals("Active")
			  ) {
				customerAccountInquiryByCifDto.setNoRekeningYangValid(akunRekeningValid);
				isRekeningValid = true;
			}
		}

		if(!isRekeningValid) {
			throw new NotFoundDataException("tidak dapat menemukan Info Akun dengan data Rekening yang diberikan");
		}


		return customerAccountInquiryByCifDto;
	}

	protected String callCifNumberFromMandiri(String namaPemilikUsaha, String namaIbuKandung, String nomorKtp, String dob) {
		Map<String, Object> mapBody = new HashMap<String, Object>();
		mapBody.put("name", namaPemilikUsaha);
		mapBody.put("mothersName",namaIbuKandung);
		mapBody.put("nik",nomorKtp);
		mapBody.put("dateOfBirth", DateConverter.convertDateToAnotherDateFormat(dob, USER_OOB_DATE_FORMAT, USER_OOB_DATE_FORMAT_MANDIRI));
		System.out.println("map body : " + new Gson().toJson(mapBody));

		ResponseEntity<String> mandiriSvcResponse = callMandiriCtl(mapBody, "/openapi/customers/v1.0/CustomerCIFInquiryByNIK");

		System.out.println("callCifNumberFromMandiri svc response " + new Gson().toJson(mandiriSvcResponse));

		CustomerCifInquiryByNikDto customerCifInquiryByNikDto = new CustomerCifInquiryByNikDto();

		try {
			customerCifInquiryByNikDto = mapperJsonToSingleDto(mandiriSvcResponse.getBody(), CustomerCifInquiryByNikDto.class);
		} catch (Exception e) {
			e.printStackTrace();
			throw new NotAcceptanceDataException("Tidak dapat memetakan mandiriSvcResponse ke customerCifInquiryByNikDto");
		}

		if(customerCifInquiryByNikDto.getResponseCode().equals("4000006")) {
			throw new NotAcceptanceDataException(customerCifInquiryByNikDto.getResponseMessage());
			
		}
		
		if(!customerCifInquiryByNikDto.getResponseCode().equals("2000000")) {
			throw new NotFoundDataException("tidak dapat menemukan CIF dengan data yang diberikan");
		}

		return customerCifInquiryByNikDto.getCifNumber();

	}


	protected String encryptTheCif(String cif, String secretKey, String purposeTo) {
		CifDecryptor decryptor = new CifDecryptor();
		decryptor.setCifNumber(cif);
		decryptor.setDateEncryptor(new Date().toString());
		decryptor.setPurposeTo(purposeTo);
		String encResult = EncryptDataFiller.encrypt(new Gson().toJson(decryptor), secretKey);
		return encResult;
	}

	public AllUserOobDtlRepo getAllUserInfoByToken(String token, String secretKey) {
		String valueDecrypt = EncryptDataFiller.decrypt(token, secretKey);
		UserAuthTokenTableDto userAuthTokenTableDto = new UserAuthTokenTableDto();
		try {
			userAuthTokenTableDto = mapperJsonToSingleDto(valueDecrypt, UserAuthTokenTableDto.class);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new InternalServerErrorException("Pastikan Token Dapat Digenerate dengan key khusus yang diberikan");
		}

		AllUserOobDtlRepo repo = userOobLogicSvc.getDetailAllUserByItsId(userAuthTokenTableDto.getIdToken());
		return repo;
	}

	public String getPathSaveNameAliOss(InputStream fileStream, String fileName) {
		String bucketName = BUCKET_NAME;
		String folder = OSS_FOLDER_ENDPOINT;
		String finalName = folder+DateConverter.convertDateToString(new Date(), "ddMMyyyyHHmmSS")+";"+fileName;
		ossClient.putObject(bucketName,finalName, fileStream);
		Date expiration = new Date(new Date().getTime() + 3600 * 100000);
		String url = ossClient.generatePresignedUrl(bucketName, finalName, expiration).toString();
		return url;
	}

	public String getPathSaveNameAliOss(InputStream fileStream, String fileName, String folder) {
		String bucketName = BUCKET_NAME;
		String finalName = OSS_FOLDER_ENDPOINT+folder+DateConverter.convertDateToString(new Date(), "ddMMyyyyHHmmSS")+";"+fileName;
		System.out.println("put object with : access key : " + springCloudAlicloudAccessKey + ", secret key : " + springCloudAlicloudSecretKey + ", in endPoint : " + springCloudAlicloudOssEndpoint);
		ossClient.putObject(bucketName,finalName, fileStream);
		return finalName;
	}
	
	public String getPathSaveNameAliOssQr(InputStream fileStream, String fileName, String folder) {
		String bucketName = BUCKET_NAME;
		String finalName = folder+"/"+fileName+".png";
		System.out.println("bucket name : " + bucketName + " finalName " + finalName);
		ossClient.putObject(bucketName,finalName, fileStream);
		return finalName;
	}

	public InputStream getInputStreamAliOss(String qrUrl) {
		InputStream is = ossClient.getObject(BUCKET_NAME,qrUrl).getObjectContent();
		//byte[] bit = is.
		return is;
	}

	public String getUrlNameGetValueOss(String finalFileName) {
		String bucketName = BUCKET_NAME;
		Date expiration = new Date(new Date().getTime() + 3600 * 100000);
		String url = ossClient.generatePresignedUrl(bucketName, finalFileName, expiration).toString();
		return url;
	}

	public String getFile(Base64FileDto base64) {
		String urlnya = null;
		if(!Strings.isNullOrEmpty(base64.getBase64())) {
			InputStream in = new ByteArrayInputStream(getBase64File(base64.getBase64()));
			urlnya = getPathSaveNameAliOss(in, base64.getFileName());
		}
		return urlnya;
	}

	@Deprecated
	public String getFile(Base64FileDto base64, String folder) {
		String urlnya = null;
		if(!Strings.isNullOrEmpty(base64.getBase64())) {
			InputStream in = new ByteArrayInputStream(getBase64File(base64.getBase64()));
			urlnya = getPathSaveNameAliOss(in, base64.getFileName(),folder);
		}
		return urlnya;
	}

	public String getFileNew(Base64FileDto base64, String folder, String uuid) {
		String urlnya = null;
		if(!Strings.isNullOrEmpty(base64.getBase64())) {
			InputStream in = new ByteArrayInputStream(getBase64File(base64.getBase64()));
			String mimeType = null;
			String fileExtension = ".png";
			 try {
			        mimeType = URLConnection.guessContentTypeFromStream(in); //mimeType is something like "image/jpeg"
			        String delimiter="[/]";
			        String[] tokens = mimeType.split(delimiter);
			        fileExtension = "."+tokens[1];
			    } catch (IOException ioException){

			    }
			urlnya = getPathSaveNameAliOss(in,uuid+fileExtension,folder);
		}
		return urlnya;
	}
	
	public String getFileNewJpeg(Base64FileDto base64, String folder, String uuid) {
		String urlnya = null;
		if(!Strings.isNullOrEmpty(base64.getBase64())) {
			InputStream in = new ByteArrayInputStream(getBase64File(base64.getBase64()));
			String mimeType = null;
			String fileExtension = ".jpeg";
			 try {
			        mimeType = URLConnection.guessContentTypeFromStream(in); //mimeType is something like "image/jpeg"
			        String delimiter="[/]";
			        String[] tokens = mimeType.split(delimiter);
			        fileExtension = "."+tokens[1];
			    } catch (IOException ioException){

			    }
			urlnya = getPathSaveNameAliOss(in,uuid+fileExtension,folder);
		}
		return urlnya;
	}
	
	public String getFileNewInputStream(InputStream base64, String folder, String uuid) {
		System.out.println("save folder : " + folder + " save uuid : " + uuid);
		String urlnya = getPathSaveNameAliOss(base64,uuid+".png",folder);
		System.out.println("Urlnya adalah " + urlnya);
		return urlnya;
	}

	public void AuthorizedResponseFirst(String secretKey, String secretId) {
		if (!isAuthAndCanAccessed(secretId, secretKey)) {
			throw new AnauthorizedException("Invalid Secret Key And Secret Id");
		} else {
			return;
		}
	}

	private Boolean isAuthAndCanAccessed(String secretId, String secretKey) {
//		System.err.println("clientId : " + secretId);
//		System.err.println("clientSecret : " + secretKey);
		if (allTheAuthenticationCircuitSvc.getMyAccessWsAuthInfo(secretId, secretKey) != null) {
			return true;
		}
		return false;
	}
}
