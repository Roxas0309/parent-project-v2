package id.co.oob.merchant.onboarding.controller.otp;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import id.co.oob.db.qris.merchant.onboarding.repository.otp.OtpHandlerTable;
import id.co.oob.db.qris.merchant.onboarding.service.otp.OtpBlockHandlerSvc;
import id.co.oob.db.qris.merchant.onboarding.service.otp.OtpHandlerTableSvc;
import id.co.oob.lib.common.merchant.onboarding.convert.DateConverter;
import id.co.oob.lib.common.merchant.onboarding.dto.ResponseErrorMessage;
import id.co.oob.lib.common.merchant.onboarding.dto.mandiri.CustomerAccountInquiryByCifDto;
import id.co.oob.lib.common.merchant.onboarding.dto.mandiri.ValidateOtpDto;
import id.co.oob.lib.common.merchant.onboarding.dto.otp.OtpGenerateTokenResponse;
import id.co.oob.lib.common.merchant.onboarding.dto.otp.OtpHandlerTableDto;
import id.co.oob.lib.common.merchant.onboarding.dto.otp.OtpNotificationRequest;
import id.co.oob.lib.common.merchant.onboarding.dto.otp.OtpNotificationResponse;
import id.co.oob.lib.common.merchant.onboarding.dto.otp.SendOtpRequest;
import id.co.oob.lib.common.merchant.onboarding.dto.otp.ValidasiOtpRegistrasi;
import id.co.oob.lib.common.merchant.onboarding.dto.otp.ValidasiOtpResponse;
import id.co.oob.lib.common.merchant.onboarding.dto.party.SearchPersonResponse;
import id.co.oob.lib.common.merchant.onboarding.dto.success.SuccessValidation;
import id.co.oob.lib.common.merchant.onboarding.dto.user.CifDecryptor;
import id.co.oob.lib.common.merchant.onboarding.encryptor.EncryptDataFiller;
import id.co.oob.lib.common.merchant.onboarding.encryptor.MaskingNumber;
import id.co.oob.lib.common.merchant.onboarding.throwable.AnauthorizedException;
import id.co.oob.lib.common.merchant.onboarding.throwable.InternalServerErrorException;
import id.co.oob.lib.common.merchant.onboarding.throwable.NotAcceptanceDataException;
import id.co.oob.merchant.onboarding.controller.BaseCtl;

@RestController
@RequestMapping("/otpCtl")
public class OtpCtl extends BaseCtl{
	
	@Autowired
	private OtpBlockHandlerSvc otpBlockHandlerSvc;
	
	@Autowired
	private OtpHandlerTableSvc otpHandlerTableSvc;
		
	
	//otp-mandiri-engine
	@PostMapping("/pleaseSmsMe")
	public ResponseEntity<Object> pleaseSmsMe(@RequestBody Map<String, String> map){
		//kirimSms(map.get("userPhoneNumber"), map.get("message"));
		return new ResponseEntity<Object>(responseKirimSms(map.get("userPhoneNumber"), map.get("message")), HttpStatus.OK);
	}
	
	@PostMapping("/sendOtpNotification")
	public ResponseEntity<Object> sendOtpNotification(
			@RequestHeader(value = SECRET_ID, required = true) String secretId,
			@RequestHeader(value = SECRET_KEY, required = true) String secretKey,
			@RequestBody OtpNotificationRequest request){

		return new ResponseEntity<Object>(new SuccessValidation("Otp Send Success",getSmsOtp(request)), HttpStatus.OK);

	}
	
	@PostMapping("/sendMyOTp/forRegistration")
	public ResponseEntity<Object> sendMyOtp(
			@RequestHeader(value = SECRET_ID, required = true) String secretId,
			@RequestHeader(value = SECRET_KEY, required = true) String secretKey,
			@RequestBody SendOtpRequest cifInEncrypt){
		AuthorizedResponseFirst(secretKey, secretId);
		CifDecryptor cifDecryptor = cifDecryptor(cifInEncrypt.getCifInEncrypt(), secretKey,PURPOSE_TO_REGIST);
		CustomerAccountInquiryByCifDto personResponse = getCustomerAccountInquiryByCifDto(cifDecryptor.getCifNumber());
		String phoneNumber =getPhoneNumberFromCustomerAccountInquiry(personResponse);
		System.out.println("phone number : " + phoneNumber);
		ResponseEntity<Object> response = otpBlockHandlerSvc.validateBlockOtp(phoneNumber);
		if(response!=null) {
			return response;
		}
		
		
		OtpHandlerTableDto otpHandlerTableDto = otpHandlerTableSvc.saveOtpHandler
				(cifDecryptor.getCifNumber(), secretKey, MENU_REGISTRASI, "OTP FROM MANDIRI");
	
		Map<String, Object> mapBody = new HashMap<String, Object>();
		mapBody.put("externalId", otpHandlerTableDto.getId()+"");
		mapBody.put("timestamp", DateConverter.convertDateToString(otpHandlerTableDto.getCreateDate(),
				"yyyy-MM-dd HH:mm:ss"));
		mapBody.put("channelId", "C002");
		mapBody.put("authenticationMethod", "01");
		mapBody.put("otpKey1", otpHandlerTableDto.getId()+"");
		mapBody.put("handphone",  phoneNumber);
		mapBody.put("otpReason", "First-binding OTP");
		mapBody.put("narration", "<[Registrasi Merchant QRIS] OTP bersifat RAHASIA. OTP Anda: {0}. Jangan berikan OTP pada siapapun termasuk pihak Bank Mandiri. Kode berlaku 1 menit.");
		
		ResponseEntity<String> mandiriSvcResponse = callMandiriCtl(mapBody, "/openapi/transactions/v1.0/generateOTP");
		
		OtpGenerateTokenResponse otpGenerateTokenResponse= new OtpGenerateTokenResponse();

		try {
			otpGenerateTokenResponse = mapperJsonToSingleDto(mandiriSvcResponse.getBody(), OtpGenerateTokenResponse.class);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new NotAcceptanceDataException("Tidak dapat memetakan mandiriSvcResponse ke otpGenerateTokenResponse");
		}
		
		saveYourLogsHere(mapBody, "/openapi/transactions/v1.0/generateOTP",otpGenerateTokenResponse);
		
		otpGenerateTokenResponse.setResponseMessage("Kode verifikasi 6 digit telah dikirimkan ke nomor "+MaskingNumber.maskingPhoneNumber(phoneNumber));
		otpGenerateTokenResponse.setPhoneNumber(MaskingNumber.maskingPhoneNumber(phoneNumber));
		otpGenerateTokenResponse.setEncrpyptValidationOtp(encryptTheCif(cifDecryptor.getCifNumber(),secretKey,PURPOSE_TO_OTP));
		otpGenerateTokenResponse.setOtpStatusError(new Boolean(false));
		
		
		
		return new ResponseEntity<Object>(new SuccessValidation(otpGenerateTokenResponse.getResponseMessage(), otpGenerateTokenResponse), HttpStatus.OK);
	}
	
	@PutMapping("/validateMyOtp/forRegistration")
	public ResponseEntity<Object> validateMyOtp(
			@RequestHeader(value = SECRET_ID, required = true) String secretId,
			@RequestHeader(value = SECRET_KEY, required = true) String secretKey,
			@RequestBody ValidasiOtpRegistrasi validasiOtpRegistrasi){
		AuthorizedResponseFirst(secretKey, secretId);
		CifDecryptor cifDecryptor = cifDecryptor(validasiOtpRegistrasi.getEncrpyptValidationOtp(),
												 secretKey,PURPOSE_TO_OTP);
		CustomerAccountInquiryByCifDto personResponse = getCustomerAccountInquiryByCifDto(cifDecryptor.getCifNumber());
		String phoneNumber =getPhoneNumberFromCustomerAccountInquiry(personResponse);
	
		OtpHandlerTable otpHandlerTable =  otpHandlerTableSvc.getValueOtpHandlerForOtpOnly
				(cifDecryptor.getCifNumber(), MENU_REGISTRASI, secretKey);
		
		Map<String, Object> mapBody = new HashMap<String, Object>();
		mapBody.put("externalId", otpHandlerTable.getId()+"");
		mapBody.put("timestamp", DateConverter.convertDateToString(otpHandlerTable.getCreateDate(),
				"yyyy-MM-dd HH:mm:ss"));
		mapBody.put("channelId", "C002");
		mapBody.put("authenticationMethod", "01");
		mapBody.put("otpKey1", otpHandlerTable.getId()+"");
		mapBody.put("handphone", phoneNumber);
		mapBody.put("otpReason", "First-binding OTP");
		mapBody.put("otpCode", validasiOtpRegistrasi.getOtpValue());
		
        ResponseEntity<String> mandiriSvcResponse = callMandiriCtl(mapBody, "/openapi/transactions/v1.0/validateOTP");
		
		ValidateOtpDto otpGenerateTokenResponse= new ValidateOtpDto();

		try {
			otpGenerateTokenResponse = mapperJsonToSingleDto(mandiriSvcResponse.getBody(), ValidateOtpDto.class);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new NotAcceptanceDataException("Tidak dapat memetakan mandiriSvcResponse ke otpGenerateTokenResponse");
		}
		saveYourLogsHere(mapBody, "/openapi/transactions/v1.0/validateOTP",otpGenerateTokenResponse);
		
		
		ResponseErrorMessage message = otpHandlerTableSvc.isYourOtpValid(cifDecryptor.getCifNumber(), secretKey, MENU_REGISTRASI,
				validasiOtpRegistrasi.getOtpValue(),phoneNumber,otpGenerateTokenResponse.getResponseCode());
		
		if(!message.getStatus()) {
			throw new AnauthorizedException(message.getMessage()); 
		}
		
		ValidasiOtpResponse validasiOtpResponse = new ValidasiOtpResponse();
		return new ResponseEntity<Object>(new SuccessValidation("Otp Berhasil", validasiOtpResponse),HttpStatus.OK);
	}
	
	@DeleteMapping("/deleteAllBlockOtp")
	public ResponseEntity<Object> deleteAllBlockOtp(
			@RequestHeader(value = SECRET_ID, required = true) String secretId,
			@RequestHeader(value = SECRET_KEY, required = true) String secretKey){
		System.out.println("======================= Trying to deleteAllBlockOtp  all user : start On : " + new Date() + " ========================================= ");
		
		
		otpBlockHandlerSvc.deleteAllOtpRecounter();
		System.out.println("======================= Trying to deleteAllBlockOtp  all user : end On : " + new Date() + " ========================================= ");
		
		
		return new ResponseEntity<Object>(new SuccessValidation("Berhasil delete otp block", ""),HttpStatus.OK);
	}
	
	@Scheduled(cron = "0 0 1 * * ?")
	public void deleteAllBlockOtpNew(){
		System.out.println("======================= Trying to deleteAllBlockOtp  all user : start On : " + new Date() + " ========================================= ");
		
		
		otpBlockHandlerSvc.deleteAllOtpRecounter();
		System.out.println("======================= Trying to deleteAllBlockOtp  all user : end On : " + new Date() + " ========================================= ");
	}
	
	private CifDecryptor cifDecryptor(String cifInEncrypt, String secretKey, String purposeTo) {
		System.out.println("st");
		CifDecryptor cifDecryptor = new CifDecryptor();
		String decrypt = EncryptDataFiller.decrypt(cifInEncrypt, secretKey);
		System.out.println("result decrypt : " + decrypt);
		if(decrypt==null) {
			throw new AnauthorizedException("Cif Encryp "+cifInEncrypt+" "
					+ " Cannot Be Encrypt With Validate Your Secret Key "+secretKey);
		}		
		try {
			cifDecryptor = mapperJsonToSingleDto(decrypt, CifDecryptor.class);
		} catch (Exception e) {
			e.printStackTrace();
			throw new InternalServerErrorException("Cannot Mapping Decrypt to CifDecryptor.class");
		}
		
		if(!cifDecryptor.getPurposeTo().equals(purposeTo)) {
			throw new AnauthorizedException("cifEncrypt is not valid for purpose type : " + purposeTo);
		}
		
		return cifDecryptor;
		
	}
	
//	@PostMapping("/openapi/transactions/v1.0/generateOTP")
//	public ResponseEntity<Object> generateOTP(@RequestBody OtpGenerateTokenRequest 
//			otpGenerateTokenRequest){
//		
//		OtpGenerateTokenResponse otpGenerateTokenResponse = new OtpGenerateTokenResponse();
//		
//		System.out.println("gson : " + new Gson().
//				toJson(otpGenerateTokenRequest));
//		
//		return new ResponseEntity<Object>(otpGenerateTokenResponse, HttpStatus.OK);
//	}
//	
//
//	@PostMapping("/openapi/transactions/v1.0/validateOTP")
//	public ResponseEntity<Object> validateOTP(@RequestBody OtpGenerateTokenRequest 
//			otpGenerateTokenRequest){
//		
//		OtpGenerateTokenResponse otpGenerateTokenResponse = new OtpGenerateTokenResponse();
//		
//		System.out.println("gson : " + new Gson().
//				toJson(otpGenerateTokenRequest));
//		
//		return new ResponseEntity<Object>(otpGenerateTokenResponse, HttpStatus.OK);
//	}
	
	
	
}
