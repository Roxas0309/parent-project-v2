package id.co.oob.merchant.onboarding.controller.ftpCtl;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Vector;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.supercsv.io.CsvBeanWriter;
import org.supercsv.io.ICsvBeanWriter;
import org.supercsv.prefs.CsvPreference;

import com.google.api.client.repackaged.com.google.common.base.Strings;
import com.google.gson.Gson;
import com.google.protobuf.ByteString.Output;
import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.ChannelSftp.LsEntry;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;

import id.co.oob.db.qris.merchant.onboarding.dao.user.UserOobTableDao;
import id.co.oob.db.qris.merchant.onboarding.repository.user.UserOobTable;
import id.co.oob.db.qris.merchant.onboarding.repository.user.UserOobTableAuthDtl;
import id.co.oob.db.qris.merchant.onboarding.repository.user.UserOobTableFile;
import id.co.oob.db.qris.merchant.onboarding.service.authentication.AllTheAuthenticationCircuitSvc;
import id.co.oob.db.qris.merchant.onboarding.service.user.UserOobLogicSvc;
import id.co.oob.lib.common.merchant.onboarding.convert.DateConverter;
import id.co.oob.lib.common.merchant.onboarding.dto.user.UserOobTableDto;
import id.co.oob.lib.common.merchant.onboarding.dto.user.UserOobTableUploadFileDto;
import id.co.oob.lib.common.merchant.onboarding.email.EmailBody;
import id.co.oob.lib.common.merchant.onboarding.file.SftpConnection;
import id.co.oob.lib.common.merchant.onboarding.throwable.InternalServerErrorException;
import id.co.oob.lib.common.merchant.onboarding.validation.DateValidation;
import id.co.oob.merchant.onboarding.controller.BaseCtl;
import id.co.oob.merchant.onboarding.controller.user.RegistrasiUserOobCtl;

@RestController
@RequestMapping("/putDataUpdayeFtpsCtl")
public class PutDataUpdayeFtpsCtl extends BaseCtl {

	@Autowired
	private RegistrasiUserOobCtl registrasiUserOobCtl;

	
	@Autowired
	private UserOobLogicSvc userOobLoginSvc;

	public String makeDirectory() {
		ChannelSftp channelSftp = SftpConnection.setupJsch();
		String dateNowInString = DateConverter.convertDateToString(new Date(), "yyyyMMdd");
		String pathDate = sftpPathFile + dateNowInString;
		String path = pathDate;
		try {
			channelSftp.cd(pathDate);
		} catch (SftpException e1) {

			try {
				channelSftp.mkdir(path);
			} catch (SftpException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				throw new InternalServerErrorException("Tidak dapat membuat direktori " + path);
			}
		}

		path = path + "/oob";
		try {
			channelSftp.mkdir(path);
		} catch (SftpException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			// throw new InternalServerErrorException("Tidak dapat membuat direktori " +
			// path);
		}

		return path;
	}

	

	@GetMapping("/mkdir")
	public String makeDirectory(HttpServletResponse response) {

		return makeDirectory();
	}

//	@Scheduled(cron = "0 0 22 * * ?")
//	public Boolean tesstScheduler() {
//		
//		System.out.println("======================= Trying tesstScheduler registration all user : start On : " + new Date() + " ========================================= ");
//		System.out.println("======================= Trying tesstScheduler registration all user : end On : " + new Date() + " ========================================= ");
//		return true;
//	}
//	
	// ini untuk scheduler untuk update data ke 2



	@Scheduled(cron = "0 0 21 * * ?")
	@GetMapping("/throw/v2")
	public Boolean throwDataToFtps() {
		System.out.println(
				"======================= Trying to throwDataToFtps for update to start registration all user : start On : "
						+ new Date() + " ========================================= ");

		DateFormat dateFormatter = new SimpleDateFormat("yyyyMMdd");
		String currentDateTime = dateFormatter.format(new Date());

		String headerValue = "users_" + currentDateTime + ".csv";

		String pathDate = sftpPathFile + currentDateTime;
		try {
			InputStream isPd = exportToCsvNew(pathDate);
			SftpConnection.setupJschV2(isPd, SftpConnection.setupJschMkdir(pathDate) + "/" + headerValue);
			List<UserOobTableUploadFileDto> listFile = userOobLoginSvc.getDataForSendToFtp();
			for (UserOobTableUploadFileDto user : listFile) {
				System.out.println("trying to update no id oob : " + user.getId());
				registrasiUserOobCtl.updateTo2(user.getId(), "YRuYy-dSiaK-LLdWA", "4Vbxc-uoGWA-LVYg5");
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new InternalServerErrorException(e.getMessage());
		}
		System.out.println(
				"======================= Trying to throwDataToFtps for update to start registration all user : end On : "
						+ new Date() + " ========================================= ");

		return true;
	}

	@PostMapping("/throw/v3")
	public String throwDataToFtpsV3(@RequestBody String currentDateTime) {
		System.out.println(
				"======================= Trying to throwDataToFtps for update to start registration all user : start On : "
						+ new Date() + " ========================================= ");
	
		String headerValue = "users_" + currentDateTime + ".csv";

		
	    try {
			Date date = new SimpleDateFormat("yyyyMMdd").parse(currentDateTime);
			System.out.println(" tanggal yang dinput : " + date );
			String thisNewDate = new SimpleDateFormat("yyyyMMdd").format(new Date());
			if(date.after(new Date())) 
			{
				return "Tanggal Generate File Tidak Boleh Hari Ini Atau Hari Setelahnya.";
			}
			
			if(thisNewDate.equals(currentDateTime)) {
				return "Tanggal Generate File Tidak Boleh Hari Ini Atau Hari Setelahnya.";
			}
			
		} catch (ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			return "Format Tanggal Yang Di terima Adalah YYYYMMDD ";
 		}
		
		
		Boolean isLanjut = true;
		String pathDate = sftpPathFile + currentDateTime;
//		try {
//			@SuppressWarnings("unused")
//			InputStream is = channelSftp.get(pathDate);
//			isLanjut = false;
//		} catch (SftpException e) {
//			System.out.println("/throw/v3 tidak menemukan file " + pathDate + " maka lanjut dapat membuat datanya. ");
//		}
		
		

		if (isLanjut) {
			try {
				InputStream isPd = exportToCsvNew(pathDate);
				System.out.println("Saatnya memasukkan directory untuk isPd yoooo.....");
//				ChannelSftp channelSftp = SftpConnection.setupJsch();
//				channelSftp.put(isPd, makeDirectory(pathDate) + "/" + headerValue);
//				channelSftp.exit();
				
				SftpConnection.setupJschV2(isPd, SftpConnection.setupJschMkdir(pathDate) + "/" + headerValue);
				
				List<UserOobTableUploadFileDto> listFile = userOobLoginSvc.getDataForSendToFtp();
				for (UserOobTableUploadFileDto user : listFile) {
					System.out.println("trying to update no id oob : " + user.getId());
					registrasiUserOobCtl.updateTo2(user.getId(), "YRuYy-dSiaK-LLdWA", "4Vbxc-uoGWA-LVYg5");
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				throw new InternalServerErrorException(e.getMessage());
			}

			System.out.println(
					"======================= Trying to throwDataToFtps for update to start registration all user : end On : "
							+ new Date() + " ========================================= ");
			
			return "Throw data with on folder  " + currentDateTime + "  : end On : " + new Date();
		}

		else {
			return "File dengan path " + pathDate + " sudah ada. Tidak dapat generate file dengan nama tersebut ";
		}

	}
	
//	@PostMapping("/throw/v3/fromLocal")
//	public String throwDataToFtpsV3FromLocal(@RequestBody String currentDateTime) {
//		System.out.println(
//				"======================= Trying to throwDataToFtps for update to start registration all user : start On : "
//						+ new Date() + " ========================================= ");
//		//ChannelSftp channelSftp = SftpConnection.setupJsch();
//		String headerValue = "users_" + currentDateTime + ".csv";
//
//		
//	    try {
//			Date date = new SimpleDateFormat("yyyyMMdd").parse(currentDateTime);
//			System.out.println(" tanggal yang dinput : " + date );
//			String thisNewDate = new SimpleDateFormat("yyyyMMdd").format(new Date());
//			if(date.after(new Date())) 
//			{
//				return "Tanggal Generate File Tidak Boleh Hari Ini Atau Hari Setelahnya.";
//			}
//			
//			if(thisNewDate.equals(currentDateTime)) {
//				return "Tanggal Generate File Tidak Boleh Hari Ini Atau Hari Setelahnya.";
//			}
//			
//		} catch (ParseException e1) {
//			// TODO Auto-generated catch block
//			e1.printStackTrace();
//			return "Format Tanggal Yang Di terima Adalah YYYYMMDD ";
// 		}
//		
//		
//		Boolean isLanjut = true;
//		String pathDate = "Folder_Local" + currentDateTime;
//		try {
//			exportToCsvNewLocal(pathDate);
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		return "Coba Cek Datanya";
////		try {
////			@SuppressWarnings("unused")
////			InputStream is = channelSftp.get(pathDate);
////			isLanjut = false;
////		} catch (SftpException e) {
////			System.out.println("/throw/v3 tidak menemukan file " + pathDate + " maka lanjut dapat membuat datanya. ");
////		}
//		
//		
//
////		if (isLanjut) {
////			try {
////				InputStream isPd = exportToCsvNew(pathDate);
////				channelSftp.put(isPd, makeDirectory(pathDate) + "/" + headerValue);
////				List<UserOobTableUploadFileDto> listFile = userOobLoginSvc.getDataForSendToFtp();
////				for (UserOobTableUploadFileDto user : listFile) {
////					System.out.println("trying to update no id oob : " + user.getId());
////					registrasiUserOobCtl.updateTo2(user.getId(), "YRuYy-dSiaK-LLdWA", "4Vbxc-uoGWA-LVYg5");
////				}
////			} catch (SftpException | IOException e) {
////				// TODO Auto-generated catch block
////				e.printStackTrace();
////				throw new InternalServerErrorException(e.getMessage());
////			}
////
////			System.out.println(
////					"======================= Trying to throwDataToFtps for update to start registration all user : end On : "
////							+ new Date() + " ========================================= ");
////			
////			return "Throw data with on folder  " + currentDateTime + "  : end On : " + new Date();
////		}
////
////		else {
////			return "File dengan path " + pathDate + " sudah ada. Tidak dapat generate file dengan nama tersebut ";
////		}
//
//	}

	public void throwDataValue(InputStream is, String fileName, String pathDate) {
		SftpConnection.setupJschV2(is, SftpConnection.setupJschMkdir(pathDate) + "/" + fileName);
	}
	
	public InputStream exportToCsvNewLocal(String pathDate) throws IOException {

		List<UserOobTableUploadFileDto> listFile = userOobLoginSvc.getDataForSendToFtp();
		CsvPreference PIPE_DELIMITED = new CsvPreference.Builder('"', '|', "\n").build();
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		OutputStreamWriter writer = new OutputStreamWriter(stream);
		ICsvBeanWriter csvWriter = new CsvBeanWriter(writer, PIPE_DELIMITED);

		String[] csvHeader = { "ID", "Nama Pemilik Usaha", "tanggal lahir", "Nomor Ktp", "Nama Gadis Ibu Kandung",
				"Nomor Rekening", "Nomor Handphone", "Alamat KTP", "kode pos", "Email", "Url Foto KTP",
				"Url Foto KTP Wajah", "Url Foto NPWP", "Nomor NPWP", "Nama Usaha", "Nomor Telepon", "Jenis Usaha",
				"Omzet per Bulan", "Url Tempat Usaha", "Url Foto Barang atau Jasa", "Url Foto Pemilik di Tempat Usaha",
				"Lokasi Usaha", "Alamat Lokasi Usaha", "Kode pos alamat merchant", "Kategori Usaha" };

		String[] nameMapping = { "id", "namaPemilikUsaha", "dob", "nomorKtp", "namaIbuKandung", "nomorRekening",
				"noHandphone", "alamatPemilikUsaha", "kodePosAlamatPemilikUsaha", "emailPemilikUsaha", "pathKtp",
				"pathWajahKtp", "pathNpwp", "noNpwp", "namaUsaha", "nomorTelp", "jenisUsaha", "omzet",
				"pathFotoTempatUsaha", "pathFotoBarangAtauJasa", "pathFotoPemilikTempatUsaha", "jenisLokasiUsaha",
				"alamatLokasiUsahaSaatIni", "kodePosAlamatMerchant", "kategoriLokasiUsaha" };

		csvWriter.writeHeader(csvHeader);
		System.err.println("Siap untuk mengirimkan csv writer dengan data : " + new Gson().toJson(listFile));
		return null;
//		for (UserOobTableUploadFileDto user : listFile) {
//			InputStream isKtp = getInputStreamAliOss(user.getCompletePathKtp());
//			System.err.println("sedang input isKtp");
//			throwDataValue(isKtp, user.getPathKtp(), pathDate);
//			if (!Strings.isNullOrEmpty(user.getCompletePathNpwp())) {
//				InputStream isNpwp = getInputStreamAliOss(user.getCompletePathNpwp());
//				System.err.println("sedang input isNpwp");
//				throwDataValue(isNpwp, user.getPathNpwp(), pathDate);
//			}
//			InputStream isWajahKtp = getInputStreamAliOss(user.getCompletePathWajahKtp());
//			System.err.println("sedang input isWajahKtp");
//			throwDataValue(isWajahKtp, user.getPathWajahKtp(), pathDate);
//			InputStream isBarangJasa = getInputStreamAliOss(user.getCompletePathFotoBarangAtauJasa());
//			System.err.println("sedang input isBarangJasa");
//			throwDataValue(isBarangJasa, user.getPathFotoBarangAtauJasa(), pathDate);
//			InputStream isPemilikTempatUsaha = getInputStreamAliOss(user.getCompletePathFotoPemilikTempatUsaha());
//			System.err.println("sedang input isPemilikTempatUsaha");
//			throwDataValue(isPemilikTempatUsaha, user.getPathFotoPemilikTempatUsaha(), pathDate);
//			InputStream isFotoTempatUSaha = getInputStreamAliOss(user.getCompletePathFotoTempatUsaha());
//			System.err.println("sedang input isFotoTempatUSaha");
//			throwDataValue(isFotoTempatUSaha, user.getPathFotoTempatUsaha(), pathDate);
//			csvWriter.write(user, nameMapping);
//
//		}
//
//		csvWriter.close();
//		System.err.println("sedang input file " + pathDate + ".csv");
//		return new ByteArrayInputStream(stream.toByteArray());
	}

	public InputStream exportToCsvNew(String pathDate) throws IOException {

		List<UserOobTableUploadFileDto> listFile = userOobLoginSvc.getDataForSendToFtp();
		CsvPreference PIPE_DELIMITED = new CsvPreference.Builder('"', '|', "\n").build();
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		OutputStreamWriter writer = new OutputStreamWriter(stream);
		ICsvBeanWriter csvWriter = new CsvBeanWriter(writer, PIPE_DELIMITED);

		String[] csvHeader = { "ID", "Nama Pemilik Usaha", "tanggal lahir", "Nomor Ktp", "Nama Gadis Ibu Kandung",
				"Nomor Rekening", "Nomor Handphone", "Alamat KTP", "kode pos", "Email", "Url Foto KTP",
				"Url Foto KTP Wajah", "Url Foto NPWP", "Nomor NPWP", "Nama Usaha", "Nomor Telepon", "Jenis Usaha",
				"Omzet per Bulan", "Url Tempat Usaha", "Url Foto Barang atau Jasa", "Url Foto Pemilik di Tempat Usaha",
				"Lokasi Usaha", "Alamat Lokasi Usaha", "Kode pos alamat merchant", "Kategori Usaha" };

		String[] nameMapping = { "id", "namaPemilikUsaha", "dob", "nomorKtp", "namaIbuKandung", "nomorRekening",
				"noHandphone", "alamatPemilikUsaha", "kodePosAlamatPemilikUsaha", "emailPemilikUsaha", "pathKtp",
				"pathWajahKtp", "pathNpwp", "noNpwp", "namaUsaha", "nomorTelp", "jenisUsaha", "omzet",
				"pathFotoTempatUsaha", "pathFotoBarangAtauJasa", "pathFotoPemilikTempatUsaha", "jenisLokasiUsaha",
				"alamatLokasiUsahaSaatIni", "kodePosAlamatMerchant", "kategoriLokasiUsaha" };

		csvWriter.writeHeader(csvHeader);
		System.err.println("Siap untuk mengirimkan csv writer dengan data : " + new Gson().toJson(listFile));

		for (UserOobTableUploadFileDto user : listFile) {
			InputStream isKtp = getInputStreamAliOss(user.getCompletePathKtp());
			System.err.println("sedang input isKtp");
			throwDataValue(isKtp, user.getPathKtp(), pathDate);
			if (!Strings.isNullOrEmpty(user.getCompletePathNpwp())) {
				InputStream isNpwp = getInputStreamAliOss(user.getCompletePathNpwp());
				System.err.println("sedang input isNpwp");
				throwDataValue(isNpwp, user.getPathNpwp(), pathDate);
			}
			InputStream isWajahKtp = getInputStreamAliOss(user.getCompletePathWajahKtp());
			System.err.println("sedang input isWajahKtp");
			throwDataValue(isWajahKtp, user.getPathWajahKtp(), pathDate);
			InputStream isBarangJasa = getInputStreamAliOss(user.getCompletePathFotoBarangAtauJasa());
			System.err.println("sedang input isBarangJasa");
			throwDataValue(isBarangJasa, user.getPathFotoBarangAtauJasa(), pathDate);
			InputStream isPemilikTempatUsaha = getInputStreamAliOss(user.getCompletePathFotoPemilikTempatUsaha());
			System.err.println("sedang input isPemilikTempatUsaha");
			throwDataValue(isPemilikTempatUsaha, user.getPathFotoPemilikTempatUsaha(), pathDate);
			InputStream isFotoTempatUSaha = getInputStreamAliOss(user.getCompletePathFotoTempatUsaha());
			System.err.println("sedang input isFotoTempatUSaha");
			throwDataValue(isFotoTempatUSaha, user.getPathFotoTempatUsaha(), pathDate);
			csvWriter.write(user, nameMapping);

		}

		csvWriter.close();
		System.err.println("sedang input file " + pathDate + ".csv");
		return new ByteArrayInputStream(stream.toByteArray());
	}

	@GetMapping("/users/export")
	public void exportToCsv(HttpServletResponse response) throws IOException {

		response.setContentType("text/csv");
		DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");
		String currentDateTime = dateFormatter.format(new Date());

		String headerKey = "Content-Disposition";
		String headerValue = "attachment; filename=users_" + currentDateTime + ".csv";
		response.setHeader(headerKey, headerValue);

		List<UserOobTableUploadFileDto> listFile = userOobLoginSvc.getDataForSendToFtp();
		CsvPreference PIPE_DELIMITED = new CsvPreference.Builder('"', '|', "\n").build();
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		OutputStreamWriter writer = new OutputStreamWriter(stream);
		ICsvBeanWriter csvWriter = new CsvBeanWriter(response.getWriter(), PIPE_DELIMITED);

		String[] csvHeader = { "ID", "Nama Pemilik Usaha", "tanggal lahir", "Nomor Ktp", "Nama Gadis Ibu Kandung",
				"Nomor Rekening", "Nomor Handphone", "Alamat KTP", "kode pos", "Email", "Url Foto KTP",
				"Url Foto KTP Wajah", "Url Foto NPWP", "Nomor NPWP", "Nama Usaha", "Nomor Telepon", "Jenis Usaha",
				"Omzet per Bulan", "Url Tempat Usaha", "Url Foto Barang atau Jasa", "Url Foto Pemilik di Tempat Usaha",
				"Lokasi Usaha", "Alamat Lokasi Usaha", "kode pos alamat merchant", "Kategori Usaha" };

		String[] nameMapping = { "id", "namaPemilikUsaha", "dob", "nomorKtp", "namaIbuKandung", "nomorRekening",
				"noHandphone", "alamatPemilikUsaha", "kodePosAlamatPemilikUsaha", "emailPemilikUsaha", "pathKtp",
				"pathWajahKtp", "pathNpwp", "noNpwp", "namaUsaha", "nomorTelp", "jenisUsaha", "omzet",
				"pathFotoTempatUsaha", "pathFotoBarangAtauJasa", "pathFotoPemilikTempatUsaha", "jenisLokasiUsaha",
				"alamatLokasiUsahaSaatIni", "kodePosAlamatMerchant", "kategoriLokasiUsaha" };

		csvWriter.writeHeader(csvHeader);
		System.err.println("Siap untuk mengirimkan csv writer dengan data : " + new Gson().toJson(listFile));
		for (UserOobTableUploadFileDto user : listFile) {
			csvWriter.write(user, nameMapping);
		}
		// InputStream is = csvWriter.
		csvWriter.close();
	}

}
