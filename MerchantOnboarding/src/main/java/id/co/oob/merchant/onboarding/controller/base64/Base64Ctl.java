package id.co.oob.merchant.onboarding.controller.base64;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.plugins.jpeg.JPEGImageWriteParam;
import javax.imageio.stream.ImageOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.tomcat.jni.File;
import org.imgscalr.Scalr;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.CacheControl;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.api.client.repackaged.com.google.common.base.Strings;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.BinaryBitmap;
import com.google.zxing.LuminanceSource;
import com.google.zxing.MultiFormatReader;
import com.google.zxing.NotFoundException;
import com.google.zxing.Result;
import com.google.zxing.WriterException;
import com.sun.jersey.core.util.Base64;

import id.co.oob.db.qris.merchant.onboarding.dao.user.UserOobMidDtlTableDao;
import id.co.oob.db.qris.merchant.onboarding.repository.user.AllUserOobDtlRepo;
import id.co.oob.db.qris.merchant.onboarding.repository.user.UserOobMidDtlTable;
import id.co.oob.lib.common.merchant.onboarding.dto.success.SuccessValidation;
import id.co.oob.lib.common.merchant.onboarding.file.Base64FileDto;
import id.co.oob.lib.common.merchant.onboarding.throwable.AnauthorizedException;
import id.co.oob.merchant.onboarding.controller.BaseCtl;

import com.google.zxing.client.j2se.BufferedImageLuminanceSource;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.common.HybridBinarizer;
import com.google.zxing.qrcode.QRCodeWriter;

@RestController
@RequestMapping("/base64")
public class Base64Ctl extends BaseCtl {
	@Autowired
	private UserOobMidDtlTableDao userOobMidDtlTableDao;

	@PostMapping(value = "/beforeCompress/TryingTheBestForCompress", produces = MediaType.IMAGE_JPEG_VALUE, headers = "Accept=image/jpeg, image/jpg, image/png, image/gif")
	public byte[] tryingTheBestForCompress(HttpServletResponse httpServletResponse, @RequestBody String url)
			throws IOException {
		InputStream isKtp = getInputStreamAliOss(url);
		BufferedImage bi = ImageIO.read(isKtp);
	
////	    MarvinImage image = new MarvinImage(bi);
////	    Scale scale = new Scale();
////	    scale.load();
////	    scale.setAttribute("newWidth", 500);
////	    scale.setAttribute("newHeight", 650);
////	    scale.process(image.clone(), image, null, null, false);
//	    bi = Scalr.resize(bi, Scalr.Method.AUTOMATIC, Scalr.Mode.AUTOMATIC, 500, 650, Scalr.OP_ANTIALIAS);
		ByteArrayOutputStream bao = new ByteArrayOutputStream();
////	    Crop crop = new Crop();
		ImageIO.write(bi, "png", bao);

		return bao.toByteArray();
	}

	@PostMapping(value = "/afterCompress/TryingTheBestForCompress", produces = MediaType.IMAGE_JPEG_VALUE, headers = "Accept=image/jpeg, image/jpg, image/png, image/gif")
	public byte[] tryingTheBestForCompressAfterCompress(HttpServletResponse httpServletResponse,
			@RequestHeader("spot") Double spot, @RequestBody String url) throws IOException {
		InputStream isKtp = getInputStreamAliOss(url);
		BufferedImage image = ImageIO.read(isKtp);

		image = Scalr.resize(image, Scalr.Method.SPEED, Scalr.Mode.FIT_EXACT, (int) (image.getWidth() * (spot)),
				(int) (image.getHeight() * (spot)), Scalr.OP_ANTIALIAS);
		image.getScaledInstance((int) (image.getWidth() * (0.05)), (int) (image.getHeight() * (0.05)),
				Image.SCALE_FAST);

		ByteArrayOutputStream bao = new ByteArrayOutputStream();
		ImageIO.write(image, "png", bao);
		return bao.toByteArray();

	}

	@PostMapping(value = "/afterCompress/saveCompress", produces = MediaType.IMAGE_JPEG_VALUE, headers = "Accept=image/jpeg, image/jpg, image/png, image/gif")
	public byte[] saveCompress(HttpServletResponse httpServletResponse, @RequestHeader("spot") Double spot,
			@RequestBody String url) throws IOException {
		System.out.println("put object with : access key : " + springCloudAlicloudAccessKey + ", " + "secret key : "
				+ springCloudAlicloudSecretKey + ", " + " \n in endPoint : " + springCloudAlicloudOssEndpoint);

		InputStream isKtp = getInputStreamAliOss(url);
		BufferedImage image = ImageIO.read(isKtp);

		image = Scalr.resize(image, Scalr.Method.SPEED, Scalr.Mode.FIT_EXACT, (int) (image.getWidth() * (spot)),
				(int) (image.getHeight() * (spot)), Scalr.OP_ANTIALIAS);
		image.getScaledInstance((int) (image.getWidth() * (0.05)), (int) (image.getHeight() * (0.05)),
				Image.SCALE_FAST);

		ByteArrayOutputStream bao = new ByteArrayOutputStream();
		ImageIO.write(image, "png", bao);

		getFileNewInputStream(new ByteArrayInputStream(bao.toByteArray()),
				"oob-nasabah-registrasi-folder-test/registrasi-menu/10000001310/", "090320211327488;FOTO_KTP_NEW");

		return bao.toByteArray();

	}

	@PostMapping("/TestYourBase64")
	public void testYourBase64(@RequestBody String base64, HttpServletResponse httpServletResponse) {
		try {

			InputStream in = new ByteArrayInputStream(getBase64File(base64));
			HttpHeaders headers = new HttpHeaders();
			headers.setCacheControl(CacheControl.noCache().getHeaderValue());
			httpServletResponse.setContentType(MediaType.IMAGE_JPEG_VALUE);
			IOUtils.copy(in, httpServletResponse.getOutputStream());
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

//	 @GetMapping(value = "/getPictureQr", produces = MediaType.IMAGE_JPEG_VALUE,
//			 headers="Accept=image/jpeg, image/jpg, image/png, image/gif")
//	 public @ResponseBody byte[] getPictureQr(HttpServletResponse httpServletResponse) throws IllegalStateException, IOException {
//
//		 
//		   InputStreamResource resource = new InputStreamResource(getInputStreamAliOss
//		    		("oob-nasabah-qr-code/ID1020049086918/ID1020049086918_A01.png"));
//		    InputStream is = resource.getInputStream();
//		    BufferedImage  bi = ImageIO.read(is);
//		    MarvinImage image = new MarvinImage(bi);
//		    Scale scale = new Scale();
//		    scale.load();
//		    scale.setAttribute("newWidth", 500);
//		    scale.setAttribute("newHeight", 650);
//		    scale.process(image.clone(), image, null, null, false);
//		    ByteArrayOutputStream bao=new ByteArrayOutputStream();
//		    ImageIO.write(image.getBufferedImageNoAlpha(),"png",bao);
//		    
//		    return bao.toByteArray();
//	 }

	// http://localhost:28080/base64/getPictureCrop?x=88&y=210&w=324&h=322

	@GetMapping(value = "/getBase64DataQr")
	public @ResponseBody String getBase64DataQr(@RequestParam("mid") Long id, HttpServletResponse httpServletResponse)
			throws IllegalStateException, IOException {
		UserOobMidDtlTable userOobMidDtlTable = userOobMidDtlTableDao.getOne(id);
		InputStreamResource resource = new InputStreamResource(
				getInputStreamAliOss(userOobMidDtlTable.getPathQrNumber()));
		System.out.println("testing get data base64 qr");
		InputStream is = resource.getInputStream();
		byte[] bytes = IOUtils.toByteArray(is);
		return org.apache.commons.codec.binary.Base64.encodeBase64String(bytes);
	}

	@GetMapping(value = "/getPictureCrop", produces = MediaType.IMAGE_JPEG_VALUE, headers = "Accept=image/jpeg, image/jpg, image/png, image/gif")
	public @ResponseBody byte[] getPictureQrCrop(@RequestParam("x") int x, @RequestParam("y") int y,
			@RequestParam("w") int w, @RequestParam("h") int h, @RequestParam("mid") Long id,
			HttpServletResponse httpServletResponse) throws IllegalStateException, IOException {

		UserOobMidDtlTable userOobMidDtlTable = userOobMidDtlTableDao.getOne(id);
		InputStreamResource resource = new InputStreamResource(
				getInputStreamAliOss(userOobMidDtlTable.getPathQrNumber()));
		InputStream is = resource.getInputStream();
		BufferedImage bi = ImageIO.read(is);
//		    MarvinImage image = new MarvinImage(bi);
//		    Scale scale = new Scale();
//		    scale.load();
//		    scale.setAttribute("newWidth", 500);
//		    scale.setAttribute("newHeight", 650);
//		    scale.process(image.clone(), image, null, null, false);

		LuminanceSource source = new BufferedImageLuminanceSource(bi);
		BinaryBitmap bitmap = new BinaryBitmap(new HybridBinarizer(source));

		String decodeQrCode = null;
		try {
			Result result = new MultiFormatReader().decode(bitmap);
			decodeQrCode = result.getText();
		} catch (NotFoundException e) {
			System.out.println("There is no QR code in the image");
		}

		ByteArrayOutputStream bao = new ByteArrayOutputStream();
		
		if (Strings.isNullOrEmpty(decodeQrCode)) {
			bi = createCustomQr(bi, x, y, w, h);
		}else {
			QRCodeWriter barcodeWriter = new QRCodeWriter();
			try {
				BitMatrix bitMatrix = 
					      barcodeWriter.encode(decodeQrCode, BarcodeFormat.QR_CODE, 700, 700);
				bi = MatrixToImageWriter.toBufferedImage(bitMatrix);
			} catch (WriterException e) {
				bi = createCustomQr(bi, x, y, w, h);
			}
		}
		
		ImageIO.write(bi, "png", bao);

		return bao.toByteArray();
	}
	
	private BufferedImage createCustomQr(BufferedImage bi, int x, int y, int w, int h) {
		if (applicationProfileName.equalsIgnoreCase("prod")) {
			//production
			bi = Scalr.resize(bi, Scalr.Method.AUTOMATIC, Scalr.Mode.AUTOMATIC, 700, 700, Scalr.OP_ANTIALIAS);
			x = x + 12;
			bi = bi.getSubimage(x, y, w, h);
		} else {
		    //development
			bi = Scalr.resize(bi, Scalr.Method.AUTOMATIC, Scalr.Mode.AUTOMATIC, 500, 650, Scalr.OP_ANTIALIAS);
			bi = bi.getSubimage(x, y, w, h);
		}
		return bi;
	}

	@GetMapping("/getYourQrPicture")
	public Map<String, Object> getYourQrPicture(@RequestHeader(value = SECRET_ID, required = true) String secretId,
			@RequestHeader(value = SECRET_KEY, required = true) String secretKey,
			@RequestHeader(value = SECRET_TOKEN, required = true) String secretToken) {
		AllUserOobDtlRepo allUserOobDtlRepo = getAllUserInfoByToken(secretToken, secretKey);
		String yourQrPath = allUserOobDtlRepo.getUserOobMidDtlTableDto().getPathQrNumber();
		String yourUrl = getUrlNameGetValueOss(yourQrPath);
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("yourUrl", yourUrl);
		map.put("yourUrlCrop", "/base64/getPictureCrop?x=79&y=213&w=314&h=312&mid="
				+ allUserOobDtlRepo.getUserOobMidDtlTableDto().getIdOob());
		return map;
	}

//	 @GetMapping("/downloadYourQrPicture")
//	 public ResponseEntity<InputStream> downloadYourQrPicture(
////				@RequestHeader(value = SECRET_ID, required = true) String secretId,
////				@RequestHeader(value = SECRET_KEY, required = true) String secretKey,
////				@RequestHeader(value = SECRET_TOKEN, required = true) String secretToken
//			 ){
//		 HttpHeaders responseHeaders = new HttpHeaders();
//			responseHeaders.setContentType(MediaType.parseMediaType("application/png"));
//			responseHeaders.set("Content-Disposition", "attachment");
//			responseHeaders.setContentDispositionFormData("ID1020049086918_A01.png", "ID1020049086918_A01.png");
//	     return new ResponseEntity<InputStream>
//	        (getInputStreamAliOss(), responseHeaders, HttpStatus.OK);
//	 }

	@GetMapping("/downloadYourQrPicture/{idMid}")
	public ResponseEntity<Resource> downloadYourQrPicture(@PathVariable(value = "idMid", required = true) Long idMid
//				@RequestHeader(value = SECRET_ID, required = true) String secretId,
//				@RequestHeader(value = SECRET_KEY, required = true) String secretKey,
//				@RequestHeader(value = SECRET_TOKEN, required = true) String secretToken
	) {
		UserOobMidDtlTable userOobMidDtlTable = userOobMidDtlTableDao.getOne(idMid);
		InputStreamResource resource = new InputStreamResource(
				getInputStreamAliOss(userOobMidDtlTable.getPathQrNumber()));

		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.setContentType(MediaType.parseMediaType("application/png"));
		responseHeaders.set("Content-Disposition", "attachment");
		responseHeaders.setContentDispositionFormData("Qr_number.png", "Qr_number.png");

		return ResponseEntity.ok().headers(responseHeaders).contentType(MediaType.APPLICATION_OCTET_STREAM)
				.body(resource);
	}

	@PostMapping("/getOssPictureUrl")
	public ResponseEntity<Object> getOssPictureUrl(@RequestBody Map<String, Object> map,
			@RequestHeader(value = SECRET_ID, required = true) String secretId,
			@RequestHeader(value = SECRET_KEY, required = true) String secretKey) {
		AuthorizedResponseFirst(secretKey, secretId);
		String pathName = (String) map.get("pathName");
		return new ResponseEntity<Object>(new SuccessValidation("Url didapatkan", getUrlNameGetValueOss(pathName)),
				HttpStatus.OK);
	}

	@PostMapping("/saveYourPicture/{tagPassword}")
	public ResponseEntity<Object> saveYourPicture(@PathVariable("tagPassword") String tagPassword,
			@RequestParam("testFolder") String testFolder, @RequestBody String base64) {
		if (!tagPassword.equalsIgnoreCase("Roxas0309.")) {
			throw new AnauthorizedException("Your Tag Password Wrong");
		}
		String getFileResponseAllyun = getFile(new Base64FileDto(base64, "TestingFile.png"), testFolder);
		return new ResponseEntity<Object>(getFileResponseAllyun, HttpStatus.OK);
	}

//	 @PostMapping("/TestYourBase64")
//	 public ResponseEntity<InputStreamResource> getFile(
//	    		@RequestBody String base64, HttpServletResponse httpServletResponse) {
//		 InputStream in = new ByteArrayInputStream(getBase64File(base64));
//		 HttpHeaders headers = new HttpHeaders();
//		 InputStreamResource inputStreamResource = new InputStreamResource(in);
//		 headers.setCacheControl(CacheControl.noCache().getHeaderValue());
//		 return new ResponseEntity<InputStreamResource>(inputStreamResource, headers, HttpStatus.OK);
//	}

}
