package id.co.oob.lib.common.merchant.onboarding.dto.user;

public class StatusPendaftaranBodyRequest {
	private String qNoPendaftaran;
	private String qNoHp;
	public String getqNoPendaftaran() {
		return qNoPendaftaran;
	}
	public void setqNoPendaftaran(String qNoPendaftaran) {
		this.qNoPendaftaran = qNoPendaftaran;
	}
	public String getqNoHp() {
		return qNoHp;
	}
	public void setqNoHp(String qNoHp) {
		this.qNoHp = qNoHp;
	}
	
}
