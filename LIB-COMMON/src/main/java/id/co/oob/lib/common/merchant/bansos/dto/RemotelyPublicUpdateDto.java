package id.co.oob.lib.common.merchant.bansos.dto;

public class RemotelyPublicUpdateDto {

	private String versionBefore;
	private String versionAfter;
	private Boolean isNeedUpdate;
	private String operatingSystem;
	private String linkUpdate;
	
	public String getOperatingSystem() {
		return operatingSystem;
	}
	public void setOperatingSystem(String operatingSystem) {
		this.operatingSystem = operatingSystem;
	}
	public String getLinkUpdate() {
		return linkUpdate;
	}
	public void setLinkUpdate(String linkUpdate) {
		this.linkUpdate = linkUpdate;
	}
	public String getVersionBefore() {
		return versionBefore;
	}
	public void setVersionBefore(String versionBefore) {
		this.versionBefore = versionBefore;
	}
	public String getVersionAfter() {
		return versionAfter;
	}
	public void setVersionAfter(String versionAfter) {
		this.versionAfter = versionAfter;
	}
	public Boolean getIsNeedUpdate() {
		return isNeedUpdate;
	}
	public void setIsNeedUpdate(Boolean isNeedUpdate) {
		this.isNeedUpdate = isNeedUpdate;
	}
	
}
