package id.co.oob.lib.common.merchant.onboarding.dto.success;

import java.util.Date;

import org.springframework.http.HttpStatus;

public class BaseSuccess {

	public final Date timestamp = new Date();
	public final Integer status = HttpStatus.OK.value();
	public Date getTimestamp() {
		return timestamp;
	}
	public Integer getStatus() {
		return status;
	}
	
	
	
	
	
}
