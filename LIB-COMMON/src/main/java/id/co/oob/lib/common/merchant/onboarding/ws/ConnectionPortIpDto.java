package id.co.oob.lib.common.merchant.onboarding.ws;

public class ConnectionPortIpDto {

	public String ip;
	public Integer port;
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	public Integer getPort() {
		return port;
	}
	public void setPort(Integer port) {
		this.port = port;
	}
	
	
	
}
