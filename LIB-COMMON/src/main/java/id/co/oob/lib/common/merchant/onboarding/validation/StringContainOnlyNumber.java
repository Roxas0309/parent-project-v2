package id.co.oob.lib.common.merchant.onboarding.validation;


public class StringContainOnlyNumber {

	
	public static Boolean isStringContainOnlyNumber(String str) {
		for (int i = 0; i < str.length(); i++) { 
			if(Character.isAlphabetic(str.charAt(i))) {
				return false;
			}
        } 
        return true; 
	}
}
