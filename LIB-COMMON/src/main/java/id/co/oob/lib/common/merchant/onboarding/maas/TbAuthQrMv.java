package id.co.oob.lib.common.merchant.onboarding.maas;

import java.math.BigDecimal;
import java.math.BigInteger;

public class TbAuthQrMv {
	
	private String authSeqNo;
	private String merTransId;
	private String merPan;
	private String custPan;
	private String qrFeat;
	private String qrKndCd;
	private String qrBrndKndCd;
	private String qrKndCdSwitch;
	private String qrMode;
	private String qrRrefNo;
	private String qrString;
	private String qrProcCode;
	private String qrTransDateTime;
	private String mbNo;
	private String midMbNo;
	private String tidMbNo;
	private String authNo;
	private String authRspnClcd;
	private String authRspnCd;
	private String authIntnRspnRsonClcd;
	private String authIntnRspnCd;
	private String issurRspnRsonClcd;
	private String issurRspnCd;
	private String authProcPgrsStatCd;
	private String authDate;
	private String authTime;
	private String authReqChnlCd;
	private String authTrnsTpCd;
	private String authTrnsAmt;
	private String authAmt;
	private String mgsTpId;
	private String rrefNo;
	private String invoiceNo;
	private String localDate;
	private String localTime;
	private String mid;
	private String tid;
	private String trnsCurcyCd;
	private String nationalMid;
	private String asndInstCode;
	private String asndProcCode;
	private String asndTrnsAmount;
	private String asndTrnsDttm;
	private String asndStan;
	private String asndLocalDttm;
	private String asndSettlementDate;
	private String asndCaptureDate;
	private String asndMerchantType;
	private String asndPosEmode;
	private String asndFeeType;
	private BigDecimal asndFeeAmt;
	private BigDecimal	asndTipAmt;
	private String asndAcqId;
	private String asndIssuerId;
	private String asndFwdingId;
	private String asndRrefNo;
	private String asndApprovalCode;
	private String asndTid;
	private String asndMid;
	private String asndMcntName;
	private String asndMcntCity;
	private String asndMcntCountry;
	private String asndProductIndicator;
	private String asndCustomerData;
	private String asndMcntCriteria;
	private String asndCurrencyCode;
	private String asndPostalCode;
	private String asndAdditionalField;
	private String asndInvoiceNumber;
	private String arcvInstCode;
	private String arcvProcCode;
	private BigDecimal arcvTrnsAmount;
	private String arcvTrnsDttm;
	private String arcvStan;
	private String arcvLocalDttm;
	private String arcvSettlementDate;
	private String arcvCaptureDate;
	private String arcvMerchantType;
	private String arcvPosEmode;
	private String arcvFeeType;
	private BigDecimal arcvFeeAmt;
	private BigDecimal arcvTipAmt;
	private String arcvAcqId;
	private String arcvIssuerId;
	private String arcvFwdingId;
	private String arcvRrefNo;
	private String arcvApprovalCode;
	private String arcvTid;
	private String arcvMid;
	private String arcvMcntName;
	private String arcvMcntCity;
	private String arcvMcntCountry;
	private String arcvProductIndicator;
	private String arcvCustomerData;
	private String arcvMcntCriteria;
	private String arcvCurrencyCode;
	private String arcvPostalCode;
	private String arcvAdditionalField;
	private String arcvInvoiceName;
	private String inpUsrId;
	private String inpPgmId;
	private String dataInpDttm;
	private String chngUsrId;
	private String chngPgmId;
	private String dataChngDttm;
	private String mbTid;
	private String mbMid;
	private String sourceFund;
	private String customerName;
	private BigDecimal tipsAmt;
	private BigDecimal feeAmt;
	private String feeType;
	private String acquirerId;
	private String issuerId;
	private String forwardingId;
	private String qrExparation;
	private String qrTrack2;
	private String issuerName;
	private String befAuthSeqNo;
	private String befAuthDate;
	private String authCnclRsonCd;
	private String qrReasonCode;
	private String qrType;
	private String onusFlag;
	public String getAuthSeqNo() {
		return authSeqNo;
	}
	public void setAuthSeqNo(String authSeqNo) {
		this.authSeqNo = authSeqNo;
	}
	public String getMerTransId() {
		return merTransId;
	}
	public void setMerTransId(String merTransId) {
		this.merTransId = merTransId;
	}
	public String getMerPan() {
		return merPan;
	}
	public void setMerPan(String merPan) {
		this.merPan = merPan;
	}
	public String getCustPan() {
		return custPan;
	}
	public void setCustPan(String custPan) {
		this.custPan = custPan;
	}
	public String getQrFeat() {
		return qrFeat;
	}
	public void setQrFeat(String qrFeat) {
		this.qrFeat = qrFeat;
	}
	public String getQrKndCd() {
		return qrKndCd;
	}
	public void setQrKndCd(String qrKndCd) {
		this.qrKndCd = qrKndCd;
	}
	public String getQrBrndKndCd() {
		return qrBrndKndCd;
	}
	public void setQrBrndKndCd(String qrBrndKndCd) {
		this.qrBrndKndCd = qrBrndKndCd;
	}
	public String getQrKndCdSwitch() {
		return qrKndCdSwitch;
	}
	public void setQrKndCdSwitch(String qrKndCdSwitch) {
		this.qrKndCdSwitch = qrKndCdSwitch;
	}
	public String getQrMode() {
		return qrMode;
	}
	public void setQrMode(String qrMode) {
		this.qrMode = qrMode;
	}
	public String getQrRrefNo() {
		return qrRrefNo;
	}
	public void setQrRrefNo(String qrRrefNo) {
		this.qrRrefNo = qrRrefNo;
	}
	public String getQrString() {
		return qrString;
	}
	public void setQrString(String qrString) {
		this.qrString = qrString;
	}
	public String getQrProcCode() {
		return qrProcCode;
	}
	public void setQrProcCode(String qrProcCode) {
		this.qrProcCode = qrProcCode;
	}
	public String getQrTransDateTime() {
		return qrTransDateTime;
	}
	public void setQrTransDateTime(String qrTransDateTime) {
		this.qrTransDateTime = qrTransDateTime;
	}
	public String getMbNo() {
		return mbNo;
	}
	public void setMbNo(String mbNo) {
		this.mbNo = mbNo;
	}
	public String getMidMbNo() {
		return midMbNo;
	}
	public void setMidMbNo(String midMbNo) {
		this.midMbNo = midMbNo;
	}
	public String getTidMbNo() {
		return tidMbNo;
	}
	public void setTidMbNo(String tidMbNo) {
		this.tidMbNo = tidMbNo;
	}
	public String getAuthNo() {
		return authNo;
	}
	public void setAuthNo(String authNo) {
		this.authNo = authNo;
	}
	public String getAuthRspnClcd() {
		return authRspnClcd;
	}
	public void setAuthRspnClcd(String authRspnClcd) {
		this.authRspnClcd = authRspnClcd;
	}
	public String getAuthRspnCd() {
		return authRspnCd;
	}
	public void setAuthRspnCd(String authRspnCd) {
		this.authRspnCd = authRspnCd;
	}
	public String getAuthIntnRspnRsonClcd() {
		return authIntnRspnRsonClcd;
	}
	public void setAuthIntnRspnRsonClcd(String authIntnRspnRsonClcd) {
		this.authIntnRspnRsonClcd = authIntnRspnRsonClcd;
	}
	public String getAuthIntnRspnCd() {
		return authIntnRspnCd;
	}
	public void setAuthIntnRspnCd(String authIntnRspnCd) {
		this.authIntnRspnCd = authIntnRspnCd;
	}
	public String getIssurRspnRsonClcd() {
		return issurRspnRsonClcd;
	}
	public void setIssurRspnRsonClcd(String issurRspnRsonClcd) {
		this.issurRspnRsonClcd = issurRspnRsonClcd;
	}
	public String getIssurRspnCd() {
		return issurRspnCd;
	}
	public void setIssurRspnCd(String issurRspnCd) {
		this.issurRspnCd = issurRspnCd;
	}
	public String getAuthProcPgrsStatCd() {
		return authProcPgrsStatCd;
	}
	public void setAuthProcPgrsStatCd(String authProcPgrsStatCd) {
		this.authProcPgrsStatCd = authProcPgrsStatCd;
	}
	public String getAuthDate() {
		return authDate;
	}
	public void setAuthDate(String authDate) {
		this.authDate = authDate;
	}
	public String getAuthTime() {
		return authTime;
	}
	public void setAuthTime(String authTime) {
		this.authTime = authTime;
	}
	public String getAuthReqChnlCd() {
		return authReqChnlCd;
	}
	public void setAuthReqChnlCd(String authReqChnlCd) {
		this.authReqChnlCd = authReqChnlCd;
	}
	public String getAuthTrnsTpCd() {
		return authTrnsTpCd;
	}
	public void setAuthTrnsTpCd(String authTrnsTpCd) {
		this.authTrnsTpCd = authTrnsTpCd;
	}
	public String getAuthTrnsAmt() {
		return authTrnsAmt;
	}
	public void setAuthTrnsAmt(String authTrnsAmt) {
		this.authTrnsAmt = authTrnsAmt;
	}
	public String getAuthAmt() {
		return authAmt;
	}
	public void setAuthAmt(String authAmt) {
		this.authAmt = authAmt;
	}
	public String getMgsTpId() {
		return mgsTpId;
	}
	public void setMgsTpId(String mgsTpId) {
		this.mgsTpId = mgsTpId;
	}
	public String getRrefNo() {
		return rrefNo;
	}
	public void setRrefNo(String rrefNo) {
		this.rrefNo = rrefNo;
	}
	public String getInvoiceNo() {
		return invoiceNo;
	}
	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}
	public String getLocalDate() {
		return localDate;
	}
	public void setLocalDate(String localDate) {
		this.localDate = localDate;
	}
	public String getLocalTime() {
		return localTime;
	}
	public void setLocalTime(String localTime) {
		this.localTime = localTime;
	}
	public String getMid() {
		return mid;
	}
	public void setMid(String mid) {
		this.mid = mid;
	}
	public String getTid() {
		return tid;
	}
	public void setTid(String tid) {
		this.tid = tid;
	}
	public String getTrnsCurcyCd() {
		return trnsCurcyCd;
	}
	public void setTrnsCurcyCd(String trnsCurcyCd) {
		this.trnsCurcyCd = trnsCurcyCd;
	}
	public String getNationalMid() {
		return nationalMid;
	}
	public void setNationalMid(String nationalMid) {
		this.nationalMid = nationalMid;
	}
	public String getAsndInstCode() {
		return asndInstCode;
	}
	public void setAsndInstCode(String asndInstCode) {
		this.asndInstCode = asndInstCode;
	}
	public String getAsndProcCode() {
		return asndProcCode;
	}
	public void setAsndProcCode(String asndProcCode) {
		this.asndProcCode = asndProcCode;
	}
	public String getAsndTrnsAmount() {
		return asndTrnsAmount;
	}
	public void setAsndTrnsAmount(String asndTrnsAmount) {
		this.asndTrnsAmount = asndTrnsAmount;
	}
	public String getAsndTrnsDttm() {
		return asndTrnsDttm;
	}
	public void setAsndTrnsDttm(String asndTrnsDttm) {
		this.asndTrnsDttm = asndTrnsDttm;
	}
	public String getAsndStan() {
		return asndStan;
	}
	public void setAsndStan(String asndStan) {
		this.asndStan = asndStan;
	}
	public String getAsndLocalDttm() {
		return asndLocalDttm;
	}
	public void setAsndLocalDttm(String asndLocalDttm) {
		this.asndLocalDttm = asndLocalDttm;
	}
	public String getAsndSettlementDate() {
		return asndSettlementDate;
	}
	public void setAsndSettlementDate(String asndSettlementDate) {
		this.asndSettlementDate = asndSettlementDate;
	}
	public String getAsndCaptureDate() {
		return asndCaptureDate;
	}
	public void setAsndCaptureDate(String asndCaptureDate) {
		this.asndCaptureDate = asndCaptureDate;
	}
	public String getAsndMerchantType() {
		return asndMerchantType;
	}
	public void setAsndMerchantType(String asndMerchantType) {
		this.asndMerchantType = asndMerchantType;
	}
	public String getAsndPosEmode() {
		return asndPosEmode;
	}
	public void setAsndPosEmode(String asndPosEmode) {
		this.asndPosEmode = asndPosEmode;
	}
	public String getAsndFeeType() {
		return asndFeeType;
	}
	public void setAsndFeeType(String asndFeeType) {
		this.asndFeeType = asndFeeType;
	}
	public BigDecimal getAsndFeeAmt() {
		return asndFeeAmt;
	}
	public void setAsndFeeAmt(BigDecimal asndFeeAmt) {
		this.asndFeeAmt = asndFeeAmt;
	}
	public BigDecimal getAsndTipAmt() {
		return asndTipAmt;
	}
	public void setAsndTipAmt(BigDecimal asndTipAmt) {
		this.asndTipAmt = asndTipAmt;
	}
	public String getAsndAcqId() {
		return asndAcqId;
	}
	public void setAsndAcqId(String asndAcqId) {
		this.asndAcqId = asndAcqId;
	}
	public String getAsndIssuerId() {
		return asndIssuerId;
	}
	public void setAsndIssuerId(String asndIssuerId) {
		this.asndIssuerId = asndIssuerId;
	}
	public String getAsndFwdingId() {
		return asndFwdingId;
	}
	public void setAsndFwdingId(String asndFwdingId) {
		this.asndFwdingId = asndFwdingId;
	}
	public String getAsndRrefNo() {
		return asndRrefNo;
	}
	public void setAsndRrefNo(String asndRrefNo) {
		this.asndRrefNo = asndRrefNo;
	}
	public String getAsndApprovalCode() {
		return asndApprovalCode;
	}
	public void setAsndApprovalCode(String asndApprovalCode) {
		this.asndApprovalCode = asndApprovalCode;
	}
	public String getAsndTid() {
		return asndTid;
	}
	public void setAsndTid(String asndTid) {
		this.asndTid = asndTid;
	}
	public String getAsndMid() {
		return asndMid;
	}
	public void setAsndMid(String asndMid) {
		this.asndMid = asndMid;
	}
	public String getAsndMcntName() {
		return asndMcntName;
	}
	public void setAsndMcntName(String asndMcntName) {
		this.asndMcntName = asndMcntName;
	}
	public String getAsndMcntCity() {
		return asndMcntCity;
	}
	public void setAsndMcntCity(String asndMcntCity) {
		this.asndMcntCity = asndMcntCity;
	}
	public String getAsndMcntCountry() {
		return asndMcntCountry;
	}
	public void setAsndMcntCountry(String asndMcntCountry) {
		this.asndMcntCountry = asndMcntCountry;
	}
	public String getAsndProductIndicator() {
		return asndProductIndicator;
	}
	public void setAsndProductIndicator(String asndProductIndicator) {
		this.asndProductIndicator = asndProductIndicator;
	}
	public String getAsndCustomerData() {
		return asndCustomerData;
	}
	public void setAsndCustomerData(String asndCustomerData) {
		this.asndCustomerData = asndCustomerData;
	}
	public String getAsndMcntCriteria() {
		return asndMcntCriteria;
	}
	public void setAsndMcntCriteria(String asndMcntCriteria) {
		this.asndMcntCriteria = asndMcntCriteria;
	}
	public String getAsndCurrencyCode() {
		return asndCurrencyCode;
	}
	public void setAsndCurrencyCode(String asndCurrencyCode) {
		this.asndCurrencyCode = asndCurrencyCode;
	}
	public String getAsndPostalCode() {
		return asndPostalCode;
	}
	public void setAsndPostalCode(String asndPostalCode) {
		this.asndPostalCode = asndPostalCode;
	}
	public String getAsndAdditionalField() {
		return asndAdditionalField;
	}
	public void setAsndAdditionalField(String asndAdditionalField) {
		this.asndAdditionalField = asndAdditionalField;
	}
	public String getAsndInvoiceNumber() {
		return asndInvoiceNumber;
	}
	public void setAsndInvoiceNumber(String asndInvoiceNumber) {
		this.asndInvoiceNumber = asndInvoiceNumber;
	}
	public String getArcvInstCode() {
		return arcvInstCode;
	}
	public void setArcvInstCode(String arcvInstCode) {
		this.arcvInstCode = arcvInstCode;
	}
	public String getArcvProcCode() {
		return arcvProcCode;
	}
	public void setArcvProcCode(String arcvProcCode) {
		this.arcvProcCode = arcvProcCode;
	}
	public BigDecimal getArcvTrnsAmount() {
		return arcvTrnsAmount;
	}
	public void setArcvTrnsAmount(BigDecimal arcvTrnsAmount) {
		this.arcvTrnsAmount = arcvTrnsAmount;
	}
	public String getArcvTrnsDttm() {
		return arcvTrnsDttm;
	}
	public void setArcvTrnsDttm(String arcvTrnsDttm) {
		this.arcvTrnsDttm = arcvTrnsDttm;
	}
	public String getArcvStan() {
		return arcvStan;
	}
	public void setArcvStan(String arcvStan) {
		this.arcvStan = arcvStan;
	}
	public String getArcvLocalDttm() {
		return arcvLocalDttm;
	}
	public void setArcvLocalDttm(String arcvLocalDttm) {
		this.arcvLocalDttm = arcvLocalDttm;
	}
	public String getArcvSettlementDate() {
		return arcvSettlementDate;
	}
	public void setArcvSettlementDate(String arcvSettlementDate) {
		this.arcvSettlementDate = arcvSettlementDate;
	}
	public String getArcvCaptureDate() {
		return arcvCaptureDate;
	}
	public void setArcvCaptureDate(String arcvCaptureDate) {
		this.arcvCaptureDate = arcvCaptureDate;
	}
	public String getArcvMerchantType() {
		return arcvMerchantType;
	}
	public void setArcvMerchantType(String arcvMerchantType) {
		this.arcvMerchantType = arcvMerchantType;
	}
	public String getArcvPosEmode() {
		return arcvPosEmode;
	}
	public void setArcvPosEmode(String arcvPosEmode) {
		this.arcvPosEmode = arcvPosEmode;
	}
	public String getArcvFeeType() {
		return arcvFeeType;
	}
	public void setArcvFeeType(String arcvFeeType) {
		this.arcvFeeType = arcvFeeType;
	}
	public BigDecimal getArcvFeeAmt() {
		return arcvFeeAmt;
	}
	public void setArcvFeeAmt(BigDecimal arcvFeeAmt) {
		this.arcvFeeAmt = arcvFeeAmt;
	}
	public BigDecimal getArcvTipAmt() {
		return arcvTipAmt;
	}
	public void setArcvTipAmt(BigDecimal arcvTipAmt) {
		this.arcvTipAmt = arcvTipAmt;
	}
	public String getArcvAcqId() {
		return arcvAcqId;
	}
	public void setArcvAcqId(String arcvAcqId) {
		this.arcvAcqId = arcvAcqId;
	}
	public String getArcvIssuerId() {
		return arcvIssuerId;
	}
	public void setArcvIssuerId(String arcvIssuerId) {
		this.arcvIssuerId = arcvIssuerId;
	}
	public String getArcvFwdingId() {
		return arcvFwdingId;
	}
	public void setArcvFwdingId(String arcvFwdingId) {
		this.arcvFwdingId = arcvFwdingId;
	}
	public String getArcvRrefNo() {
		return arcvRrefNo;
	}
	public void setArcvRrefNo(String arcvRrefNo) {
		this.arcvRrefNo = arcvRrefNo;
	}
	public String getArcvApprovalCode() {
		return arcvApprovalCode;
	}
	public void setArcvApprovalCode(String arcvApprovalCode) {
		this.arcvApprovalCode = arcvApprovalCode;
	}
	public String getArcvTid() {
		return arcvTid;
	}
	public void setArcvTid(String arcvTid) {
		this.arcvTid = arcvTid;
	}
	public String getArcvMid() {
		return arcvMid;
	}
	public void setArcvMid(String arcvMid) {
		this.arcvMid = arcvMid;
	}
	public String getArcvMcntName() {
		return arcvMcntName;
	}
	public void setArcvMcntName(String arcvMcntName) {
		this.arcvMcntName = arcvMcntName;
	}
	public String getArcvMcntCity() {
		return arcvMcntCity;
	}
	public void setArcvMcntCity(String arcvMcntCity) {
		this.arcvMcntCity = arcvMcntCity;
	}
	public String getArcvMcntCountry() {
		return arcvMcntCountry;
	}
	public void setArcvMcntCountry(String arcvMcntCountry) {
		this.arcvMcntCountry = arcvMcntCountry;
	}
	public String getArcvProductIndicator() {
		return arcvProductIndicator;
	}
	public void setArcvProductIndicator(String arcvProductIndicator) {
		this.arcvProductIndicator = arcvProductIndicator;
	}
	public String getArcvCustomerData() {
		return arcvCustomerData;
	}
	public void setArcvCustomerData(String arcvCustomerData) {
		this.arcvCustomerData = arcvCustomerData;
	}
	public String getArcvMcntCriteria() {
		return arcvMcntCriteria;
	}
	public void setArcvMcntCriteria(String arcvMcntCriteria) {
		this.arcvMcntCriteria = arcvMcntCriteria;
	}
	public String getArcvCurrencyCode() {
		return arcvCurrencyCode;
	}
	public void setArcvCurrencyCode(String arcvCurrencyCode) {
		this.arcvCurrencyCode = arcvCurrencyCode;
	}
	public String getArcvPostalCode() {
		return arcvPostalCode;
	}
	public void setArcvPostalCode(String arcvPostalCode) {
		this.arcvPostalCode = arcvPostalCode;
	}
	public String getArcvAdditionalField() {
		return arcvAdditionalField;
	}
	public void setArcvAdditionalField(String arcvAdditionalField) {
		this.arcvAdditionalField = arcvAdditionalField;
	}
	public String getArcvInvoiceName() {
		return arcvInvoiceName;
	}
	public void setArcvInvoiceName(String arcvInvoiceName) {
		this.arcvInvoiceName = arcvInvoiceName;
	}
	public String getInpUsrId() {
		return inpUsrId;
	}
	public void setInpUsrId(String inpUsrId) {
		this.inpUsrId = inpUsrId;
	}
	public String getInpPgmId() {
		return inpPgmId;
	}
	public void setInpPgmId(String inpPgmId) {
		this.inpPgmId = inpPgmId;
	}
	public String getDataInpDttm() {
		return dataInpDttm;
	}
	public void setDataInpDttm(String dataInpDttm) {
		this.dataInpDttm = dataInpDttm;
	}
	public String getChngUsrId() {
		return chngUsrId;
	}
	public void setChngUsrId(String chngUsrId) {
		this.chngUsrId = chngUsrId;
	}
	public String getChngPgmId() {
		return chngPgmId;
	}
	public void setChngPgmId(String chngPgmId) {
		this.chngPgmId = chngPgmId;
	}
	public String getDataChngDttm() {
		return dataChngDttm;
	}
	public void setDataChngDttm(String dataChngDttm) {
		this.dataChngDttm = dataChngDttm;
	}
	public String getMbTid() {
		return mbTid;
	}
	public void setMbTid(String mbTid) {
		this.mbTid = mbTid;
	}
	public String getMbMid() {
		return mbMid;
	}
	public void setMbMid(String mbMid) {
		this.mbMid = mbMid;
	}
	public String getSourceFund() {
		return sourceFund;
	}
	public void setSourceFund(String sourceFund) {
		this.sourceFund = sourceFund;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public BigDecimal getTipsAmt() {
		return tipsAmt;
	}
	public void setTipsAmt(BigDecimal tipsAmt) {
		this.tipsAmt = tipsAmt;
	}
	public BigDecimal getFeeAmt() {
		return feeAmt;
	}
	public void setFeeAmt(BigDecimal feeAmt) {
		this.feeAmt = feeAmt;
	}
	public String getFeeType() {
		return feeType;
	}
	public void setFeeType(String feeType) {
		this.feeType = feeType;
	}
	public String getAcquirerId() {
		return acquirerId;
	}
	public void setAcquirerId(String acquirerId) {
		this.acquirerId = acquirerId;
	}
	public String getIssuerId() {
		return issuerId;
	}
	public void setIssuerId(String issuerId) {
		this.issuerId = issuerId;
	}
	public String getForwardingId() {
		return forwardingId;
	}
	public void setForwardingId(String forwardingId) {
		this.forwardingId = forwardingId;
	}
	public String getQrExparation() {
		return qrExparation;
	}
	public void setQrExparation(String qrExparation) {
		this.qrExparation = qrExparation;
	}
	public String getQrTrack2() {
		return qrTrack2;
	}
	public void setQrTrack2(String qrTrack2) {
		this.qrTrack2 = qrTrack2;
	}
	public String getIssuerName() {
		return issuerName;
	}
	public void setIssuerName(String issuerName) {
		this.issuerName = issuerName;
	}
	public String getBefAuthSeqNo() {
		return befAuthSeqNo;
	}
	public void setBefAuthSeqNo(String befAuthSeqNo) {
		this.befAuthSeqNo = befAuthSeqNo;
	}
	public String getBefAuthDate() {
		return befAuthDate;
	}
	public void setBefAuthDate(String befAuthDate) {
		this.befAuthDate = befAuthDate;
	}
	public String getAuthCnclRsonCd() {
		return authCnclRsonCd;
	}
	public void setAuthCnclRsonCd(String authCnclRsonCd) {
		this.authCnclRsonCd = authCnclRsonCd;
	}
	public String getQrReasonCode() {
		return qrReasonCode;
	}
	public void setQrReasonCode(String qrReasonCode) {
		this.qrReasonCode = qrReasonCode;
	}
	public String getQrType() {
		return qrType;
	}
	public void setQrType(String qrType) {
		this.qrType = qrType;
	}
	public String getOnusFlag() {
		return onusFlag;
	}
	public void setOnusFlag(String onusFlag) {
		this.onusFlag = onusFlag;
	}
	
	
}
