package id.co.oob.lib.common.merchant.bansos.dto;

import java.math.BigDecimal;
import java.util.List;


public class BarangJualanBansosOnListEditDto {

	
	private List<BarangJualanBansosEditDto> jualanBansosDtos;
	private String sessionLogin;

	

	

	public List<BarangJualanBansosEditDto> getJualanBansosDtos() {
		return jualanBansosDtos;
	}

	public void setJualanBansosDtos(List<BarangJualanBansosEditDto> jualanBansosDtos) {
		this.jualanBansosDtos = jualanBansosDtos;
	}

	public String getSessionLogin() {
		return sessionLogin;
	}

	public void setSessionLogin(String sessionLogin) {
		this.sessionLogin = sessionLogin;
	}
	
}
