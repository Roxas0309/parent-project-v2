package id.co.oob.lib.common.merchant.onboarding.convert;

import java.util.List;

import com.google.gson.Gson;

import id.co.oob.lib.common.merchant.onboarding.throwable.NotAcceptanceDataException;

public class ConvertPhoneNumber {
	
	public static String INDONESIA_IDX_NUMBER = "+62";
	public static String INDONESIA_IDX_NUMBER_62 = "62";
	public static String INDONESIA_IDX_NUMBER_0 = "0";

	
	public static String[] getAllNumberInAllFormat(String number, String ifError) {
		System.out.println("Your number : " + number);
		String[] all = new String[3];
		if(number.contains(INDONESIA_IDX_NUMBER)) {
			all[0] = number;
			all[1] = number.replaceFirst("(?:62)", "0").substring(1);
			all[2] = number.replaceAll("\\+", "");
		}
		else if(number.contains(INDONESIA_IDX_NUMBER_62)) {
			all[0] = number.replaceFirst("(?:62)", "0");
			all[1] = number;
			all[2] = "+"+number;
		}
		else if(number.substring(0,1).equals(INDONESIA_IDX_NUMBER_0)) {
			all[0] = number.replaceFirst("(?:0)", "+62");
			all[1] = number.replaceFirst("(?:0)", "62");
			all[2] = number;
		}
		else {
			throw new NotAcceptanceDataException(ifError);
		}
//		System.out.println(all);
		System.err.println("all number formatted " + new Gson().toJson(all));
		return all;
	}
	
	public static void main(String[] args) {
	System.out.println("6287768248230".replaceFirst("62", "0").replaceAll("\\+", ""));
	}
	
}
