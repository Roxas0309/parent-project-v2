package id.co.oob.lib.common.merchant.onboarding.dto.user;

import java.util.Date;


public class UserOobNotificationTableDto {
private String tokenPage;
	
    private Long idOob;
	private Date createDate;
	private Date updateDate;
	public String getTokenPage() {
		return tokenPage;
	}
	public void setTokenPage(String tokenPage) {
		this.tokenPage = tokenPage;
	}
	public Long getIdOob() {
		return idOob;
	}
	public void setIdOob(Long idOob) {
		this.idOob = idOob;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	public Date getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
	
	
}
