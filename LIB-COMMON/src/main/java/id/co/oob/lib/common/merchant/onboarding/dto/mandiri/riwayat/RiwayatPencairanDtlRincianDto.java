package id.co.oob.lib.common.merchant.onboarding.dto.mandiri.riwayat;

public class RiwayatPencairanDtlRincianDto {

	private String authTime;
	private String reffNo;
	private String issuerName;
	private String authNumberInRupiah;
	private String authNo;
	
	
	
	public String getReffNo() {
		return reffNo;
	}
	public void setReffNo(String reffNo) {
		this.reffNo = reffNo;
	}
	public String getAuthNo() {
		return authNo;
	}
	public void setAuthNo(String authNo) {
		this.authNo = authNo;
	}
	public String getAuthTime() {
		return authTime;
	}
	public void setAuthTime(String authTime) {
		this.authTime = authTime;
	}
	public String getIssuerName() {
		return issuerName;
	}
	public void setIssuerName(String issuerName) {
		this.issuerName = issuerName;
	}
	public String getAuthNumberInRupiah() {
		return authNumberInRupiah;
	}
	public void setAuthNumberInRupiah(String authNumberInRupiah) {
		this.authNumberInRupiah = authNumberInRupiah;
	}
	
	
}
