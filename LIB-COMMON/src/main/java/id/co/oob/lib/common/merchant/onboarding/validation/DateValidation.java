package id.co.oob.lib.common.merchant.onboarding.validation;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import com.google.common.base.Strings;
import com.google.gson.Gson;

import id.co.oob.lib.common.merchant.onboarding.convert.DateConverter;
import id.co.oob.lib.common.merchant.onboarding.throwable.InternalServerErrorException;
import id.co.oob.lib.common.merchant.onboarding.throwable.NotAcceptanceDataException;

public class DateValidation {

	public static boolean checkBetweenHour(String target, String start, String end) {
		 String  HOUR_FORMAT = "HH:mm";
		 SimpleDateFormat sdfHour = new SimpleDateFormat(HOUR_FORMAT);
		 return ((target.compareTo(start) >= 0)&& (target.compareTo(end) <= 0));
	}
	

	public static void main(String[] args) throws ParseException {
		System.out.println(addDate(1L));
	}
	
	
	
	public static Integer getAge(String dateStr, String neededFormat) {
		if(!isStringDateValid(dateStr, neededFormat, false)) {
			throw new InternalServerErrorException("Tanggal \""+dateStr+"\" Yang Diberikan tidak sesuai dengan format " + neededFormat);
		}
		
		DateFormat sdf = new SimpleDateFormat(neededFormat);
		Date date = null;
		try {
			date = sdf.parse(dateStr);
			System.out.println("you got date " + date);
		} catch (ParseException e) {
			throw new InternalServerErrorException("Validation Error In String " + dateStr + " with format " + neededFormat);
		}
		  Calendar c = Calendar.getInstance();
		  c.setTime(date);
		  int tahun = c.get(Calendar.YEAR);
		  int bulan = c.get(Calendar.MONTH)+1;
		  int tanggal = c.get(Calendar.DATE);
		  LocalDate l1 = LocalDate.of(tahun, bulan, tanggal);
		  LocalDate now1 = LocalDate.now();
		  Period diff1 = Period.between(l1, now1);
		  int yourAge = diff1.getYears();
		  System.out.println("your ages : " + yourAge);
		  return yourAge;
	}
	
	public static Long getDifferentTime(Date date) {
		if(date == null) {
			throw new InternalServerErrorException("Date yang diberikan null");
		}
		
		Date now = new Date();
		
        long difference_In_Time 
            = now.getTime() - date.getTime(); 
        
        long difference_In_Hours 
        = (difference_In_Time 
           / (1000 * 60 * 60)) 
          % 24;
        
        return difference_In_Hours;
    }
	
	public static Long getDifferentDays(Date date) {
		if(date == null) {
			throw new InternalServerErrorException("Date yang diberikan null");
		}
		
		 long diffInMillies = Math.abs(date.getTime() - new Date().getTime());
		 long diff = TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS);
		return diff;
	}
	
	public static Long getDifferentDays(String dateFrom, String dateTo) {
		if(dateFrom == null) {
			throw new InternalServerErrorException("Date yang diberikan null");
		}
		
		if(dateTo == null) {
			throw new InternalServerErrorException("Date yang diberikan null");
		}
		
		Date from = null;
		Date to = null;
		try {
			from = new SimpleDateFormat("yyyyMMdd").parse(dateFrom);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new NotAcceptanceDataException("Start Date Harus Format yyyyMMdd");
		}
		
		try {
			to = new SimpleDateFormat("yyyyMMdd").parse(dateTo);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new NotAcceptanceDataException("End Date Harus Format yyyyMMdd");
		}
		
	//	System.out.println("from " + from + " to " + to);
		
        long difference_In_Time 
            = to.getTime() - from.getTime(); 
        
       // System.out.println("diff in time " + difference_In_Time);
        
        long difference_In_Days 
            = (difference_In_Time 
               / (1000 * 60 * 60 * 24)) 
              % 365; 

		Long years = (getDifferentYears(dateFrom, dateTo)*365);
		return years+difference_In_Days;
	}
	
	public static Long getDifferentYears(String dateFrom, String dateTo) {
		if(dateFrom == null) {
			throw new InternalServerErrorException("Date yang diberikan null");
		}
		
		if(dateTo == null) {
			throw new InternalServerErrorException("Date yang diberikan null");
		}
		
		Date from = null;
		Date to = null;
		try {
			from = new SimpleDateFormat("yyyyMMdd").parse(dateFrom);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new NotAcceptanceDataException("Start Date Harus Format yyyyMMdd");
		}
		
		try {
			to = new SimpleDateFormat("yyyyMMdd").parse(dateTo);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new NotAcceptanceDataException("End Date Harus Format yyyyMMdd");
		}
		
	//	System.out.println("from " + from + " to " + to);
		
        long difference_In_Time 
            = to.getTime() - from.getTime(); 
        
       // System.out.println("diff in time " + difference_In_Time);
        
        long difference_In_Years 
        = (difference_In_Time 
           / (1000l * 60 * 60 * 24 * 365)); 

		
		return difference_In_Years;
	}
	
	public static Long getDifferentMinute(Date date) {
		if(date == null) {
			throw new InternalServerErrorException("Date yang diberikan null");
		}
		
		Date now = new Date();
		
		// Calucalte time difference 
        // in milliseconds 
        long difference_In_Time 
            = now.getTime() - date.getTime(); 
        long difference_In_Minutes 
        = (difference_In_Time 
           / (1000 * 60)) 
          % 60; 
        return difference_In_Minutes;
	}
	
	public static Long getDifferentMinuteNew(Date date) {
		if(date == null) {
			throw new InternalServerErrorException("Date yang diberikan null");
		}
		
		 long diffInMillies = Math.abs(date.getTime() - new Date().getTime());
		 long diff = TimeUnit.MINUTES.convert(diffInMillies, TimeUnit.MILLISECONDS);
		return diff;
	}
	
	public static Long getDifferentHour(Date date) {
		if(date == null) {
			throw new InternalServerErrorException("Date yang diberikan null");
		}
		
		Date now = new Date();
		
		// Calucalte time difference 
        // in milliseconds 
        long difference_In_Time 
            = now.getTime() - date.getTime(); 

        // Calucalte time difference in 
        // seconds, minutes, hours, years, 
        // and days 
        long difference_In_Seconds 
            = (difference_In_Time 
               / 1000) 
              % 60; 

        long difference_In_Minutes 
            = (difference_In_Time 
               / (1000 * 60)) 
              % 60; 

        long difference_In_Hours 
            = (difference_In_Time 
               / (1000 * 60 * 60)) 
              % 24; 

        long difference_In_Years 
            = (difference_In_Time 
               / (1000l * 60 * 60 * 24 * 365)); 

        long difference_In_Days 
            = (difference_In_Time 
               / (1000 * 60 * 60 * 24)) 
              % 365; 

		System.out.println("final difference hour : " +difference_In_Hours);
		return difference_In_Minutes ;
	}
	
	public static Date addDate(Long time) {
		//static final long ONE_MINUTE_IN_MILLIS=60000;//millisecs

		Calendar date = Calendar.getInstance();
		long t= date.getTimeInMillis();
		Date afterAddingTenMins=new Date(t + (time));
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(afterAddingTenMins);
		calendar.set(Calendar.SECOND, 0);
		return calendar.getTime();
	}
	
	
	
	public static Boolean isStringDateValid(String dateStr, String neededFormat, boolean isAcceptNull) {
		if (Strings.isNullOrEmpty(dateStr) && !isAcceptNull) {
			throw new InternalServerErrorException("Pastikan Date String Ada Untuk Divalidasi");
		}
		
		
		if(!StringContainOnlyNumber.isStringContainOnlyNumber(dateStr)) {
			return false;
		}

		if (!Strings.isNullOrEmpty(dateStr)) {
			
			if(dateStr.length() != neededFormat.length()) {
				return false;
			}
			
			System.out.println("validasi date : " + dateStr + " dengan format : " + neededFormat);
			DateFormat sdf = new SimpleDateFormat(neededFormat);
			try {
				sdf.parse(dateStr);
			} catch (ParseException e) {
				e.printStackTrace();
				return false;
			}
		}
		return true;
	}

}
