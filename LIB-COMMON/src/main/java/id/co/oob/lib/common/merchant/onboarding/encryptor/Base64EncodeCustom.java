package id.co.oob.lib.common.merchant.onboarding.encryptor;

import java.util.Base64;

public class Base64EncodeCustom {

	public static String resultEncodeStringSmsBlast(String username, String password) {
		System.out.println("Informasi authorization sms-blast : Username : " + username + ", password : " + password);
		
		String originalInput = username+":"+password;
		return Base64.getEncoder().encodeToString(originalInput.getBytes());
	}
	
	public static String resultDecodeStringSmsBlast(String encodeString) {
		byte[] decodedBytes = Base64.getDecoder().decode(encodeString);
		String decodedString = new String(decodedBytes);
		return decodedString;
	}
	
	public static void main(String[] args) {
		String encode = resultEncodeStringSmsBlast("testing", "testing");
		System.out.println(encode);
		System.out.println(resultDecodeStringSmsBlast(encode));
	}
}
