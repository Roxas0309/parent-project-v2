package id.co.oob.lib.common.merchant.bansos.dto;

import java.math.BigDecimal;


public class PembeliBarangBansosDto {
	private String pembeliNo;
	
	private String transaksiOwner;
	
	private String noEktp;
	
	private String namaPembeli;

	private BigDecimal totalBelanjaan;
	
	private String totalPembayaran;
	
	private String dateTransaksi;
	
	private String timeTransaksi;
	
	

	public String getTimeTransaksi() {
		return timeTransaksi;
	}

	public void setTimeTransaksi(String timeTransaksi) {
		this.timeTransaksi = timeTransaksi;
	}

	public String getDateTransaksi() {
		return dateTransaksi;
	}

	public void setDateTransaksi(String dateTransaksi) {
		this.dateTransaksi = dateTransaksi;
	}

	public String getPembeliNo() {
		return pembeliNo;
	}

	public void setPembeliNo(String pembeliNo) {
		this.pembeliNo = pembeliNo;
	}

	public String getTransaksiOwner() {
		return transaksiOwner;
	}

	public void setTransaksiOwner(String transaksiOwner) {
		this.transaksiOwner = transaksiOwner;
	}

	public String getNoEktp() {
		return noEktp;
	}

	public void setNoEktp(String noEktp) {
		this.noEktp = noEktp;
	}

	public String getNamaPembeli() {
		return namaPembeli;
	}

	public void setNamaPembeli(String namaPembeli) {
		this.namaPembeli = namaPembeli;
	}

	public BigDecimal getTotalBelanjaan() {
		return totalBelanjaan;
	}

	public void setTotalBelanjaan(BigDecimal totalBelanjaan) {
		this.totalBelanjaan = totalBelanjaan;
	}

	public String getTotalPembayaran() {
		return totalPembayaran;
	}

	public void setTotalPembayaran(String totalPembayaran) {
		this.totalPembayaran = totalPembayaran;
	}
	
	
}
