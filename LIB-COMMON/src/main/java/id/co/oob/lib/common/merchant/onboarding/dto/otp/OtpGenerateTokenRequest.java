package id.co.oob.lib.common.merchant.onboarding.dto.otp;

public class OtpGenerateTokenRequest {
			private String credential;
			private String externalId;
			private String timestamp;
			private String channelId;
			private String authenticationMethod;
			private String otpKey1;
			private String otpKey2;
			private String otpKey3;
			private String handphone;
			private String otpReason;
			private String narration;
			
			public String getCredential() {
				return credential;
			}
			public void setCredential(String credential) {
				this.credential = credential;
			}
			public String getExternalId() {
				return externalId;
			}
			public void setExternalId(String externalId) {
				this.externalId = externalId;
			}
			public String getTimestamp() {
				return timestamp;
			}
			public void setTimestamp(String timestamp) {
				this.timestamp = timestamp;
			}
			public String getChannelId() {
				return channelId;
			}
			public void setChannelId(String channelId) {
				this.channelId = channelId;
			}
			public String getAuthenticationMethod() {
				return authenticationMethod;
			}
			public void setAuthenticationMethod(String authenticationMethod) {
				this.authenticationMethod = authenticationMethod;
			}
			public String getOtpKey1() {
				return otpKey1;
			}
			public void setOtpKey1(String otpKey1) {
				this.otpKey1 = otpKey1;
			}
			public String getOtpKey2() {
				return otpKey2;
			}
			public void setOtpKey2(String otpKey2) {
				this.otpKey2 = otpKey2;
			}
			public String getOtpKey3() {
				return otpKey3;
			}
			public void setOtpKey3(String otpKey3) {
				this.otpKey3 = otpKey3;
			}
			public String getHandphone() {
				return handphone;
			}
			public void setHandphone(String handphone) {
				this.handphone = handphone;
			}
			public String getOtpReason() {
				return otpReason;
			}
			public void setOtpReason(String otpReason) {
				this.otpReason = otpReason;
			}
			public String getNarration() {
				return narration;
			}
			public void setNarration(String narration) {
				this.narration = narration;
			}
			
}
