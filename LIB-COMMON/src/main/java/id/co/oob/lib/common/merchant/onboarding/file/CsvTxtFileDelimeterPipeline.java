package id.co.oob.lib.common.merchant.onboarding.file;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.opencsv.CSVWriter;

public class CsvTxtFileDelimeterPipeline {

	public static void main(String[] args) {
		 File file = new File("/home/iforce/Desktop/110001.csv");

		    try { 
		        // create FileWriter object with file as parameter 
		        FileWriter outputfile = new FileWriter(file); 
		  
		        // create CSVWriter with '|' as separator 
		        CSVWriter writer = new CSVWriter(outputfile, '|', 
		                                         CSVWriter.NO_QUOTE_CHARACTER, 
		                                         CSVWriter.DEFAULT_ESCAPE_CHARACTER, 
		                                         CSVWriter.DEFAULT_LINE_END); 
		  
		        // create a List which contains String array 
		        List<String[]> data = new ArrayList<String[]>(); 
		        data.add(new String[] { "Name", "Class", "Marks" }); 
		        data.add(new String[] { "Aman", "10", "620" }); 
		        data.add(new String[] { "Suraj", "10", "630" }); 
		        writer.writeAll(data); 
		  
		        // closing writer connection 
		        writer.close(); 
		    } 
		    catch (IOException e) { 
		        // TODO Auto-generated catch block 
		        e.printStackTrace(); 
		    } 
	}
	
}
