package id.co.oob.lib.common.merchant.onboarding.dto.user;

import java.util.Date;


public class UserOobTableDto {
		private Boolean isUserAgree;
		private String namaPemilikUsaha;
		private String namaPemilikUsahaValidasi;
		private String dob;
		private String nomorKtp;
		private String namaIbuKandung;
		private String nomorRekening;
		private String alamatPemilikUsaha;
		private String emailPemilikUsaha;
	    private String noNpwp;
	    private String namaUsaha;
	    private String nomorTelp;
	    private String jenisUsaha;
	    private String omzet;
	    private String kategoriLokasiUsaha;
	    private String jenisLokasiUsaha;
	    private String alamatLokasiUsahaSaatIni;
	    private Long idKelurahan;
	    
	    private String base64Ktp;
		
	    private String base64WajahKtp;
	    
		private String base64Npwp;
		
		private String base64FotoTempatUsaha;
		
		private String base64FotoBarangAtauJasa;
		
		private String base64FotoPemilikTempatUsaha;
	    
		
	    
		public String getNamaPemilikUsahaValidasi() {
			return namaPemilikUsahaValidasi;
		}
		public void setNamaPemilikUsahaValidasi(String namaPemilikUsahaValidasi) {
			this.namaPemilikUsahaValidasi = namaPemilikUsahaValidasi;
		}
		public Long getIdKelurahan() {
			return idKelurahan;
		}
		public void setIdKelurahan(Long idKelurahan) {
			this.idKelurahan = idKelurahan;
		}
		public Boolean getIsUserAgree() {
			return isUserAgree;
		}
		public void setIsUserAgree(Boolean isUserAgree) {
			this.isUserAgree = isUserAgree;
		}
		public String getNamaPemilikUsaha() {
			return namaPemilikUsaha;
		}
		public void setNamaPemilikUsaha(String namaPemilikUsaha) {
			this.namaPemilikUsaha = namaPemilikUsaha;
		}
		public String getDob() {
			return dob;
		}
		public void setDob(String dob) {
			this.dob = dob;
		}
		public String getNomorKtp() {
			return nomorKtp;
		}
		public void setNomorKtp(String nomorKtp) {
			this.nomorKtp = nomorKtp;
		}
		public String getNamaIbuKandung() {
			return namaIbuKandung;
		}
		public void setNamaIbuKandung(String namaIbuKandung) {
			this.namaIbuKandung = namaIbuKandung;
		}
		public String getNomorRekening() {
			return nomorRekening;
		}
		public void setNomorRekening(String nomorRekening) {
			this.nomorRekening = nomorRekening;
		}
		public String getAlamatPemilikUsaha() {
			return alamatPemilikUsaha;
		}
		public void setAlamatPemilikUsaha(String alamatPemilikUsaha) {
			this.alamatPemilikUsaha = alamatPemilikUsaha;
		}
		public String getEmailPemilikUsaha() {
			return emailPemilikUsaha;
		}
		public void setEmailPemilikUsaha(String emailPemilikUsaha) {
			this.emailPemilikUsaha = emailPemilikUsaha;
		}

		public String getNoNpwp() {
			return noNpwp;
		}
		public void setNoNpwp(String noNpwp) {
			this.noNpwp = noNpwp;
		}
		public String getNamaUsaha() {
			return namaUsaha;
		}
		public void setNamaUsaha(String namaUsaha) {
			this.namaUsaha = namaUsaha;
		}
		public String getNomorTelp() {
			return nomorTelp;
		}
		public void setNomorTelp(String nomorTelp) {
			this.nomorTelp = nomorTelp;
		}
		public String getJenisUsaha() {
			return jenisUsaha;
		}
		public void setJenisUsaha(String jenisUsaha) {
			this.jenisUsaha = jenisUsaha;
		}
		public String getOmzet() {
			return omzet;
		}
		public void setOmzet(String omzet) {
			this.omzet = omzet;
		}

		public String getKategoriLokasiUsaha() {
			return kategoriLokasiUsaha;
		}
		public void setKategoriLokasiUsaha(String kategoriLokasiUsaha) {
			this.kategoriLokasiUsaha = kategoriLokasiUsaha;
		}
		public String getJenisLokasiUsaha() {
			return jenisLokasiUsaha;
		}
		public void setJenisLokasiUsaha(String jenisLokasiUsaha) {
			this.jenisLokasiUsaha = jenisLokasiUsaha;
		}
		public String getAlamatLokasiUsahaSaatIni() {
			return alamatLokasiUsahaSaatIni;
		}
		public void setAlamatLokasiUsahaSaatIni(String alamatLokasiUsahaSaatIni) {
			this.alamatLokasiUsahaSaatIni = alamatLokasiUsahaSaatIni;
		}
		public String getBase64Ktp() {
			return base64Ktp;
		}
		public void setBase64Ktp(String base64Ktp) {
			this.base64Ktp = base64Ktp;
		}
		public String getBase64WajahKtp() {
			return base64WajahKtp;
		}
		public void setBase64WajahKtp(String base64WajahKtp) {
			this.base64WajahKtp = base64WajahKtp;
		}
		public String getBase64Npwp() {
			return base64Npwp;
		}
		public void setBase64Npwp(String base64Npwp) {
			this.base64Npwp = base64Npwp;
		}
		public String getBase64FotoTempatUsaha() {
			return base64FotoTempatUsaha;
		}
		public void setBase64FotoTempatUsaha(String base64FotoTempatUsaha) {
			this.base64FotoTempatUsaha = base64FotoTempatUsaha;
		}
		public String getBase64FotoBarangAtauJasa() {
			return base64FotoBarangAtauJasa;
		}
		public void setBase64FotoBarangAtauJasa(String base64FotoBarangAtauJasa) {
			this.base64FotoBarangAtauJasa = base64FotoBarangAtauJasa;
		}
		public String getBase64FotoPemilikTempatUsaha() {
			return base64FotoPemilikTempatUsaha;
		}
		public void setBase64FotoPemilikTempatUsaha(String base64FotoPemilikTempatUsaha) {
			this.base64FotoPemilikTempatUsaha = base64FotoPemilikTempatUsaha;
		}
	
		
		
	    
}
