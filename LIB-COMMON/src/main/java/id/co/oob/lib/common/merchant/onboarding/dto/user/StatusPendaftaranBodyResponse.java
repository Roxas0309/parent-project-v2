package id.co.oob.lib.common.merchant.onboarding.dto.user;

import java.util.HashMap;
import java.util.Map;

public class StatusPendaftaranBodyResponse {
	private Map<Integer, CheckStatusPendaftaranDto> mapStatus = new HashMap<>();
	private UserOobTableDto infoNasabah;
	public Map<Integer, CheckStatusPendaftaranDto> getMapStatus() {
		return mapStatus;
	}
	public void setMapStatus(Map<Integer, CheckStatusPendaftaranDto> mapStatus) {
		this.mapStatus = mapStatus;
	}
	public UserOobTableDto getInfoNasabah() {
		return infoNasabah;
	}
	public void setInfoNasabah(UserOobTableDto infoNasabah) {
		this.infoNasabah = infoNasabah;
	}
	
	
}
