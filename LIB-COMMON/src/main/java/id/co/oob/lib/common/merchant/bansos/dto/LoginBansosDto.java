package id.co.oob.lib.common.merchant.bansos.dto;

public class LoginBansosDto {

	private String userName;
	private String password;
	private String result;
	private String expoToken;
	
	public String getExpoToken() {
		return expoToken;
	}
	public void setExpoToken(String expoToken) {
		this.expoToken = expoToken;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getResult() {
		return result;
	}
	public void setResult(String result) {
		this.result = result;
	}
	
	
	
}
