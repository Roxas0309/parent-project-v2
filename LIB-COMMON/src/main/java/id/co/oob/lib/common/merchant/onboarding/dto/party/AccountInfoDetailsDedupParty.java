package id.co.oob.lib.common.merchant.onboarding.dto.party;

public class AccountInfoDetailsDedupParty {
		private String accountNo;
		private String accountStatus;
		private String productType;
		public String getAccountNo() {
			return accountNo;
		}
		public void setAccountNo(String accountNo) {
			this.accountNo = accountNo;
		}
		public String getAccountStatus() {
			return accountStatus;
		}
		public void setAccountStatus(String accountStatus) {
			this.accountStatus = accountStatus;
		}
		public String getProductType() {
			return productType;
		}
		public void setProductType(String productType) {
			this.productType = productType;
		}
		
		
}
