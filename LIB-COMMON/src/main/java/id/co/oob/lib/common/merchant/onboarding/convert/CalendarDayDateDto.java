package id.co.oob.lib.common.merchant.onboarding.convert;

public class CalendarDayDateDto {
	public Integer tanggal;
	public String hari;
	public String formatIn;
	public Boolean isActiveDay = false;
	public Integer sequence;
	
	
	
	
	
	public Integer getSequence() {
		return sequence;
	}
	public void setSequence(Integer sequence) {
		this.sequence = sequence;
	}
	public String getFormatIn() {
		return formatIn;
	}
	public void setFormatIn(String formatIn) {
		this.formatIn = formatIn;
	}
	public Boolean getIsActiveDay() {
		return isActiveDay;
	}
	public void setIsActiveDay(Boolean isActiveDay) {
		this.isActiveDay = isActiveDay;
	}
	public Integer getTanggal() {
		return tanggal;
	}
	public void setTanggal(Integer tanggal) {
		this.tanggal = tanggal;
	}
	public String getHari() {
		return hari;
	}
	public void setHari(String hari) {
		this.hari = hari;
	}
	
	
	
}
