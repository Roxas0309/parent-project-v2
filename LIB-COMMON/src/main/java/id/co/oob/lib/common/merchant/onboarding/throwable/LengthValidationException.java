package id.co.oob.lib.common.merchant.onboarding.throwable;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.LENGTH_REQUIRED)
public class LengthValidationException extends RuntimeException{
	private static final long serialVersionUID = -7709761715178137895L;

	public LengthValidationException(String message) {
		super(message);
	}

}
