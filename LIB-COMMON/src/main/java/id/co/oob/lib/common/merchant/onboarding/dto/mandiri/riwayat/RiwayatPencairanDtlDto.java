package id.co.oob.lib.common.merchant.onboarding.dto.mandiri.riwayat;

import java.math.BigDecimal;
import java.util.List;

public class RiwayatPencairanDtlDto {
	
	private String pencairanType; //Pencairan 1, Pencairan 2, Pencairan 3, PENCAIRAN SUSULAN, TERTUNDA
	private String pencairanTypeCode; // 1,2,3,0,-1
	private BigDecimal pencairanMoney;
	private Boolean isPencairanLanjutan;
	private Boolean isPencairanSusulan;
	private String pencairanMoneyInString;
	private Integer countTransaksi;
	private String pencairanDate;
	private String namaHari;
	private String pencairanTime;
	private String pencairanDateWithoutYear;
	private String pencairanBeforeWithoutYear;
	private BigDecimal pencairanMoneyWithoutFee;
	private String pencairanMoneyWithoutFeeInString;
	private BigDecimal feeAmount;
	private String feeAmountInString;
	private List<RiwayatPencairanDtlRincianDto> riwayatPencairanDtlRincianDtos;
	
	
	
	public String getPencairanBeforeWithoutYear() {
		return pencairanBeforeWithoutYear;
	}
	public void setPencairanBeforeWithoutYear(String pencairanBeforeWithoutYear) {
		this.pencairanBeforeWithoutYear = pencairanBeforeWithoutYear;
	}
	public String getNamaHari() {
		return namaHari;
	}
	public void setNamaHari(String namaHari) {
		this.namaHari = namaHari;
	}
	public Integer getCountTransaksi() {
		return countTransaksi;
	}
	public void setCountTransaksi(Integer countTransaksi) {
		this.countTransaksi = countTransaksi;
	}
	public String getPencairanTime() {
		return pencairanTime;
	}
	public void setPencairanTime(String pencairanTime) {
		this.pencairanTime = pencairanTime;
	}
	public Boolean getIsPencairanLanjutan() {
		return isPencairanLanjutan;
	}
	public void setIsPencairanLanjutan(Boolean isPencairanLanjutan) {
		this.isPencairanLanjutan = isPencairanLanjutan;
	}
	public Boolean getIsPencairanSusulan() {
		return isPencairanSusulan;
	}
	public void setIsPencairanSusulan(Boolean isPencairanSusulan) {
		this.isPencairanSusulan = isPencairanSusulan;
	}
	public String getPencairanType() {
		return pencairanType;
	}
	public void setPencairanType(String pencairanType) {
		this.pencairanType = pencairanType;
	}
	public String getPencairanTypeCode() {
		return pencairanTypeCode;
	}
	public void setPencairanTypeCode(String pencairanTypeCode) {
		this.pencairanTypeCode = pencairanTypeCode;
	}
	public BigDecimal getPencairanMoney() {
		return pencairanMoney;
	}
	public void setPencairanMoney(BigDecimal pencairanMoney) {
		this.pencairanMoney = pencairanMoney;
	}
	public String getPencairanMoneyInString() {
		return pencairanMoneyInString;
	}
	public void setPencairanMoneyInString(String pencairanMoneyInString) {
		this.pencairanMoneyInString = pencairanMoneyInString;
	}
	public String getPencairanDate() {
		return pencairanDate;
	}
	public void setPencairanDate(String pencairanDate) {
		this.pencairanDate = pencairanDate;
	}
	public String getPencairanDateWithoutYear() {
		return pencairanDateWithoutYear;
	}
	public void setPencairanDateWithoutYear(String pencairanDateWithoutYear) {
		this.pencairanDateWithoutYear = pencairanDateWithoutYear;
	}
	public BigDecimal getPencairanMoneyWithoutFee() {
		return pencairanMoneyWithoutFee;
	}
	public void setPencairanMoneyWithoutFee(BigDecimal pencairanMoneyWithoutFee) {
		this.pencairanMoneyWithoutFee = pencairanMoneyWithoutFee;
	}
	public String getPencairanMoneyWithoutFeeInString() {
		return pencairanMoneyWithoutFeeInString;
	}
	public void setPencairanMoneyWithoutFeeInString(String pencairanMoneyWithoutFeeInString) {
		this.pencairanMoneyWithoutFeeInString = pencairanMoneyWithoutFeeInString;
	}
	public BigDecimal getFeeAmount() {
		return feeAmount;
	}
	public void setFeeAmount(BigDecimal feeAmount) {
		this.feeAmount = feeAmount;
	}
	public String getFeeAmountInString() {
		return feeAmountInString;
	}
	public void setFeeAmountInString(String feeAmountInString) {
		this.feeAmountInString = feeAmountInString;
	}
	public List<RiwayatPencairanDtlRincianDto> getRiwayatPencairanDtlRincianDtos() {
		return riwayatPencairanDtlRincianDtos;
	}
	public void setRiwayatPencairanDtlRincianDtos(List<RiwayatPencairanDtlRincianDto> riwayatPencairanDtlRincianDtos) {
		this.riwayatPencairanDtlRincianDtos = riwayatPencairanDtlRincianDtos;
	}
	
	
	
}
