package id.co.oob.lib.common.merchant.onboarding.encryptor;

public class MaskingNumber {

	public static void main(String[] args) {
		//System.err.println(maskingNomorPendaftaran(String.valueOf(121L), 8));
		System.err.println(maskingPhoneNumber("08776824820"));
	}
	
	public static String maskingNomorPendaftaran(String number, int regexZeroNumber) {
		int numberCharCount = number.length();
		regexZeroNumber = regexZeroNumber - numberCharCount;
		String finalMasking = "";
		while(regexZeroNumber!=0) {
			finalMasking += "0";
			regexZeroNumber--;
		}
		
		return finalMasking+number;
	}
	
	public static String maskingPhoneNumber(String phoneNumber) {
		
		System.out.println("Start masking number : " + phoneNumber);
		int maskingNumber = phoneNumber.length() - 3;
		int counter = 1;
		String masknum = "";
		for (Character c : phoneNumber.toCharArray()) {
			if(maskingNumber>0) {
				if(counter>= 1 && counter<=4) {
					masknum += String.valueOf(c);
				}
				else {
				masknum += "*";
				}
			}
			else {
				masknum += String.valueOf(c);
			}
			counter++;
			maskingNumber--;
		}
		return masknum;
	}
	
}
