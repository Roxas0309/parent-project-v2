package id.co.oob.lib.common.merchant.onboarding.dto.user;

public class ChangePasswordUser {
	public String password;

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	
}
