package id.co.oob.lib.common.merchant.onboarding.file;

import com.google.common.base.Strings;

import id.co.oob.lib.common.merchant.onboarding.throwable.AnauthorizedException;
import id.co.oob.lib.common.merchant.onboarding.throwable.NotAcceptanceDataException;

public class Base64FileDto {

	private String base64;
	private String fileName;
	private String fileType;
	
	
	
	public Base64FileDto(String base64, String fileName) {
		super();
		this.base64 = base64;
		this.fileName = fileName;
		
		if(Strings.isNullOrEmpty(base64) && !Strings.isNullOrEmpty(fileName)) {
			throw new NotAcceptanceDataException("File Name Ditemukan Tetapi Base 64 Tidak Ditemukan");
		}
		
		if(!Strings.isNullOrEmpty(base64) && Strings.isNullOrEmpty(fileName)) {
			throw new NotAcceptanceDataException("Base 64 Ditemukan Tetapi File Name Tidak Ditemukan");
		}
	}
	
	
	public Base64FileDto(String base64) {
		super();
		this.base64 = base64;
	}


	public String getBase64() {
		return base64;
	}
	public void setBase64(String base64) {
		this.base64 = base64;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getFileType() {
		return fileType;
	}
	public void setFileType(String fileType) {
		this.fileType = fileType;
	}
	
}
