package id.co.oob.lib.common.merchant.onboarding.dto.mandiri;

public class AccountInfoDetailDto {
		private String productType;
		private String accountStatus;
		private String accountNo;
		public String getProductType() {
			return productType;
		}
		public void setProductType(String productType) {
			this.productType = productType;
		}
		public String getAccountStatus() {
			return accountStatus;
		}
		public void setAccountStatus(String accountStatus) {
			this.accountStatus = accountStatus;
		}
		public String getAccountNo() {
			return accountNo;
		}
		public void setAccountNo(String accountNo) {
			this.accountNo = accountNo;
		}
		
		
}
