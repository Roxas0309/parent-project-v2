	package id.co.oob.lib.common.merchant.onboarding.dto.user;

import java.util.Date;



public class UserOobTableUploadFileDto {
	private Long id;
	
		private Boolean isUserAgree;
		private String namaPemilikUsaha;
		private String dob;
		private String nomorKtp;
		private String namaIbuKandung;
		private String nomorRekening;
		private String alamatPemilikUsaha;
		private String emailPemilikUsaha;
	    private String noNpwp;
	    private String namaUsaha;
	    private String nomorTelp;
	    private String jenisUsaha;
	    private String omzet;
	    private String kategoriLokasiUsaha;
	    private String jenisLokasiUsaha;
	    private String alamatLokasiUsahaSaatIni;
	    private String kodePosAlamatMerchant;
	    private Long idKelurahan;
	    private String noHandphone;
		private String pathKtp;
		private String completePathKtp;
		private String kodePosAlamatPemilikUsaha;

		private String pathWajahKtp;
		private String completePathWajahKtp;

		private String pathNpwp;
		private String completePathNpwp;
		
		private String pathFotoTempatUsaha;
		private String completePathFotoTempatUsaha;
		
		private String pathFotoBarangAtauJasa;
		private String completePathFotoBarangAtauJasa;

		private String pathFotoPemilikTempatUsaha;
		private String completePathFotoPemilikTempatUsaha;
		
	    
		
		public String getKodePosAlamatMerchant() {
			return kodePosAlamatMerchant;
		}
		public void setKodePosAlamatMerchant(String kodePosAlamatMerchant) {
			this.kodePosAlamatMerchant = kodePosAlamatMerchant;
		}
		public String getCompletePathKtp() {
			return completePathKtp;
		}
		public void setCompletePathKtp(String completePathKtp) {
			this.completePathKtp = completePathKtp;
		}
		public String getCompletePathWajahKtp() {
			return completePathWajahKtp;
		}
		public void setCompletePathWajahKtp(String completePathWajahKtp) {
			this.completePathWajahKtp = completePathWajahKtp;
		}
		public String getCompletePathNpwp() {
			return completePathNpwp;
		}
		public void setCompletePathNpwp(String completePathNpwp) {
			this.completePathNpwp = completePathNpwp;
		}
		public String getCompletePathFotoTempatUsaha() {
			return completePathFotoTempatUsaha;
		}
		public void setCompletePathFotoTempatUsaha(String completePathFotoTempatUsaha) {
			this.completePathFotoTempatUsaha = completePathFotoTempatUsaha;
		}
		public String getCompletePathFotoBarangAtauJasa() {
			return completePathFotoBarangAtauJasa;
		}
		public void setCompletePathFotoBarangAtauJasa(String completePathFotoBarangAtauJasa) {
			this.completePathFotoBarangAtauJasa = completePathFotoBarangAtauJasa;
		}
		public String getCompletePathFotoPemilikTempatUsaha() {
			return completePathFotoPemilikTempatUsaha;
		}
		public void setCompletePathFotoPemilikTempatUsaha(String completePathFotoPemilikTempatUsaha) {
			this.completePathFotoPemilikTempatUsaha = completePathFotoPemilikTempatUsaha;
		}
		public String getNoHandphone() {
			return noHandphone;
		}
		public void setNoHandphone(String noHandphone) {
			this.noHandphone = noHandphone;
		}
		public String getKodePosAlamatPemilikUsaha() {
			return kodePosAlamatPemilikUsaha;
		}
		public void setKodePosAlamatPemilikUsaha(String kodePosAlamatPemilikUsaha) {
			this.kodePosAlamatPemilikUsaha = kodePosAlamatPemilikUsaha;
		}
		public Long getId() {
			return id;
		}
		public void setId(Long id) {
			this.id = id;
		}
		public Long getIdKelurahan() {
			return idKelurahan;
		}
		public void setIdKelurahan(Long idKelurahan) {
			this.idKelurahan = idKelurahan;
		}
		public Boolean getIsUserAgree() {
			return isUserAgree;
		}
		public void setIsUserAgree(Boolean isUserAgree) {
			this.isUserAgree = isUserAgree;
		}
		public String getNamaPemilikUsaha() {
			return namaPemilikUsaha;
		}
		public void setNamaPemilikUsaha(String namaPemilikUsaha) {
			this.namaPemilikUsaha = namaPemilikUsaha;
		}
		public String getDob() {
			return dob;
		}
		public void setDob(String dob) {
			this.dob = dob;
		}
		public String getNomorKtp() {
			return nomorKtp;
		}
		public void setNomorKtp(String nomorKtp) {
			this.nomorKtp = nomorKtp;
		}
		public String getNamaIbuKandung() {
			return namaIbuKandung;
		}
		public void setNamaIbuKandung(String namaIbuKandung) {
			this.namaIbuKandung = namaIbuKandung;
		}
		public String getNomorRekening() {
			return nomorRekening;
		}
		public void setNomorRekening(String nomorRekening) {
			this.nomorRekening = nomorRekening;
		}
		public String getAlamatPemilikUsaha() {
			return alamatPemilikUsaha;
		}
		public void setAlamatPemilikUsaha(String alamatPemilikUsaha) {
			this.alamatPemilikUsaha = alamatPemilikUsaha;
		}
		public String getEmailPemilikUsaha() {
			return emailPemilikUsaha;
		}
		public void setEmailPemilikUsaha(String emailPemilikUsaha) {
			this.emailPemilikUsaha = emailPemilikUsaha;
		}

		public String getNoNpwp() {
			return noNpwp;
		}
		public void setNoNpwp(String noNpwp) {
			this.noNpwp = noNpwp;
		}
		public String getNamaUsaha() {
			return namaUsaha;
		}
		public void setNamaUsaha(String namaUsaha) {
			this.namaUsaha = namaUsaha;
		}
		public String getNomorTelp() {
			return nomorTelp;
		}
		public void setNomorTelp(String nomorTelp) {
			this.nomorTelp = nomorTelp;
		}
		public String getJenisUsaha() {
			return jenisUsaha;
		}
		public void setJenisUsaha(String jenisUsaha) {
			this.jenisUsaha = jenisUsaha;
		}
		public String getOmzet() {
			return omzet;
		}
		public void setOmzet(String omzet) {
			this.omzet = omzet;
		}

		public String getKategoriLokasiUsaha() {
			return kategoriLokasiUsaha;
		}
		public void setKategoriLokasiUsaha(String kategoriLokasiUsaha) {
			this.kategoriLokasiUsaha = kategoriLokasiUsaha;
		}
		public String getJenisLokasiUsaha() {
			return jenisLokasiUsaha;
		}
		public void setJenisLokasiUsaha(String jenisLokasiUsaha) {
			this.jenisLokasiUsaha = jenisLokasiUsaha;
		}
		public String getAlamatLokasiUsahaSaatIni() {
			return alamatLokasiUsahaSaatIni;
		}
		public void setAlamatLokasiUsahaSaatIni(String alamatLokasiUsahaSaatIni) {
			this.alamatLokasiUsahaSaatIni = alamatLokasiUsahaSaatIni;
		}
		public String getPathKtp() {
			return pathKtp;
		}
		public void setPathKtp(String pathKtp) {
			this.pathKtp = pathKtp;
		}
		public String getPathWajahKtp() {
			return pathWajahKtp;
		}
		public void setPathWajahKtp(String pathWajahKtp) {
			this.pathWajahKtp = pathWajahKtp;
		}
		public String getPathNpwp() {
			return pathNpwp;
		}
		public void setPathNpwp(String pathNpwp) {
			this.pathNpwp = pathNpwp;
		}
		public String getPathFotoTempatUsaha() {
			return pathFotoTempatUsaha;
		}
		public void setPathFotoTempatUsaha(String pathFotoTempatUsaha) {
			this.pathFotoTempatUsaha = pathFotoTempatUsaha;
		}
		public String getPathFotoBarangAtauJasa() {
			return pathFotoBarangAtauJasa;
		}
		public void setPathFotoBarangAtauJasa(String pathFotoBarangAtauJasa) {
			this.pathFotoBarangAtauJasa = pathFotoBarangAtauJasa;
		}
		public String getPathFotoPemilikTempatUsaha() {
			return pathFotoPemilikTempatUsaha;
		}
		public void setPathFotoPemilikTempatUsaha(String pathFotoPemilikTempatUsaha) {
			this.pathFotoPemilikTempatUsaha = pathFotoPemilikTempatUsaha;
		}
	
		
		
	    
}
