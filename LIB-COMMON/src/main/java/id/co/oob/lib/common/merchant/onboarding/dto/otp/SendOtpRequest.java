package id.co.oob.lib.common.merchant.onboarding.dto.otp;

public class SendOtpRequest {

		private String cifInEncrypt;

		public String getCifInEncrypt() {
			return cifInEncrypt;
		}

		public void setCifInEncrypt(String cifInEncrypt) {
			this.cifInEncrypt = cifInEncrypt;
		}
		
		
}
