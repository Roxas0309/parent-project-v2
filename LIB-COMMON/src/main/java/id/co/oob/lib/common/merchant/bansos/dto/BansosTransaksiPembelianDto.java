package id.co.oob.lib.common.merchant.bansos.dto;

import java.math.BigDecimal;

public class BansosTransaksiPembelianDto {
		
	private Long idData;
	private BigDecimal jumlahBarang;
	private String kodeBarang;
	
	
	
	public String getKodeBarang() {
		return kodeBarang;
	}
	public void setKodeBarang(String kodeBarang) {
		this.kodeBarang = kodeBarang;
	}
	public Long getIdData() {
		return idData;
	}
	public void setIdData(Long idData) {
		this.idData = idData;
	}
	public BigDecimal getJumlahBarang() {
		return jumlahBarang;
	}
	public void setJumlahBarang(BigDecimal jumlahBarang) {
		this.jumlahBarang = jumlahBarang;
	}
	
	
	
}
