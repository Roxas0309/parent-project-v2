package id.co.oob.lib.common.merchant.onboarding.dto.mandiri;

import java.util.List;

public class MaasResponseDto {

	private List<Object[]> response;

	public List<Object[]> getResponse() {
		return response;
	}

	public void setResponse(List<Object[]> response) {
		this.response = response;
	}

	
}
