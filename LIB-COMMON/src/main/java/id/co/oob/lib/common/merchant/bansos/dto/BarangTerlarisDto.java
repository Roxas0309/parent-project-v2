package id.co.oob.lib.common.merchant.bansos.dto;

import java.math.BigDecimal;

public class BarangTerlarisDto {

	private String barangTerlaris;
	private BigDecimal jumlahBarang;
	public String getBarangTerlaris() {
		return barangTerlaris;
	}
	public void setBarangTerlaris(String barangTerlaris) {
		this.barangTerlaris = barangTerlaris;
	}
	public BigDecimal getJumlahBarang() {
		return jumlahBarang;
	}
	public void setJumlahBarang(BigDecimal jumlahBarang) {
		this.jumlahBarang = jumlahBarang;
	}
	
	
}
