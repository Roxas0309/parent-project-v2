package id.co.oob.lib.common.merchant.onboarding.encryptor;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.KeyFactory;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.security.SignatureException;
import java.security.UnrecoverableKeyException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.HashMap;
import java.util.Map;

import org.apache.tomcat.util.codec.binary.Base64;

import com.google.gson.Gson;

import id.co.oob.lib.common.merchant.onboarding.throwable.NotAcceptanceDataException;
import id.co.oob.lib.common.merchant.onboarding.throwable.NotFoundDataException;

public class SHA256withRSAEncrypted {

	public static final String ALI_CLOUD = "ALI_CLOUD";
	public static final String LOCAL = "LOCAL";
	public static final String SIGNATURE = "signature";
	
    public static void main(String[] args) throws Exception {
    	  String message = "f3150daa-30ef-4206-a24d-d3b7f459c733|2020-12-14T11:24:00.000T+0700";
    	  String password = "mandiri1PUSAT";
    	  String alias = "mtiopenapi.yokke.co.id";
    	  String keyStoreUrl = "/home/iforce/Desktop/mtiopenapi.yokke.co.id.jks";
    	  Map<String, Object> map = mySignature(message, password, alias, keyStoreUrl, LOCAL);
    	  System.out.println(new Gson().toJson(map));
    }
    
    public static Map<String, Object> mySignature(String message, 
    		String keyPassword, String keyAlias, String keyStoreUrl, String deployMentFrom) throws 
    KeyStoreException, NoSuchAlgorithmException, CertificateException, 
    IOException, UnrecoverableKeyException, InvalidKeyException, 
    SignatureException{
    	
    	 FileInputStream is = null;
    	 if(deployMentFrom.equals(LOCAL)) {
    			is = new FileInputStream(keyStoreUrl);
    	 }
    	 else {
    		 throw new NotFoundDataException("Mohon Maaf Deployment From : " + deployMentFrom + " Tidak Ditemukan");
    	 }
         KeyStore keystore = KeyStore.getInstance(KeyStore.getDefaultType());
         String password = keyPassword;
         char[] passwd = password.toCharArray();
         keystore.load(is, passwd);
         String alias = keyAlias;
         Key key = keystore.getKey(alias, passwd);
         System.out.println("testing");
         String publicKeyString = null;
         if (key instanceof PrivateKey) {
           // Get certificate of public key
           Certificate cert = keystore.getCertificate(alias);
           // Get public key
           PublicKey publicKey = cert.getPublicKey();

           publicKeyString = Base64.encodeBase64String(publicKey
                     .getEncoded());
           System.out.println(publicKeyString);
           
           
           Signature privateSignature = Signature.getInstance("SHA256withRSA");
           privateSignature.initSign((PrivateKey) key);
          // String message = "f3150daa-30ef-4206-a24d-d3b7f459c733|2020-12-14T11:24:00.000T+0700";
           privateSignature.update(message.getBytes("UTF-8"));
           byte[] s = privateSignature.sign();
           String enc = Base64.encodeBase64String(s);
           System.err.println(enc);
           
           Map<String, Object> mappo = new HashMap<String, Object>();
           mappo.put("signature", enc);
           return mappo;
         }
         else {
         throw new NotAcceptanceDataException("Data Yang Diberikan Tidak Valid");
         }
    }
    
    public static Map<String, Object> mySignatureWithInputStream(String message, 
    		String keyPassword, String keyAlias, InputStream is) throws 
    KeyStoreException, NoSuchAlgorithmException, CertificateException, 
    IOException, UnrecoverableKeyException, InvalidKeyException, 
    SignatureException{
    	
         KeyStore keystore = KeyStore.getInstance(KeyStore.getDefaultType());
         String password = keyPassword;
         char[] passwd = password.toCharArray();
         keystore.load(is, passwd);
         String alias = keyAlias;
         Key key = keystore.getKey(alias, passwd);
         System.out.println("testing");
         String publicKeyString = null;
         if (key instanceof PrivateKey) {
           // Get certificate of public key
           Certificate cert = keystore.getCertificate(alias);
           // Get public key
           PublicKey publicKey = cert.getPublicKey();

           publicKeyString = Base64.encodeBase64String(publicKey
                     .getEncoded());
           System.out.println(publicKeyString);
           
           
           Signature privateSignature = Signature.getInstance("SHA256withRSA");
           privateSignature.initSign((PrivateKey) key);
          // String message = "f3150daa-30ef-4206-a24d-d3b7f459c733|2020-12-14T11:24:00.000T+0700";
           privateSignature.update(message.getBytes("UTF-8"));
           byte[] s = privateSignature.sign();
           String enc = Base64.encodeBase64String(s);
           System.err.println(enc);
           
           Map<String, Object> mappo = new HashMap<String, Object>();
           mappo.put("signature", enc);
           return mappo;
         }
         else {
         throw new NotAcceptanceDataException("Data Yang Diberikan Tidak Valid");
         }
    }

	
}
