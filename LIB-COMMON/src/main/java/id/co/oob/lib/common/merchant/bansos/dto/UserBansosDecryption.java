package id.co.oob.lib.common.merchant.bansos.dto;

public class UserBansosDecryption {

	private Long idSeq;
	private String userName;
	private String password;
	private String dateNow;
	
	
	public Long getIdSeq() {
		return idSeq;
	}
	public void setIdSeq(Long idSeq) {
		this.idSeq = idSeq;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getDateNow() {
		return dateNow;
	}
	public void setDateNow(String dateNow) {
		this.dateNow = dateNow;
	}
	
	
	
}
