package id.co.oob.lib.common.merchant.onboarding.dto.user;

public class UserOobValidasiInformasiRekening {

	private String namaPemilikUsaha;
	private String namaIbuKandung;
	private String nomorRekening;
	private String dob;
	private String nomorKtp;
	
	
	
	public String getNomorKtp() {
		return nomorKtp;
	}
	public void setNomorKtp(String nomorKtp) {
		this.nomorKtp = nomorKtp;
	}
	public String getNamaPemilikUsaha() {
		return namaPemilikUsaha;
	}
	public void setNamaPemilikUsaha(String namaPemilikUsaha) {
		this.namaPemilikUsaha = namaPemilikUsaha;
	}
	public String getNamaIbuKandung() {
		return namaIbuKandung;
	}
	public void setNamaIbuKandung(String namaIbuKandung) {
		this.namaIbuKandung = namaIbuKandung;
	}
	public String getNomorRekening() {
		return nomorRekening;
	}
	public void setNomorRekening(String nomorRekening) {
		this.nomorRekening = nomorRekening;
	}
	public String getDob() {
		return dob;
	}
	public void setDob(String dob) {
		this.dob = dob;
	}
	
	
	
}
