package id.co.oob.lib.common.merchant.onboarding.convert;

import java.util.List;

public class CalendarDto {

	public Integer bulan;
	public String bulanDesc;
	public List<CalendarDayDateDto> calendarDayDateDtos;
	public Integer tahun;
	public Integer getBulan() {
		return bulan;
	}
	public void setBulan(Integer bulan) {
		this.bulan = bulan;
	}
	public String getBulanDesc() {
		return bulanDesc;
	}
	public void setBulanDesc(String bulanDesc) {
		this.bulanDesc = bulanDesc;
	}
	public List<CalendarDayDateDto> getCalendarDayDateDtos() {
		return calendarDayDateDtos;
	}
	public void setCalendarDayDateDtos(List<CalendarDayDateDto> calendarDayDateDtos) {
		this.calendarDayDateDtos = calendarDayDateDtos;
	}
	public Integer getTahun() {
		return tahun;
	}
	public void setTahun(Integer tahun) {
		this.tahun = tahun;
	}
	
	
	
}
