package id.co.oob.lib.common.merchant.onboarding.file;

import java.io.InputStream;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;

import id.co.oob.lib.common.merchant.onboarding.throwable.InternalServerErrorException;

public class SftpConnection {

	static String SFTPHOST = "147.139.188.79";
	 // static String SFTPHOST = "172.16.200.100";
    // static String SFTPHOST = "172.16.56.100";
	
	public static ChannelSftp setupJsch() {
		
		int SFTPPORT = 22;
		String SFTPUSER = "user01";
		String SFTPPASS = "Mti123!@#";
		JSch jsch = new JSch();
		Session session = null;
		Channel channel = null;
		ChannelSftp channelSftp = null;
		try {
			session = jsch.getSession(SFTPUSER, SFTPHOST, SFTPPORT);
		} catch (JSchException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new InternalServerErrorException("Tidak dapat Login untuk masuk ke dalam sftp");
		}
		session.setPassword(SFTPPASS);
		java.util.Properties config = new java.util.Properties();
		config.put("StrictHostKeyChecking", "no");
		session.setConfig(config);
		try {
			session.connect();
		} catch (JSchException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new InternalServerErrorException("Tidak dapat koneksi ke sftp");
		}
		System.out.println("Host connected.");
		try {
			channel = session.openChannel("sftp");
		} catch (JSchException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			throw new InternalServerErrorException("Tidak dapat session untuk masuk ke dalam sftp");
		}
		try {
			channel.connect(10000);
		} catch (JSchException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new InternalServerErrorException("Tidak dapat konek untuk masuk ke dalam sftp");
		}
		System.out.println("sftp channel opened and connected.");
		channelSftp = (ChannelSftp) channel;
		return channelSftp;
	}
	
	public static String  setupJschMkdir(String pathDir) {
		int SFTPPORT = 22;
		String SFTPUSER = "user01";
		String SFTPPASS = "Mti123!@#";
		JSch jsch = new JSch();
		Session session = null;
		Channel channel = null;
		ChannelSftp channelSftp = null;
		try {
			session = jsch.getSession(SFTPUSER, SFTPHOST, SFTPPORT);
		} catch (JSchException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new InternalServerErrorException("Tidak dapat Login untuk masuk ke dalam sftp");
		}
		session.setPassword(SFTPPASS);
		java.util.Properties config = new java.util.Properties();
		config.put("StrictHostKeyChecking", "no");
		session.setConfig(config);
		try {
			session.connect();
		} catch (JSchException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new InternalServerErrorException("Tidak dapat koneksi ke sftp");
		}
		System.out.println("Host connected.");
		try {
			channel = session.openChannel("sftp");
		} catch (JSchException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			throw new InternalServerErrorException("Tidak dapat session untuk masuk ke dalam sftp");
		}
		try {
			channel.connect(10000);
		} catch (JSchException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new InternalServerErrorException("Tidak dapat konek untuk masuk ke dalam sftp");
		}
		
		// String dateNowInString = DateConverter.convertDateToString(new Date(),
		// "yyyyMMdd");
		channelSftp = (ChannelSftp) channel;
		String pathDate = pathDir;
		String path = pathDate;
		try {
			channelSftp.cd(pathDate);
		} catch (SftpException e1) {

			try {
				channelSftp.mkdir(path);
			} catch (SftpException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				throw new InternalServerErrorException("Tidak dapat membuat direktori " + path);
			}
		}

		path = path + "/oob";
		try {
			channelSftp.mkdir(path);
		} catch (SftpException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			// throw new InternalServerErrorException("Tidak dapat membuat direktori " +
			// path);
		}

		
		channelSftp.exit();
		session.disconnect();
		return path;
	}
	
	
	public static void setupJschV2(InputStream is, String dest) {
		int SFTPPORT = 22;
		String SFTPUSER = "user01";
		String SFTPPASS = "Mti123!@#";
		JSch jsch = new JSch();
		Session session = null;
		Channel channel = null;
		ChannelSftp channelSftp = null;
		try {
			session = jsch.getSession(SFTPUSER, SFTPHOST, SFTPPORT);
		} catch (JSchException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new InternalServerErrorException("Tidak dapat Login untuk masuk ke dalam sftp");
		}
		session.setPassword(SFTPPASS);
		java.util.Properties config = new java.util.Properties();
		config.put("StrictHostKeyChecking", "no");
		session.setConfig(config);
		try {
			session.connect();
		} catch (JSchException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new InternalServerErrorException("Tidak dapat koneksi ke sftp");
		}
		System.out.println("Host connected.");
		try {
			channel = session.openChannel("sftp");
		} catch (JSchException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			throw new InternalServerErrorException("Tidak dapat session untuk masuk ke dalam sftp");
		}
		try {
			channel.connect(10000);
		} catch (JSchException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new InternalServerErrorException("Tidak dapat konek untuk masuk ke dalam sftp");
		}
		System.out.println("sftp channel opened and connected.");
		channelSftp = (ChannelSftp) channel;
		try {
			channelSftp.put(is,dest);
		} catch (SftpException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new InternalServerErrorException(e.getMessage());
		}
		channelSftp.exit();
		session.disconnect();
	}
	
	public static void main(String[]args) {
			setupJsch();
	}
	
}
