package id.co.oob.lib.common.merchant.bansos.dto;

public class QuerySingleTransactionBansos {

	private String idPembeliNo;
	private String sessionUser;
	private String startDate;
	private String endDate;
	private String ktpId;
	
	
		
	public String getKtpId() {
		return ktpId;
	}
	public void setKtpId(String ktpId) {
		this.ktpId = ktpId;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public String getIdPembeliNo() {
		return idPembeliNo;
	}
	public void setIdPembeliNo(String idPembeliNo) {
		this.idPembeliNo = idPembeliNo;
	}
	public String getSessionUser() {
		return sessionUser;
	}
	public void setSessionUser(String sessionUser) {
		this.sessionUser = sessionUser;
	}
	
	
}
