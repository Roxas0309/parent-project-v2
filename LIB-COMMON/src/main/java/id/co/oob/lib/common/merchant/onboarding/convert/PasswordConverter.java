package id.co.oob.lib.common.merchant.onboarding.convert;

import java.util.regex.Pattern;

public class PasswordConverter {

	public static Boolean checkPasswordConf(String password) {
		
		if(password==null) {
			System.out.println("password tidak ada dikasih");
			return false;
		}
		
		if(password.length()<8 || password.length()>255) {
			System.out.println("panjang karakter password " + password + " lebih kecil dari 8 atau besar 255");
			return false;
		}
		
		if(!isContainString(password)) {
			return false;
		}
		
		if(!isPasswordContainNonValidChar(password)) {
			System.out.println("password " +password + " mengandung non valid character. ");
			return false;
		}
		
	    return true;
		
	}
	
	private static Boolean isPasswordContainNonValidChar(String str) {
		String regex = "^[a-zA-Z0-9]+$";
		 
		Pattern pattern = Pattern.compile(regex);
		return pattern.matcher(str).matches();
		
	}
	
	private static Boolean isContainString(String str) {
		
		   char ch;
		    boolean capitalFlag = false;
		    boolean lowerCaseFlag = false;
		    boolean numberFlag = false;
		
		 for(int i=0;i < str.length();i++) {
		        ch = str.charAt(i);
		        if( Character.isDigit(ch)) {
		            numberFlag = true;
		        }
		        else if (Character.isUpperCase(ch)) {
		            capitalFlag = true;
		        } else if (Character.isLowerCase(ch)) {
		            lowerCaseFlag = true;
		        }
		        if(numberFlag && capitalFlag && lowerCaseFlag)
		            return true;
		   }
		 System.out.println(" password " + str + " tidak tepat di isContainString.");
		 return false;
	}
	
	public static void main(String[] args) {
		System.out.println(checkPasswordConf("Roxas012/"));
	}
	
}
