package id.co.oob.lib.common.merchant.onboarding.dto.otp;

import java.util.Date;


public class OtpHandlerTableDto {
    private Long id; 
	private String userPurposed;
	private String otpValue;
	private String secretKey;
	private Integer wrongCounter;
	private String statusOtp;  /* Put Only 
								* ON_VALIDATION, 
	                            * CLEARED, 
	                            * WRONG_AND_INVALID, 
	                            * ERROR_ON_SENDING
	                            * AND EXPIRED
	                            */
    private Date createDate;
    private String otpOnMenu;
	
	public String getOtpOnMenu() {
		return otpOnMenu;
	}
	public void setOtpOnMenu(String otpOnMenu) {
		this.otpOnMenu = otpOnMenu;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getUserPurposed() {
		return userPurposed;
	}
	public void setUserPurposed(String userPurposed) {
		this.userPurposed = userPurposed;
	}
	public String getOtpValue() {
		return otpValue;
	}
	public void setOtpValue(String otpValue) {
		this.otpValue = otpValue;
	}
	public String getSecretKey() {
		return secretKey;
	}
	public void setSecretKey(String secretKey) {
		this.secretKey = secretKey;
	}
	public Integer getWrongCounter() {
		return wrongCounter;
	}
	public void setWrongCounter(Integer wrongCounter) {
		this.wrongCounter = wrongCounter;
	}
	public String getStatusOtp() {
		return statusOtp;
	}
	public void setStatusOtp(String statusOtp) {
		this.statusOtp = statusOtp;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
    
    
}
