package id.co.oob.lib.common.merchant.onboarding.dto.token;

public class AuthenticationSpiritMaker {
		private String authorization;
		private String xSignature;
		private String xTimestamp;
		public String getAuthorization() {
			return authorization;
		}
		public void setAuthorization(String authorization) {
			this.authorization = authorization;
		}
		public String getxSignature() {
			return xSignature;
		}
		public void setxSignature(String xSignature) {
			this.xSignature = xSignature;
		}
		public String getxTimestamp() {
			return xTimestamp;
		}
		public void setxTimestamp(String xTimestamp) {
			this.xTimestamp = xTimestamp;
		}
		
}
