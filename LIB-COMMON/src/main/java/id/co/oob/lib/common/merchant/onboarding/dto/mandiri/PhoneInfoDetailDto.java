package id.co.oob.lib.common.merchant.onboarding.dto.mandiri;

public class PhoneInfoDetailDto {
	
	private String phoneNumber;
	private String phoneSourceSystem;
	private String phoneNoType;
	private String phoneNoUpdateDate;
	
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getPhoneSourceSystem() {
		return phoneSourceSystem;
	}
	public void setPhoneSourceSystem(String phoneSourceSystem) {
		this.phoneSourceSystem = phoneSourceSystem;
	}
	public String getPhoneNoType() {
		return phoneNoType;
	}
	public void setPhoneNoType(String phoneNoType) {
		this.phoneNoType = phoneNoType;
	}
	public String getPhoneNoUpdateDate() {
		return phoneNoUpdateDate;
	}
	public void setPhoneNoUpdateDate(String phoneNoUpdateDate) {
		this.phoneNoUpdateDate = phoneNoUpdateDate;
	}
	
	
}
