package id.co.oob.lib.common.merchant.bansos.dto;

import java.math.BigDecimal;


public class BarangJualanBansosEditDto {

	private Long idData;
	
	private String jenisBarang;
	
	private String merk;
	
	private String satuanBarang;
	
	private Long beratBarangJualan;
	
	private String kategori;
	
	private Long beratBarang;
	
	private BigDecimal hargaBarangInValue;
	
	private String hargaBarangInRupiah;
	
	private String kodeBarang;
	
	private Integer banyakDibeli;
	
	private Boolean isNew = false;
	
	
	

	public Boolean getIsNew() {
		return isNew;
	}

	public void setIsNew(Boolean isNew) {
		this.isNew = isNew;
	}

	public Long getBeratBarangJualan() {
		return beratBarangJualan;
	}

	public void setBeratBarangJualan(Long beratBarangJualan) {
		this.beratBarangJualan = beratBarangJualan;
	}

	public String getKategori() {
		return kategori;
	}

	public void setKategori(String kategori) {
		this.kategori = kategori;
	}

	public Long getBeratBarang() {
		return beratBarang;
	}

	public void setBeratBarang(Long beratBarang) {
		this.beratBarang = beratBarang;
	}

	public Long getIdData() {
		return idData;
	}

	public void setIdData(Long idData) {
		this.idData = idData;
	}

	public BigDecimal getHargaBarangInValue() {
		return hargaBarangInValue;
	}

	public void setHargaBarangInValue(BigDecimal hargaBarangInValue) {
		this.hargaBarangInValue = hargaBarangInValue;
	}

	public String getKodeBarang() {
		return kodeBarang;
	}

	public void setKodeBarang(String kodeBarang) {
		this.kodeBarang = kodeBarang;
	}

	public Integer getBanyakDibeli() {
		return banyakDibeli;
	}

	public void setBanyakDibeli(Integer banyakDibeli) {
		this.banyakDibeli = banyakDibeli;
	}

	public String getJenisBarang() {
		return jenisBarang;
	}

	public void setJenisBarang(String jenisBarang) {
		this.jenisBarang = jenisBarang;
	}

	public String getMerk() {
		return merk;
	}

	public void setMerk(String merk) {
		this.merk = merk;
	}

	public String getSatuanBarang() {
		return satuanBarang;
	}

	public void setSatuanBarang(String satuanBarang) {
		this.satuanBarang = satuanBarang;
	}

	public String getHargaBarangInRupiah() {
		return hargaBarangInRupiah;
	}

	public void setHargaBarangInRupiah(String hargaBarangInRupiah) {
		this.hargaBarangInRupiah = hargaBarangInRupiah;
	}


	
}
