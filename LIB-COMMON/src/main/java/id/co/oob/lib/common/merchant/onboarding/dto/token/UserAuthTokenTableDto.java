package id.co.oob.lib.common.merchant.onboarding.dto.token;

public class UserAuthTokenTableDto {
	private String idToken;

	public String getIdToken() {
		return idToken;
	}

	public void setIdToken(String idToken) {
		this.idToken = idToken;
	}
	
}
