package id.co.oob.lib.common.merchant.onboarding.dto.mandiri.riwayat;

import java.util.List;

public class RiwayatTransaksiHdrDto {

	private String sumAllTransaksi;
	private String sumAllTransaksiAfterFee;
	private Integer countAllTransaksi;
	private String startDate;
	private String startDateWithoutYear;
	private String endDate;
	private String endDateWithoutYear;
	private String mid;
	private Long idOob;
	private List<RiwayatTransaksiDtlDto> detail;
	
	
	
	public String getStartDateWithoutYear() {
		return startDateWithoutYear;
	}
	public void setStartDateWithoutYear(String startDateWithoutYear) {
		this.startDateWithoutYear = startDateWithoutYear;
	}
	public String getEndDateWithoutYear() {
		return endDateWithoutYear;
	}
	public void setEndDateWithoutYear(String endDateWithoutYear) {
		this.endDateWithoutYear = endDateWithoutYear;
	}
	public String getSumAllTransaksiAfterFee() {
		return sumAllTransaksiAfterFee;
	}
	public void setSumAllTransaksiAfterFee(String sumAllTransaksiAfterFee) {
		this.sumAllTransaksiAfterFee = sumAllTransaksiAfterFee;
	}
	public String getSumAllTransaksi() {
		return sumAllTransaksi;
	}
	public void setSumAllTransaksi(String sumAllTransaksi) {
		this.sumAllTransaksi = sumAllTransaksi;
	}
	
	public Integer getCountAllTransaksi() {
		return countAllTransaksi;
	}
	public void setCountAllTransaksi(Integer countAllTransaksi) {
		this.countAllTransaksi = countAllTransaksi;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public String getMid() {
		return mid;
	}
	public void setMid(String mid) {
		this.mid = mid;
	}
	
	public Long getIdOob() {
		return idOob;
	}
	public void setIdOob(Long idOob) {
		this.idOob = idOob;
	}
	public List<RiwayatTransaksiDtlDto> getDetail() {
		return detail;
	}
	public void setDetail(List<RiwayatTransaksiDtlDto> detail) {
		this.detail = detail;
	}
	
	
	
}
