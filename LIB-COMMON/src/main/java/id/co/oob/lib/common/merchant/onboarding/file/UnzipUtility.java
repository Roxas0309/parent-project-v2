package id.co.oob.lib.common.merchant.onboarding.file;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import javax.xml.bind.DatatypeConverter;

import com.google.gson.Gson;
public class UnzipUtility {
	
    private static final int BUFFER_SIZE = 4096;

//    public static void main(String[] args) {
//    	try {
//    		for (InputStream is : unzipInList(SnippetBase64.base64Zip)) {
//				System.out.println(new Gson().toJson(is));
//			}
//			//unzip(SnippetBase64.base64Zip, "/home/iforce/Desktop/zip_testing/new");
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//    }
//    
	public static byte[] getBase64File(String base64) {
		byte[] fileData = DatatypeConverter.parseBase64Binary(base64.substring(base64.indexOf(",") + 1));
		return fileData;
	}
    
    public static void unzip(String base64Zip, String destDirectory) throws IOException {
        File destDir = new File(destDirectory);
        if (!destDir.exists()) {
            destDir.mkdir();
        }
        
        InputStream in = new ByteArrayInputStream(getBase64File(base64Zip));
        ZipInputStream zipIn = new ZipInputStream(in);
        ZipEntry entry = zipIn.getNextEntry();
        while (entry != null) {
            String filePath = destDirectory + File.separator + entry.getName();
            if (!entry.isDirectory()) {
                extractFile(zipIn, filePath);
            } else {
                File dir = new File(filePath);
                dir.mkdirs();
            }
            zipIn.closeEntry();
            entry = zipIn.getNextEntry();
        }
        zipIn.close();
    }
    
    public static List<InputStream> unzipInList(String base64Zip) throws IOException {
    	List<InputStream> ins = new ArrayList<InputStream>();
        InputStream in = new ByteArrayInputStream(getBase64File(base64Zip));
        ZipInputStream zipIn = new ZipInputStream(in);
        ins = getInputStream(zipIn);
        zipIn.close();
        return ins;
    }
    
    
    private static void extractFile(ZipInputStream zipIn, String filePath) throws IOException {
        BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(filePath));
        byte[] bytesIn = new byte[BUFFER_SIZE];
        int read = 0;
        while ((read = zipIn.read(bytesIn)) != -1) {
            bos.write(bytesIn, 0, read);
        }
        bos.close();
    }
    
    static List<InputStream> getInputStream(ZipInputStream zipIn) throws IOException {
      
        List<InputStream> inputStreams = new ArrayList<InputStream>();
        for (ZipEntry e; (e = zipIn.getNextEntry()) != null;) {
        	inputStreams.add(zipIn);
        }
        
        return inputStreams;
    }
}
