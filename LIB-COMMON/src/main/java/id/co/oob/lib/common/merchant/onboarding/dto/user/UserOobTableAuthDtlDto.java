package id.co.oob.lib.common.merchant.onboarding.dto.user;

import java.util.Date;

public class UserOobTableAuthDtlDto {

    private Long idUserOob;
	
	private String password;
	
	private Integer wrongPasswordCount; 
	
	private Integer isLocked; 
	
	private Date createDate;
	
	private Date lastUpdateDate;
	
	private String lastUpdateBy;
	
	private Boolean isFirstLogin;

	public Long getIdUserOob() {
		return idUserOob;
	}

	public void setIdUserOob(Long idUserOob) {
		this.idUserOob = idUserOob;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Integer getWrongPasswordCount() {
		return wrongPasswordCount;
	}

	public void setWrongPasswordCount(Integer wrongPasswordCount) {
		this.wrongPasswordCount = wrongPasswordCount;
	}

	public Integer getIsLocked() {
		return isLocked;
	}

	public void setIsLocked(Integer isLocked) {
		this.isLocked = isLocked;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getLastUpdateDate() {
		return lastUpdateDate;
	}

	public void setLastUpdateDate(Date lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
	}

	public String getLastUpdateBy() {
		return lastUpdateBy;
	}

	public void setLastUpdateBy(String lastUpdateBy) {
		this.lastUpdateBy = lastUpdateBy;
	}

	public Boolean getIsFirstLogin() {
		return isFirstLogin;
	}

	public void setIsFirstLogin(Boolean isFirstLogin) {
		this.isFirstLogin = isFirstLogin;
	}
	
	
}
