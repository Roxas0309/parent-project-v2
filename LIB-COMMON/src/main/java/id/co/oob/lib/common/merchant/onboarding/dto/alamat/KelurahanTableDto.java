package id.co.oob.lib.common.merchant.onboarding.dto.alamat;

public class KelurahanTableDto {
	private Long id; 
	
	private Long id_kecamatan; 
	
	private String nama;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getId_kecamatan() {
		return id_kecamatan;
	}

	public void setId_kecamatan(Long id_kecamatan) {
		this.id_kecamatan = id_kecamatan;
	}

	public String getNama() {
		return nama;
	}

	public void setNama(String nama) {
		this.nama = nama;
	}
	
	
	
}
