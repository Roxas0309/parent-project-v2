package id.co.oob.lib.common.merchant.onboarding.dto.mandiri;

public class CustomerCifInquiryByNikDto {
	private String responseCode;
	private String responseMessage;
	private String cifNumber;
	public String getResponseCode() {
		return responseCode;
	}
	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}
	public String getResponseMessage() {
		return responseMessage;
	}
	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}
	public String getCifNumber() {
		return cifNumber;
	}
	public void setCifNumber(String cifNumber) {
		this.cifNumber = cifNumber;
	}
	
	
}
