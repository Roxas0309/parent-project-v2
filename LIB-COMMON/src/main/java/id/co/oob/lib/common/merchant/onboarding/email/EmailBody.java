package id.co.oob.lib.common.merchant.onboarding.email;

import java.util.Date;

import id.co.oob.lib.common.merchant.onboarding.file.LogStatusEmailDto;

public class EmailBody {

//	public static void bodyForPendaftaranUsahaMandiri(String userName,
//			 String nomorPendaftaran ,String email, String urlStatus) {
//		String body = "";
//		
//		body = "<div> Hai " + userName + ", </div>"
//				+ " <br><br>"
//		        + "<div> Terima kasih telah melakukan Pendaftaran Usaha lewat Aplikasi Pendaftaran MORIS.</div>"
//		        + " <br>"
//		        + "<div> Nomor Pendaftaran Anda adalah "+nomorPendaftaran+" </div>"
//		        + " </p>"
//		        + "Silahkan cek status pendaftaran di :"
//		        + " <a href=\""+urlStatus+"\"> "+urlStatus+" </a>"
//		        ;
//		
//		EmailContent.sendMailTest(email, "Pendaftaran Usaha Mandiri", body, null,null);
//		
//		
//	}
	
	public static LogStatusEmailDto bodyForPendaftaranUsahaMandiri(String userName,
			 String nomorPendaftaran ,String email, String urlStatus) {
		String body = "";
		
		body = "<div> Hai " + userName + ", </div>"
				+ " <br><br>"
		        + "<div> Terima kasih telah melakukan Pendaftaran Usaha lewat Aplikasi Pendaftaran MORIS.</div>"
		        + " <br>"
		        + "<div> Nomor Pendaftaran Anda adalah "+nomorPendaftaran+" </div>"
		        + " </p>"
		        + "Silahkan cek status pendaftaran di :"
		        + " <a href=\""+urlStatus+"\"> "+urlStatus+" </a>"
		        ;
	
		EmailContent.sendMailTest(email, "Pendaftaran Usaha Mandiri", body, null,null);
		LogStatusEmailDto logStatusEmailDto = new LogStatusEmailDto();
    	logStatusEmailDto.setLogStatus("OK");
    	
    	logStatusEmailDto.setLogSubject(body);
    	logStatusEmailDto.setLogTujuan(email);
    	logStatusEmailDto.setLogUser(userName);
		return logStatusEmailDto;
	}
	
//	public static void bodyPerubahanPasswordMitraQris(String userNameHandphone,
//			 String namaNasabah ,String email,String passwordSementara, String urlUbahPassword) {
//		String body = "";
//		
//		body = "<div> Hai " + namaNasabah + ", </div>"
//				+ " <br>"
//		        + "<div> Untuk mengubah password, silakan klik link: </div>"
//		        + " <a href=\""+urlUbahPassword+"\"> "+urlUbahPassword+" </a>"
//		        + " <br>"
//		        + " <br>"
//		        + " <div> dan login dengan password sementara berikut: </div> "
//		        + " <b> username : </b> " + userNameHandphone
//		        + " <br>"
//		        + " <b> password sementara : </b> " + passwordSementara
//		        + " <br>"
//		        + " <br>"
//		        + " <div>Link dan password sementara ini berlaku selama 2x24 jam. Setelah login, mohon ubah password sesuai dengan kriteria keamanan kami dan mudah Anda ingat.</div>"
//		        + " <br></br>"
//		        + " Hubungi kami di 14002 jika Anda mengalami kendala pada proses ini."
//		        + " </p>"
//		        + " <br>"
//		        + " <br>"
//		        + " <div>Salam, </div>"
//		        + " <div>Bank Mandiri</div>"
//		        ;
//		EmailContent.sendMailTest(email, "Perubahan Password MORIS", body, null,null);
//		
////		LogStatusEmailDto logStatusEmailDto = new LogStatusEmailDto();
////    	logStatusEmailDto.setLogStatus("OK");
////    	logStatusEmailDto.setLogSubject(body);
////    	logStatusEmailDto.setLogTujuan(email);
////    	logStatusEmailDto.setLogUser(userNameHandphone);
////		return logStatusEmailDto;
//		
//	}
	
	public static LogStatusEmailDto bodyPerubahanPasswordMitraQris(String userNameHandphone,
			 String namaNasabah ,String email,String passwordSementara, String urlUbahPassword) {
		String body = "";
		body = "<div> Hai " + namaNasabah + ", </div>"
				+ " <br>"
		        + "<div> Untuk mengubah password, silakan klik link: </div>"
		        + " <a href=\""+urlUbahPassword+"\"> "+urlUbahPassword+" </a>"
		        + " <br>"
		        + " <br>"
		        + " <div> dan login dengan user name berikut: </div> "
		        + " <b> username : </b> " + userNameHandphone
		        + " <br>"
		        + " <br>"
		        + " <div>Link ini berlaku selama 2x24 jam. Setelah berhasil mengganti password, "
		        + " Anda dapat menggunakan username dan password yang terbaru untuk login.</div>"
		        + " <br></br>"
		        + " Hubungi kami di 14002 jika Anda mengalami kendala pada proses ini."
		        + " </p>"
		        + " <br>"
		        + " <br>"
		        + " <div>Salam, </div>"
		        + " <div>Bank Mandiri</div>"
		        ;
		EmailContent.sendMailTest(email, "Perubahan Password MORIS", body, null,null);
		LogStatusEmailDto logStatusEmailDto = new LogStatusEmailDto();
   	logStatusEmailDto.setLogStatus("OK");
   	logStatusEmailDto.setLogSubject(body);
   	logStatusEmailDto.setLogTujuan(email);
   	logStatusEmailDto.setLogUser(userNameHandphone);
		return logStatusEmailDto;
		
	}
	
//	public static void bodyForSuccessPendaftaran(String namaNasabah,String userName, String passwordSementara
//			,String email, String loginUrl, String gantiPasswordUrl) {
//		String body = "";
//		
//		body = "<div> Hai " + namaNasabah + ", </div>"
//				+ " <br><br>"
//		        + "<div>Proses Pendaftaran MORIS berhasil diproses. Klik link berikut untuk mengakses akun Anda: </div>"
//		        + " <br>"
//		        + " <a href=\""+loginUrl+"\"> "+loginUrl+" </a>"
//		        + " <br><br>"
//		        + " <div> dan login dengan password sementara berikut: </div> "
//		        + " <b> username : </b> " + userName
//		        + " <br>"
//		        + " <b> password sementara : </b> " + passwordSementara
//		        + " <br>"
//		        + " <br>"
//		        + " <div>"
//		        + " Link dan password sementara ini berlaku selama 2x24 jam. Setelah login,"
//		        + " mohon ubah password sesuai dengan kriteria keamanan kami dan mudah Anda ingat. "
//		        + " </div>"
//		        + " <br>"
//		        + " <br>"
//		        + " <div>Klik link berikut jika password sementara tidak berlaku: </div>"
//		        + " <a href=\""+gantiPasswordUrl+"\"> "+gantiPasswordUrl+" </a>"
//		        ;
//		EmailContent.sendMailTest(email, "Pendaftaran Usaha Mandiri", body, null,null);
//	}
	
	public static LogStatusEmailDto bodyForSuccessPendaftaranSementara(String userName,
			String namaNasabah, String passwordSementara
			,String email, String loginUrl, String gantiPasswordUrl) {
		String body = "";
		
		body = "<div> Hai " + namaNasabah + ", </div>"
				+ " <br><br>"
		        + "<div>Untuk password sementara yang tidak berlaku. Klik link berikut untuk mengakses akun Anda: </div>"
		        + " <br>"
		        + " <a href=\""+loginUrl+"\"> "+loginUrl+" </a>"
		        + " <br><br>"
		        + " <div> dan login dengan password sementara berikut: </div> "
		        + " <b> username : </b> " + userName
		        + " <br>"
		        + " <b> password sementara : </b> " + passwordSementara
		        + " <br>"
		        + " <br>"
		        + " <div>"
		        + " Link dan password sementara ini berlaku selama 2x24 jam. Setelah login,"
		        + " mohon ubah password sesuai dengan kriteria keamanan kami dan mudah Anda ingat. "
		        + " </div>"
		        + " <br>"
		        + " <br>"
		        + " <div>Klik link berikut jika password sementara tidak berlaku: </div>"
		        + " <a href=\""+gantiPasswordUrl+"\"> "+gantiPasswordUrl+" </a>"
		        ;
		EmailContent.sendMailTest(email, "Pendaftaran Usaha Mandiri", body, null,null);
		LogStatusEmailDto logStatusEmailDto = new LogStatusEmailDto();
	   	logStatusEmailDto.setLogStatus("OK");
	   	logStatusEmailDto.setLogSubject(body);
	   	logStatusEmailDto.setLogTujuan(email);
	   	logStatusEmailDto.setLogUser(namaNasabah);
		return logStatusEmailDto;
	}
	
	public static LogStatusEmailDto bodyForSuccessPendaftaran(String namaNasabah,String userName, String passwordSementara
			,String email, String loginUrl, String gantiPasswordUrl) {
		String body = "";
		
		body = "<div> Hai " + namaNasabah + ", </div>"
				+ " <br><br>"
		        + "<div>Proses Pendaftaran MORIS berhasil diproses. Klik link berikut untuk mengakses akun Anda: </div>"
		        + " <br>"
		        + " <a href=\""+loginUrl+"\"> "+loginUrl+" </a>"
		        + " <br><br>"
		        + " <div> dan login dengan password sementara berikut: </div> "
		        + " <b> username : </b> " + userName
		        + " <br>"
		        + " <b> password sementara : </b> " + passwordSementara
		        + " <br>"
		        + " <br>"
		        + " <div>"
		        + " Link dan password sementara ini berlaku selama 2x24 jam. Setelah login,"
		        + " mohon ubah password sesuai dengan kriteria keamanan kami dan mudah Anda ingat. "
		        + " </div>"
		        + " <br>"
		        + " <br>"
		        + " <div>Klik link berikut jika password sementara tidak berlaku: </div>"
		        + " <a href=\""+gantiPasswordUrl+"\"> "+gantiPasswordUrl+" </a>"
		        ;
		EmailContent.sendMailTest(email, "Pendaftaran Usaha Mandiri", body, null,null);
		LogStatusEmailDto logStatusEmailDto = new LogStatusEmailDto();
	   	logStatusEmailDto.setLogStatus("OK");
	   	logStatusEmailDto.setLogSubject(body);
	   	logStatusEmailDto.setLogTujuan(email);
	   	logStatusEmailDto.setLogUser(namaNasabah);
		return logStatusEmailDto;
	}
	
	public static void main(String[] args) {
		bodyPerubahanPasswordMitraQris("087768248230", "Bima Satrya Sebayang", "bimasebayang11@gmail.com", 
				"0sokdu", "https://devoob.yokke.co.id/login");
//		bodyForSuccessPendaftaran("Bima Satrya Sebayang", "087768248230", "Roxas0309.", "bimasebayang12@gmail.com", 
//				"https://devoob.yokke.co.id/login", "https://devoob.yokke.co.id/makePassword");
//		bodyForPendaftaranUsahaMandiri("Bima Satrya Sebayang","0000001","bimasebayang11@gmail.com",
//				"https://devoob.yokke.co.id/cekStatus");
	}
	
}
