package id.co.oob.lib.common.merchant.onboarding.convert;

import com.google.common.base.Strings;

import id.co.oob.lib.common.merchant.onboarding.throwable.InternalServerErrorException;
import id.co.oob.lib.common.merchant.onboarding.throwable.NotFoundDataException;

public class CharacterRemoval {

	public static String makeNumber(Integer number) {
		String numberInStr = Integer.toString(number);
		if(numberInStr.length()==1) {
			numberInStr = "0"+numberInStr;
		}
		return numberInStr;
	}
	
	
	
	public static String removeAllCharForNewAddress(String words) {
		if(Strings.isNullOrEmpty(words)) {
			return words;
		}
		words = words.replaceAll("[^a-zA-Z0-9-@_.,/ ():]", "");
		String[] splitW = words.split(" ");
		words = "";
		for (String sw : splitW) {
			//System.out.println("sw : " + sw.trim());
			if(!Strings.isNullOrEmpty(sw.trim()))
			words += sw.trim()+" ";
		}
		return words.trim();
	}
	
	public static String removeAllCharForNewUsaha(String words) {
		if(Strings.isNullOrEmpty(words)) {
			return words;
		}
		words = words.replaceAll("[^a-zA-Z0-9 ]", "");
		String[] splitW = words.split(" ");
		words = "";
		for (String sw : splitW) {
			//System.out.println("sw : " + sw.trim());
			if(!Strings.isNullOrEmpty(sw.trim()))
			words += sw.trim()+" ";
		}
		return words.trim();
	}
	
	
	
	public static String removeAllChar(String words) {
		String wordAfterRemove = "";
		
		for(int i = 0 ; i< words.toCharArray().length; i++) {
			if(Character.isAlphabetic(words.charAt(i)) || Character.isDigit(words.charAt(i))) {
				wordAfterRemove += Character.toString(words.charAt(i));
			}
		} 
		
		return wordAfterRemove;
	}
	
	public static String maskingValueCpan(String value) {
		String masking = "";
		for (int i = value.length()-1; i >=value.length()-3 ; i--) {
			masking += value.charAt(i);
		}
		
		return "**"+reverseString(masking);
	}
	
	protected static String reverseString(String strm) {
		String val = "";
		for(int i = strm.length()-1; i>=0; i--) {
			val = val + strm.charAt(i);
		}
		return val;
	}
	
	public static Long removeStaticZero(String number, String ifError) {
		Long long1 = null;
		try {
			long1 = new Long(number);
		}catch(NumberFormatException next) {
			throw new NotFoundDataException(ifError);
		}
		
		return long1;
	}
	
	//data:image/png;base64
	public static String getFormatRemoveRest(String base64) {
		if(base64==null) {
			throw new InternalServerErrorException("Base 64 Tidak Dapat Ditemukan [internal_server_error].");
		}
		
		String format = "";
		
		String[] regexVal = base64.split(";");
		if(regexVal==null) {
			throw new InternalServerErrorException("Format Yang Diberikan Bukan Base 64 "
					+ "[internal_server_error : regexVal==null ].") ;
		}
		
		if(regexVal.length!=2) {
			throw new InternalServerErrorException("Format Yang Diberikan Bukan Base 64 "
					+ "[internal_server_error : regexVal.length!=2].");
		}
		
		String[] regexNew = regexVal[0].split(":");
		if(regexNew==null) {
			throw new InternalServerErrorException("Format Yang Diberikan Bukan Base 64 "
					+ "[internal_server_error : regexNew==null].");
		}
		
		if(regexNew.length!=2) {
			throw new InternalServerErrorException("Format Yang Diberikan Bukan Base 64 "
					+ " [internal_server_error : regexNew.length!=2].");
		}
		
		String[] formatNew = regexNew[1].split("/");
		if(formatNew==null) {
			throw new InternalServerErrorException("Format Yang Diberikan Bukan Base 64 "
					+ " [internal_server_error : formatNew==null].");
		}
		
		if(formatNew.length!=2) {
			throw new InternalServerErrorException("Format Yang Diberikan Bukan Base 64 "
					+ " [internal_server_error : formatNew.length!=2].");
		}
		
		
		return "."+formatNew[1];
	}
	
	
	public static void main(String[] args) {
		String allCh = removeAllCharForNewUsaha("Perdana710, Mentri");
		System.out.println(allCh);
	}
	
	
}
