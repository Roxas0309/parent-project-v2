package id.co.oob.lib.common.merchant.onboarding.dto.user;

import java.util.Date;

public class LinkChangePassDto {

	private Long id;
	private String date;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	
	
	
}
