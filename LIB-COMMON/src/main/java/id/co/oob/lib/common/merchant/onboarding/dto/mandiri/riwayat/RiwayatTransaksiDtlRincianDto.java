package id.co.oob.lib.common.merchant.onboarding.dto.mandiri.riwayat;

import java.math.BigDecimal;

public class RiwayatTransaksiDtlRincianDto {
	private String number;
	private Boolean isTransferToRek = false;
	private String transferAmount;
	private BigDecimal transferAmountNumber;
	private String feeAmount;
	private BigDecimal feeAmountNumber;
	private String authAmount;
	private BigDecimal authAmountNumber;
	private String percentageFeeAmount;
	private String percentageFeeAmountNumber;
	private String reffNumber;
	private String issuerName;
	private String customerName;
	private String mpan;
	private String tid;
	private String cpan;
	private String authDateTime;
	private String timeDataChange;
	private String settleDate;
	
	
	
	
	
	public BigDecimal getTransferAmountNumber() {
		return transferAmountNumber;
	}
	public void setTransferAmountNumber(BigDecimal transferAmountNumber) {
		this.transferAmountNumber = transferAmountNumber;
	}
	public BigDecimal getFeeAmountNumber() {
		return feeAmountNumber;
	}
	public void setFeeAmountNumber(BigDecimal feeAmountNumber) {
		this.feeAmountNumber = feeAmountNumber;
	}
	public BigDecimal getAuthAmountNumber() {
		return authAmountNumber;
	}
	public void setAuthAmountNumber(BigDecimal authAmountNumber) {
		this.authAmountNumber = authAmountNumber;
	}
	
	
	public String getPercentageFeeAmountNumber() {
		return percentageFeeAmountNumber;
	}
	public void setPercentageFeeAmountNumber(String percentageFeeAmountNumber) {
		this.percentageFeeAmountNumber = percentageFeeAmountNumber;
	}
	public String getSettleDate() {
		return settleDate;
	}
	public void setSettleDate(String settleDate) {
		this.settleDate = settleDate;
	}
	public String getTimeDataChange() {
		return timeDataChange;
	}
	public void setTimeDataChange(String timeDataChange) {
		this.timeDataChange = timeDataChange;
	}
	public String getAuthDateTime() {
		return authDateTime;
	}
	public void setAuthDateTime(String authDateTime) {
		this.authDateTime = authDateTime;
	}
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	public Boolean getIsTransferToRek() {
		return isTransferToRek;
	}
	public void setIsTransferToRek(Boolean isTransferToRek) {
		this.isTransferToRek = isTransferToRek;
	}
	public String getTransferAmount() {
		return transferAmount;
	}
	public void setTransferAmount(String transferAmount) {
		this.transferAmount = transferAmount;
	}
	public String getFeeAmount() {
		return feeAmount;
	}
	public void setFeeAmount(String feeAmount) {
		this.feeAmount = feeAmount;
	}
	public String getAuthAmount() {
		return authAmount;
	}
	public void setAuthAmount(String authAmount) {
		this.authAmount = authAmount;
	}
	public String getPercentageFeeAmount() {
		return percentageFeeAmount;
	}
	public void setPercentageFeeAmount(String percentageFeeAmount) {
		this.percentageFeeAmount = percentageFeeAmount;
	}
	public String getReffNumber() {
		return reffNumber;
	}
	public void setReffNumber(String reffNumber) {
		this.reffNumber = reffNumber;
	}
	public String getIssuerName() {
		return issuerName;
	}
	public void setIssuerName(String issuerName) {
		this.issuerName = issuerName;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getMpan() {
		return mpan;
	}
	public void setMpan(String mpan) {
		this.mpan = mpan;
	}
	public String getTid() {
		return tid;
	}
	public void setTid(String tid) {
		this.tid = tid;
	}
	public String getCpan() {
		return cpan;
	}
	public void setCpan(String cpan) {
		this.cpan = cpan;
	}
	
}
