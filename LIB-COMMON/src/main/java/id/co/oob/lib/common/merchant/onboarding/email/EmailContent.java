package id.co.oob.lib.common.merchant.onboarding.email;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import id.co.oob.lib.common.merchant.onboarding.throwable.InternalServerErrorException;

public class EmailContent {

	private static String SMTP_SERVER_EMAIL = "mail.yokke.co.id";
	private static String PORT_EMAIL = "465";
	private static String USERNAME_EMAIL = "no-reply@yokke.co.id";
	private static String PASSWORD_EMAIL = "Mti123!@#";
	
//	private static String SMTP_SERVER_EMAIL = "smtp.gmail.com";
//	private static String PORT_EMAIL = "465";
//	private static String USERNAME_EMAIL = "bimasebayang12@gmail.com";
//	private static String PASSWORD_EMAIL = "Roxas0309.";
	
	public static void main(String[] args) {
		sendMailTest("bimasebayang11@gmail.com", 
				"Pendaftaran Usaha Mandiri", "This is only for mail testing", null, null);
	}
	
	
	public static boolean sendMailTest(String to, String subject, String body, String attachment,
			Map<String, String> mapInlineImages) {
    	Properties props = new Properties();
    	System.err.println("start send-email on : " + new Date());
		// final String username_from = "digirec.ifwd@fwd.co.id";
		//Bankmega7*
		
		System.err.println("pass : " + USERNAME_EMAIL + " user : " + PASSWORD_EMAIL);
	
		props.put("mail.smtp.auth", "true");
		props.setProperty("mail.transport.protocol", "smtp");
		props.setProperty("mail.smtp.quitwait", "false");
		props.setProperty("mail.host",SMTP_SERVER_EMAIL);
		props.put("mail.smtp.socketFactory.fallback", "false");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", SMTP_SERVER_EMAIL);
		props.put("mail.smtp.port", PORT_EMAIL);
		props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");

		
		final String userNameFinal = USERNAME_EMAIL;
		final String userPasswordFinal = PASSWORD_EMAIL;
		Session session = Session.getInstance(props, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(userNameFinal, userPasswordFinal);
			}
		});

		try {

			// creates a new e-mail message
			MimeMessage msg = new MimeMessage(session);

			System.err.println("user Internet address : userNameFinal : "  
			 + userNameFinal  + " username : " + USERNAME_EMAIL);
			try { 
				msg.setFrom(new InternetAddress(userNameFinal,userNameFinal));
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				throw new InternalServerErrorException(e.getMessage());
				
			}
			
			
			// msg.setFrom(new InternetAddress(username_from));
			InternetAddress[] toAddresses = { new InternetAddress(to) };
			msg.setRecipients(Message.RecipientType.TO, toAddresses);
			msg.setSubject(subject);
			msg.setSentDate(new Date());

			// creates message part
			MimeBodyPart messageBodyPart = new MimeBodyPart();
			messageBodyPart.setContent(body, "text/html");

			// creates multi-part
			Multipart multipart = new MimeMultipart();
			multipart.addBodyPart(messageBodyPart);

			// adds inline image attachments
			if (mapInlineImages != null && mapInlineImages.size() > 0) {
				Set<String> setImageID = mapInlineImages.keySet();

				for (String contentId : setImageID) {
					MimeBodyPart imagePart = new MimeBodyPart();
					imagePart.setHeader("Content-ID", "<" + contentId + ">");
					imagePart.setDisposition(MimeBodyPart.INLINE);

					String imageFilePath = mapInlineImages.get(contentId);
					try {
						imagePart.attachFile(imageFilePath);
					} catch (IOException ex) {
						ex.printStackTrace();
						throw new InternalServerErrorException(ex.getMessage());
					}

					multipart.addBodyPart(imagePart);
				}
			}
			msg.setContent(multipart);
			System.err.println("masuk kesini");
			Transport.send(msg);
			System.out.println("sending mail success to : " + to);
			System.out.println("Done");

			return true;
		} catch (MessagingException e) {
			e.printStackTrace();
			//throw new InternalServerErrorException(e.getMessage());
		}
		
		return true;
	}
	
}
