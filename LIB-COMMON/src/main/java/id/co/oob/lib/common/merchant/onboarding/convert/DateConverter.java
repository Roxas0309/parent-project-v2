package id.co.oob.lib.common.merchant.onboarding.convert;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import id.co.oob.lib.common.merchant.onboarding.throwable.InternalServerErrorException;

public class DateConverter {

	public static String convertDateToString(Date date, String format) {
//		System.out.println("your date want to convert " + date + " with format " + format);
		DateFormat dateFormat = new SimpleDateFormat(format);
		String formattedDate =dateFormat.format(date);
		return formattedDate;
	}
	
	public static Date convertStringToDate(String date, String format) {
//		System.out.println("your date want to convert " + date + " with format " + format);
		DateFormat dateFormat = new SimpleDateFormat(format);
		try {
			return dateFormat.parse(date);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return  null;
		}
	}
	
	public static String convertDateToAnotherDateFormat(String dateBefore, String formatBefore, String formatAfter) {
		DateFormat originalFormat = new SimpleDateFormat(formatBefore);
		DateFormat targetFormat = new SimpleDateFormat(formatAfter);
		Date date = null;
		try {
			date = originalFormat.parse(dateBefore);
		} catch (ParseException e) {
			e.printStackTrace();
			throw new InternalServerErrorException("There is problem when formated date");
		}
		String formattedDate = targetFormat.format(date);
		return formattedDate;
	}
	
	public static String getdayOfDate(String date, String format) {
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		Calendar calendar = Calendar.getInstance();
		try {
			calendar.setTime(sdf.parse(date));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new InternalServerErrorException("tidak dapat menconver format addDays yang diberikan");
		}
		int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
		String hari = "";
		switch (dayOfWeek) {
		case Calendar.SUNDAY:
			hari = "Minggu";
			break;
		case Calendar.MONDAY:
			hari = "Senin";
			break;
		case Calendar.TUESDAY:
			hari = "Selasa";
			break;
		case Calendar.WEDNESDAY:
			hari = "Rabu";
			break;
		case Calendar.THURSDAY:
			hari = "Kamis";
			break;
		case Calendar.FRIDAY:
			hari = "Jumat";
			break;
		case Calendar.SATURDAY:
			hari = "Sabtu";
			break;
		default:
			break;
		}
		return hari;
	}
	
	public static String addDays(String date, String format, int summition) {
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		Calendar calendar = Calendar.getInstance();
		try {
			calendar.setTime(sdf.parse(date));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new InternalServerErrorException("tidak dapat menconver format addDays yang diberikan");
		}
		calendar.add(Calendar.DAY_OF_MONTH, summition);  
		String newDate = sdf.format(calendar.getTime());  
		return newDate;
	}
	
	public static String convertTimeToAnotherTime(String time, String delimeter) {
		String timeFormated = "";
		int delim = 0;
		int stopper = 0;
		for (char ch : time.toCharArray()) {
			if(delim==2) {
				timeFormated+=delimeter;
			}
			timeFormated+= Character.toString(ch);
			
			if(stopper==3) {
				break;
			}
			
			delim++;
			stopper++;
			
		}
		return timeFormated;
	}
	//121120201033140
	public static void main(String[] args) {//HHmmSS
		System.out.println(convertDateToString(new Date(), "yyyy-MM-dd HH:mm:ss"));
	}
}
