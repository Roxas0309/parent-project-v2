package id.co.dbmaas.qris.merchant.onboarding.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import id.co.dbmaas.qris.merchant.onboarding.service.ConnectionCheckingPort;
import id.co.dbmaas.qris.merchant.onboarding.service.RestartServiceMaas;
import id.co.oob.lib.common.merchant.onboarding.ws.ConnectionPortIpDto;


@RestController
@RequestMapping("/configuration-component-maker")
public class ConfigurationComponentMaker {

	@Autowired
	private RestartServiceMaas restartServiceMaas;
	

	  @PostMapping("/restart")
	    public String restartApp() {
		 restartServiceMaas.restartApp();
		 return "restart done..";
	  } 
	 
	 @PostMapping("/check-connection")
	 public String checkConnection(@RequestBody ConnectionPortIpDto connectionPortIpDto) {
		 Boolean isConnect = ConnectionCheckingPort.isConnectedBetweenPortAndIp(connectionPortIpDto.getIp(), connectionPortIpDto.getPort());
	 
		 if(isConnect) {
			 return "Connected to IP : " + connectionPortIpDto.getIp() + ", PORT : " + connectionPortIpDto.getPort();
		 }
		 else{
			 return "[obbadmin@OOB-PROD1 ~]$ telnet " + connectionPortIpDto.getIp() + " " + connectionPortIpDto.getPort() + " \n "
					 +"Trying " + connectionPortIpDto.getIp()+"... \n"
					 +"telnet: connect to address " + connectionPortIpDto.getIp() + ": " + "connection timed out";
		 }
	 
	 }
	 
	 @GetMapping("/check-connection-telneting")
	 @Scheduled(fixedRate = 1*60*1000L)
	 public String checkConnectionAndTelneting() {
		 
		 //147.139.188.79:2230
		 //String connIp = "172.16.200.161";
		 //Integer connPort = 2230;
		 
		 String connIp = "147.139.188.79";
		 Integer connPort = 2230;
		 System.out.println("trying to telneting, IP : " + connIp + ", port : "+connPort);
		 Boolean isConnect = ConnectionCheckingPort.isConnectedBetweenPortAndIp(connIp,connPort);
		
		 if(isConnect) {
			 return "Connected to IP : " + connIp + ", PORT : " + connPort;
		 }
		 else{
			 return "[obbadmin@OOB-PROD1 ~]$ telnet " + connIp + " " + connPort + " \n "
					 +"Trying " + connIp+"... \n"
					 +"telnet: connect to address " +connPort + ": " + "connection timed out";
		 }
	 
	 }
	
}
