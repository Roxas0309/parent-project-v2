package id.co.dbmaas.qris.merchant.onboarding.service;

import java.io.*;
import java.net.*;
import java.util.*;

import id.co.oob.lib.common.merchant.onboarding.tester.TestingTelnetData;

public class ConnectionCheckingPort extends TimerTask {
	
	
	public static void main(String args[]) {
		System.out.println("is connect : " + isConnectedBetweenPortAndIp("147.139.188.79", 2230));
	}
	
	public static Boolean isConnectedBetweenPortAndIp(String ip, Integer port) {
		Boolean isConnected = false;
		try {
			System.out.println("Please enter ip address");
			TimerTask con = new TestingTelnetData();
			Timer timer = new Timer();
			timer.scheduleAtFixedRate(con, 1, 1000);
			Socket s1 = new Socket(ip,port);
			InputStream is = s1.getInputStream();
			DataInputStream dis = new DataInputStream(is);
			System.out.println("Connected with ip " + ip + " and port " + port);
			isConnected = true;
			dis.close();
			s1.close();
		} catch (Exception e) {
			System.out.println("Not Connected,Please enter proper input : " + " explain : " + e.getMessage());
		}
		return isConnected;
	}
	
	

	@Override
	public void run() {
		// TODO Auto-generated method stub

	}
}
