package id.co.dbmaas.qris.merchant.onboarding.controller;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import id.co.dbmaas.qris.merchant.onboarding.MaasDbOobApplication;

@RestController
public class RestartController {
    
    @PostMapping("/restart")
    public void restart() {
    	MaasDbOobApplication.restart();
    } 
}