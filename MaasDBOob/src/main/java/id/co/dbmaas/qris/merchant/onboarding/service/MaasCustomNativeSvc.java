package id.co.dbmaas.qris.merchant.onboarding.service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
@Service
public class MaasCustomNativeSvc {

	@PersistenceContext
	private EntityManager entityManager;
	
	public Object resultQuery(String query) {
		return entityManager.createNativeQuery(query).getResultList();
	}

//	public Object resultQueryDelete(String query) {
//		return entityManager.createNativeQuery(query).executeUpdate();
//	}
	
}
