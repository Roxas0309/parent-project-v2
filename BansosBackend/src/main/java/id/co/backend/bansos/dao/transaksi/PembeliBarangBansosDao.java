package id.co.backend.bansos.dao.transaksi;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import id.co.backend.bansos.repository.transaksi.PembeliBarangBansos;
import id.co.backend.bansos.repository.transaksi.id.PembeliBarangBansosId;
@Repository
public interface PembeliBarangBansosDao extends JpaRepository<PembeliBarangBansos, PembeliBarangBansosId>{
	
	@Query("select a from PembeliBarangBansos a where a.transaksiOwner = ?1 ")
	public List<PembeliBarangBansos> getAllPembeliBarangBansosByTransaksiOwner(String transaksiOwner);
	
	@Query("select a from PembeliBarangBansos a where a.noEktp = ?1 order by a.createdDate DESC, a.createdTime DESC")
	public List<PembeliBarangBansos> getAllCurrentHistoryBarangBansos(String noEktp);
	
	
	
	@Query("select a from PembeliBarangBansos a where a.transaksiOwner = ?1 and a.createdDate = ?2")
	public List<PembeliBarangBansos> getAllPembeliBarangBansosByTransaksiOwnerAndToday(String transaksiOwner, String createdDate);
	
	@Query("select a from PembeliBarangBansos a where a.transaksiOwner = ?1 and a.pembeliNo = ?2")
	public PembeliBarangBansos getAllPembeliBarangBansosByTransaksiOwnerAndNoPembeli(String transaksiOwner, String noPembeli);
	
	@Query("select a from PembeliBarangBansos a where a.transaksiOwner = ?1 and a.createdDate between ?2 and ?3 order by a.createdDate desc, a.createdTime desc")
	public List<PembeliBarangBansos> getAllPembeliBarangBansosByTransaksiByFiltering(String transaksiOwner, String startDate, String endDate);
	
	
	@Query("select count(a) from PembeliBarangBansos a where a.transaksiOwner = ?1 ")
	public Integer countJumlahTransaksi(String transaksiOwner);
	
	@Query("select sum(a.totalPembayaran) from PembeliBarangBansos a where a.transaksiOwner = ?1 ")
	public BigDecimal countTotalPenjualan(String transaksiOwner);
	
	@Query("select count(a) from PembeliBarangBansos a where a.transaksiOwner = ?1 and a.createdDate =?2")
	public Integer countJumlahTransaksiToday(String transaksiOwner, String createdDate);
	
	@Query("select sum(a.totalPembayaran) from PembeliBarangBansos a where a.transaksiOwner = ?1 and a.createdDate =?2")
	public BigDecimal countTotalPenjualanToday(String transaksiOwner, String createdDate);
	
}
