package id.co.backend.bansos.dao.usermgmt;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import id.co.backend.bansos.repository.usermgmt.UserBansos;

@Repository
public interface UserBansosDao extends JpaRepository<UserBansos, String>{

	@Query("select a from UserBansos a where a.userName = ?1 and a.password = ?2")
	public UserBansos getUserBansosByUserNameAndPassword(String userName, String password);
	
	@Query("select a from UserBansos a where a.userName = ?1")
	public UserBansos getUserBansosByUserName(String userName);
	
}
