package id.co.backend.bansos.controller.transaksi;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.google.api.client.util.Strings;
import com.google.gson.Gson;

import id.co.backend.bansos.controller.BaseCtl;
import id.co.backend.bansos.dao.barang.BarangJualanBansosDao;
import id.co.backend.bansos.dao.transaksi.PembeliBarangBansosDao;
import id.co.backend.bansos.dao.transaksi.TransaksiBarangBansosDao;
import id.co.backend.bansos.repository.barang.BarangJualanBansos;
import id.co.backend.bansos.repository.transaksi.PembeliBarangBansos;
import id.co.backend.bansos.repository.transaksi.TransaksiBarangBansos;
import id.co.oob.lib.common.merchant.bansos.dto.BansosSavePembelian;
import id.co.oob.lib.common.merchant.bansos.dto.BansosTransaksiPembelianDto;
import id.co.oob.lib.common.merchant.bansos.dto.BansosUserPembeliDetail;
import id.co.oob.lib.common.merchant.bansos.dto.PembeliBarangBansosDto;
import id.co.oob.lib.common.merchant.bansos.dto.QuerySingleTransactionBansos;
import id.co.oob.lib.common.merchant.bansos.dto.UserBansosDecryption;
import id.co.oob.lib.common.merchant.onboarding.convert.DateConverter;
import id.co.oob.lib.common.merchant.onboarding.convert.MoneyConverter;
import id.co.oob.lib.common.merchant.onboarding.file.Base64FileDto;

@Repository
@RestController
@RequestMapping("/bansosTransaksiPembelianBarangCtl")
public class BansosTransaksiPembelianBarangCtl extends BaseCtl {

	@Autowired
	private PembeliBarangBansosDao pembeliBarangBansosDao;

	@Autowired
	private TransaksiBarangBansosDao transaksiBarangBansosDao;

	@Autowired
	private BarangJualanBansosDao barangJualanBansosDao;
	

	private static final String TOTAL_BELANJAAN = "TOTAL_BELANJAAN";
	private static final String TOTAL_PEMBAYARAN = "TOTAL_PEMBAYARAN";
	
	
	
	@PutMapping("/getCurrentPembeliDatas")
	public ResponseEntity<Object> queryGetCurrentPembeliDatas(@RequestBody  QuerySingleTransactionBansos querySingleTransactionBansos){
		System.out.println("queryTransaction barangPalingDicari : " + new Gson().toJson(querySingleTransactionBansos));
		@SuppressWarnings("unused")
		UserBansosDecryption userBansosDecryption = getUserBansosDecryption(querySingleTransactionBansos.getSessionUser(),
				BANSOS_LOGIN);
		 List<PembeliBarangBansos> barangBansoses = pembeliBarangBansosDao.getAllCurrentHistoryBarangBansos
				 			(querySingleTransactionBansos.getKtpId());
		 BansosUserPembeliDetail bansosUserPembeliDetail = new BansosUserPembeliDetail();
		 if(barangBansoses==null||barangBansoses.size()==0) {
			 return new ResponseEntity<Object>("Id Ktp Tidak Ditemukan", HttpStatus.NOT_FOUND);
		 }
		 else {
			 PembeliBarangBansos bansos = barangBansoses.get(0);
			 bansosUserPembeliDetail.setNoEktpPembeli(bansos.getNoEktp());
			 bansosUserPembeliDetail.setNamaLengkapPembeli(bansos.getNamaPembeli());
			 InputStream ektp = getInputStreamAliOss(bansos.getFotoEktpBase64());
			 InputStream fotoWajahKtp = getInputStreamAliOss(bansos.getFotoWajahdenganEktp());
			try {
				bansosUserPembeliDetail.setFotoEktpBase64(inputStreamToBase64(ektp));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			try {
				bansosUserPembeliDetail.setFotoWajahdenganEktp(inputStreamToBase64(fotoWajahKtp));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
				 return new ResponseEntity<Object>(bansosUserPembeliDetail, HttpStatus.OK);
		 }
	}

	@PutMapping("/barangPalingDicari")
	public ResponseEntity<Object> queryTransactionBarangPalingDicari(@RequestBody QuerySingleTransactionBansos querySingleTransactionBansos){
		System.out.println("queryTransaction barangPalingDicari : " + new Gson().toJson(querySingleTransactionBansos));
		UserBansosDecryption userBansosDecryption = getUserBansosDecryption(querySingleTransactionBansos.getSessionUser(),
				BANSOS_LOGIN);
		
//		String selection = 	 " from bansos.transaksi_barang_bansos a cross join bansos.pembeli_barang_bansos b "
//				+ " where a.pembeli_no=b.pembeli_no and (a.created_date between '"+querySingleTransactionBansos.getStartDate()+"'"
//				+ " and '"+querySingleTransactionBansos.getEndDate()+"') "
//				+ " and b.transaksi_owner='"+userBansosDecryption.getUserName()+"' group BY a.jenis_barang ";
//		
//		String query = "select value.* from  (SELECT COUNT(a.jenis_barang) as count_value, jenis_barang "
//			    + selection
//                + " ORDER BY COUNT(a.jenis_barang) desc)value join "
//                + " ( SELECT max(COUNT(a.jenis_barang)) as max_value"
//                + selection
//                + " )maks "
//                + " on value.count_value = maks.max_value";
		
		String selection = "SELECT COUNT(CONCAT(a.JENIS_BARANG,CONCAT(' ',concat(a.MERK, concat(' ', concat(a.berat_barang, concat(' ', a.satuan_barang))))))) as count_value,\r\n"
				+ " CONCAT(a.JENIS_BARANG,CONCAT(' ',concat(a.MERK, concat(' ', concat(a.berat_barang, concat(' ', a.satuan_barang))))))\r\n"
				+ " from bansos.transaksi_barang_bansos a cross join bansos.pembeli_barang_bansos b  \r\n"
				+ " where a.pembeli_no=b.pembeli_no and (a.created_date between '"+querySingleTransactionBansos.getStartDate()+ "'"
				+ " and '"+querySingleTransactionBansos.getEndDate()+"')  \r\n"
				+ " and b.transaksi_owner='"+userBansosDecryption.getUserName()+"' group BY "
				+ " CONCAT(a.JENIS_BARANG,CONCAT(' ',concat(a.MERK, concat(' ', concat(a.berat_barang, concat(' ', a.satuan_barang)))))) ";
		
		String max = "SELECT Max(COUNT(CONCAT(a.JENIS_BARANG,CONCAT(' ',concat(a.MERK, concat(' ', concat(a.berat_barang, concat(' ', a.satuan_barang)))))))) as max_value\r\n"
				+ " from bansos.transaksi_barang_bansos a cross join bansos.pembeli_barang_bansos b  \r\n"
				+ " where a.pembeli_no=b.pembeli_no and (a.created_date between '"+querySingleTransactionBansos.getStartDate()+"'"
				+ " and '"+querySingleTransactionBansos.getEndDate()+"')  \r\n"
				+ " and b.transaksi_owner='"+userBansosDecryption.getUserName()+"' group BY "
				+ " CONCAT(a.JENIS_BARANG,CONCAT(' ',concat(a.MERK, concat(' ', concat(a.berat_barang, concat(' ', a.satuan_barang))))))\r\n";
		
		String query = "select value.* from ("+selection+") value join "
				+ " ("+max+")maks "
				+ " on value.count_value = maks.max_value";
		System.out.println("query : " + query);
		Object mostBought = customNativeSvc.resultQuery(query);
		List<Object[]> valueObj = new ArrayList<Object[]>();
		
		try {
			valueObj = mapperJsonToListDto(new Gson().toJson(mostBought), Object[].class);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		return new ResponseEntity<Object>(valueObj, HttpStatus.OK);
	}
	
	@PutMapping("/queryTransaction")
	public ResponseEntity<Object> queryTransaction(@RequestBody QuerySingleTransactionBansos querySingleTransactionBansos){
		System.out.println("queryTransaction laporan penjualan : " + new Gson().toJson(querySingleTransactionBansos));
		UserBansosDecryption userBansosDecryption = getUserBansosDecryption(querySingleTransactionBansos.getSessionUser(),
				BANSOS_LOGIN);
		List<PembeliBarangBansos> pembeliBarangBansos = pembeliBarangBansosDao.
					getAllPembeliBarangBansosByTransaksiByFiltering(userBansosDecryption.getUserName(),
							querySingleTransactionBansos.getStartDate(),
							querySingleTransactionBansos.getEndDate());
		Map<String, Object> mappo = new HashMap<String, Object>();
		
		BigDecimal bdTransaksi = new BigDecimal(0);
		BigDecimal bdJumlahTransaksi = new BigDecimal(0);
		List<PembeliBarangBansosDto> pembeliBarangBansosDtos = new ArrayList<PembeliBarangBansosDto>();
		for (PembeliBarangBansos bansos : pembeliBarangBansos) {
			PembeliBarangBansosDto bansosDto = new PembeliBarangBansosDto();
			bansosDto.setDateTransaksi(DateConverter.convertDateToAnotherDateFormat
					(bansos.getCreatedDate(), "yyyyMMdd", "dd MMM yyyy"));
			bansosDto.setTimeTransaksi(DateConverter.convertDateToAnotherDateFormat
					(bansos.getCreatedTime(), "HHmmSS", "HH:mm"));
			bansosDto.setNamaPembeli(bansos.getNamaPembeli());
			bansosDto.setNoEktp(bansos.getNoEktp());
			bansosDto.setPembeliNo(bansos.getPembeliNo());
			bansosDto.setTransaksiOwner(userBansosDecryption.getUserName());
			bansosDto.setTotalBelanjaan(bansos.getTotalBelanjaan());
			bansosDto.setTotalPembayaran("Rp "+MoneyConverter.convert(bansos.getTotalPembayaran()));
			bdTransaksi = bdTransaksi.add(bansos.getTotalPembayaran());
			bdJumlahTransaksi = bdJumlahTransaksi.add(new BigDecimal(1));
			pembeliBarangBansosDtos.add(bansosDto);
		}
		
		mappo.put("totalTransaksi", "Rp "+MoneyConverter.convert(bdTransaksi));
		mappo.put("jumlahTransksi", bdJumlahTransaksi);
		mappo.put("datas", pembeliBarangBansosDtos);
		
		
		return new ResponseEntity<Object>(mappo, HttpStatus.OK);
	}
	
	@PutMapping("/querySingleTransaction")
	public ResponseEntity<Object> querySingleTransaction(@RequestBody QuerySingleTransactionBansos querySingleTransactionBansos){
		System.out.println("querySingleTransactionBansos : " + new Gson().toJson(querySingleTransactionBansos));
		UserBansosDecryption userBansosDecryption = getUserBansosDecryption(querySingleTransactionBansos.getSessionUser(),
				BANSOS_LOGIN);
		PembeliBarangBansos pembeliBarangBansos = pembeliBarangBansosDao.getAllPembeliBarangBansosByTransaksiOwnerAndNoPembeli
				(userBansosDecryption.getUserName(), querySingleTransactionBansos.getIdPembeliNo());
		PembeliBarangBansosDto pembeliBarangBansosDto = mapperFacade.map(pembeliBarangBansos, PembeliBarangBansosDto.class);
		pembeliBarangBansosDto.setTotalPembayaran(MoneyConverter.convert(pembeliBarangBansos.getTotalPembayaran()));
		String dateTransaksi = DateConverter.convertDateToAnotherDateFormat(pembeliBarangBansos.getCreatedDate(), 
				"yyyyMMdd", "dd MMM yyyy")+" " + DateConverter.convertDateToAnotherDateFormat
				(pembeliBarangBansos.getCreatedDate(), "HHmmss", "HH:mm");
		pembeliBarangBansosDto.setDateTransaksi(dateTransaksi);
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("pembeliBarangBansos", pembeliBarangBansosDto);
		return new ResponseEntity<Object>(map, HttpStatus.OK);
	}
	
	@PostMapping("/saveTransaksi")
	public ResponseEntity<Object> saveTransaksi(@RequestBody BansosSavePembelian bansosSavePembelian) {
		// System.out.println(" bansos value bansosSavePembelian : " + new
		// Gson().toJson(bansosSavePembelian.getBansosTransaksiPembelianDtos()));

		UserBansosDecryption userBansosDecryption = getUserBansosDecryption(bansosSavePembelian.getUserSession(),
				BANSOS_LOGIN);

		Date date = new Date();
		String userOwner = userBansosDecryption.getUserName();
		String idPembeliNo = "PEMB" + createIdWithTimeStamp(date) + "_00" + countTransaksiOwner(userOwner);

		BigDecimal bdTotalBelanjaan = countKeranjang(bansosSavePembelian.getBansosTransaksiPembelianDtos(), userOwner,
				TOTAL_BELANJAAN);
		BigDecimal bdTotalPembayaran = countKeranjang(bansosSavePembelian.getBansosTransaksiPembelianDtos(), userOwner,
				TOTAL_PEMBAYARAN);
		System.out.println("bdTotalBelanjaan : " + bdTotalBelanjaan + " bdTotalPembayaran : " + bdTotalPembayaran);
		// private String fotoEktpBase64;
//		private String fotoWajahdenganEktp;
//		private String fotoSuratKeterangan;
//		private String fotoPembeliDanBarang;
		String fotoEktpBase64 = getFileNew(
				new Base64FileDto(bansosSavePembelian.getBansosUserPembeliDetail().getFotoEktpBase64()),
				userOwner + "/FOTO_EKTP/", idPembeliNo + "_");
		String fotoWajahdenganEktp = getFileNew(
				new Base64FileDto(bansosSavePembelian.getBansosUserPembeliDetail().getFotoWajahdenganEktp()),
				 userOwner + "/FOTO_WAJAH_KTP/", idPembeliNo + "_");
		String fotoPembeliDanBarang = getFileNew(
				new Base64FileDto(bansosSavePembelian.getBansosUserPembeliDetail().getFotoPembeliDanBarang()),
				 userOwner + "/FOTO_PEMBELIAN_BARANG/", idPembeliNo + "_");
		String fotoSuratKeterangan = null;
		if (!Strings.isNullOrEmpty(bansosSavePembelian.getBansosUserPembeliDetail().getFotoSuratKeterangan())) {
			fotoSuratKeterangan = getFileNew(
					new Base64FileDto(bansosSavePembelian.getBansosUserPembeliDetail().getFotoSuratKeterangan()),
					 userOwner + "/SURAT_KETERANGAN/", idPembeliNo + "_");
		}

		System.out.println("fotoEktpBase64 : " + fotoEktpBase64);
		System.out.println("fotoWajahdenganEktp : " + fotoWajahdenganEktp);
		System.out.println("fotoPembeliDanBarang : " + fotoPembeliDanBarang);
		System.out.println("fotoSuratKeterangan : " + fotoSuratKeterangan);

		PembeliBarangBansos pembeliBarangBansos = new PembeliBarangBansos();
		pembeliBarangBansos.setFotoEktpBase64(fotoEktpBase64);
		pembeliBarangBansos.setFotoPembeliDanBarang(fotoPembeliDanBarang);
		pembeliBarangBansos.setFotoSuratKeterangan(fotoSuratKeterangan);
		pembeliBarangBansos.setFotoWajahdenganEktp(fotoWajahdenganEktp);
		pembeliBarangBansos.setNamaPembeli(bansosSavePembelian.getBansosUserPembeliDetail().getNamaLengkapPembeli());
		pembeliBarangBansos.setNoEktp(bansosSavePembelian.getBansosUserPembeliDetail().getNoEktpPembeli());
		pembeliBarangBansos.setPembeliNo(idPembeliNo);
		pembeliBarangBansos.setQrCode(bansosSavePembelian.getQrCapture());
		pembeliBarangBansos.setTotalBelanjaan(bdTotalBelanjaan);
		pembeliBarangBansos.setTotalPembayaran(bdTotalPembayaran);
		pembeliBarangBansos.setTransaksiOwner(userOwner);
		pembeliBarangBansos.setCreatedDate(changeDateToBansosFormatDate(date));
		pembeliBarangBansos.setCreatedTime(changeDateToBansosFormatTime(date));
		pembeliBarangBansosDao.save(pembeliBarangBansos);
		
		Integer i = 1;
		for (BansosTransaksiPembelianDto trx : bansosSavePembelian.getBansosTransaksiPembelianDtos()) {
			BarangJualanBansos barangJualanBansos = barangJualanBansosDao.findByUserNameAndSeq(userOwner, trx.getIdData()); 
			
			if(barangJualanBansos!=null) {
				String transaksiNo = "TRX"+createIdWithTimeStamp(date)+"_00"+i;	
				TransaksiBarangBansos transaksiBarangBansos = mapperFacade.map(barangJualanBansos, TransaksiBarangBansos.class);
				transaksiBarangBansos.setCreatedDate(changeDateToBansosFormatDate(date));
				transaksiBarangBansos.setCreatedTime(changeDateToBansosFormatTime(date));
				transaksiBarangBansos.setJumlahBarang(trx.getJumlahBarang());
				transaksiBarangBansos.setBeratBarang(barangJualanBansos.getBeratBarang());
				transaksiBarangBansos.setPembeliNo(idPembeliNo);
				transaksiBarangBansos.setTransaksiNo(transaksiNo);
				transaksiBarangBansosDao.save(transaksiBarangBansos);
				i++;
			}
			
		}
		System.out.println("Transaksi save berhasil dengan id save pembeli adalah " + idPembeliNo);
		return new ResponseEntity<Object>(idPembeliNo, HttpStatus.OK);
	}

	private BigDecimal countKeranjang(List<BansosTransaksiPembelianDto> bansosTransaksiPembelianDtos, String userOwner,
			String TIPE) {
		if (TIPE.equals(TOTAL_BELANJAAN)) {
			if (bansosTransaksiPembelianDtos == null) {
				return new BigDecimal(0);
			}
			return new BigDecimal(bansosTransaksiPembelianDtos.size());
		} else if (TIPE.equals(TOTAL_PEMBAYARAN)) {
			BigDecimal total = new BigDecimal(0);
			if (bansosTransaksiPembelianDtos == null) {
				return total;
			}

			for (BansosTransaksiPembelianDto bansosTransaksiPembelianDto : bansosTransaksiPembelianDtos) {
				BigDecimal totalQuality = bansosTransaksiPembelianDto.getJumlahBarang();
				if (bansosTransaksiPembelianDto.getJumlahBarang() == null) {
					totalQuality = new BigDecimal(0);
				}
				BarangJualanBansos barangJualanBansos = barangJualanBansosDao.findByUserNameAndSeq(userOwner,
						bansosTransaksiPembelianDto.getIdData());
				BigDecimal harga = new BigDecimal(0);
				if (barangJualanBansos != null) {
					harga = barangJualanBansos.getHargaBarang();
				}

				BigDecimal perkalianBarang = harga.multiply(totalQuality);
				total = total.add(perkalianBarang);
				// total = total.add(new )
			}

			return total;
		}

		return null;
	}

	private Integer countTransaksiOwner(String transaksiOwner) {
		List<PembeliBarangBansos> pembeliBarangBansos = pembeliBarangBansosDao
				.getAllPembeliBarangBansosByTransaksiOwner(transaksiOwner);
		if (pembeliBarangBansos == null) {
			return 1;
		}
		return pembeliBarangBansos.size() + 1;

	}

}
