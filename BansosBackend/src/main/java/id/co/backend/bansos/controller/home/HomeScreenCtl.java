package id.co.backend.bansos.controller.home;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import id.co.backend.bansos.controller.BaseCtl;
import id.co.backend.bansos.dao.transaksi.PembeliBarangBansosDao;
import id.co.backend.bansos.dao.usermgmt.UserBansosDao;
import id.co.backend.bansos.repository.transaksi.PembeliBarangBansos;
import id.co.backend.bansos.repository.usermgmt.UserBansos;
import id.co.oob.lib.common.merchant.bansos.dto.SessionInfoDto;
import id.co.oob.lib.common.merchant.bansos.dto.UserBansosDecryption;
import id.co.oob.lib.common.merchant.onboarding.convert.DateConverter;
import id.co.oob.lib.common.merchant.onboarding.convert.MoneyConverter;

@RestController
@RequestMapping("/homeScreenCtl")
public class HomeScreenCtl extends BaseCtl{
	
	@Autowired
	private UserBansosDao userBansosDao;
	
	@Autowired
	private PembeliBarangBansosDao pembeliBarangBansosDao;
	
	@PutMapping("/getMyInfo")
	public ResponseEntity<Object> getAllMyInfo(@RequestBody SessionInfoDto sessionInfoDto){
		Map<String, Object> map = new HashMap<String, Object>();
		UserBansosDecryption userBansosDecryption = getUserBansosDecryption(sessionInfoDto.getSessionLogin(),
				BANSOS_LOGIN);
		UserBansos userBansos = userBansosDao.getUserBansosByUserName(userBansosDecryption.getUserName());
	
		String dateNow = DateConverter.convertDateToString(new Date(), "yyyyMMdd");
	    List<PembeliBarangBansos> pembeliBarangBansos = pembeliBarangBansosDao.getAllPembeliBarangBansosByTransaksiOwnerAndToday
	    		(userBansosDecryption.getUserName(),DateConverter.convertDateToString(new Date(), dateNow));

		List<Map<String, Object>> listTransaksi = new ArrayList<Map<String,Object>>();
		for (PembeliBarangBansos maps : pembeliBarangBansos) {
			Map<String, Object> mappo = new HashMap<String, Object>();
			mappo.put("namaPembeli", maps.getNamaPembeli());
			mappo.put("jumlahTransaksi", "Rp "+MoneyConverter.convert(maps.getTotalPembayaran()));
			listTransaksi.add(mappo);
		}
		
		map.put("userName", userBansos.getUserName());
		map.put("totalPenjualan", "Rp " + MoneyConverter.convert(pembeliBarangBansosDao.countTotalPenjualanToday(userBansosDecryption.getUserName(),dateNow)));
		map.put("jumlahTransaksi", pembeliBarangBansosDao.countJumlahTransaksiToday(userBansosDecryption.getUserName(),dateNow));
		map.put("listTransaksi", listTransaksi);
		
		return new ResponseEntity<Object>(map, HttpStatus.OK);
	}

}
