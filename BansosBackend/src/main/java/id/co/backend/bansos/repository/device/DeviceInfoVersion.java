package id.co.backend.bansos.repository.device;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "Device_Info_Version")
public class DeviceInfoVersion {

	@Id
	private Long id;
	
	@Column(name = "Version_Aplication", nullable = false)
	private String versionAplication;
	
	@Column(name = "version_OS", nullable = false)
	private String versionOS;
	
	@Column(name = "Download_Link", nullable = false)
	private String downloadLink;
	
	@Column(name = "Version_Date_Release", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date versionDateRelease;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getVersionAplication() {
		return versionAplication;
	}

	public void setVersionAplication(String versionAplication) {
		this.versionAplication = versionAplication;
	}

	public String getVersionOS() {
		return versionOS;
	}

	public void setVersionOS(String versionOS) {
		this.versionOS = versionOS;
	}

	public String getDownloadLink() {
		return downloadLink;
	}

	public void setDownloadLink(String downloadLink) {
		this.downloadLink = downloadLink;
	}

	public Date getVersionDateRelease() {
		return versionDateRelease;
	}

	public void setVersionDateRelease(Date versionDateRelease) {
		this.versionDateRelease = versionDateRelease;
	}
	
	
	
}
