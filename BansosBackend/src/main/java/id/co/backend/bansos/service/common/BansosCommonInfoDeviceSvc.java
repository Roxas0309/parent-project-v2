package id.co.backend.bansos.service.common;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import id.co.backend.bansos.dao.common.BansosCommonInfoDeviceDao;
import id.co.backend.bansos.repository.common.BansosCommonInfoDevice;
import id.co.backend.bansos.service.BaseSvc;
import id.co.oob.lib.common.merchant.bansos.dto.BansosCommonInfoDeviceDto;

@Service
public class BansosCommonInfoDeviceSvc extends BaseSvc{

	@Autowired
	private BansosCommonInfoDeviceDao bansosCommonInfoDeviceDao;
	
	public ResponseEntity<Object> saveCommonDeviceInfo(BansosCommonInfoDeviceDto bansosCommonInfoDeviceDto) {
		   BansosCommonInfoDevice bansosCommonInfoDevice = mapperFacade.map(bansosCommonInfoDeviceDto, BansosCommonInfoDevice.class);
		   List<BansosCommonInfoDevice> existingBansosCommonInfoDevice = bansosCommonInfoDeviceDao.getExpoPushNotificationTokenThatExist
				   							(bansosCommonInfoDeviceDto.getExpoPushNotificationToken());
		   Integer seqNo = 0;
		   if(existingBansosCommonInfoDevice!=null && existingBansosCommonInfoDevice.size()>0) {
			   return new ResponseEntity<Object>("Device Id Sudah Terdaftar", HttpStatus.CONFLICT);
		   }
		   bansosCommonInfoDevice.setSeqDuplicate(seqNo);
		   bansosCommonInfoDeviceDao.save(bansosCommonInfoDevice);
		   return new ResponseEntity<Object>(bansosCommonInfoDevice, HttpStatus.OK);
	}
	
	
}
