package id.co.backend.bansos.dao.common;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import id.co.backend.bansos.repository.common.BansosCommonInfoDevice;
import id.co.backend.bansos.repository.common.id.BansosCommonInfoDeviceId;

@Repository
public interface BansosCommonInfoDeviceDao extends JpaRepository<BansosCommonInfoDevice, BansosCommonInfoDeviceId>{

	@Query("select a from BansosCommonInfoDevice a where a.expoPushNotificationToken = ?1")
	public List<BansosCommonInfoDevice> getExpoPushNotificationTokenThatExist(String expoPushNotificationToken);
	
}
