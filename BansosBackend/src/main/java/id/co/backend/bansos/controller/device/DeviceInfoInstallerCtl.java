package id.co.backend.bansos.controller.device;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.view.RedirectView;

import com.google.gson.Gson;

import id.co.backend.bansos.controller.BaseCtl;
import id.co.backend.bansos.repository.device.DeviceInfoVersion;
import id.co.oob.lib.common.merchant.bansos.dto.RemotelyPublicUpdateDto;

@Controller
@RequestMapping("/DeviceInfoInstaller")
public class DeviceInfoInstallerCtl extends BaseCtl{

	@Autowired
	private id.co.backend.bansos.dao.device.DeviceInfoVersionDao deviceInfoVersionDao;
	
	@GetMapping("/installBansos/{operatingSystem}")
	public String redirectInstallBansos(@PathVariable(name = "operatingSystem") String operatingSystem) {
		
		System.out.println("operatingSystem : " + operatingSystem);
		String url = deviceInfoVersionDao.urlRedirect(operatingSystem);
		return "redirect:"+url;
	}
	
	
	
		
}
