package id.co.backend.bansos;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BansosBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(BansosBackendApplication.class, args);
	}

}
