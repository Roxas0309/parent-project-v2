package id.co.backend.bansos.dao.usermgmt;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import id.co.backend.bansos.repository.usermgmt.UserBansosLoginSession;

@Repository
public interface UserBansosLoginSessionDao extends JpaRepository<UserBansosLoginSession, String>{

	@Query("select a from UserBansosLoginSession a where a.userName = ?1 ")
	public UserBansosLoginSession getLoginSessionByUserName(String userName);
	
	@Query("select a from UserBansosLoginSession a where a.sessionLogin = ?1 ")
	public UserBansosLoginSession getLoginSessionBySessionLogin(String sessionLogin);
	
	@Modifying
	@Query("delete from UserBansosLoginSession a where a.userName = ?1 ")
	public void logOutMe(String userName);
}
