package id.co.backend.bansos.repository.usermgmt;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "User_Bansos")
public class UserBansos {
	
	@Id
	@Column(name = "user_name")
	private String userName;
	
	private String password;
	
	@Column(name = "first_time_login", nullable = true)
	@Temporal(TemporalType.TIMESTAMP)
	private Date firstTimeLogin;

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Date getFirstTimeLogin() {
		return firstTimeLogin;
	}

	public void setFirstTimeLogin(Date firstTimeLogin) {
		this.firstTimeLogin = firstTimeLogin;
	}
	
	
	
}
