package id.co.backend.bansos.repository.usermgmt;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import id.co.backend.bansos.repository.usermgmt.id.UserBansosLoginHistoryId;

@Entity
@Table(name = "User_Bansos_Login_History")
@IdClass(UserBansosLoginHistoryId.class)
public class UserBansosLoginHistory {

	@Id
	@Column(name = "id_Seq")
	private Long idSeq;
	
	@Id
	@Column(name="user_name")
	private String userName;
	
	@Column(name = "login_time", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date loginTime;
	
	@Column(name = "logout_time", nullable = true)
	@Temporal(TemporalType.TIMESTAMP)
	private Date logout_time;

	public Long getIdSeq() {
		return idSeq;
	}

	public void setIdSeq(Long idSeq) {
		this.idSeq = idSeq;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Date getLoginTime() {
		return loginTime;
	}

	public void setLoginTime(Date loginTime) {
		this.loginTime = loginTime;
	}

	public Date getLogout_time() {
		return logout_time;
	}

	public void setLogout_time(Date logout_time) {
		this.logout_time = logout_time;
	}
	
	
	
}
