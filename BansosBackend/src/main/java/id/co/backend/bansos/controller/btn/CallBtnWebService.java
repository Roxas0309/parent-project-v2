package id.co.backend.bansos.controller.btn;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;

import id.co.backend.bansos.controller.BaseCtl;
import id.co.oob.lib.common.merchant.onboarding.convert.DateConverter;
import id.co.oob.lib.common.merchant.onboarding.dto.token.AuthenticationSpiritMaker;
import id.co.oob.lib.common.merchant.onboarding.dto.token.MandiriTokenResponse;
import id.co.oob.lib.common.merchant.onboarding.encryptor.SHA256withRSAEncrypted;
import id.co.oob.lib.common.merchant.onboarding.throwable.InternalServerErrorException;
import id.co.oob.lib.common.merchant.onboarding.throwable.NotAcceptanceDataException;

@RestController
@RequestMapping("/btnWebService")
public class CallBtnWebService extends BaseCtl {

	@PostMapping("/postBtn")
	public ResponseEntity<String> callBtn(@RequestBody Object body, @RequestHeader(name = "path", required = true)String path)
	{
		System.out.println("body : " + new Gson().toJson(body));
		System.out.println("path : " + path);
		Map<String, String> headerMap = new HashMap<String, String>();
		headerMap.put("apikey", "0e3c963e-3c94-4092-b43d-2e060fe48465");
		ResponseEntity<String> response = wsBody(path, body, HttpMethod.POST, headerMap);
		System.err.println("url otp auth adalah : " + new Gson().toJson(response));

		return response;

	}

}
