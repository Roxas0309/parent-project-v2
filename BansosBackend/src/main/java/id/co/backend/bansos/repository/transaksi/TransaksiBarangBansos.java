package id.co.backend.bansos.repository.transaksi;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

import id.co.backend.bansos.repository.transaksi.id.TransaksiBarangBansosId;

@Entity
@Table(name = "Transaksi_Barang_Bansos")
@IdClass(TransaksiBarangBansosId.class)
public class TransaksiBarangBansos {

	@Id
	private String pembeliNo;
	
	@Id
	private String transaksiNo;
	
	
	@Column(name = "jenis_Barang", nullable = false)
	private String jenisBarang;
	
	@Column(name = "merk", nullable = false )
	private String merk;
	
	@Column(name = "satuan_barang", nullable = false )
	private String satuanBarang;
	
	@Column(name = "harga_barang", nullable = false )
	private BigDecimal hargaBarang;
	
	@Column(name = "berat_Barang")
	private Long beratBarang;
	
	
	@Column(nullable = false )
	private BigDecimal jumlahBarang; 
	
	@Column(name = "created_date", nullable = false) //format yyyyMMdd
	private String createdDate;
	
	@Column(name = "created_time", nullable = false) //format HHmmSS
	private String createdTime;
	
	

	public Long getBeratBarang() {
		return beratBarang;
	}

	public void setBeratBarang(Long beratBarang) {
		this.beratBarang = beratBarang;
	}

	public String getJenisBarang() {
		return jenisBarang;
	}

	public void setJenisBarang(String jenisBarang) {
		this.jenisBarang = jenisBarang;
	}

	public String getMerk() {
		return merk;
	}

	public void setMerk(String merk) {
		this.merk = merk;
	}

	public String getSatuanBarang() {
		return satuanBarang;
	}

	public void setSatuanBarang(String satuanBarang) {
		this.satuanBarang = satuanBarang;
	}

	public BigDecimal getHargaBarang() {
		return hargaBarang;
	}

	public void setHargaBarang(BigDecimal hargaBarang) {
		this.hargaBarang = hargaBarang;
	}

	public BigDecimal getJumlahBarang() {
		return jumlahBarang;
	}

	public void setJumlahBarang(BigDecimal jumlahBarang) {
		this.jumlahBarang = jumlahBarang;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public String getCreatedTime() {
		return createdTime;
	}

	public void setCreatedTime(String createdTime) {
		this.createdTime = createdTime;
	}

	public String getTransaksiNo() {
		return transaksiNo;
	}

	public void setTransaksiNo(String transaksiNo) {
		this.transaksiNo = transaksiNo;
	}

	public String getPembeliNo() {
		return pembeliNo;
	}

	public void setPembeliNo(String pembeliNo) {
		this.pembeliNo = pembeliNo;
	}

	
	
	
	
}
