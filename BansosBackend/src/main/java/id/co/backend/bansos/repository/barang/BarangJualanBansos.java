package id.co.backend.bansos.repository.barang;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import id.co.backend.bansos.repository.barang.id.BarangJualanBansosId;
import id.co.backend.bansos.repository.usermgmt.id.UserBansosLoginHistoryId;

@Entity
@Table(name = "barang_jualan_bansos")
@IdClass(BarangJualanBansosId.class)
public class BarangJualanBansos {

	@Id
	@Column(name = "id_seq")
	private Long idSeq;
	
	@Id
	@Column(name = "user_Name")
	private String userName;
	
	@Column(name = "add_grouping")
	private Long addGrouping;
	
	@Column(name = "berat_Barang")
	private Long beratBarang;
	
	@Column(name = "kategori")
	private String kategori;
	
	@Column(name = "jenis_Barang", nullable = false)
	private String jenisBarang;
	
	@Column(name = "merk", nullable = false )
	private String merk;
	
	@Column(name = "satuan_barang", nullable = false )
	private String satuanBarang;
	
	@Column(name = "harga_barang", nullable = false )
	private BigDecimal hargaBarang;
	
	@Column(name = "created_date", nullable = true) //format yyyyMMdd
	private String createdDate;
	
	@Column(name = "created_time", nullable = true) //format HHmmSS
	private String createdTime;
	
	@Column(name = "is_deleted", nullable = true)
	private Boolean isDeleted;
	
	

	
	public String getKategori() {
		return kategori;
	}

	public void setKategori(String kategori) {
		this.kategori = kategori;
	}

	public Long getAddGrouping() {
		return addGrouping;
	}

	public void setAddGrouping(Long addGrouping) {
		this.addGrouping = addGrouping;
	}

	public Long getBeratBarang() {
		return beratBarang;
	}

	public void setBeratBarang(Long beratBarang) {
		this.beratBarang = beratBarang;
	}

	public Long getIdSeq() {
		return idSeq;
	}

	public void setIdSeq(Long idSeq) {
		this.idSeq = idSeq;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getJenisBarang() {
		return jenisBarang;
	}

	public void setJenisBarang(String jenisBarang) {
		this.jenisBarang = jenisBarang;
	}

	public String getMerk() {
		return merk;
	}

	public void setMerk(String merk) {
		this.merk = merk;
	}

	

	public String getSatuanBarang() {
		return satuanBarang;
	}

	public void setSatuanBarang(String satuanBarang) {
		this.satuanBarang = satuanBarang;
	}

	public BigDecimal getHargaBarang() {
		return hargaBarang;
	}

	public void setHargaBarang(BigDecimal hargaBarang) {
		this.hargaBarang = hargaBarang;
	}

	

	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public String getCreatedTime() {
		return createdTime;
	}

	public void setCreatedTime(String createdTime) {
		this.createdTime = createdTime;
	}

	public Boolean getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}
	
	
}
