package id.co.backend.bansos.repository.usermgmt.id;

import java.io.Serializable;

public class UserBansosLoginHistoryId implements Serializable{

	private static final long serialVersionUID = -4493696529321181395L;
	private Long idSeq;
	private String userName;
	
	public Long getIdSeq() {
		return idSeq;
	}
	public void setIdSeq(Long idSeq) {
		this.idSeq = idSeq;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	
}
