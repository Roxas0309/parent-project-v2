package id.co.backend.bansos.dao.transaksi;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import id.co.backend.bansos.repository.transaksi.TransaksiBarangBansos;
import id.co.backend.bansos.repository.transaksi.id.TransaksiBarangBansosId;

@Repository
public interface TransaksiBarangBansosDao extends JpaRepository<TransaksiBarangBansos, TransaksiBarangBansosId>{

	
	@Query("select count(a.jenisBarang), a.jenisBarang, a from TransaksiBarangBansos a, PembeliBarangBansos b "
			+ " where a.pembeliNo = b.pembeliNo  "
			+ "	and a.createdDate between ?2 and ?3 and b.transaksiOwner = ?1 group by a.jenisBarang ")
	public List<Object[]> getMostBought(String transaksiOwner, String startDate, String endDate);
}
