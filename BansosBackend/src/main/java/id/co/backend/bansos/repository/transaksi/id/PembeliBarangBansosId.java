package id.co.backend.bansos.repository.transaksi.id;

import java.io.Serializable;

public class PembeliBarangBansosId implements Serializable{

	private static final long serialVersionUID = -4598701302894647196L;
	
	private String pembeliNo;
	private String transaksiOwner;
	
	
	
	public String getPembeliNo() {
		return pembeliNo;
	}
	public void setPembeliNo(String pembeliNo) {
		this.pembeliNo = pembeliNo;
	}
	public String getTransaksiOwner() {
		return transaksiOwner;
	}
	public void setTransaksiOwner(String transaksiOwner) {
		this.transaksiOwner = transaksiOwner;
	}
	
	

}
