package id.co.backend.bansos.service.barang;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.google.api.client.util.Strings;
import com.google.gson.Gson;

import id.co.backend.bansos.dao.barang.BarangJualanBansosDao;
import id.co.backend.bansos.repository.barang.BarangJualanBansos;
import id.co.backend.bansos.repository.usermgmt.UserBansosLoginHistory;
import id.co.backend.bansos.service.BaseSvc;
import id.co.oob.lib.common.merchant.bansos.dto.BansosHapusDataDto;
import id.co.oob.lib.common.merchant.bansos.dto.BarangJualanBansosDto;
import id.co.oob.lib.common.merchant.onboarding.convert.DateConverter;
import id.co.oob.lib.common.merchant.onboarding.convert.MoneyConverter;
import id.co.oob.lib.common.merchant.onboarding.throwable.InternalServerErrorException;

@Service
@Transactional
public class BarangJualanBansosSvc extends BaseSvc{

	@Autowired
	private BarangJualanBansosDao barangJualanBansosDao;
	
	public ResponseEntity<Object> hapusBarangJualanBansos(BansosHapusDataDto bansosHapusDataDto,String userName){
		for (Long data : bansosHapusDataDto.getIdDataHapus()) {
			 barangJualanBansosDao.hapusBarangByUserNameAndId(userName, data);
		}
		return new ResponseEntity<Object>("Data Berhasil Dihapus", HttpStatus.OK);
	}
	
	public ResponseEntity<Object> getBarangJualanBansosAll(String userName, String search){
		 List<BarangJualanBansos> barangJualanBansos = barangJualanBansosDao.findByUserNameOrderByDateAndTime
				 (userName, "%"+search+"%");
		 
		 System.out.println("barangJualanBansos " + new Gson().toJson(barangJualanBansos));
		 
		 List<Long> contactGrouping = barangJualanBansosDao.CountCapabiltyToCheckCapability(userName);
		System.out.println("contactGrouping : " + new Gson().toJson(contactGrouping));
		 Long contactGroupTerbaru = 0L;
		 if(contactGrouping!=null&&contactGrouping.size()>0) {
			 //System.out.println("all group : " + new Gson().toJson(contactGrouping));
			 Collections.sort(contactGrouping);
			System.out.println("contactGrouping : " + contactGrouping.get(contactGrouping.size()-1));
			contactGroupTerbaru = contactGrouping.get(contactGrouping.size()-1);
		 }
		 
		 List<BarangJualanBansosDto> barangJualanBansosQuery = new ArrayList<>();
		 for (BarangJualanBansos barangJualan : barangJualanBansos) {
			 BarangJualanBansosDto barangJualanBansosDto = mapperFacade.map(barangJualan,BarangJualanBansosDto.class);
			 barangJualanBansosDto.setHargaBarangInRupiah("Rp " + MoneyConverter.convert( barangJualan.getHargaBarang()) );
			 barangJualanBansosDto.setKodeBarang(barangJualan.getIdSeq()+"-"+barangJualan.getUserName());
			 barangJualanBansosDto.setHargaBarangInValue(barangJualan.getHargaBarang());
			 barangJualanBansosDto.setIdData(barangJualan.getIdSeq());
			 barangJualanBansosDto.setBeratBarangJualan(barangJualan.getBeratBarang());
			 String dateNow = DateConverter.convertDateToString(new Date(), "yyyyMMdd");
			 System.out.println("date now : " + dateNow);
			 if(dateNow.equalsIgnoreCase(barangJualan.getCreatedDate())&&contactGroupTerbaru==barangJualan.getAddGrouping()) {
				 barangJualanBansosDto.setIsNew(true);
			 }
			 barangJualanBansosQuery.add(barangJualanBansosDto);
		}
		 
		 return new ResponseEntity<Object>(barangJualanBansosQuery, HttpStatus.OK);
	}
	
	public ResponseEntity<Object> editBarangDagangBansos(List<BarangJualanBansos> barangJualanBansoses, String userName){
		List<BarangJualanBansos> jualanBansos = new ArrayList<>();
		for (BarangJualanBansos barangJualanBansos : barangJualanBansoses) {
			BarangJualanBansos jualanBansosBfr = barangJualanBansosDao.findByUserNameAndSeq(barangJualanBansos.getUserName(), 
					barangJualanBansos.getIdSeq());
			barangJualanBansos.setCreatedDate(jualanBansosBfr.getCreatedDate());
			barangJualanBansos.setCreatedTime(jualanBansosBfr.getCreatedTime());
			barangJualanBansos.setIsDeleted(jualanBansosBfr.getIsDeleted());
			barangJualanBansos.setAddGrouping(jualanBansosBfr.getAddGrouping());
			barangJualanBansosDao.save(barangJualanBansos);
			jualanBansos.add(barangJualanBansos);
		}
		return new ResponseEntity<Object>(jualanBansos, HttpStatus.OK);
	}
	
	public ResponseEntity<Object> saveBarangDagangBansos(List<BarangJualanBansos> barangJualanBansoses, String userName){
		System.out.println("yang akan disave banggg : " + new Gson().toJson(barangJualanBansoses));
		if(barangJualanBansoses==null||barangJualanBansoses.size()==0 ) {
			return new ResponseEntity<Object>("Mohon Lengkapi Barang Jualan Dulu", HttpStatus.NOT_ACCEPTABLE);
		}
		
		List<BarangJualanBansos> jualanBansos = new ArrayList<>();
		List<Long> contactGrouping = barangJualanBansosDao.CountCapabiltyToCheckCapability(userName);
		System.out.println("contactGrouping : " + contactGrouping);
		
		
		
		Integer countQrupu = 1;
		Integer i = 1;
		
		while(contactGrouping.contains(Integer.toUnsignedLong(countQrupu))) {
			countQrupu++;
		}
		
		System.out.println("int i : " + i);
//		if(true) {
//		throw new InternalServerErrorException("Nice one");
//		}
//		
		for (BarangJualanBansos barangJualanBansos : barangJualanBansoses) {
			
			if(Strings.isNullOrEmpty(barangJualanBansos.getJenisBarang())||
					Strings.isNullOrEmpty(barangJualanBansos.getMerk())||
					Strings.isNullOrEmpty(barangJualanBansos.getSatuanBarang())||
					barangJualanBansos.getHargaBarang()==null 
			  ) {
				Map<String, Object> map = new HashMap<>();
				map.put("jualanKe", i);
				map.put("errorWording", "Mohon Lengkapi Barang Jualan Dulu");
				if(Strings.isNullOrEmpty(barangJualanBansos.getJenisBarang())) {
				map.put("errorField", "jenisBarang");
				return new ResponseEntity<Object>(map, HttpStatus.NOT_ACCEPTABLE);
				}
				
				if(Strings.isNullOrEmpty(barangJualanBansos.getMerk())) {
					map.put("errorField", "merk");
					return new ResponseEntity<Object>(map, HttpStatus.NOT_ACCEPTABLE);
				}
				
				if(Strings.isNullOrEmpty(barangJualanBansos.getSatuanBarang())) {
					map.put("errorField", "satuanBarang");
					return new ResponseEntity<Object>(map, HttpStatus.NOT_ACCEPTABLE);
				}
				
				if(barangJualanBansos.getHargaBarang()==null ) {
					map.put("errorField", "hargaBarang");
					return new ResponseEntity<Object>(map, HttpStatus.NOT_ACCEPTABLE);
				}
				
				if(Strings.isNullOrEmpty(barangJualanBansos.getKategori()) ) {
					map.put("errorField", "kategori");
					return new ResponseEntity<Object>(map, HttpStatus.NOT_ACCEPTABLE);
				}
				
			}
			barangJualanBansos.setIsDeleted(false);
			Date date = new Date();
			barangJualanBansos.setCreatedDate(changeDateToBansosFormatDate(date));
			barangJualanBansos.setCreatedTime(changeDateToBansosFormatTime(date));
			barangJualanBansos.setIdSeq(getIdSeqBarang(barangJualanBansos.getUserName()));
			barangJualanBansos.setAddGrouping(Integer.toUnsignedLong(countQrupu));
			barangJualanBansosDao.save(barangJualanBansos);
			jualanBansos.add(barangJualanBansos);
			i++;
		}
		
	
		return new ResponseEntity<Object>(jualanBansos, HttpStatus.OK);
	}
	
	private Long getIdSeqBarang(String userName) {
		List<BarangJualanBansos> barangJualanBansos =  barangJualanBansosDao.findByUserName(userName);
		if (barangJualanBansos == null) {
			return 0L;
		}
		
		Integer value = barangJualanBansos.size()+1;
		while(barangJualanBansosDao.findByBarangByUserNameAndId(userName,Integer.toUnsignedLong(value))!=null) {
			value++;
		}
		return Integer.toUnsignedLong(value);

	}
	
}
