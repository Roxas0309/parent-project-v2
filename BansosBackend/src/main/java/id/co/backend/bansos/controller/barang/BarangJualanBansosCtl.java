package id.co.backend.bansos.controller.barang;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;

import id.co.backend.bansos.controller.BaseCtl;
import id.co.backend.bansos.repository.barang.BarangJualanBansos;
import id.co.backend.bansos.service.barang.BarangJualanBansosSvc;
import id.co.oob.lib.common.merchant.bansos.dto.BansosHapusDataDto;
import id.co.oob.lib.common.merchant.bansos.dto.BarangJualanBansosDto;
import id.co.oob.lib.common.merchant.bansos.dto.BarangJualanBansosOnListDto;
import id.co.oob.lib.common.merchant.bansos.dto.BarangJualanBansosQuery;
import id.co.oob.lib.common.merchant.bansos.dto.UserBansosDecryption;
import id.co.oob.lib.common.merchant.onboarding.convert.MoneyConverter;

@RestController
@RequestMapping("/barangJualanBansosCtl")
public class BarangJualanBansosCtl extends BaseCtl{
	
		@Autowired
		private BarangJualanBansosSvc barangJualanBansosSvc;
		
		@PutMapping("/deleteData")
		private ResponseEntity<Object> deleteData(@RequestBody BansosHapusDataDto bansosHapusDataDto){
			System.out.println("data hapus : " + new Gson().toJson(bansosHapusDataDto));
			UserBansosDecryption userBansosDecryption = getUserBansosDecryption(bansosHapusDataDto.getSessionUser(), 
					BANSOS_LOGIN);
			return barangJualanBansosSvc.hapusBarangJualanBansos(bansosHapusDataDto ,userBansosDecryption.getUserName());
		}
		
		@PostMapping("/save")
		private ResponseEntity<Object> saveBarangJualan(@RequestBody BarangJualanBansosOnListDto barangJualanBansosOnListDto){
			System.out.println("barang dagang yang akan disave adalah : " + new  Gson().toJson(barangJualanBansosOnListDto));
			UserBansosDecryption userBansosDecryption = getUserBansosDecryption(barangJualanBansosOnListDto.getSessionLogin(), 
					BANSOS_LOGIN);
			
			List<BarangJualanBansos> barangJualanBansoses = new ArrayList<BarangJualanBansos>();
			for (BarangJualanBansosDto barangJualanBansoDto : barangJualanBansosOnListDto.getJualanBansosDtos()) {
				BarangJualanBansos barangJualanBansos = mapperFacade.map(barangJualanBansoDto, BarangJualanBansos.class);
				barangJualanBansos.setUserName(userBansosDecryption.getUserName());
				barangJualanBansos.setHargaBarang(MoneyConverter.convertStringToMoney(barangJualanBansoDto.
										getHargaBarangInRupiah()));
				barangJualanBansoses.add(barangJualanBansos);
			}
			
			
			return barangJualanBansosSvc.saveBarangDagangBansos(barangJualanBansoses, userBansosDecryption.getUserName());
			     
		}
		
		@PostMapping("/edit")
		private ResponseEntity<Object> editBarangJualan(@RequestBody BarangJualanBansosOnListDto barangJualanBansosOnListDto){
			System.out.println("barang dagang yang akan diedit adalah : " + new  Gson().toJson(barangJualanBansosOnListDto));
			UserBansosDecryption userBansosDecryption = getUserBansosDecryption(barangJualanBansosOnListDto.getSessionLogin(), 
					BANSOS_LOGIN);
			
			List<BarangJualanBansos> barangJualanBansoses = new ArrayList<BarangJualanBansos>();
			for (BarangJualanBansosDto barangJualanBansoDto : barangJualanBansosOnListDto.getJualanBansosDtos()) {
				BarangJualanBansos barangJualanBansos = mapperFacade.map(barangJualanBansoDto, BarangJualanBansos.class);
				barangJualanBansos.setIdSeq(barangJualanBansoDto.getIdData());
				barangJualanBansos.setUserName(userBansosDecryption.getUserName());
				barangJualanBansos.setHargaBarang(MoneyConverter.convertStringToMoney(barangJualanBansoDto.
										getHargaBarangInRupiah()));
				barangJualanBansoses.add(barangJualanBansos);
			}
			
			
			return barangJualanBansosSvc.editBarangDagangBansos(barangJualanBansoses, userBansosDecryption.getUserName());
			     
		}
		
		@PutMapping("/query")
		private ResponseEntity<Object> queryBarangJualan(@RequestBody BarangJualanBansosQuery barangJualanBansosQuery){
			System.out.println("barangJualanBansosQuery adalah : " + new  Gson().toJson(barangJualanBansosQuery));
			UserBansosDecryption userBansosDecryption =getUserBansosDecryption(barangJualanBansosQuery.getSessionToken(), 
					BANSOS_LOGIN);
			return barangJualanBansosSvc.getBarangJualanBansosAll(userBansosDecryption.getUserName(), barangJualanBansosQuery.getSearchData());
		}
		
		@PutMapping("/query/barangWarung")
		private ResponseEntity<Object> queryBarangJualanBarangWarung(@RequestBody BarangJualanBansosQuery barangJualanBansosQuery){
			System.out.println("barangJualanBansosQuery adalah : " + new  Gson().toJson(barangJualanBansosQuery));
			UserBansosDecryption userBansosDecryption =getUserBansosDecryption(barangJualanBansosQuery.getSessionToken(), 
					BANSOS_LOGIN);
			return barangJualanBansosSvc.getBarangJualanBansosAll(userBansosDecryption.getUserName(), barangJualanBansosQuery.getSearchData());
		}
		
}
