package id.co.backend.bansos.service.native_q;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
@Service
public class CustomNativeSvc {

	@PersistenceContext
	private EntityManager entityManager;
	
	public Object resultQuery(String query) {
		return entityManager.createNativeQuery(query).getResultList();
	}
	
	public Object resultQueryTop1(String query) {
		return entityManager.createNativeQuery(query).setMaxResults(1).getResultList();
	}
}
